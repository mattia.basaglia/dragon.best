class SizeChart
{
    constructor(svg_chart, table, provider)
    {
        this.provider = provider;
        this.svg = svg_chart;
        this.svg.setAttribute("viewBox", "0 -400 400 400");
        this.svg_container = this.svg.appendChild(create_element_svg("g"));

        this.table = table;
        this._build_table();

        this.dergs = [];
        this.scale_factor = null;
        this.details_derg = null;
        this.move = null;
        this.pinch = 1;
        this.max_x = 0;
        this.max_y = 0;
        this.filters = null;

        this._build_details_box();
        this._setup_events();
        this.adjust_view();
    }

    _build_table()
    {
        this.table.style.display = "none";
        let thead = this.table.appendChild(document.createElement("thead"));
        let tr = thead.appendChild(document.createElement("tr"));
        for ( let h of ["Dragon", "Height", "Length"] )
            tr.appendChild(document.createElement("th")).appendChild(document.createTextNode(h));
        this.table_body = this.table.appendChild(document.createElement("tbody"));
    }

    _build_details_box()
    {
        this.details = this.svg.parentNode.insertBefore(document.createElement("div"), this.svg);
        this.details.classList.add("details");
        let header = this.details.appendChild(document.createElement("header"));
        this.details_img = header.appendChild(document.createElement("img"));
        this.details_img.addEventListener("load", function(ev){
            ev.target.style.visibility = "visible";
        });
        this.details_name = header.appendChild(document.createElement("span"));
        let table = this.details.appendChild(document.createElement("table"));
        this.details_row_height = table.appendChild(document.createElement("tr"));
        this.details_row_height.appendChild(document.createElement("th"));
        this.details_row_height.appendChild(document.createElement("td"));
        this.details_row_length = table.appendChild(document.createElement("tr"));
        this.details_row_length.appendChild(document.createElement("th"));
        this.details_row_length.appendChild(document.createElement("td"));
    }

    update()
    {
        this.max_x = 0;
        this.max_y = 0;
        this.provider.list(this.on_update.bind(this));
    }

    on_update(dergs)
    {
        this.dergs = dergs;

        for ( let derg of this.dergs )
        {
            let length = derg.length;
            let height = derg.height;
            derg.type = dragon_types[derg.type];
            let fac = derg.type.img_wh * derg.type.height_mult / derg.type.length_mult;
            if ( derg.height == 0 || !derg.type.height )
                derg.height = derg.length / fac;
            if ( derg.length == 0 || !derg.type.length )
                derg.length = derg.height * fac;
        }

        this.dergs.sort((a, b) => a.height * a.length - b.height * b.length);
        this.show_dergs(this.filtered_dergs(dergs));
    }

    set_filters(filters)
    {
        this.filters = filters;
        this.show_dergs(this.filtered_dergs(this.dergs));
    }

    filtered_dergs(dergs)
    {
        if ( this.filters == null )
            return dergs;
        return dergs.filter(x => this.filters.has(x.type.slug));
    }

    show_dergs(dergs)
    {
        clear(this.svg_container);
        clear(this.table_body);
        this.table.style.display = "none";
        if ( dergs.length == 0 )
            return;

        this.table.style.display = "table";
        let max_height = Math.max.apply(null,
            dergs.map(derg => derg.height * derg.type.height_mult).concat([0])
        );
        let lines = this.svg_container.appendChild(create_element_svg("g"));
        let images = this.svg_container.appendChild(create_element_svg("g"));
        let x = 0;
        let max = 0;
        let last = null;
        const wrap_x = 3000;

        for ( let derg of dergs )
        {
            if ( x > wrap_x )
            {
                x = 0;
            }
            let width = Math.min(50000, derg.length * derg.type.length_mult);
            let height = Math.min(50000, derg.height * derg.type.height_mult);
            let img = images.insertBefore(create_element_svg("use"), last);
            last = img;
            img.setAttribute("href", '#'+derg.type.slug);
            set_px(img.width, width);
            set_px(img.y, -height);
            set_px(img.height, height);
            set_px(img.x, x);
            img.style.fill = derg.color;
            if ( x + width > max )
                max = x + width;
            x += Math.min(10000, width*derg.type.x_offset);
            img._derg = derg;
            this._derg_table_entry(derg);
        }

        this.max_x = max;
        this.max_y = max_height;

        for ( let i = 0; i < max_height; )
        {
            let line = lines.appendChild(create_element_svg("line"));
            line.setAttribute("x1", 0);
            line.setAttribute("x2", max);
            line.setAttribute("y1", -i);
            line.setAttribute("y2", -i);
            line.setAttribute("style", "stroke-width: 1px; stroke: gray; fill: none;");
            line.setAttribute("vector-effect", "non-scaling-stroke");

            if ( i >= 10*100 )
                i += 10*100;
            else
                i += 100;
        }

        if ( this.scale_factor == null )
        {
            let rect = this.svg.getBoundingClientRect();
            this.scale(wrap_x/200, rect.x, rect.y);
        }
        this.scale(this.scale_factor, 0, 0)
    }

    _derg_table_entry(derg)
    {
        let tr = this.table_body.appendChild(document.createElement("tr"));

        let dergtd = tr.appendChild(document.createElement("td"));

        let pfp = dergtd.appendChild(document.createElement("img"));
        pfp.src = derg.icon_url;

        let icon = dergtd.appendChild(create_element_svg("svg"));
        icon.setAttribute("title", derg.type.name);
        let maxw = 48;
        let maxh = 32;
        let ih = maxh;
        let iw = ih * derg.type.img_wh;
        if ( iw > maxw )
        {
            iw = maxw;
            ih = maxw / derg.type.img_wh;
        }
        let icon_use = icon.appendChild(create_element_svg("use"));
        icon_use.setAttribute("href", '#'+derg.type.slug);
        icon.setAttribute("width", maxw);
        icon.setAttribute("height", maxh);
        set_px(icon_use.width, iw);
        set_px(icon_use.height, ih);
        set_px(icon_use.x, (maxw-iw)/2);
        set_px(icon_use.y, (maxh-ih)/2);
        icon_use.style.fill = derg.color;

        dergtd.appendChild(document.createTextNode(derg.name));

        let data = [
            derg.type.height ? this._format_size_value(derg.height) : "",
            derg.type.length ? this._format_size_value(derg.length) : "",
        ];
        for ( let v of data )
            tr.appendChild(document.createElement("td")).appendChild(document.createTextNode(v));
    }

    adjust_view()
    {
        let ratio = this.svg.clientWidth / this.svg.clientHeight;
        let box = this.svg.viewBox.baseVal;
        box.width = box.height * ratio;
        box.x = Math.max(0, Math.min(box.x, this.max_x - box.width));
        box.y = Math.max(-this.max_y, Math.min(box.y, -box.height));
    }

    _format_size_value(value)
    {
        value = Math.round(value);
        let unit = "cm";
        if ( value > 120 )
        {
            value /= 100;
            unit = "m";

            if ( value > 1000 )
            {
                value = Math.round(value/1000, 2);
                unit = "km";
            }

            if ( value % 1 != 0 )
                value = value.toFixed(2);
        }
        return value + unit;
    }

    _update_details_row(row, name, value)
    {
        if ( value > 0 && name )
        {
            row.style.display = "table-row";
            row.querySelector("th").innerText = name;
            row.querySelector("td").innerText = this._format_size_value(value);
        }
        else
        {
            row.style.display = "none";
        }
    }

    update_details(derg)
    {
        if ( this.details_derg == derg )
            return;

        this.details_derg = derg;
        this.details_img.style.visibility = "hidden";
        this.details_img.src = derg.icon_url;
        this.details_name.innerText = derg.name;

        this._update_details_row(this.details_row_height, derg.type.height, derg.height);
        this._update_details_row(this.details_row_length, derg.type.length, derg.length);
    }

    scale(factor, x, y)
    {
        let box = this.svg.viewBox.baseVal;
        let wh = this.svg.clientWidth / this.svg.clientHeight;
        let new_h = Math.max(4, Math.min(
            100 * factor,
            Math.max(this.max_y, this.max_x / wh),
        ));
        this.scale_factor = new_h / 100;

        // align
        let rect = this.svg.getBoundingClientRect();
        let norm_x = (x - rect.x) / rect.width;
        let norm_y = (y - rect.y) / rect.height;
        box.y -= (new_h - box.height) * norm_y;
        box.x -= (new_h * wh - box.width) * norm_x;

        box.height = new_h;
        this.adjust_view();
    }

    show_details(target, clientX, clientY)
    {
        this.details.style.display = "block";
        let rect = this.svg.getBoundingClientRect()
        let x = Math.min(clientX + 32, rect.x + rect.width - this.details.clientWidth);
        let y = Math.max(rect.y, Math.min(
            clientY - this.details.clientHeight / 2,
            rect.y + rect.height - this.details.clientHeight
        ));
        this.details.style.left = x + "px";
        this.details.style.top = y+ "px";
        this.update_details(target._derg);
    }

    _setup_events()
    {
        for ( let ev of ["mousedown", "mousemove", "mouseup", "mouseleave", "wheel"] )
            this.svg.addEventListener(ev, this["_on_"+ev].bind(this));

        this.hammertime = new Hammer(this.svg);
        this.hammertime.get("pan").set({ direction: Hammer.DIRECTION_ALL });
        this.hammertime.get("pinch").set({ enable: true });

        for ( let ev of ["panstart", "panmove", "panend", "pinchstart", "pinchmove"] )
            this.hammertime.on(ev, this["_on_"+ev].bind(this));
    }

    _on_mousedown(e)
    {
        let box = this.svg.viewBox.baseVal;
        this.move = {
            cx: e.clientX,
            cy: e.clientY,
            bx: box.x,
            by: box.y,
        };
        e.preventDefault();
    }

    _on_mousemove(e)
    {
        this.details.style.display = "none";

        if ( this.move != null )
        {
            let box = this.svg.viewBox.baseVal;
            let dx = e.clientX - this.move.cx;
            let dy = e.clientY - this.move.cy;
            box.x = this.move.bx - dx * box.width / this.svg.clientWidth;
            box.y = this.move.by - dy * box.height / this.svg.clientHeight;
            this.adjust_view();
        }
        else if ( e.target.tagName == "use" )
        {
            this.show_details(e.target, e.clientX, e.clientY);
        }
    }

    _on_mouseup(e) { this.move = null; }

    _on_mouseleave(e)
    {
        this.move = null;
        this.details.style.display = "none";
    }

    _on_wheel(e)
    {
        let scale_f = e.deltaY > 0 ? 1.03 : 0.97;
        this.scale(this.scale_factor * scale_f, e.clientX, e.clientY);
        e.preventDefault();
    }

    _on_panstart(e)
    {
        let box = this.svg.viewBox.baseVal;
        this.move = {
            cx: e.pointers[0].clientX,
            cy: e.pointers[0].clientY,
            bx: box.x,
            by: box.y,
        };
    }

    _on_panmove(e)
    {
        if ( this.move != null )
        {
            let box = this.svg.viewBox.baseVal;
            let dx = e.pointers[0].clientX - this.move.cx;
            let dy = e.pointers[0].clientY - this.move.cy;
            box.x = this.move.bx - dx * box.width / this.svg.clientWidth;
            box.y = this.move.by - dy * box.height / this.svg.clientHeight;
            this.adjust_view();
        }
    }

    _on_panend(e)
    {
        this.move = null;
    }

    _on_pinchstart(e) {
        this.pinch = this.scale_factor;
    }

    _on_pinchmove(e) {
        let cx = (e.pointers[0].clientX + e.pointers[1].clientX) / 2;
        let cy = (e.pointers[0].clientY + e.pointers[1].clientY) / 2;
        this.scale(this.pinch / e.scale, cx, cy);
    }

}

function api_request(method, data, on_ok, on_fail)
{
    let xhr = new XMLHttpRequest();
    xhr.open(method, "/api/dragons_size.json", true);
    xhr.onreadystatechange = function() {
        if ( this.readyState === XMLHttpRequest.DONE )
        {
            if ( this.status === 200 )
                on_ok(JSON.parse(this.responseText));
            else
                on_fail(JSON.parse(this.responseText));
        }
    };
    xhr.send(data);
}

function clear(element)
{
    while ( element.firstChild )
        element.removeChild(element.firstChild);
}

function create_element_svg(tag)
{
    return document.createElementNS("http://www.w3.org/2000/svg", tag);
}

function set_px(prop, value)
{
    prop.baseVal.unitType = SVGLength.SVG_LENGTHTYPE_PX;
    prop.baseVal.value = value;
}

class SizeChartApiData
{
    list(on_ok, on_fail = console.warn)
    {
        api_request("GET", null, on_ok, on_fail);
    }

    add(dragon, on_ok)
    {
        var form_json = JSON.stringify(dragon);
        api_request("PUT", form_json, on_ok, console.warn);
    }

    update(dragon, on_ok)
    {
        var form_json = JSON.stringify(dragon);
        api_request("PATCH", form_json, on_ok, console.warn);
    }

    remove(dragon, on_ok)
    {
        var form_json = JSON.stringify(dragon);
        api_request("DELETE", form_json, on_ok, console.warn);
    }
}

class SizeChartFixedData
{
    constructor(dragons = [])
    {
        this.dragons = dragons;
        this.pk = 0;
        for ( let derg of dragons )
            if ( derg.id >= this.pk )
                this.pk = derg.id + 1;

        this.units = {
            cm: 1,
            m: 100,
            ft: 30.48,
        };
    }

    save_state()
    {
        window.history.replaceState(null, "", "?dragons=" + encodeURIComponent(JSON.stringify(this.dragons)));
    }

    list(on_ok, on_fail = console.warn)
    {
        on_ok(this.dragons.map(d => { return {...d, owned: true}; }));
    }

    clean(dragon)
    {
        return {
            id: dragon.id,
            name: dragon.name,
            type: dragon.type,
            height: Number(dragon.height) * this.units[dragon["height-unit"]],
            length: Number(dragon.length) * this.units[dragon["length-unit"]],
            color: dragon.color
        };
    }

    add(dragon, on_ok)
    {
        var clean_derg = {
            ...this.clean(dragon),
            id: this.pk,
        };
        this.pk += 1;
        this.dragons.push(clean_derg);
        this.save_state();
        on_ok();
    }

    update(dragon, on_ok)
    {
        for ( let derg of this.dragons )
        {
            if ( derg.id == dragon.id )
            {
                for ( let [name, value] of Object.entries(this.clean(dragon)) )
                    derg[name] = value;
                this.save_state();
                on_ok();
                return;
            }
        }
    }

    remove(dragon, on_ok)
    {
        this.dragons = this.dragons.filter(d => d.id != dragon.id);
        this.save_state();
        on_ok();
    }
}

class SizeChartForm
{
    constructor(svg_chart, form_start, form)
    {
        this.form_mode = null;
        this.svg_chart = svg_chart;
        this.form_start = form_start;
        this.form = form;
    }

    submit_ok()
    {
        this.svg_chart.update();
        this.hide();
    }

    submit()
    {
        var object = {};
        (new FormData(this.form)).forEach(function(value, key){
            object[key] = value;
        });

        try {
            if ( this.form_mode == "create" )
            {
                this.svg_chart.provider.add(object, this.submit_ok.bind(this));
            }
            else if ( this.form_mode == "modify" )
            {
                this.svg_chart.provider.update(object, this.submit_ok.bind(this));
            }
            else if ( this.form_mode == "delete" )
            {
                this.svg_chart.provider.remove(object, this.submit_ok.bind(this));
            }
        } catch (e) {
            console.error(e);
        }
    }

    toggle_rows(ids)
    {
        for ( let row of this.form.querySelectorAll("tr") )
            this.toggle_row(row, ids.indexOf(row.id) != -1);
    }

    toggle_row(element, show)
    {
        element.style.display = show ? "table-row" : "none";
    }

    hide()
    {
        this.form_mode = null;
        this.form_start.style.display = "flex";
        this.form.style.display = "none";
    }

    show(mode)
    {
        this.form_mode = mode;
        this.form_start.style.display = "none";
        this.form.style.display = "block";
        this.form.reset();

        if ( mode == "create" )
        {
            this.toggle_rows(["form-row-name", "form-row-type", "form-row-save", "form-row-color"]);
            document.getElementById("type").value = "derg";
            this.type();
            document.getElementById("button-save").innerText = "Create";
        }
        else if ( mode == "modify" )
        {
            this.toggle_rows(["form-row-dragon", "form-row-save"]);
            this.dragon_setup();
            document.getElementById("button-save").innerText = "Save";
        }
        else if ( mode == "delete" )
        {
            this.toggle_rows(["form-row-dragon", "form-row-save"]);
            this.dragon_setup();
            document.getElementById("button-save").innerText = "Delete";
        }
    }

    type()
    {
        let type = dragon_types[document.getElementById("type").value];
        if ( !type )
            return;

        this.toggle_row(document.getElementById("form-row-height"), type.height);
        document.querySelector("#form-row-height label").innerText = type.height;
        this.toggle_row(document.getElementById("form-row-length"), type.length);
        document.querySelector("#form-row-length label").innerText = type.length;
    }

    dragon_setup()
    {
        let select = document.getElementById("edit-id");
        clear(select);
        let empty = select.appendChild(document.createElement("option"));
        empty.disabled = true;
        empty.appendChild(document.createTextNode("Please wait..."));
        let form = this;
        this.svg_chart.provider.list(function(data){
            let count = 0;
            for ( let dragon of data )
            {
                if ( dragon.owned )
                {
                    count++;
                    let opt = select.appendChild(document.createElement("option"));
                    opt.appendChild(document.createTextNode(dragon.name));
                    opt.setAttribute("value", dragon.id);
                    opt.dragon = dragon;
                }
            }
            empty.innerText = "Select Dragon";
            if ( count == 0 )
            {
                console.warn("No dragons");
                form.hide();
            }
        }, function(data){
            console.warn(data);
            form.hide();
        });
    }

    dragon()
    {
        if ( this.form_mode == "modify" )
        {
            let select = document.getElementById("edit-id");
            let dragon = select.options[select.selectedIndex].dragon;
            if ( dragon )
            {
                this.toggle_rows([
                    "form-row-dragon", "form-row-name",
                    "form-row-type", "form-row-save",
                    "form-row-color",
                ]);
                for ( let prop of ["type", "name", "length", "height", "color"] )
                    document.getElementById(prop).value = dragon[prop];
                document.getElementById("length-unit").value = "cm";
                document.getElementById("height-unit").value = "cm";
                this.type();
            }
        }
    }
}

