function randomize_control_seed()
{
    var x = Math.round(Math.random() * Number.MAX_SAFE_INTEGER);
    document.getElementById("custom_seed").value = x;
}

function current_row()
{
    return document.querySelector("#board > .row[data-state=empty]");
}

function next_empty_tile()
{
    if ( selected_tile )
    {
        if ( can_be_selected(selected_tile) )
            return selected_tile;
        selected_tile = null;
    }

    var row = current_row();
    if ( !row )
        return null;
    return row.querySelector(".tile[data-state=empty]");
}

function last_full_tile()
{
    if ( selected_tile && can_be_selected(selected_tile) )
    {
        if ( selected_tile.dataset.state == "tbd" )
            return selected_tile;
        return null;
    }

    var row = current_row();
    if ( !row )
        return null;

    var written = row.querySelectorAll(".tile[data-state=tbd]");
    if ( !written.length )
        return null;
    return written[written.length - 1];
}

function set_tile_sticker(tile, sticker, src = null)
{
    if ( tile.dataset.state == "tbd" )
    {
        while ( tile.firstChild )
            tile.removeChild(tile.firstChild);
    }

    if ( !tile.firstChild && tile.dataset.sticker != sticker)
    {
        var img = document.createElement("img");
        tile.appendChild(img);
        img.setAttribute("alt", sticker);
        img.setAttribute("aria-label", sticker);
        if ( !src )
            src = document.querySelector(`#keyboard .button img[data-sticker='${sticker}']`).src;
        img.setAttribute("src", src);
        img.addEventListener("click", tile_press);
        img.dataset.sticker = sticker;
        tile.dataset.sticker = sticker;
    }
}

function can_be_selected(tile)
{
    return tile.parentNode == current_row();
}

function clear_selection()
{
    if ( selected_tile )
    {
        selected_tile.dataset.selected = "false";
        selected_tile = null;
    }
}

function select_tile(tile)
{
    if ( tile == selected_tile )
    {
        clear_selection();
        return;
    }

    if ( can_be_selected(tile) && tile.dataset.selected == "false" )
    {
        if ( selected_tile )
            clear_selection();
        selected_tile = tile;
        tile.dataset.selected = "true";
        tile.dataset.animation = "";
    }
}

function tile_press(ev)
{
    ev.stopPropagation();
    if ( ev.target.parentNode.dataset.state != "tbd" )
        button_press(ev);
    else
        select_tile(ev.target.parentNode);
}

function button_press(ev)
{
    var sticker = ev.target.dataset.sticker;
    var tile = next_empty_tile();
    if ( tile )
    {
        tile.dataset.state = "tbd";
        tile.dataset.animation = "pop";
        set_tile_sticker(tile, sticker, ev.target.getAttribute("src"));
        clear_selection();
    }
}

function button_delete()
{
    var tile = last_full_tile();
    if ( tile )
    {
        tile.removeChild(tile.firstChild);
        tile.dataset.state = "empty";
        tile.dataset.animation = "idle";
        tile.dataset.sticker = "";
    }
}

function mark_sticker_keyboard(sticker, state, congratulate)
{
    var button = document.querySelector(`#keyboard .button[data-sticker='${sticker}']`);
    if ( button.dataset.state == "correct" )
        return;
    if ( state == "absent" && button.dataset.state == "present" )
        return;

    button.dataset.state = state;
}

function apply_row_results(tiles, results, entered)
{
    var completed = true;
    for ( var i = 0; i < tiles.length; i++ )
    {
        tiles[i].dataset.state = results[i].state;
        set_tile_sticker(tiles[i], results[i].sticker);
        mark_sticker_keyboard(results[i].sticker, results[i].state);

        if ( results[i].state == "correct" )
        {
            summary_pic += "🟩";
        }
        else
        {
            completed = false;
            if ( results[i].state == "present" )
                summary_pic += "🟨";
            else if ( results[i].state == "absent" )
                summary_pic += "⬛️";
        }
    }
    summary_pic += "\n";

    var delay_1;

    if ( entered )
    {
        delay_1 = staggered_animation(tiles, "Flip");
        saved_state.guesses.push(results);
        localStorage.setItem("state", JSON.stringify(saved_state));
    }

    guess_count += 1;
    if ( completed )
    {
        finished = true;
        document.getElementById("stats-count").innerText = guess_count;
        document.getElementById("stats-preview").innerText = `Glaxle # ${seed} ${guess_count}/${max_guesses}\n\n` +
            summary_pic + "\n\n" + window.location.href;
        document.getElementById("stats-not-solved").style.display = "none";
        document.getElementById("stats-solved").style.display = "block";

        if ( entered )
        {
            toast("Congratulations");
            window.setTimeout(function() {
                var delay_2 = staggered_animation(tiles, "Jump");
                window.setTimeout(function() {
                    show("stats");
                }, delay_2);
            }, delay_1);
        }
    }
}

function button_enter()
{
    var row = current_row();
    if ( finished || !row || row.querySelector(".tile[data-state=empty]") )
        return;

    clear_selection();

    var check_url = new URL(window.location.href);
    check_url.pathname += "check/";
    var tiles = Array.from(row.querySelectorAll(".tile[data-state=tbd]"));
    var guess = tiles.map(e => e.dataset.sticker).join(";");
    check_url.searchParams.set("guess", guess);

    row.dataset.state = "full";

    var request = new Request(check_url);
    fetch(request)
    .then(response => response.json())
    .then(json => {
        if ( json.error )
            return;
        apply_row_results(tiles, json.result, true);
    })
}

function load_saved_state(seed)
{
    var state_str = localStorage.getItem("state");
    if ( state_str )
    {
        var state = JSON.parse(state_str);
        if ( state.seed == seed )
        {
            for ( var guess_row of state.guesses )
            {
                var row = current_row();
                var tiles = Array.from(row.querySelectorAll(".tile"));

                row.dataset.state = "full";
                apply_row_results(tiles, guess_row, false);
            }
            return state;
        }

    }

    return {
        seed: seed,
        guesses: []
    };
}

function staggered_animation(tiles, animation, duration=500, stagger=0.5)
{
    var duration = 500;
    for ( var i = 0; i < tiles.length; i++ )
        tiles[i].setAttribute("style", `animation: ${duration}ms ${animation} linear ${i*duration*stagger}ms;`);
    return tiles.length * duration * stagger + duration;
}

function show(id)
{
    var page = document.getElementById(id);
    page.classList.remove("close");
    page.classList.add("open");
    document.getElementById("content").style = "overflow-y: hidden";
}

function hide(id)
{
    var page = document.getElementById(id);
    page.classList.remove("open");
    page.classList.add("close");
    window.setTimeout(function() {
        document.getElementById("content").style = "overflow-y: auto";
        page.classList.remove("close");
    }, 100);
}

function load_settings()
{
    var defaults = {
        "dark-mode": false,
        colorblind: false,
        smolwidth: true,
        largewidth: false,
        fullwidth: false,
    };
    var stored = localStorage.getItem("glaxle_settings");
    if ( !stored )
        return defaults;

    var loaded = JSON.parse(stored);
    for ( var key of Object.keys(defaults) )
    {
        if ( key in loaded )
        {
            var value = loaded[key];
            toggle_class(value, key, [], false);
            document.getElementById("setting_" + key).checked = value;
            defaults[key] = value;
        }
    }

    return defaults;

}

function toggle_class(enable, cls, disable=[], store=true)
{
    if ( enable )
        document.documentElement.classList.add(cls);
    else
        document.documentElement.classList.remove(cls);

    if ( disable.length )
    {
        for ( let x of disable )
            toggle_class(!enable, x, [], store);
    }

    if ( store )
    {
        preferences[cls] = enable;
        localStorage.setItem("glaxle_settings", JSON.stringify(preferences));
    }
}

function share()
{
    navigator.clipboard.writeText(document.getElementById("stats-preview").innerText);
    toast("Copied results to the clipboard");
}

function toast(text, duration=2000)
{
    var toaster = document.getElementById("toaster");
    var toast = document.createElement("div");
    toaster.appendChild(toast);
    toast.classList.add("toast");
    toast.appendChild(document.createTextNode(text));
    window.setTimeout(function() {
        toast.setAttribute("style", "opacity: 1");
    }, 1);

    window.setTimeout(function() {
        toast.setAttribute("style", "opacity: 0");
    }, duration);

    window.setTimeout(function() {
        toaster.removeChild(toast);
    }, duration + 300);
}

