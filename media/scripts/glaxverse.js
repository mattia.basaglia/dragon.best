function nonsense(callback, who=null, template=null)
{
    var url = new URL("https://insult.mattbas.org/api/en_corporate/insult.txt");
    if ( who )
        url.searchParams.set("who", who);
    if ( template )
        url.searchParams.set("template", template);

    fetch(url)
        .then(response => response.text())
        .then(callback)
        .catch(error => console.warn(error))
    ;
}

function nonsense_to_element(element, who=null, template=null)
{
    nonsense(text => element.appendChild(document.createTextNode(text)), who, template);
}

function nonsense_blocks(parent, template_title, who, template, count=3)
{
    for ( var i = 0; i < count; i++ )
    {
        var box = parent.appendChild(document.createElement("div"));
        box.classList.add("text-box");

        var title = box.appendChild(document.createElement("h3"));
        nonsense_to_element(title, null, typeof template_title == "function" ? template_title() : template_title);

        var description = box.appendChild(document.createElement("p"));

        nonsense_to_element(description, who, typeof template == "function" ? template() : template);
    }
}

function join_nonsense(items)
{
    var template = "";

    for ( var i = 0; i < items.length; i++ )
    {
        var sentence = items[i];
        sentence = sentence.replaceAll("id=", `id=id_${i}`);
        sentence = sentence.replaceAll("helper=", `helper=id_${i}`);
        sentence = sentence.replaceAll("target=", `target=id_${i}`);
        template += sentence + "\n";
    }

    return template;
}

function random_choice(choices)
{
    return choices[Math.floor(Math.random() * choices.length)];
}

function mega_nonsense(question_probability=0)
{
    var sentences = [
        "The Glaxverse is <article_singular target='adj' helper='noun'> <adjective id='adj'> <noun id='noun'> that <verb id=verb1><verb_noun_3rd target=verb1 helper=noun> <adjective min=2 max=3> <noun> and <adverb> <verb id=verb2><verb_noun_3rd target=verb2 helper=noun> <adjective> <noun>.",
        "At the Glaxverse we value <adjective min=1 max=2> and <adjective> <noun>.",
        "The Glaxverse <adverb> <verb id=verb><verb_3rd target=verb> <adjective min=1 max=3> <noun>.",
        "We at the Glaxverse <adverb> <verb> <adjective min=1 max=2> <noun> and <verb> <adjective min=1 max=2> <noun>.",
        "For all your <adjective min=1 max=3> <noun> needs, the Glaxverse has the best <adjective> <noun>.",
        "You can't go wrong with the Glaxverse's <adjective min=1 max=3> <noun>.",
        "The Glaxverse <verb id=verb><verb_3rd target=verb> <adverb> <adjective> <noun>.",
    ];

    var questions = [
        "Need someone to <adverb> <verb id=verb><verb_3rd target=verb> <adjective min=1 max=3> <noun>?",
        "Are you looking for a <adjective> <noun> to <adverb> <verb id=verb><verb_3rd target=verb> <adjective min=1 max=3> <noun>?",
        "Do you need someone who can <adverb> <verb id=verb><verb_3rd target=verb> <adjective min=1 max=3> <noun>?",
        "Who can <adverb> <verb id=verb><verb_3rd target=verb> <adjective min=1 max=3> <noun>?",
    ];

    var answer = random_choice(sentences);

    if ( Math.random() < question_probability )
    {
        var question = random_choice(questions);
        return join_nonsense([question, answer]);
    }

    return answer;
}

function nonsense_functor(question_probability)
{
    return function() { return mega_nonsense(question_probability); }
}

function random_title()
{
    var titles = ["<noun>", "<adjective>", "<verb>", "<adverb> <adjective>"];
    return random_choice(titles);

}

function new_section(parent)
{
    var section = parent.appendChild(document.createElement("section"));
    section.appendChild(document.createElement("div")).classList.add("background");
    var content = section.appendChild(document.createElement("div"));
    content.classList.add("container");
    content.classList.add("reveal");
    var titles = ["<adjective> <noun>", "<adverb> <adjective>", "<verb> the <adjective>", "<noun> &amp; <noun>"];
    var title = content.appendChild(document.createElement("h2"));
    nonsense_to_element(title, null, random_choice(titles));
    var nonsense = content.appendChild(document.createElement("div"));
    nonsense.classList.add("text-container");
    nonsense_blocks(nonsense, random_title, null, nonsense_functor(0));
}
