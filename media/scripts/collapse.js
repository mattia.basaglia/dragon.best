// @license magnet:?xt=urn:btih:0b31508aeb0634b347b8270c7bee4d411b5d4109&dn=agpl-3.0.txt AGPLv3+

function init_collapsables(start_collapsed=true, parent=null)
{
    if ( parent === null )
        parent = document.body;

    for ( let element of parent.querySelectorAll("[data-collapse]") )
    {
        let target = document.getElementById(element.getAttribute("data-collapse"));

        let actual_style = window.getComputedStyle(target);
        let original_display = actual_style.display;
        if ( original_display == "none" )
            original_display = "block";

        function hide()
        {
            element.classList.add("collapsed");
            target.style.display = "none";
        }

        function show()
        {
            target.style.display = original_display;
            element.classList.remove("collapsed");
        }

        if ( start_collapsed )
            hide();
        else
            show();

        element.addEventListener("click", function()
        {
            if ( actual_style.display == "none" )
                show();
            else
                hide();
        })
    }
}

function init_collapsables_on_load(start_collapsed=true, parent=null)
{
    window.addEventListener("load", function(){
        init_collapsables(start_collapsed, parent);
    });
}
