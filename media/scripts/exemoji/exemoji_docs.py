from xml_helper import mkcell

descriptions = {
    "swap": "Swaps the value of {a[0]} and {a[1]}",
    "int": "Generates interrupt {a[0]}",
    "halt": "Stops program execution",
    "set": "Assigns {a[1]} to {a[0]}",
    "mov": "Assigns the value of {a[1]} to {a[0]}",
    "add": "Adds {r[0]} to {r[1]}, storing the result in {r[2]} and overflow in {r[3]}",
    "sub": "Subtracts {r[1]} from {r[0]}, storing the result in {r[2]}, " +
           "if the result is negative, {r[3]} will hold the absolute value of the negative result",
    "mul": "Multiplies {r[0]} to {r[1]}, storing the result in {r[2]} and overflow in {r[3]}",
    "div": "Divides {r[0]} by {r[1]}, storing the result in {r[2]} and remainder in {r[3]}",
    "or": "Bitwise OR between {r[0]} and {r[1]}, storing the result in {r[2]}",
    "xor": "Bitwise XOR between {r[0]} and {r[1]}, storing the result in {r[2]}",
    "and": "Bitwise AND between {r[0]} and {r[1]}, storing the result in {r[2]}",
    "not": "Bitwise NOT of {r[0]}, storing the result in {r[2]}",
    "cmp": "Compares {r[0]} and {r[1]}, storing the result in {r[2]}: 0 if equal, 1 if {r[1]} is greater ff if {r[0]} is greater",
    "ign": "Ignores {a[0]}",
    "jrz": "Jumps forwards when {r[2]} is 0, if {a[0]} is an immediate vaue by that many bytes, otherwise after the next occurrence of that emoji",
    "jrf": "Jumps forwards, if {a[0]} is an immediate vaue by that many bytes, otherwise after the next occurrence of that emoji",
    "jrb": "Jumps backwards, if {a[0]} is an immediate vaue by that many bytes, otherwise after the previous occurrence of that emoji",
    "jmp": "Jumps to the absolute location obtained by combining {a[0]} and {a[1]}",
    "ldl": "Loads in {a[0]} the absolute memory address obtained by combining {a[1]} and {a[2]}",
    "stl": "Stores {a[0]} at the absolute memory address obtained by combining {a[1]} and {a[2]}",
    "push": "Pushes {a[0]} into the stack, incrementing the stack pointer",
    "pop": "Pops {a[0]} from the stack, decrementing the stack pointer",
    "ldpc": "Loads into {a[0]} the memory location {a[1]} bytes after the program counter",
    "ldsp": "Loads into {a[0]} the memory location {a[1]} bytes before the stack pointer",
    "call": "Pushes the program counter into the stack (most significant byte first) " +
            "and jumps to the absolute location obtained by combining {a[0]} and {a[1]}",
    "ret": "Pops the program counter from the stack (least significant byte first)",
    "trig": "Polar coordinates of {r[0]} as an angle (2pi = 256) and {r[1]} as length, offset by 127. {r[2]} will be x and {r[3]} will be y"
}
interrupt_sections = [
    "Text Output",
    "Graphics",
    "Graphics Path Commands",
]
interrupt_docs = {
    0x00: "Prints {r[2]} as a character to the output",
    0x10: "Enables / clears graphics output",
    0x11: "Sets graphics stroke color to r {r[0]} g {r[1]} b {r[2]}",
    0x12: "Sets graphics stroke width to {r[0]}",
    0x13: "Applies stroke style to the current path",
    0x14: "Sets graphics fill color to r {r[0]} g {r[1]} b {r[2]}",
    0x15: "Applies fill style to the current path",
    0x20: "Begins a new path",
    0x21: "Closes the current path",
    0x22: "Moves the path pen to x {r[0]} y {r[1]}",
    0x23: "Draws a line from pen position to x {r[0]} y {r[1]}",
}
