import io
import traceback

import js
import pyodide.ffi
from pyodide.http import pyfetch

import exemoji
from exemoji_docs import *


class WebMachine(exemoji.Machine):
    def __init__(self, *a):
        super().__init__(*a)
        self.last_instruction = None
        self.last_fetch = 0
        self.examples = {}
        self.running = False
        self.timer_callback = None
        self.dirty_bytes = []

    def input_to_bytes(self):
        memory = bytearray()
        program = self.input_element.value
        program_bytes = io.BytesIO(program.encode("utf8"))
        while True:
            ch = program_bytes.read(1)
            if ch == b'':
                break
            elif ch == b'\\':
                ch = program_bytes.read(1)
                if ch == b'n':
                    memory.append(0xa)
                elif ch == b'0':
                    memory.append(0)
                elif ch == b'x':
                    memory.append(int(program_bytes.read(2), 16))
                else:
                    memory.append(ord(ch))
            elif ch == b'\n':
                continue
            else:
                memory.append(ord(ch))
        return memory

    def web_load_program(self):
        self.memory = self.input_to_bytes()
        self.program_counter = 0
        self.align(16)
        self.allocate_stack(int(self.stack_size.value))
        self.halted = False
        self.registers = [0] * len(self.regmoji)
        self.last_instruction = None
        self.last_fetch = 0
        self.output_element.innerText = ""
        self.prepare_inspect()
        self.update_inspect()
        self.graphics_reset()
        self.dirty_bytes = []

    def core_dump(self):
        pass

    def interrupt(self, code):
        match code:
            case 0x00:
                self.output_element.innerText += chr(self.registers[2])
                self.output_scroll.scrollTop = self.output_scroll.scrollHeight
            case 0x10:
                self.graphics_clear()
            case 0x11:
                self.graphics_stroke_color(self.registers[0], self.registers[1], self.registers[2])
            case 0x12:
                self.graphics_stroke_width(self.registers[0])
            case 0x13:
                self.graphics_stroke_apply()
            case 0x14:
                self.graphics_fill_color(self.registers[0], self.registers[1], self.registers[2])
            case 0x15:
                self.graphics_fill_apply()
            case 0x20:
                self.graphics_path_begin()
            case 0x21:
                self.graphics_path_close()
            case 0x22:
                self.graphics_path_move(self.registers[0], self.registers[1])
            case 0x23:
                self.graphics_path_line(self.registers[0], self.registers[1])

    def web_run(self):
        if self.running:
            return
        if self.halted:
            self.web_load_program()
        self.running = True
        self.play_speed = int(js.document.getElementById("run-speed").value)
        if self.timer_callback is None:
            self.timer_callback = pyodide.ffi.create_proxy(self.web_run_tick)
        js.setTimeout(self.timer_callback, 0)

    def web_run_tick(self):
        self.run_step()
        if not self.halted and self.running:
            js.setTimeout(self.timer_callback, self.play_speed)

    def fetch_instruction(self):
        self.last_fetch = self.program_counter
        self.last_instruction = super().fetch_instruction()
        return self.last_instruction

    def prepare_inspect(self):
        # Memory
        self.inspect_elements = {}
        self.inspect_cells = []
        self.inspect_cells_text = []

        width = 16
        if len(self.memory) > 0x0250:
            width = 32

        memory = js.document.getElementById("inspect-memory")
        memory.innerHTML = ""

        tr = memory.appendChild(js.document.createElement("tr"))
        for i in range(width//16):
            cell(tr, "", tag="th")
            for i in range(0x10):
                cell(tr, "%x" % i, tag="th")

        for i in range(0, len(self.memory), width):
            tr = memory.appendChild(js.document.createElement("tr"))
            tr.classList.add("mem-bytes")

            chars = []
            for j in range(width):
                address = i+j
                if address >= len(self.memory):
                    break
                if j % 16 == 0:
                    cell(tr, "%04x" % address, tag="th", rowspan="2")
                value = self.memory[address]
                chars.append(self._partial_to_emoji(address, address+1, False))

                td = cell(tr, "%02x" % value, "xmem")
                self.inspect_cells.append(td)
                if address == self.last_fetch:
                    self.inspect_elements["li"] = cell(td, "LI", "pointer", "span", title="Last Instruction")
                if address == self.program_counter:
                    self.inspect_elements["pc"] = cell(td, "PC", "pointer", "span", title="Program Counter")
                if address == self.stack_pointer:
                    self.inspect_elements["sp"] = cell(td, "SP", "pointer", "span", title="Stack Pointer")

            # if chars:
            tr = memory.appendChild(js.document.createElement("tr"))
            tr.classList.add("mem-text")
            for ch in chars:
                self.inspect_cells_text.append(cell(tr, ch))

        # Registers
        self.inspect_regs = []

        registers = js.document.getElementById("inspect-registers")
        registers.innerHTML = ""
        for emoji, val in zip(self.regmoji, self.registers):
            tr = registers.appendChild(js.document.createElement("tr"))
            row = []
            center_cell(tr, emoji)
            row.append(center_cell(tr, ""))
            row.append(cell(tr, "", "number"))
            row.append(cell(tr, ""))
            self.inspect_regs.append(row)

        tr = registers.appendChild(js.document.createElement("tr"))
        row = []
        center_cell(tr, "PC")
        row.append(center_cell(tr, ""))
        row.append(cell(tr, "", "number"))
        row.append(cell(tr, ""))
        self.inspect_regs.append(row)

        tr = registers.appendChild(js.document.createElement("tr"))
        row = []
        center_cell(tr, "SP")
        row.append(center_cell(tr, ""))
        row.append(cell(tr, "", "number"))
        cell(tr, "")
        self.inspect_regs.append(row)

        # Instruction
        self.inspect_instr = [
            js.document.getElementById("last-instruction-emoji"),
            js.document.getElementById("last-instruction-name"),
            js.document.getElementById("last-instruction-op0"),
            js.document.getElementById("last-instruction-op1"),
            js.document.getElementById("last-instruction-op2"),
            js.document.getElementById("last-instruction-desc"),
        ]

    def inspect_format_instruction(self, instr, argr):
        cells = [instr.opcode, instr.name]
        args = []
        for i in range(3):
            if i < len(instr.arguments):
                argt = instr.arguments[i]
                argv = self.format_operand(argr[i], argt)
                args.append(argv)
                cells.append(argv)
            else:
                cells.append("")

        desc = descriptions[instr.mnemonic]
        if instr.mnemonic in ("jrz", "jrf", "jrb"):
            desc = desc.split(",")[0]
            if isinstance(argr[0], int):
                desc += " by {a[0]} bytes"
            else:
                desc += " to the memory location after the nearest occurrence of {a[0]}"
        elif instr.mnemonic == "int":
            desc += ". " + interrupt_docs.get(argr[0], "").format(r=machine.regmoji)
        cells.append(desc.format(a=args, r=self.regmoji))
        return cells

    def update_inspect(self):
        for i, val in enumerate(self.registers):
            row = self.inspect_regs[i]
            row[0].textContent = "%02x" % val
            row[1].textContent = str(val)
            if val >= 0x20 and val < 0x80:
                row[2].textContent = repr(chr(val))
            else:
                row[2].textContent = ""

        for i, val in [(0, self.program_counter), (1, self.stack_pointer)]:
            row = self.inspect_regs[i + len(self.registers)]
            row[0].textContent = "%04x" % val
            row[1].textContent = str(val)

        if self.last_instruction:
            instr = self.last_instruction[0]
            argr = self.last_instruction[1]
            cells = self.inspect_format_instruction(instr, argr)
            for i, text in enumerate(cells):
                self.inspect_instr[i].textContent = text

        for id, val in [("li", self.last_fetch), ("pc", self.program_counter), ("sp", self.stack_pointer)]:
            iel = self.inspect_elements[id]
            mcell = self.inspect_cells[val]
            iel.parentElement.removeChild(iel)
            mcell.appendChild(iel)

        for i in self.dirty_bytes:
            mcell = self.inspect_cells[i]
            newval = self.memory[i]
            mcell.firstChild.textContent = "%02x" % newval
            tcell = self.inspect_cells_text[i]
            tcell.textContent = self._partial_to_emoji(i, i+1, False)
        self.dirty_bytes = []

    def write_memory(self, address, byte):
        super().write_memory(address, byte)
        self.dirty_bytes.append(address)

    def run_step(self):
        try:
            instr = self.fetch_instruction()
            self.exec_instruction(*instr)
        except exemoji.Emojiception as e:
            self.output_element.innerText += "\n%s" % e
            self.halted = True
        except Exception:
            self.output_element.innerText += "\nVM Error"
            self.halted = True
            traceback.print_exc()

        if self.halted:
            self.running = False
        self.update_inspect()

    def web_step(self):
        if not self.running:
            self.run_step()

    def web_pause(self):
        self.running = False

    def graphics_clear(self):
        self.output_context.clearRect(0, 0, self.output_canvas.width, self.output_canvas.height)
        self.output_canvas.style.display = "block"

    def graphics_reset(self):
        self.graphics_clear()
        self.output_context.lineWidth = 1
        self.output_context.strokeStyle = "black"
        self.output_context.fillStyle = "white"
        self.output_context.beginPath()

    def graphics_stroke_width(self, w):
        self.output_context.lineWidth = w

    def graphics_stroke_color(self, r, g, b):
        self.output_context.strokeStyle = "rgb(%s, %s, %s)" % (r, g, b)

    def graphics_fill_color(self, r, g, b):
        self.output_context.fillStyle = "rgb(%s, %s, %s)" % (r, g, b)

    def graphics_path_begin(self):
        self.output_context.beginPath()

    def graphics_path_close(self):
        self.output_context.closePath()

    def graphics_stroke_apply(self):
        self.output_context.stroke()

    def graphics_fill_apply(self):
        self.output_context.fill()

    def graphics_path_move(self, x, y):
        self.output_context.moveTo(x, y)

    def graphics_path_line(self, x, y):
        self.output_context.lineTo(x, y)


def cell(*a, **kw):
    return mkcell(js.document, *a, **kw)


def center_cell(tr, text):
    return cell(tr, text, "centered")


def explain_code(ev):
    out = js.document.getElementById("playground-explain")
    out.innerHTML = ""
    out.style.display = "table"

    cell(out, "Step by step explanation", tag="caption")

    tr = cell(out, "", tag="tr")
    cell(tr, "Address", tag="th")
    cell(tr, "Opcode", tag="th")
    cell(tr, "Operands", tag="th", colspan=3)
    cell(tr, "Explanation", tag="th")

    memcpy = machine.memory
    pc = machine.program_counter
    halt = machine.halted
    machine.memory = machine.input_to_bytes()
    machine.program_counter = 0
    machine.halted = False

    while machine.program_counter < len(machine.memory):
        tr = cell(out, "", tag="tr")
        cell(tr, "%04x" % machine.program_counter, ["mono", "number"])
        try:
            instr, args = machine.fetch_instruction()
            text = machine.inspect_format_instruction(instr, args)
            center_cell(tr, instr.opcode)
            cell(tr, text[2], "mono centered")
            cell(tr, text[3], "mono centered")
            cell(tr, text[4], "mono centered")
            cell(tr, text[5])
        except exemoji.Emojiception as e:
            if e.extra and ord(e.extra) > 0x10000:
                cell(tr, e.extra or "", "mono centered")
                cell(tr, "")
                cell(tr, "")
                cell(tr, "")
                cell(tr, "Unknown")
            else:
                tr.parentNode.removeChild(tr)
                break

    machine.halted = halt
    machine.program_counter = pc
    machine.memory = memcpy


async def setup_playground():
    # Inputs
    machine.stack_size = js.document.getElementById("stack-size")
    machine.output_element = js.document.getElementById("playground-output")
    machine.output_scroll = js.document.getElementById("playground-output-p")
    machine.input_element = js.document.getElementById("playground-editor")
    event_listener(js.document.getElementById("btn-load"), "click", lambda ev: machine.web_load_program())
    event_listener(js.document.getElementById("btn-play"), "click", lambda ev: machine.web_run())
    event_listener(js.document.getElementById("btn-step"), "click", lambda ev: machine.web_step())
    event_listener(js.document.getElementById("btn-pause"), "click", lambda ev: machine.web_pause())
    event_listener(js.document.getElementById("btn-explain"), "click", explain_code)
    machine.output_canvas = js.document.getElementById("playground-graphics")
    machine.output_context = machine.output_canvas.getContext("2d")

    # Keyboard
    keyboard = js.document.getElementById("keyboard")
    rows = []

    def add_row():
        rows.append(cell(keyboard, "", "key-row", "div"))

    def key_click(ev):
        # JS string and all the offsets are in utf-16 with surrogates (yuck)
        pointer_start = 0
        pointer_end = 0
        sel_start = machine.input_element.selectionStart
        locs = []
        for i, ch in enumerate(machine.input_element.value):
            locs.append(i)
            if ord(ch) > 0xffff:
                locs.append(i)
        locs.append(len(machine.input_element.value))
        pointer_start = locs[machine.input_element.selectionStart]
        pointer_end = locs[machine.input_element.selectionEnd]

        emoji = ev.target.textContent
        machine.input_element.value = (
            machine.input_element.value[:pointer_start] +
            emoji +
            machine.input_element.value[pointer_end:]
        )
        offset = 2 if ord(emoji) > 0xffff else 1
        machine.input_element.selectionStart = machine.input_element.selectionEnd = sel_start + offset

    key_click_proxy = pyodide.ffi.create_proxy(key_click)

    def add_key(emoji, title):
        key = cell(rows[-1], emoji, ["key", "border"], "div", title=title)
        key.addEventListener("click", key_click_proxy)

    add_row()
    for i, reg in enumerate(machine.regmoji):
        add_key(reg, "reg %s" % i)

    add_row()
    for i, imm in enumerate(machine.immediates):
        if i == 8:
            add_row()
        add_key(imm, "%x" % i)
    add_key(machine.long_immediate, "octet")

    add_row()
    for ins in machine.instructions.values():
        if ins.mnemonic in ("add", "jrf"):
            add_row()
        add_key(ins.opcode, ins.name)

    # Examples
    base_path = "/media/scripts/exemoji/examples/exe/"

    examples_element = js.document.getElementById("examples")

    def on_example(ev):
        machine.input_element.value = machine.examples[ev.target.value]
        machine.halted = True

    event_listener(examples_element, "input", on_example)

    for slug, title in examples:
        processed = bytearray()
        if slug != "":
            raw = await (await pyfetch("%s/%s.exemoji" % (base_path, slug))).bytes()
            # with open("%s/%s.exemoji" % (base_path, slug), "rb") as file:
            with io.BytesIO(raw) as file:
                while True:
                    ch = file.read(1)
                    if ch == b'':
                        break
                    val = ord(ch)

                    if val & 0x80 or val >= 0x20:
                        processed.append(val)
                    elif val == 0:
                        processed += b'\\' + b'0'
                    elif val == 0xa:
                        processed += b'\\' + b'n'
                    else:
                        processed += ('\\x%02x' % val).encode("ascii")
        machine.examples[slug] = processed.decode("utf-8")
        cell(examples_element, title, None, "option", value=slug)


machine: WebMachine = exemoji.build_machine(WebMachine)

examples = [
    ("", "🚫🧹 Empty"),
    ("helloworld", "👋🌍 Hello World"),
    ("hello_nomem", "👋🌍 Hello World (Code only)"),
    ("fibonacci", "👩‍🏫🔢 Fibonacci Numbers"),
    ("99bottles", "9️⃣🍾 99 Bottles of Beer"),
    ("fizzbuzz", "🥤🍷 Fizz Buzz"),
    ("graphics", "🏠🖌️ Graphics"),
    ("OwO", "🙀👀 OwO"),
]


async def init():
    try:
        await setup_playground()
        js.document.getElementById("main-loading").style = "display: none"
        js.document.getElementById("main-loaded").style = "display: block"
    except Exception:
        js.document.getElementById("main-loading").innerText = "Loading failed x_x"
        raise


def event_listener(element, event, callback):
    proxy = pyodide.ffi.create_proxy(callback)
    element.addEventListener(event, proxy)
    return proxy
