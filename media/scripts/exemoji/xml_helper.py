
def mkcell(document, tr, text=None, cls=None, tag="td", **attrs):
    td = tr.appendChild(document.createElement(tag))
    if text is not None:
        td.appendChild(document.createTextNode(str(text)))
    if isinstance(cls, str):
        td.setAttribute("class", cls)
    elif isinstance(cls, list):
        td.setAttribute("class", " ".join(cls))

    for k, v in attrs.items():
        td.setAttribute(k, str(v))

    return td


def setup_ids(document):
    ids = {}
    for node in document.getElementsByTagName("*"):
        ids[node.getAttribute("id")] = node
    document.getElementById = ids.get
    return ids
