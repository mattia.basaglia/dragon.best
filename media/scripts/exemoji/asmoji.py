#!/usr/bin/env python3
import sys
import enum
import pathlib
import argparse
import dataclasses

import exemoji


class ParseError(Exception):
    pass


class Assembler:
    class PlaceholderByte(enum.Enum):
        All = enum.auto()
        High = enum.auto()
        Low = enum.auto()

    @dataclasses.dataclass
    class Placeholder:
        pos: int
        label: str
        relative: bool
        size: int
        byte: "PlaceholderByte"
        line: int
        col: int
        placeholder: bytes = b''

    def __init__(self, file, machine):
        self.file = file
        self.machine = machine
        self.out = bytearray()
        self.labels = {}
        self.placeholders = []
        self.instructions = {
            inst.mnemonic: inst
            for inst in machine.instructions.values()
        }
        imm0 = machine.immediates[0].encode("utf8")
        imm_long = machine.long_immediate.encode("utf8")
        self.placeholder_bytes = {
            1: imm0,
            2: imm_long + imm0 + imm0,
            3: imm0 + imm_long + imm0 + imm0,
            4: imm_long + imm0 + imm0 + imm_long + imm0 + imm0,
        }
        self.line = 1
        self.col = 1

    def _get_ch(self):
        ch = self.file.read(1)
        if ch == '\n':
            self.line += 1
            self.col = 1
        else:
            self.col += 1
        return ch

    def _skip_ws(self):
        ch = self._get_ch()
        while ch.isspace():
            ch = self._get_ch()
        return ch

    def _skip_line(self, ch):
        while ch != '' and ch != '\n':
            ch = self._get_ch()

    def _lex_label(self):
        label = ""
        while True:
            ch = self._get_ch()
            if not ch.isalnum() and ch not in "_:^":
                break
            label += ch
        return label

    def _lex(self):
        ch = self._skip_ws()

        # Eof
        if ch == '':
            return ch

        # comment
        if ch in ";#":
            self._skip_line(ch)
            return self._lex()

        # label
        if ch == ":":
            return ":" + self._lex_label()

        if ch.isalpha() or ch.isdigit():
            token = ""
            while ch.isalpha() or ch.isdigit() or ch == "_":
                token += ch
                ch = self._get_ch()
            return token

        if ch == "'":
            ch = self._get_ch()
            self._get_ch()
            return "'%s" % ch

        if ch == "!":
            return "!" + self._get_ch()

        raise ParseError("Unknown token %r %s" % (ch, ch))

    def assemble(self):
        while True:
            token = self._lex()
            # Eof
            if token == '':
                break
            # Label
            elif token.startswith(":"):
                self.labels[token[1:]] = len(self.out)
                if token == ":data":
                    self._assemble_data()
                    break
            # Emoji "Label"
            elif token.startswith("!"):
                self.out += token[1:].encode("utf8")
            # Align to size
            elif token == "align":
                align = int(self._lex(), 16)
                fill = int(self._lex(), 16)
                self.out += bytearray([fill] * align)
            # Instruction
            else:
                instr = self.instructions[token]
                self.out += instr.opcode.encode("utf8")
                skip = False
                for arg in instr.arguments:
                    if skip:
                        skip = False
                    else:
                        skip = self._assemble_arg(arg)

        self._resolve_labels()
        return self.out

    def _assemble_data(self):
        last_was_newline = True
        while True:
            ch = self._get_ch()
            if ch == '\\':
                ch = self._get_ch()
                if ch == 'n':
                    ch = '\n'
                elif ch == "0":
                    ch = '\0'
                elif ch == 'x':
                    self.out.append(int(self._get_ch() + self._get_ch(), 16))
                    continue
            elif ch == '':
                if last_was_newline:
                    self.out.pop()
                break
            elif ch == "\n":
                last_was_newline = True
            elif ch == ":" and last_was_newline:
                last_was_newline = False
                label = self._lex_label()
                self.labels[label] = len(self.out)
                continue
            self.out.append(ord(ch))

    def _arg_reg(self, token):
        self.out += self.machine.regmoji[ord(token[-1]) - ord('0')].encode("utf8")

    def _arg_imm(self, token):
        if token[0] == ":":
            label = token[1:]
            relative = False
            size = 3
            low_high = self.PlaceholderByte.All
            if label[0] == "_":
                label = label[1:]
                low_high = self.PlaceholderByte.Low
                size = 2
            elif label[0] == "^":
                label = label[1:]
                low_high = self.PlaceholderByte.High
                size = 2
            elif label[0] == ":":
                label = label[1:]
                relative = True
                size = 2
            if label[0] in "1234":
                size = ord(label[0]) - ord("0")
                label = label[1:]

            holder = self.Placeholder(len(self.out), label, relative, size, low_high, self.line, self.col)
            holder.placeholder = self.placeholder_bytes[holder.size]
            self.placeholders.append(holder)
            self.out += holder.placeholder
            return True
        if token[0] == "'":
            self.out += self._encode_immediate(ord(token[1:]))
        else:
            try:
                self.out += self._encode_immediate(int(token, 16))
            except ValueError:
                raise ParseError("Invalid immediate %s" % token)

    def _assemble_arg(self, arg):
        token = self._lex()
        if arg == exemoji.InstructionArgument.Register:
            self._arg_reg(token)
        elif arg == exemoji.InstructionArgument.Immediate:
            return self._arg_imm(token)
        elif arg == exemoji.InstructionArgument.Emoji:
            if token.startswith("r"):
                self._arg_reg(token)
            elif token.startswith("!"):
                self.out += token[1:].encode("utf8")
            elif token in self.instructions:
                self.out += self.instructions[token].mnemonic.encode("utf8")
            else:
                self._arg_imm(token)

    def _encode_immediate(self, imm, long=None):
        immstr = ""
        if long is True or (long is None and imm & 0xf0):
            immstr = self.machine.long_immediate + self.machine.immediates[(imm >> 4) & 0xf]
        elif long is False and imm & 0xf0 != 0:
            raise ParseError("Truncating value %s %x" % (imm, imm))
        immstr += self.machine.immediates[imm & 0xf]
        return immstr.encode("utf8")

    def _encode_label(self, labp, size):
        encoded = bytearray()
        if size > 2:
            encoded = self._encode_immediate((labp >> 8) & 0xff, size > 3)
        elif (labp >> 8) & 0xff != 0:
            raise ParseError("Truncating 2 byte label")
        encoded += self._encode_immediate(labp & 0xff, size > 1)
        return encoded

    def _resolve_labels(self):
        for holder in self.placeholders:
            self.line = holder.line
            self.col = holder.col
            if holder.label.startswith("eval:"):
                chunks = holder.label.split(":")[1:]
                stack = []
                for chunk in chunks:
                    if chunk.isdigit():
                        stack.append(int(chunk))
                    elif chunk in self.labels:
                        stack.append(self.labels[chunk])
                    elif chunk == "_size":
                        stack.append(len(self.out))
                    elif chunk == "_align":
                        align = stack.pop()
                        size = stack.pop()
                        stack.append(size + exemoji.aligned_size(size, align))
                    elif chunk == "_add":
                        a = stack.pop()
                        b = stack.pop()
                        stack.append(a + b)
                    elif chunk == "_sub":
                        b = stack.pop()
                        a = stack.pop()
                        stack.append(a - b)
                    elif chunk == "_abs":
                        v = stack.pop()
                        stack.append(abs(v))
                    else:
                        raise ParseError("Unknown operand %s" % chunk)
                ptr = stack[-1]
                if ptr < 0:
                    raise ParseError("eval values must be positive %s" % stack)

            else:
                ptr = self.labels[holder.label]
            effective_size = len(holder.placeholder)
            if holder.relative:
                ptr = abs(holder.pos + effective_size - ptr)
            if holder.byte == self.PlaceholderByte.Low:
                ptr = ptr & 0xff
            elif holder.byte == self.PlaceholderByte.High:
                ptr = (ptr >> 8) & 0xff
            encoded = self._encode_label(ptr, holder.size)
            self.out[holder.pos:holder.pos + effective_size] = encoded


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--memory", type=int, default=128)
    parser.add_argument("file", type=pathlib.Path)
    parser.add_argument("--output", "-o")
    args = parser.parse_args()

    machine = exemoji.build_machine()

    with open(args.file) as f:
        ass = Assembler(f, machine)
        try:
            bytecode = ass.assemble()
        except Exception as e:
            f.seek(0)
            sys.stdout.write(list(f)[ass.line-2])
            sys.stdout.write("%s:Error:%s:%s: %s\n" % (args.file.name, ass.line - 1, ass.col, e))
            raise

    if args.output:
        with open(args.output, "wb") as f:
            f.write(bytecode)

    print(bytecode.decode("utf8"))
