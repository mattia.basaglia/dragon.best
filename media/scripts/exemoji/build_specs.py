import pathlib
import argparse
from xml.dom.minidom import parse

import exemoji
from exemoji_docs import *
from xml_helper import mkcell, setup_ids


def cell(tr, *a, **kw):
    td = mkcell(document, tr, *a, **kw)
    tr.appendChild(document.createTextNode("\n"))
    return td


parser = argparse.ArgumentParser()
parser.add_argument("infile")
parser.add_argument("outfile")
args = parser.parse_args()

document = parse(args.infile)
machine = exemoji.build_machine()

setup_ids(document)

document.getElementById("reg-count").appendChild(document.createTextNode(str(len(machine.registers))))

registers = document.getElementById("registers")
for i, register in enumerate(machine.regmoji):
    tr = registers.appendChild(document.createElement("tr"))
    cell(tr, register, "centered")
    cell(tr, str(i), "centered")
    td = tr.appendChild(document.createElement("td"))
    if i == 0:
        td.appendChild(document.createTextNode("Left handside operand for mathematical operations"))
    elif i == 1:
        td.appendChild(document.createTextNode("Right handside operand for mathematical operations"))
    elif i == 2:
        td.appendChild(document.createTextNode("Main result for mathematical operations"))
    elif i == 3:
        td.appendChild(document.createTextNode("Secondary result for mathematical operations"))

immediates = document.getElementById("immediates")
for i, imm in enumerate(machine.immediates):
    tr = immediates.appendChild(document.createElement("tr"))
    cell(tr, imm, "centered")
    cell(tr, "%x" % i, "centered")
    cell(tr, "%d" % i, "centered")
document.getElementById("immediate-long").appendChild(document.createTextNode(machine.long_immediate))

interrupts = document.getElementById("interrupts")
for code, dec in sorted(interrupt_docs.items()):
    if code & 0xf == 0:
        tr = interrupts.appendChild(document.createElement("tr"))
        cell(tr, interrupt_sections[code >> 4], tag="th", colspan=2)

    tr = interrupts.appendChild(document.createElement("tr"))
    cell(tr, "%02x" % code, "centered")
    cell(tr, dec.format(r=machine.regmoji))

instructions = document.getElementById("instructions")
subscripts = "₁₂₃₄"
for instr in machine.instructions.values():
    instr: exemoji.Instruction
    tr = instructions.appendChild(document.createElement("tr"))
    cell(tr, instr.opcode, "centered")
    cell(tr, instr.name, "unbreakable")
    iargs = []
    for i in range(3):
        if i >= len(instr.arguments):
            cell(tr, "")
        else:
            arg_name = instr.arguments[i].value + subscripts[i]
            cell(tr, arg_name)
            iargs.append(arg_name)
    cell(tr, descriptions[instr.mnemonic].format(r=machine.regmoji, a=iargs))


with open(args.outfile, "wb") as file:
    file.write(document.documentElement.toxml("utf8"))
