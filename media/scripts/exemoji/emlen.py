#!/usr/bin/env python3
import sys
from exemoji import emoji_length, emoji_to_hexstring


for arg in sys.argv[1:]:
    print(" ".join(map(str, [
        arg,
        len(arg),
        emoji_length(arg),
        emoji_to_hexstring(arg)
    ])))
