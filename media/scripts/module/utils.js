// @license magnet:?xt=urn:btih:0b31508aeb0634b347b8270c7bee4d411b5d4109&dn=agpl-3.0.txt AGPLv3+

export function randint(min, max)
{
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

export function percent_to_string(percent)
{
    return Math.round(percent * 100) + "%";
}


export function random_shuffle(arr)
{
    for ( let i = arr.length - 1; i > 0; i--)
    {
        const j = Math.floor(Math.random() * (i + 1));
        [arr[i], arr[j]] = [arr[j], arr[i]];
    }

    return arr;
}

export function random_item(arr)
{
    return arr[randint(0, arr.length - 1)];
}

/**
 * @brief Returns a weighted random of an object with {item: weight} or array of [item, weight]
 */
export function weighted_item(object)
{
    let entries = object;
    if ( !Array.isArray(object) )
        entries = Object.entries(object);

    let max = 0;
    for ( let [item, weight] of entries )
        max += weight;

    let selected = Math.random() * max;
    for ( let [item, weight] of entries )
    {
        if ( selected <= weight )
            return item;
        selected -= weight;
    }
}
