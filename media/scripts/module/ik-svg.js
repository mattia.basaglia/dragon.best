import {IKGraphics, IKAnimator} from "./ik.js"
import {average_angle, offset_point, lerp_point} from "./ik-math.js"


export class IKSvg extends IKGraphics
{

    add_to_element(element)
    {}

    update()
    {
    }

    create_element(tag)
    {
        return document.createElementNS("http://www.w3.org/2000/svg", tag);
    }

    get end()
    {
        return this.parent.end;
    }

    get start()
    {
        return this.parent.start;
    }

    get angle()
    {
        return this.parent.angle;
    }


    get length()
    {
        return this.parent.length;
    }
}

export class SkeletonViewer extends IKSvg
{
    static Boxes = 0;
    static Pointy = 1;
    static BallStick = 2;
    static Stick = 3;
    static Smooth = 4;
    static BallBoxes = 5;

    constructor(style=SkeletonViewer.Boxes, order=0)
    {
        super(order);
        this.style = style;
        this.shape = this.create_element("path");
        this.grow = 0;
    }

    add_to_element(element)
    {
        this.default_style(this.shape);
        element.appendChild(this.shape);
    }

    default_style(shape)
    {
        shape.style.fill = "green";
        shape.style.fillOpacity = 0.3
        shape.style.stroke = "lime";
        shape.style.strokeWidth = "1px";
    }

    update()
    {
        var d = "";

        switch ( this.style )
        {
            case SkeletonViewer.Boxes:
                for ( let bone of this.solver.bones )
                {
                    d += this._stick(bone, this.radius(bone.start), this.radius(bone.end));
                }
                break;

            case SkeletonViewer.BallBoxes:
                let balls = " " + this._circle(this.solver.root);
                for ( let bone of this.solver.bones )
                {
                    d += this._stick(bone, this.radius(bone.start), this.radius(bone.end));
                    balls += this._circle(bone.end) + this._stick(bone, 5, 5);
                }
                d += balls;
                break;

            case SkeletonViewer.Pointy:
                for ( let bone of this.solver.bones )
                {
                    d += "M" + points_to_svg([
                        bone.start,
                        offset_point(bone.start, this.radius(bone.start), bone.angle - Math.PI / 4),
                        bone.end,
                        offset_point(bone.start, this.radius(bone.start), bone.angle + Math.PI / 4),
                    ]) + "Z ";
                }
                break;

            case SkeletonViewer.BallStick:
                d += this._circle(this.solver.root);
                for ( let bone of this.solver.bones )
                {
                    d += this._circle(bone.end) + this._stick(bone, 5, 5);
                }
                break;

            case SkeletonViewer.Stick:
                for ( let bone of this.solver.bones )
                {
                    d += this._stick(bone, 5, 5);
                }
                break;


            case SkeletonViewer.Smooth:
                d = this.smooth_wrapper(this.solver.root);
                break;
        }

        this.shape.setAttribute("d", d);
    }

    smooth_wrapper(first_node, child_index=0)
    {
        if ( first_node.children.length == 0 )
            return this._circle(first_node);

        let d = "";

        let points_forward = [];
        let points_backward = [];

        let prev_bone = null;
        let node = first_node;
        let start_angle = node.children[child_index] ? node.children[child_index].angle : 0;
        let extra = [];

        while ( true )
        {
            let next_bone = node.children[child_index];

            if ( child_index == 0 && node.children.length > 1 )
            {
                for ( let i = 1; i < node.children.length; i++ )
                    extra.push([node, i]);
            }
            child_index = 0;

            let angle = 0;
            if ( next_bone && prev_bone )
                angle = average_angle(prev_bone.angle, next_bone.angle);
            else if ( next_bone )
                angle = next_bone.angle;
            else if ( prev_bone )
                angle = prev_bone.angle;
            points_forward.push(offset_point(node, this.radius(node), angle + Math.PI / 2));
            points_backward.unshift(offset_point(node, this.radius(node), angle - Math.PI / 2));

            if ( !next_bone )
                break;

            prev_bone = next_bone;
            node = next_bone.end;
        }

        let last_node = node;
        let end_angle = prev_bone ? prev_bone.angle : 0;

        d += "M " + point_to_svg(offset_point(first_node, this.radius(first_node), start_angle + Math.PI / 2));
        d += " C " + points_to_svg(autosmooth_points(points_forward, start_angle + Math.PI, end_angle + Math.PI));
        d += this._end_cap(last_node, end_angle, -1);
        d += " C " + points_to_svg(autosmooth_points(points_backward, end_angle, start_angle));
        d += this._end_cap(first_node, start_angle, 1);
        d += " Z";

        for ( let args of extra )
            d += " " + this.smooth_wrapper(...args);

        return d;
    }

    radius(bone)
    {
        return bone.radius + this.grow;
    }

    _end_cap(node, angle, direction)
    {
        return ` A ${this.radius(node)} ${this.radius(node)} 0 0 0 ` + point_to_svg(offset_point(node, this.radius(node), angle + direction * Math.PI / 2)) + " ";
    }

    _circle(node)
    {
        return "M " + point_to_svg(node) + ` m -${this.radius(node)} 0 a `
            + `${this.radius(node)} ${this.radius(node)} 0 0 0 ${this.radius(node) * 2} 0 `
            + `${this.radius(node)} ${this.radius(node)} 0 0 0 -${this.radius(node) * 2} 0 `
            + "Z "
    }

    _stick(bone, rstart, rend)
    {
        return "M " + points_to_svg([
            offset_point(bone.start, rstart, bone.angle + Math.PI / 2),
            offset_point(bone.end, rend, bone.angle + Math.PI / 2),
            offset_point(bone.end, rend, bone.angle - Math.PI / 2),
            offset_point(bone.start, rstart, bone.angle - Math.PI / 2),
        ]) + "Z ";
    }
}

export class SvgCircle extends IKSvg
{
    constructor(order=0, radius_override=null)
    {
        super(order);
        this.radius_override = radius_override;
        this.shape = this.create_element("circle");
        if ( this.radius_override !== null )
            this.shape.r.baseVal.value = this.radius_override;
    }

    add_to_element(element)
    {
        element.appendChild(this.shape);
    }

    update()
    {
        this.shape.cx.baseVal.value = this.parent.x;
        this.shape.cy.baseVal.value = this.parent.y;
        if ( this.radius_override === null )
            this.shape.r.baseVal.value = this.parent.radius;
    }
}

export class SvgImage extends IKSvg
{
    constructor(order=0, url=null, factor=0, offset={x: 256, y: 256}, angle_offset=0)
    {
        super(order);
        this.factor = factor;
        this.image = this.create_element("image");
        this.offset = offset;
        this.angle_offset = angle_offset;
        if ( url )
            this.set_url(url);
    }

    add_to_element(element)
    {
        element.appendChild(this.image);
        var svg = element.closest("svg");
        this.transform = svg.createSVGTransform();
        this.image.transform.baseVal.appendItem(this.transform);
    }

    set_url(url)
    {
        this.image.setAttributeNS("http://www.w3.org/1999/xlink", "href", url);
    }

    get_angle()
    {
        // if ( this.parent instanceof IKNode )
            // return this.parent.
    }

    update()
    {
        this.update_transform(lerp_point(this.start, this.end, this.factor), this.angle);
    }

    update_transform(pos, angle)
    {
        var x = pos.x - this.offset.x;
        var y = pos.y - this.offset.y;
        this.image.x.baseVal.value = x;
        this.image.y.baseVal.value = y;
        angle += this.angle_offset;
        this.transform.setRotate(angle * 180 / Math.PI, this.offset.x + x, this.offset.y + y);
    }
}


export function autosmooth_points(points, start_angle, end_angle, include_start = false)
{
    let out_points = [];
    let point_data = points.map((p, i) => {
        let dx = 0, dy = 0, next;
        if ( i < points.length - 1 )
        {
            next = points[i+1];
            dy = p.y - next.y;
            dx = p.x - next.x
        }

        return {
            point: p,
            length: Math.hypot(dx, dy) / 3,
            angle: Math.atan2(dy, dx)
        };
    });

    let prev = point_data[0];

    for ( let i = 0; i < points.length; i++ )
    {
        let p = point_data[i];
        let tan_angle;

        if ( i == 0 )
            tan_angle = start_angle;
        else if ( i ==  points.length - 1)
            tan_angle = end_angle;
        else
            tan_angle = average_angle(prev.angle, p.angle);

        if ( i > 0 )
            out_points.push(offset_point(p.point, prev.length, tan_angle));

        if ( i > 0 || include_start )
            out_points.push(p.point);

        if ( i < points.length - 1 )
            out_points.push(offset_point(p.point, -p.length, tan_angle));

        prev = p;
    }

    return out_points;

}

export class PathStrand extends IKSvg
{
    constructor(initial_direction, render_order=0)
    {
        super(render_order);
        this.initial_direction = initial_direction;
        this.path = this.create_element("path");
    }

    add_to_element(element)
    {
        element.appendChild(this.path);
    }

    update()
    {
        let p = this.parent.start;
        let points = [this.parent.start];
        let bone = this.parent;
        let start_angle = bone.angle;

        while ( true )
        {
            points.push(bone.end);

            if ( !bone.end.children.length )
                break;

            bone = bone.end.children[0];
        }

        let d = `M ${point_to_svg(p)} C ` + points_to_svg(autosmooth_points(points, start_angle, bone.angle + Math.PI));
        this.path.setAttribute("d", d);
    }

}

export function point_to_svg(p)
{
    return p.x.toFixed(2) + " " + p.y.toFixed(2);
}

export function points_to_svg(points)
{
    return points.map(point_to_svg).reduce((a, b) => a + " " + b);
}

/**
 * \brief Calculates x and y from an event client position to SVG coordinates
 * \param x X in client coordinates
 * \param y Y in client coordinates
 * \param svg SVG root element
 * \param element Elements within the SVG to get coordinates relative to
 */
export function client_to_svg_point(x, y, svg, element)
{
    var pt = svg.createSVGPoint();
    pt.x = x;
    pt.y = y;
    var global_pt = pt.matrixTransform(svg.getScreenCTM().inverse());
    var matrix = svg.getScreenCTM().inverse().multiply(element.getScreenCTM()).inverse();
    return global_pt.matrixTransform(matrix);
}

export class MouseAnimator extends IKAnimator
{
    constructor(cursor_target, svg, element, solver)
    {
        super(solver);
        this.cursor_target = cursor_target;
        this.svg = svg;
        this.follow_mouse = true;
        this.idle = true;
        this.element = element;

        window.addEventListener("mousemove", this.mouse_move.bind(this));
        window.addEventListener("touchmove", this.touch_move.bind(this));
        window.addEventListener("touchend", this.touch_end.bind(this));
        window.addEventListener("touchcancel", this.touch_end.bind(this));
        window.addEventListener("mouseout", e => {this.idle = true});
    }

    move_helper(pos, element)
    {
        if ( this.follow_mouse )
        {
            this.idle = false;
            let p = client_to_svg_point(pos.clientX, pos.clientY, this.svg, this.element);
            this.cursor_target.x = p.x;
            this.cursor_target.y = p.y;
        }
    }

    mouse_move(event)
    {
        this.move_helper(event);
        event.stopPropagation();
    }

    touch_move(event)
    {
        // let box = element.getClientRects()[0];
        let touch = event.touches[0];
        this.move_helper(touch);
        event.stopPropagation();
        event.preventDefault();
    }

    touch_end(event)
    {
        if ( event.touches.length == 0 )
            this.idle = true;
    }

}
