// @license magnet:?xt=urn:btih:0b31508aeb0634b347b8270c7bee4d411b5d4109&dn=agpl-3.0.txt AGPLv3+

import {RandomColorRange, RandomColorFixed, RandomColorHSL} from "./color.js";
import {randint, random_item, random_shuffle} from "./utils.js";

export const ElementFlag = Object.freeze({
    Neutral:    0x00,
    Fire:       0x01,
    Earth:      0x02,
    Water:      0x04,
});

const f = ElementFlag.Fire;
const e = ElementFlag.Earth;
const w = ElementFlag.Water;

const RandomFeatures = {
    show_back: {
        none: f|e|w,
        fin: w,
        spikes: f|w,
        mane: e
    },
    show_cheek: {
        none: f|e|w,
        sideburns: e,
        spikes: f|e|w,
        horn: f,
        fluff: e
    },
    show_ear: {
        furry: e|f,
        hole: w,
        frill: w,
        "frilly-ear": w|f,
        round: e|w,
        triangular: f|e
    },
    show_horn: {
        straight: f|e|w,
        none: e|w,
        curled: e|f,
        antler: e,
        wavy: f|e|w
    },
    show_nose: {
        derg: f|e|w,
        dog: e,
        cow: e,
        sheep: f|e
    },
    "show_tongue-shape": {
        normal: f|e|w,
        forked: f|w
    },
    show_pattern: {
        none: f|e|w,
        stripes: w|f|e,
        "back-stripe": f|e|w,
        dots: f|e|w,
        patches: f|e|w
    },
    show_whiskers: {
        none: f|e|w,
        snoodles: e|w,
        dots: e
    }
};


export function random_element()
{
    return new DragonElement(random_item(Object.values(ElementFlag)));
}

export class DragonElement
{
    static Rainbow = 0xff;
    static _syllables = null;

    constructor(flags)
    {
        this.flags = flags;
    }

    to_flag_array(split_neutral)
    {
        return DragonElement.flag_to_array(this.flags, split_neutral);
    }

    static flag_to_array(element, split_neutral)
    {
        if ( element == ElementFlag.Neutral )
        {
            if ( split_neutral )
                return [ElementFlag.Fire, ElementFlag.Earth, ElementFlag.Water];
            return [ElementFlag.Neutral];
        }

        return Object.values(ElementFlag).filter(e => e & element);
    }

    color_randomizer(light_min, light_max)
    {
        var h;
        var s;
        var l = new RandomColorRange(light_min, light_max);

        if ( this.flags == ElementFlag.Neutral )
        {
            s = new RandomColorFixed(0);
            h = new RandomColorFixed(0);
        }
        else
        {
            var flag = random_item(this.to_flag_array(false));
            s = new RandomColorRange(33, 100);

            switch ( flag )
            {
                case ElementFlag.Water:
                    h = new RandomColorRange(170, 280);
                    break;
                case ElementFlag.Fire:
                    l = new RandomColorFixed(l.get_value());
                    h = new RandomColorRange(0, 55);
                    if ( l.value < 60 )
                        s = new RandomColorRange(75, 100);
                    break;
                case ElementFlag.Earth:
                    if ( randint(0, 1) == 0 )
                    {
                        h = new RandomColorRange(20, 60);
                        s = new RandomColorRange(0, 40);
                    }
                    else
                    {
                        h = new RandomColorRange(60, 140);
                    }
                    break;
            }
        }

        return new RandomColorHSL(h, s, l);
    }

    random_eye_color()
    {
        return new RandomColorHSL(
            new RandomColorRange(0, 360),
            new RandomColorRange(80, 100),
            new RandomColorRange(35, 65),
        ).get_color();
    }

    random_colors()
    {
        var colors = [];
        colors.push(this.color_randomizer(10, 40).get_color());
        colors.push(this.color_randomizer(35, 65).get_color());
        colors.push(this.color_randomizer(80, 100).get_color());
        return colors;
    }

    random_feature(values)
    {
        let choices = [];
        for ( let [value, flags] of Object.entries(values) )
        {
            if ( (flags & this.flags) == this.flags )
                choices.push(value);
        }
        return random_item(choices);
    }

    random_features()
    {
        let state = {};
        state["enable_nose-horn"] = Math.random() > 0.75;
        for ( let [name, values] of Object.entries(RandomFeatures) )
            state[name] = this.random_feature(values);
        return state;
    }

    full_random()
    {
        let colors = this.random_colors();
        let eye_color = this.random_eye_color();
        let horn_color = colors[0];
        random_shuffle(colors);
        let outline = colors[0].lightness_mod(-0.4);

        let state = this.random_features();

        state.base = colors[0].to_hex();
        state.eye = eye_color.to_hex();
        state["enable_lower-jaw"] = Math.random() < 0.66;
        state.belly = state["lower-jaw"] = state["inner-ear"] = colors[1].to_hex();
        state.ear = state.fin = random_item([colors[0], colors[2]]).to_hex();
        state.back = state.pattern = state.sideburns = state.beans = colors[2].to_hex();
        state.whiskers = state.outline = outline.to_hex();
        state.horn = state["nose-horn"] = horn_color.to_hex();
        for ( let pattern of ["top", "bottom", "ring"] )
        {
            let pat_name = "muzzle-" + pattern;
            state[pat_name] = random_item(colors).to_hex();
            state["enable_" + pat_name] = Math.random() > 0.6;
        }

        state.head = state.neck = colors[2];
        state.enable_head = Math.random() < 0.2;
        state.enable_neck = !state.enable_head && Math.random() < 0.2;


        if ( Math.random() > 0.4 )
            state.show_whiskers = "none";

        return {
            name: this.random_name(),
            colors: colors,
            eye_color: eye_color,
            state: state,
            horn_color: horn_color
        };
    }

    toJson()
    {
        return this.flags;
    }

    random_name()
    {
        return this.build_random_name(randint(2, 3), 8);
    }

    build_random_name(syllables, no_more = -1)
    {
        if ( DragonElement._syllables === null )
            DragonElement._syllables = DragonElement._init_syllables();

        var split = this.to_flag_array(false);
        var generator = DragonElement._syllables.neutral;
        if ( split.length == 1 )
        {
            generator = DragonElement._syllables[split[0]];
        }
        else
        {
            generator = new SyllableNode();
            generator.children = split.map(x => DragonElement._syllables[split]);
        }

        return generator.generate(syllables, no_more);
    }

    static _init_syllables()
    {
        var neutral = new SyllableNode();

        var water = neutral.add("");
        //water.add("b");
        water.add("bfg").add("l").add("A");
        water.add("l").add("A");
        water.add("xs").add("A").add("lbs");
        water.add("dt").add("r").add("A");
        water.add("kn").add("A").add("sn");
        water.add("p").add("s").add("A");

        var fire = neutral.add("");
        fire.add("bdvt").add("A").add("r");
        fire.add("n").add("AAy").add("xr");
        fire.add("x").add("Ay").add("r");
        fire.add("Ay").add("x");
        fire.add("Ay").add("r");
        fire.add("r").add("Ay").add("n");

        var earth = neutral.add("");
        earth.add("dtnvz").add("AA");
        earth.add("dtnvz").add("AA").add("rn");
        earth.add("dts").add("h").add("A");
        earth.add("dts").add("h").add("A").add("rm");
        earth.add("bd").add("y").add("A");
        earth.add("mr").add("A").add("nn");

        return {
            [ElementFlag.Neutral]: neutral,
            [ElementFlag.Water]: water,
            [ElementFlag.Fire]: fire,
            [ElementFlag.Earth]: earth,
        };
    }
}

class SyllableNode
{
    constructor(letters = "")
    {
        this.letters = letters;
        this.children = [];
    }

    _vowels(code)
    {
        var base = ["a", "e", "i", "o", "u", "oo", "ee"];
        if ( code.indexOf("y") != -1 )
            base.push("y");
        if ( code.indexOf("AA") != -1 )
            base = base.concat(["ie", "ae", "oe", "eo", "aa", "ei"]);
        return base;
    }

    walk()
    {
        var text = "";

        if ( this.letters != "" )
        {
            if  ( this.letters[0] == "A" )
                text = random_item(this._vowels(this.letters));
            else
                text = random_item(this.letters);
        }

        if ( this.children.length )
        {
            var child = randint(0, this.children.length -1);
            text += this.children[child].walk();
        }

        return text;
    }

    add(letters)
    {
        var child;
        if ( letters instanceof SyllableNode )
            child = letters;
        else
            child = new SyllableNode(letters);
        this.children.push(child);
        return child;
    }

    generate(syllables, no_more)
    {
        var text = "";
        for ( var i = 0; i < syllables && (no_more == -1 || text.length < no_more); i++ )
            text += this.walk();
        return text;
    }
}


