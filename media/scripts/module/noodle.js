import {
    IKSvg, SvgImage, SvgCircle, PathStrand, point_to_svg, points_to_svg,
    client_to_svg_point, MouseAnimator
} from "./ik-svg.js"
import {
    IKSolver, IKTarget, IKAnimator, MaxAngleDifferenceConstraint, AngleOffsetConstraint
} from "./ik.js"
import {
    lerp, average_angle, offset_point, lerp_point, point_distance,
    polar_difference, angle_difference, offset_point_cartesian,
} from "./ik-math.js"

class NoodleGraphics extends IKSvg
{
    constructor(config, order)
    {
        super(order);
        this.config = config
    }
}


class NoodleSegment extends NoodleGraphics
{
    add_to_element(element)
    {
        // this.ahead = this.start.parent;
        // this.behind = this.end.children[0];

        this.n_scales = Math.ceil(this.end.radius / this.config.scale_size);

        this.mane = this.create_element("path");
        this.mane.classList.add("crest");
        element.appendChild(this.mane);

        this.under_element = this.mane;

        this.body = this.create_element("path");
        this.body.classList.add("body");
        element.appendChild(this.body);

        this.tail = this.n_scales == 1;

        if ( this.n_scales > 1 || Math.ceil(this.end.radius / this.config.scale_size) > 1 )
        {
            this.belly = this.create_element("path");
            this.belly.classList.add("belly");
            element.appendChild(this.belly);
            this.belly_taper = this.n_scales == 1;

            if ( this.n_scales > 1 )
                this.n_scales -= 1;
        }

        this.update_render_data();
    }

    back_arc(start, end)
    {
        let radius = point_distance(end, start) / 2;
        let center = lerp_point(start, end, 0.5);
        let mid = offset_point(center, -radius, this.angle);
        let tan = 0.5519 * radius;
        return " " + points_to_svg([
            offset_point(start, -tan, this.angle),
            offset_point(mid, tan, this.angle + Math.PI / 2),
            mid,
            offset_point(mid, tan, this.angle - Math.PI / 2),
            offset_point(end, -tan, this.angle),
            end
        ]);
    }

    update_render_data()
    {
        let start_angle = this.angle;
        let end_angle = this.angle;

        if ( this.behind )
            start_angle = average_angle(this.angle, this.behind.angle);

        if ( this.ahead )
            end_angle = average_angle(this.angle, this.ahead.angle);

        let p0 = offset_point(this.start, this.start.radius, start_angle-Math.PI/2);
        let p2 = offset_point(this.end, this.end.radius, end_angle-Math.PI/2);
        let p3 = offset_point(this.end, this.end.radius, end_angle + Math.PI/2);
        let p4 = offset_point(this.start, this.start.radius, start_angle + Math.PI/2);
        let belly_start = lerp_point(p4, p0, 1 / 3);
        let p34l = point_distance(p3, p4);
        let p02l = point_distance(p0, p2);

        this.render_data = {
            start_angle, end_angle,
            p0, p2, p3, p4, belly_start,
            p02l, p34l,
        };
    }

    update()
    {
        this.update_render_data();
        this.update_path();
    }

    update_path()
    {
        let {
            start_angle, end_angle,
            p0, p2, p3, p4, belly_start,
            p02l, p34l
        } = this.render_data;

        let r = Math.max(16, (this.start.radius + this.end.radius) * 0.4);
        let center = lerp_point(p0, p2, 0.2);
        let p1 = offset_point(center, r, this.angle - Math.PI / 2);

        // Crest
        let mane_d = "M ";
        let p0under = offset_point(p0, 4, this.angle + Math.PI / 2);
        let p2under = offset_point(p2, 4, this.angle + Math.PI / 2);
        let p0mane = offset_point(p0, 4, this.angle - Math.PI / 2);
        let p2mane = offset_point(p2, 4, this.angle - Math.PI / 2);
        mane_d += points_to_svg([p0under, p0mane]);
        mane_d += " C ";
        mane_d += points_to_svg([
            offset_point(p0mane, this.length * 0.4, this.angle - Math.PI / 4),
            offset_point(p1, this.length * 0.4, this.angle + Math.PI * 0.75),
            p1,
            offset_point(p1, this.length * 0.2, this.angle + Math.PI / 2),
            offset_point(p2mane, -this.length * 0.5, this.angle + Math.PI / 2),
            p2mane,
        ]);
        mane_d += " L " + point_to_svg(p2under) + " Z";

        this.mane.setAttribute("d", mane_d);

        // Belly
        if ( this.belly )
        {
            let belly_end;
            if ( this.ahead && this.ahead.n_scales )
                belly_end = this.ahead.render_data.belly_start;
            else
                belly_end = lerp_point(p3, p2, 1 / 3);


            let belly_d = "M " + point_to_svg(p3) + " C " + points_to_svg([
                offset_point(p3, -p34l / 3, end_angle),
                offset_point(p4, p34l / 3, start_angle),
                p4
            ]);
            if ( this.belly_taper )
            {
                belly_d += " L " + points_to_svg([belly_end]);
            }
            else
            {
                belly_d += " C " + points_to_svg([
                    offset_point(p4, -this.start.radius / 3, start_angle + Math.PI / 4),
                    offset_point(belly_start, -this.start.radius / 4, start_angle),
                    belly_start,
                    offset_point(belly_start, this.length / 3, start_angle),
                    offset_point(belly_end, -this.length / 3, end_angle),
                    belly_end
                ]);
            }
            this.belly.setAttribute("d", belly_d);
        }

        // Body
        let scale_origin = this.tail ? p4 : belly_start;
        let scale_start = scale_origin;

        let d = "M " + point_to_svg(p0) + " C ";
        d += points_to_svg([
            offset_point(p0, p02l / 3, start_angle),
            offset_point(p2, -p02l / 3, end_angle),
            p2
        ]);

        d += "L " + points_to_svg([p3, scale_start]) + " C ";

        // Scales
        d += " ";

        for ( let i = 0; i < this.n_scales; i++ )
        {
            let scale_end = lerp_point(scale_origin, p0, (i + 1) / this.n_scales);
            d += this.back_arc(scale_start, scale_end);
            scale_start = scale_end;
        }

        this.body.setAttribute("d", d);
    }

}


function smooth_target(position, target, current_angle, force_smooth_angle, options)
{
    var pd = polar_difference(position, target);

    if ( pd.length <= options.speed )
        return target;

    if ( force_smooth_angle || pd.length > options.max_angle_distance )
    {
        var angle_delta = angle_difference(pd.angle, current_angle);
        if ( Math.abs(angle_delta) > options.max_angle )
        {
            if ( angle_delta < 0 )
                pd.angle = current_angle + options.max_angle;
            else
                pd.angle = current_angle - options.max_angle;
        }
    }

    return offset_point(position, options.speed, pd.angle);
}

export class NoodleAnimator extends MouseAnimator
{
    constructor(noodle, svg, holding_pattern)
    {
        super(noodle.cursor_target, svg, null, noodle.solver);
        this.can_idle = true;
        this.holding_pattern = holding_pattern;
        this.renoodle(noodle)
    }

    renoodle(noodle)
    {
        this.solver = noodle.solver;
        this.noodle = noodle;
        this.target = this.noodle.target;
        this.cursor_target = this.noodle.cursor_target;
        this.element = this.noodle.config.svg_parent;
    }

    on_frame(t)
    {
        if ( this.idle && this.can_idle )
            this.idle_animation(t);

        let p = smooth_target(this.target, this.cursor_target, this.noodle.head_bone.angle, this.idle, this.noodle.config.animation);
        this.target.x = p.x;
        this.target.y = p.y;
    }

    idle_animation(t)
    {
        let center = client_to_svg_point(this.svg.clientWidth / 2, this.svg.clientHeight / 2, this.svg, this.element);
        let a = t / 1000;
        let p = this.holding_pattern(a, center.x * 0.8, center.y * 0.8);

        let tp = offset_point_cartesian(p, center);
        this.cursor_target.x = tp.x;
        this.cursor_target.y = tp.y;
    }
}


export function figure_8(t, width, height)
{
    let radius = height;
    let scale = 2 / (3 - Math.cos(2 * t));
    return {
        x: scale * Math.sin(2 * t) * radius / 2,
        y: scale * Math.cos(t) * radius,
    };
}

export class Noodle
{
    constructor(config)
    {
        this.config = config;

        this.max_angle = new MaxAngleDifferenceConstraint(this.config.max_angle);
        this.solver = new IKSolver({
            root: {x: this.config.start.x, y: this.config.start.y, radius: this.config.tail.tip},
        });

        // Body
        let node = this.solver.root;
        let length, radius;
        let x = this.config.start.x;
        let prev_segment = null;
        let bone;
        for ( let i = 1; i <= this.config.segments; i++ )
        {
            let f = i / this.config.segments;
            if ( f < this.config.tail.percent )
            {
                length = this.config.tail.segment_length;
                radius = lerp(this.config.tail.radius, this.config.tail.tip, 1 - f / this.config.tail.percent);
            }
            else
            {
                length = this.config.segment_length;
                radius = this.noodle_size_lerp(this.config.belly.radius, this.config.tail.radius, f)
            }

            x += length;

            bone = this.solver.add_bone(node, {x: x, y: this.config.start.y, radius: radius});
            node = bone.end;
            bone.constraints.push(this.max_angle);

            if ( i == 1 )
            {
                this.solver.attach_graphics(new SvgImage(i, config.image_path + "tail.svg", 1), bone);
            }
            else
            {
                let next_segment = new NoodleSegment(this.config, i);
                this.solver.attach_graphics(next_segment, bone);

                if ( prev_segment )
                {
                    next_segment.behind = prev_segment;
                    prev_segment.ahead = next_segment;
                }
                prev_segment = next_segment;
            }
        }

        let segment_render_order = this.config.segments + 1;
        let render_order =  segment_render_order + 1;

        // Head
        bone = this.solver.add_bone(node, {x: x + 90, y: this.config.start.y, radius: this.config.tail.radius});
        node = bone.end;
        bone.constraints.push(this.max_angle);
        this.head_bone = bone;
        this.solver.attach_graphics(new SvgImage(segment_render_order - 3.1, config.image_path + "head-b.svg"), this.head_bone);

        this.target = this.solver.add_target(bone.end);

        // Orb
        this.cursor_target = new IKTarget(this.config.start.x, this.config.start.y, null);
        this.orb = this.solver.attach_graphics(new SvgCircle(render_order++, 30), this.cursor_target);
        this.orb.shape.classList.add("orb");

        this.solver.attach_graphics(new SvgImage(render_order++, config.image_path + "head-f.svg"), this.head_bone);

        // Snoodles
        this.snoodle_max_angle = new MaxAngleDifferenceConstraint(this.config.snoodle.max_angle);
        this.snoodle_start_max_angle = new MaxAngleDifferenceConstraint(this.config.snoodle.max_angle / 4);
        this.make_snoodle(this.head_bone, {x: 56, y: -26}, [
            [this.config.max_angle / 2, Math.PI * 0.86],
            [this.config.max_angle / 2, 0],
            [this.config.max_angle, 0],
        ], render_order++);

        this.make_snoodle(this.head_bone, {x: 0, y: 0}, [], -1);

        // Limbs
        this.make_limbs(this.config.legs_percent);
        this.make_limbs(this.config.arms_percent);

        // Finalize
        for ( let graphic of this.solver.graphics )
            graphic.add_to_element(this.config.svg_parent);
    }

    make_limbs(percent)
    {
        let leg_bi = this.segment_index_from_percentage(percent);
        let leg = new SvgImage(leg_bi - 3.2, this.config.image_path + "arm.svg", 0.5);
        this.solver.attach_graphics(leg, this.solver.bones[leg_bi]);
        let leg_fi = leg_bi + this.config.front_limb_offset;
        leg = new SvgImage(leg_fi + 1.2, this.config.image_path + "arm.svg", 0.5);
        leg.angle_offset = -Math.PI / 8;
        this.solver.attach_graphics(leg, this.solver.bones[leg_fi]);
    }

    segment_index_from_percentage(pc)
    {
        return Math.floor(pc * this.config.segments);
    }

    make_snoodle(head_bone, offset, extra, order)
    {
        let p = {x: head_bone.start.x + offset.x, y: head_bone.start.y + offset.y};
        let snoodle_start = this.solver.add_bone(head_bone.start, p);
        let node = snoodle_start.end;
        AngleOffsetConstraint.fix_angle(snoodle_start, false, true);

        let snoodle_seg_len = this.config.snoodle.length / this.config.snoodle.segments;

        for ( let i = 0; i < this.config.snoodle.segments + extra.length; i++ )
        {
            p.x -= snoodle_seg_len;
            let bone = this.solver.add_bone(node, p);
            if ( i < extra.length )
                bone.constraints.push(new MaxAngleDifferenceConstraint(...extra[i]));
            else if ( i != 0 )
                bone.constraints.push(this.snoodle_max_angle);
            node = bone.end;
        }

        let graphic = new PathStrand(0, order);
        graphic.path.classList.add("snoodle");
        this.solver.attach_graphics(graphic, snoodle_start.end.children[0]);
    }

    noodle_size_lerp(a, b, f)
    {
        f = (f - this.config.tail.percent) / (1 - this.config.tail.percent);
        return lerp(a, b, 3.7*f*f-3.85*f+1);
    }

}
