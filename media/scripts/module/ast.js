import {weighted_item, randint, random_item, random_shuffle} from "./utils.js";

export class AstNode
{

}

export class BinOp extends AstNode
{
    constructor(a, op, b)
    {
        super();
        this.a = a;
        this.op = op;
        this.b = b;
    }
}

export class UnOp extends AstNode
{
    constructor(a, op, suffix=false)
    {
        super();
        this.a = a;
        this.op = op;
        this.suffix = suffix;
    }
}

export class ExprParen extends AstNode
{
    constructor(a)
    {
        super();
        this.a = a;
    }
}

export class ExpressionStatement extends AstNode
{
    constructor(expr)
    {
        super();
        this.expr = expr;
    }
}

export class StatementBlock extends AstNode
{
    constructor(statements=[])
    {
        super();
        this.statements = statements;
    }
}

export class Program extends StatementBlock
{

}

export class VariableRef extends AstNode
{
    constructor(name)
    {
        super();
        this.name = name;
    }
}

export class Literal extends AstNode
{
    constructor(value)
    {
        super();
        this.value = value;
    }
}

export class VariableDeclaration extends AstNode
{
    constructor(name, type, value=null)
    {
        super();
        this.name = name;
        this.type = type;
        this.value = value;
    }
}

export class CompoundStatement extends AstNode {};

export class If extends CompoundStatement
{
    constructor(condition, ...tail)
    {
        super();
        this.condition = condition;
        this.tail = tail;
    }
}

export class For extends CompoundStatement
{
    constructor(iterator, range, body)
    {
        super();
        this.iterator = iterator;
        this.range = range;
        this.body = body;
    }
}

export class FuncCall extends AstNode
{
    constructor(func, ...args)
    {
        super();
        this.func = func;
        this.args = args;
    }
}

export class ArrayLiteral extends AstNode
{
    constructor(elements)
    {
        super();
        this.elements = elements;
    }
}

export class ArraySubscript extends AstNode
{
    constructor(array, index)
    {
        super();
        this.array = array;
        this.index = index;
    }
}

export class LineComment extends AstNode
{
    constructor(text)
    {
        super();
        this.text = text;
    }
}

export class Print extends AstNode
{
    constructor(text)
    {
        super();
        this.text = text;
    }
}

export class Formatter
{
    constructor(name, title)
    {
        this.name = name;
        this.title = title;
        this.indent_step = " ".repeat(4);
        this.wrap_margin = 80;
    }

    format(node)
    {
        this.output = "";
        this.line_length = 0;
        this.nest_level = 0;

        this.on_node(node);

        return this.output;
    }

    append(str)
    {
        this.output += str;
        this.line_length += str.length;
    }

    indent()
    {
        if ( this.output.length && !this.output.endsWith("\n") )
            this.output += "\n";
        this.output += this.indent_step.repeat(this.nest_level);
        this.line_length = this.indent_step.length * this.nest_level;
    }

    on_node(node)
    {
        if ( node instanceof BinOp )
            this.on_binop(node);
        else if ( node instanceof UnOp )
            this.on_unop(node);
        else if ( node instanceof ExpressionStatement )
            this.on_expr(node);
        else if ( node instanceof Program )
            this.on_program(node);
        else if ( node instanceof StatementBlock )
            this.on_block(node);
        else if ( node instanceof VariableRef )
            this.on_variable(node);
        else if ( node instanceof Literal )
            this.on_literal(node);
        else if ( node instanceof VariableDeclaration )
            this.on_declaration(node);
        else if ( node instanceof If )
            this.on_if(node);
        else if ( node instanceof FuncCall )
            this.on_func_call(node);
        else if ( node instanceof ExprParen )
            this.on_expr_paren(node);
        else if ( node instanceof ArrayLiteral )
            this.on_array(node);
        else if ( node instanceof ArraySubscript )
            this.on_array_subscript(node);
        else if ( node instanceof LineComment )
            this.on_comment(node);
        else if ( node instanceof For )
            this.on_for(node);
        else if ( node instanceof Print )
            this.on_print(node);
        else
            throw new Error(`Unkown AST node ${node.constructor.name}`);

    }

    convert_operand(op, nargs)
    {
        return op;
    }

    on_binop(node)
    {
        this.on_node(node.a);
        this.append(" ");
        this.append(this.convert_operand(node.op, 2));
        this.append(" ");
        this.on_node(node.b);
    }

    on_unop(node)
    {
        if ( node.suffix )
            this.on_node(node.a);

        this.append(this.convert_operand(node.op, 1));

        if ( !node.suffix )
            this.on_node(node.a);
    }

    on_expr(node)
    {
        this.indent();
        this.on_node(node.expr);
    }

    on_program(node)
    {
        for ( let stmt of node.statements )
            this.on_node(stmt);
    }

    on_block(node)
    {
        this.on_program(node);
    }

    on_variable(node)
    {
        this.append(node.name);
    }

    on_literal(node)
    {
        this.append(JSON.stringify(node.value));
    }

    on_arg_list(args)
    {
        for ( let i = 0; i < args.length; i++ )
        {
            if ( this.line_length > this.wrap_margin )
            {
                this.indent();
                this.append(this.indent_step);
            }
            this.on_node(args[i]);
            if ( i != args.length - 1 )
                this.append(", ");
        }
    }

    on_func_call(node)
    {
        this.on_node(node.func);
        this.append("(");
        this.on_arg_list(node.args)
        this.append(")");
    }

    on_expr_paren(node)
    {
        this.append("(");
        this.on_node(node.a);
        this.append(")");
    }

    on_array(node)
    {
        this.append("[");
        this.on_arg_list(node.elements);
        this.append("]");
    }

    on_array_subscript(node)
    {
        this.on_node(node.array);
        this.append("[");
        this.on_node(node.index);
        this.append("]");
    }

    on_comment(node)
    {
        if ( node.text == null )
        {
            if ( !this.output.endsWith("\n") )
                this.output += "\n";
            this.output += "\n";
            return;
        }

        this.indent();
        this.on_comment_begin();
        this.append(node.text);
        this.on_comment_end();
    }

    on_comment_begin()
    {
        this.append("// ");
    }

    on_comment_end()
    {
    }
}

export class CLike extends Formatter
{
    braced_block(nodes, brace)
    {
        if ( brace )
        {
            this.indent();
            this.append("{");
        }

        this.nest_level += 1;

        for ( let stmt of nodes )
            this.on_node(stmt);

        this.nest_level -= 1;

        if ( brace )
        {
            this.indent();
            this.append("}");
        }
    }

    braced_node(node, brace)
    {
        if ( node instanceof StatementBlock )
            this.braced_block(node.statements, brace);
        else
            this.braced_block([node], brace);
    }

    should_brace(node)
    {
        return (node instanceof StatementBlock && (node.statements.length != 1 || this.should_brace(node.statements[0]))) ||
            node instanceof CompoundStatement;
    }

    on_block(node)
    {
        this.braced_block(node.statements, node.statements.length != 1 );
    }

    declare_var(node)
    {
        this.on_variable(node);
    }

    on_declaration(node)
    {
        this.indent();
        this.declare_var(node);
        if ( node.value )
        {
            this.append(" = ");
            this.on_node(node.value);
        }
        this.append(";");
    }

    on_expr(node)
    {
        super.on_expr(node);
        this.append(";");
    }

    on_if(node)
    {
        this.indent();
        this.append("if ( ");
        this.on_node(node.condition);
        this.append(" )");

        var should_brace = false;
        for ( let i = 0; i < node.tail.length; i += 2 )
        {
            if ( this.should_brace(node.tail[i]) )
            {
                should_brace = true;
                break;
            }
        }

        for ( let i = 0; i < node.tail.length; i++ )
        {
            if ( i % 2 == 0 )
            {
                this.braced_node(node.tail[i], should_brace);
            }
            else if ( i == node.tail.length - 1 )
            {
                this.indent();
                this.append("else");
                this.braced_node(node.tail[i], should_brace);
            }
            else
            {
                this.indent();
                this.append("else if ( ");
                this.on_node(node.tail[i]);
                this.append(" )");
            }
        }
    }

    on_for_header(node)
    {
        this.append("for ( ");
        this.declare_var(node.iterator);
        this.on_for_in();
        this.on_node(node.range);
        this.append(" )");
    }

    on_for_in()
    {
    }

    on_for(node)
    {
        this.indent();
        this.on_for_header(node);
        this.braced_node(node.body, this.should_brace(node.body));
    }
}

export class JsFormatter extends CLike
{
    constructor()
    {
        super("js", "JavaScript");
    }

    declare_var(node)
    {
        this.append(`let ${node.name}`);
    }

    on_for_in()
    {
        this.append(" of ");
    }

    on_print(node)
    {
        this.indent();
        this.append("console.log(");
        this.on_node(node.text);
        this.append(");");
    }
}


export class TsFormatter extends CLike
{
    constructor()
    {
        super("ts", "TypeScript");
    }

    declare_var(node)
    {
        let type = node.type;
        if ( type == "array" )
            type = "number[]";
        this.append(`let ${node.name}: ${type}`);
    }

    on_for_in()
    {
        this.append(" of ");
    }

    on_print(node)
    {
        this.indent();
        this.append("console.log(");
        this.on_node(node.text);
        this.append(");");
    }
}

export class CxxFormatter extends CLike
{
    constructor()
    {
        super("c++", "C++");
    }

    convert_type(type)
    {
        switch ( type )
        {
            case "number":
                return "int";
            case "string":
                return "std::string";
            case "array":
                return "std::vector<int>";
        }
    }

    declare_var(node)
    {
        this.append(`${this.convert_type(node.type)} ${node.name}`);
    }

    on_for_in()
    {
        this.append(" : ");
    }

    on_print(node)
    {
        this.indent();
        this.append("std::cout << ");
        this.on_node(node.text);
        this.append(" << std::endl;");
    }
}



export class PhpFormatter extends CLike
{
    constructor()
    {
        super("php", "PHP");
    }

    declare_var(node)
    {
        if ( node.value )
        {
            this.indent();
            this.on_variable(node);
            this.append(" = ");
            this.on_node(node.value);
        }
    }

    on_for_header(node)
    {
        this.append("foreach ( ");
        this.on_node(node.range);
        this.append(" as ");
        this.on_variable(node.iterator);
        this.append(" )");
    }

    on_variable(node)
    {
        this.append("$" + node.name);
    }


    on_func_call(node)
    {
        if ( node.func instanceof VariableRef )
            this.append(node.func.name);
        else
            this.on_node(node.func);
        this.append("(");
        this.on_arg_list(node.args)
        this.append(")");
    }


    on_print(node)
    {
        this.indent();
        this.append("echo ");
        this.on_node(node.text);
        this.append(" . \"\\n\";");
    }

}

export class PythonFormatter extends Formatter
{
    constructor()
    {
        super("py", "Python");
    }

    on_declaration(node)
    {
        if ( node.value )
        {
            this.indent();
            this.append(node.name);
            this.append(" = ");
            this.on_node(node.value);
        }
    }

    indented_node(node)
    {
        this.nest_level += 1
        this.on_node(node);
        this.nest_level -= 1

    }

    on_if(node)
    {
        this.indent();
        this.append("if ");
        this.on_node(node.condition);
        this.append(":");

        for ( let i = 0; i < node.tail.length; i++ )
        {
            if ( i % 2 == 0 )
            {
                this.indented_node(node.tail[i]);
            }
            else if ( i == node.tail.length - 1 )
            {
                this.indent();
                this.append("else:");
                this.indented_node(node.tail[i]);
            }
            else
            {
                this.indent();
                this.append("elif ");
                this.on_node(node.tail[i]);
                this.append(":");
            }
        }
    }

    on_for(node)
    {
        this.indent();
        this.append(`for ${node.iterator.name} in `);
        this.on_node(node.range);
        this.append(":");
        this.indented_node(node.body);
    }

    on_comment_begin()
    {
        this.append("# ");
    }

    convert_operand(op, nargs)
    {
        switch ( op )
        {
            case "!": return "not";
            case "||": return "or";
            case "&&": return "and";
        }
        return op;
    }

    on_print(node)
    {
        this.indent();
        this.append("print(");
        this.on_node(node.text);
        this.append(");");
    }
}

export class CodeGenerator
{
    constructor()
    {
        this.min_statements = 5;
        this.max_statements = 10;
        this.var_names = [];
    }

    generate()
    {
        let program = new Program();
        this.statements = 0;
        this.scope = {};
        this.stack = [[]];

        let decls = randint(2, 5);
        for ( let i = 0; i < decls; i++ )
            program.statements.push(this.new_var());
        program.statements.push(new LineComment(null));

        this.process_block(program, this.min_statements, this.max_statements);
        return program;
    }

    process_block(block, min, max)
    {
        let n = Math.max(1, randint(min, max));

        for ( let i = 0; i < n; n++ )
        {
            this.add_statement(block, i < n - 2);

            if ( this.statements >= this.max_statements )
                break;
        }

    }

    make_block(min, max)
    {
        let block = new StatementBlock();
        this.process_block(block, min, max);
        return block;
    }

    add_statement(block, allow_decl)
    {
        let func_name = weighted_item({
            "new_var": allow_decl ? 2 : 0,
            "stmt_for": 1,
            "stmt_if": 1,
            "stmt_ass": 4,
            "stmt_func": 4,
            "stmt_print": 1,
        });
        let func = this[func_name].bind(this);
        let stmt = func();
        this.statements += 1;
        if ( stmt instanceof CompoundStatement && block.statements.length > 1 )
            block.statements.push(new LineComment(null));
        block.statements.push(stmt);
    }

    avail_names()
    {
        return [
            "derg", "dragon", "rawr", "rerr", "kobold", "egg", "hoard",
            ...this.var_names
        ];
    }

    compound_name(names, count)
    {
        random_shuffle(names);
        return names.slice(0, count).join("_")
    }

    new_var_name()
    {
        let names = this.avail_names();

        let names_copy = [...names];

        while ( names.length )
        {
            let name = names.splice(randint(0, names.length-1), 1)[0];
            if ( !(name in this.scope) )
                return name;
        }

        for ( let i = 0; i < 100; i++ )
        {
            let name = this.compound_name(names_copy, 2);
            if ( !(name in this.scope) )
                return name;
        }

        return "foobar";
    }

    get_type()
    {
        return weighted_item({"number": 70, "string": 25, "array": 5});
    }

    declare_var(name, type)
    {
        this.scope[name] = type;
        this.stack[0].push(name);
    }

    new_var(type, allow_empty)
    {
        let name = this.new_var_name();
        if ( !type )
            type = this.get_type();

        let value = null;
        if ( !allow_empty || Math.random() < 0.3 )
        {
            if ( type == "array" )
                value = this.array_val("number", 3)[0];
            else
                value = this.simple_expr(type)[0];
        }

        this.declare_var(name, type);

        return new VariableDeclaration(name, type, value)
    }

    array_val(type, max_size)
    {
        if ( Math.random() < 0.6 )
            return this.basic_expr("array");
        let [args, comp] = this.expr_args("type", 2, max_size);
        return [new ArrayLiteral(args), comp];
    }

    simple_expr(type)
    {
        return this.expr_builder(type, 1, 2, false);
    }

    get_var(type)
    {
        let vars = Object.entries(this.scope).filter(e => type == "any" || e[1] == type);
        if ( vars.length == 0 )
            return null;
        return random_item(vars)[0];
    }

    basic_expr(type)
    {
        if ( Math.random() > 0.3 )
        {
            let varef = this.get_var(type);
            if ( varef )
                return new VariableRef(varef);
        }
        return new Literal(this.get_literal(type == "any" ? this.get_type() : type));
    }

    get_literal(type)
    {
        if ( type == "string" )
            return random_item(this.avail_names());
        if ( type == "array" )
            return [];
        return randint(1, 70);
    }

    get_func()
    {
        return this.compound_name(this.avail_names(), 3);
    }

    expr_args(type, min_size, max_size)
    {
        let arg_count = randint(min_size, Math.max(0, max_size));
        let args = [];
        let complexity = 0;

        for ( let i = 0; i < arg_count; i++ )
        {
            let [arg, size] = this.expr_builder(this.get_type(), 1, 2);
            args.push(arg);
            complexity += size;
            if ( size >= max_size )
                break;
        }
        return [args, complexity];
    }

    expr_func(type, max_size)
    {
        let name = this.get_func();
        let [args, complexity] = this.expr_args(type, 0, max_size - 2);
        complexity += 2;

        return [new FuncCall(new VariableRef(name), ... args), complexity];
    }

    expr_builder(type, min_size, max_size, is_condition)
    {
        // Primary expression
        if ( max_size <= 1 || (min_size < 2 && randint(0, max_size) < 1) )
        {
            return [this.basic_expr(type), 1];
        }

        // Function call
        if ( Math.random() < 0.3 )
        {
            return this.expr_func(type, max_size);
        }

        if ( type == "array" )
            return [this.basic_expr(type), 1];

        // Unary op
        if ( Math.random() < 0.2 )
        {
            let [subexpr, comp] = this.expr_builder(type, min_size, max_size, is_condition);
            if ( subexpr instanceof UnOp )
                return [subexpr, comp];

            if ( subexpr instanceof BinOp )
            {
                comp += 1;
                subexpr = new ExprParen(subexpr);
            }

            return [new UnOp(subexpr, is_condition ? "!" : "-"), comp];
        }

        // Array subscript
        if ( Math.random() < 0.3 )
        {
            let [arr, c1] = this.simple_expr("array");
            if ( !(arr instanceof Literal) )
            {
                let index = this.basic_expr("number");
                return [new ArraySubscript(arr, index), c1 + 1];
            }
        }

        // Binary Op
        let [a, ca] = this.expr_builder(type, min_size - 1, max_size - 2, false);
        let [b, cb] = this.expr_builder(type, min_size - 1, max_size - ca, false);
        let ops;

        // TODO modify ops based on type
        if ( is_condition  )
        {
            ops = ["&&", "||"];

            if ( type == "number" )
                ops.push(">", "<", "<=", ">=", "==", "!=");
        }
        else
        {
            ops = ["+"];

            if ( type == "number" )
                ops.push("-", "*", "/", "%");
        }

        return [new BinOp(a, random_item(ops), b), ca + cb];
    }

    stmt_ass()
    {
        let vars = Object.entries(this.scope);
        if ( vars.length == 0 )
            return this.new_var();
        let [name, type] = random_item(vars);
        return new ExpressionStatement(new BinOp(
            new VariableRef(name),
            "=",
            this.expr_builder(type, 2, 6, false)[0]
        ));
    }

    stmt_func()
    {
        return new ExpressionStatement(this.expr_func(this.get_type(), 6)[0]);
    }

    stmt_if()
    {
        let cond = this.expr_builder("number", 1, 4, true)[0];
        let tail = [this.make_block(1, Math.max(1, Math.min(4, this.max_statements)))];
        this.statements += 1;
        let keep_going = Math.random() < 0.3;

        while ( this.statements < this.max_statements && keep_going )
        {
            keep_going = Math.random() < 0.3;
            if ( keep_going || Math.random() < 0.3 )
            {
                tail.push(this.expr_builder("number", 1, 4, true)[0])
                this.statements += 1;
            }

            tail.push(this.make_block(1, Math.max(1, Math.min(4, this.max_statements))));
        }

        return new If(cond, ...tail);
    }


    stmt_for()
    {
        this.push_scope();

        let name = this.new_var_name();
        this.declare_var(name, "number");

        let range = this.simple_expr("array")[0];
        if ( range instanceof Literal )
        {
            let varname = this.get_var("array");
            if ( varname == null )
                range = this.expr_func("array", 3)[0];
            else
                range = new VariableRef(varname);
        }
        let body = this.make_block(1, Math.max(1, this.max_statements))

        this.pop_scope();

        return new For(new VariableDeclaration(name, "number", null), range, body);
    }

    pop_scope()
    {
        let vars = this.stack.shift();
        for ( let name of vars )
            delete this.scope[name];
    }

    push_scope()
    {
        this.stack.unshift([]);
    }

    stmt_print()
    {
        return new Print(this.expr_builder("string", 1, 2)[0]);
    }
}


export class TypeWriter
{
    constructor(element)
    {
        this.timer = null;
        this.html = "";
        this.index = 0;
        this.element = element;
        this.delay = 20;
        this.tag = null;
    }

    stop()
    {
        if ( this.timer )
        {
            clearTimeout(this.timer);
            this.timer = null;
        }
    }

    finish()
    {
        if ( this.timer )
        {
            this.stop();
            this.element.innerHTML = this.html;
        }
    }

    type(html)
    {
        this.html = html;
        this.element.innerHTML = "";
        this.index = -1;
        this.timer = setTimeout(this.next_char.bind(this), this.delay);
    }

    get_char()
    {
        this.index += 1;
        return this.html[this.index];
    }

    next_char()
    {
        this.timer = null;

        let str = this.get_char();
        if ( this.index >= this.html.length )
            return;

        let html = this.element.innerHTML;

        if ( this.tag )
            html = html.slice(0, -this.tag.length);

        if ( str == "&" )
        {
            while ( true )
            {
                let c = this.get_char();
                if ( this.index >= this.html.length )
                    break;
                str += c;
                if ( c == ";" )
                    break;
            }
        }
        else if ( str == "<" )
        {
            this.tag = "</";
            let tagging = true;
            while ( true )
            {
                let c = this.get_char();
                if ( this.index >= this.html.length )
                    break;

                if ( tagging )
                {
                    if ( c == " " || c == ">" )
                        tagging = false;
                    else
                        this.tag += c;
                }

                str += c;

                if ( c == ">" )
                    break;
            }

            if ( str.startsWith("</") )
                this.tag = null;
            else
                this.tag += ">";
        }

        if ( this.tag )
            str += this.tag;

        this.element.innerHTML = html + str;

        if ( this.index < this.html.length )
            this.timer = setTimeout(this.next_char.bind(this), this.delay);
    }
}
