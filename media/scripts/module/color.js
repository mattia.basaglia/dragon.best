// @license magnet:?xt=urn:btih:0b31508aeb0634b347b8270c7bee4d411b5d4109&dn=agpl-3.0.txt AGPLv3+

import {randint} from "./utils.js";

export class RandomColorRange
{
    constructor(min, max)
    {
        this.min = min;
        this.max = max;
        this.delta = max - min;
    }

    get_value_at(t)
    {
        return Math.round(this.delta * t + this.min);
    }

    get_value()
    {
        return this.get_value_at(Math.random());
    }
}

export class RandomColorMultiRange
{
    constructor(ranges = [])
    {
        this.ranges = ranges;
    }

    add_range(min, max)
    {
        this.ranges.push(new RandomColorRange(min, max));
    }

    get_value()
    {
        var total = this.ranges.map(r => r.delta).reduce((a, b) => a+b);
        var target = randint(0, total-1);
        for ( var range of this.ranges )
        {
            if ( target <= range.delta )
                return range.get_value_at(target / range.delta);
            target -= range.delta;
        }

        console.error("Shouldn't happen :/");
    }
}

export class RandomColorFixed
{
    constructor(value)
    {
        this.value = value;
    }

    get_value_at(t)
    {
        return this.value;
    }

    get_value()
    {
        return this.value;
    }
}

/**
 * \brief converts HSL to RGB (all components in [0, 1]
 */
function hsl_to_rgb1(h, s, l)
{
    if ( s == 0 )
        return [l, l, l];

    let q = l < 0.5 ? l * (1 + s) : l + s - l * s;
    let p = 2 * l - q;
    return [
        hue_to_rgb(p, q, h + 1/3),
        hue_to_rgb(p, q, h),
        hue_to_rgb(p, q, h - 1/3)
    ];
}

function hue_to_rgb(p, q, t)
{
    if ( t < 0 )
        t += 1;
    else if ( t > 1 )
        t -= 1;

    if ( t < 1/6 )
        return p + (q - p) * 6 * t;
    if ( t < 1/2 )
        return q;
    if ( t < 2/3 )
        return p + (q - p) * (2/3 - t) * 6;

    return p;
}


export class ColorHSL
{
    constructor(h, s, l)
    {
        this.h = h;
        this.s = s;
        this.l = l;
    }

    toString()
    {
        return `hsl(${this.h}, ${this.s}%, ${this.l}%)`;
    }

    lightness_mod(lightness_mod)
    {
        var lightness = this.l;
        if ( lightness_mod < 0 )
        {
            lightness *= 1+lightness_mod;
        }
        else
        {
            lightness = lightness * (1-lightness_mod) + 100 * lightness_mod;
            if ( this.l > 85 && lightness >= 95 )
                lightness = 100;
        }

        return new ColorHSL(this.h, this.s, lightness);
    }

    to_hex()
    {
        return '#' + (
            hsl_to_rgb1(this.h / 360, this.s / 100, this.l / 100)
            .map(c => Math.round(c * 255).toString(16).padStart(2, "0"))
            .join("")
        );
    }
}

export class RandomColorHSL
{
    constructor(h, s, l)
    {
        this.h = h;
        this.s = s;
        this.l = l;
    }

    get_color()
    {
        return new ColorHSL(this.h.get_value(), this.s.get_value(), this.l.get_value());
    }
}
