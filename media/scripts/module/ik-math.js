export function polar_difference(p1, p2)
{
    var dx = p2.x - p1.x;
    var dy = p2.y - p1.y;
    return {
        angle: Math.atan2(dy, dx),
        length: Math.hypot(dx, dy)
    };
}

export function offset_point(point, radius, angle)
{
    return {
        x: point.x + Math.cos(angle) * radius,
        y: point.y + Math.sin(angle) * radius
    };
}

export function point_distance(a, b)
{
    var dx = a.x - b.x;
    var dy = a.y - b.y;
    return Math.hypot(dx, dy);
}

export function angle_difference(a, b)
{
    var diff = b - a + Math.PI;
    var tau = Math.PI * 2;
    return (diff % tau + tau) % tau - Math.PI;
}

export function lerp(a, b, f)
{
    return a * (1-f) + b * f;
}

export function lerp_point(a, b, f)
{
    return {
        x: lerp(a.x, b.x, f),
        y: lerp(a.y, b.y, f)
    };
}

export function average_angle(a, b)
{
    var sin = Math.sin(a) + Math.sin(b);
    var cos = Math.cos(a) + Math.cos(b);
    var angle = Math.atan2(sin / 2, cos / 2);
    return angle;
}

export function offset_point_cartesian(point, other)
{
    return {
        x: point.x + other.x,
        y: point.y + other.y
    };
}
