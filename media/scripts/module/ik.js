import {polar_difference, offset_point, angle_difference} from "./ik-math.js";

export class IKSolver
{
    constructor(config)
    {
        this.root = new IKNode(0, config.root);
        this.nodes = [this.root];
        this.bones = [];
        this.graphics = [];
        this.config = config;
        this.targets = [];
        this._sub_bases = [];
        this.anchor = null;
        if ( config.anchored )
            this.add_target(this.root);
    }

    get all_targets()
    {
        if ( this.anchor )
            return this.targets.concat([this.anchor]);
        return this.targets;
    }

    check_graph()
    {
        for ( let node of this.nodes )
            node.visited = false;

        for ( let bone of this.bones )
            node.visited = false;

        this._traverse_node(this.root);


        for ( let node of this.nodes )
        {
            if ( node.visited == false )
                throw new Error("Detached node");
        }

        for ( let bone of this.bones )
        {
            if ( bone.visited == false )
                throw new Error("Detached bone");
        }
    }

    _traverse_node(node)
    {
        if ( node.visited )
            throw new Error("Loop detected");

        node.visited = true;

        for ( let bone of node.children )
            this._traverse_bone(bone);
    }

    _traverse_bone(bone)
    {
        if ( bone.visited )
            throw new Error("Invalid parenting");

        bone.visited = true;
        this._traverse_node(bone.end);
    }

    attach_graphics(graphic, parent)
    {
        var insert_index = this.graphics.findIndex(t => t.render_order > graphic.render_order);
        if ( insert_index == -1 )
            this.graphics.push(graphic);
        else
            this.graphics.splice(insert_index, 0, graphic);

        graphic.attach(this, parent);

        return graphic;
    }

    add_bone(start_node, config)
    {
        var end_node = new IKNode(start_node.depth + 1, config);
        var bone = new IKBone(this, start_node, end_node);
        end_node.parent = bone;
        start_node.children.push(bone);
        this.nodes.push(end_node);
        this.bones.push(bone);
        return bone;
    }

    add_target(node, x=null, y=null)
    {
        if ( node.tatget )
            throw new Error("Node already has a target");

        var target = new IKTarget(x ?? node.x, y ?? node.y, node);
        node.target = target;

        if ( this.root === node )
        {
            this.anchor = target;
            return target;
        }

        this.targets.push(target);

        if ( this.targets.length > 1 )
            this._sub_bases = null;

        node.mark_descendant_target();

        return target;
    }

    // Moves bones to reach all targets
    solve(max_iterations = 1)
    {
        this.ensure_sub_bases();

        for ( var iter = 0; iter < max_iterations; iter++ )
        {
            // Apply targets from leaves to the root
            for ( var target of this.targets )
                target.node.move_backward(target, 0, null);

            for ( var sub_base_level of this._sub_bases )
            {
                for ( var sub_base of sub_base_level )
                    sub_base.move_sub_base();
            }

            // Apply targets from the root to the leaves
            if ( this.anchor )
            {
                this.root.move_forward(this.anchor);
            }
        }
    }

    update_graphics()
    {
        for ( let graphic of this.graphics )
            graphic.update();
    }

    ensure_sub_bases()
    {
        if ( this._sub_bases === null )
        {
            this._sub_bases = Array.from({length: this.root.descendant_targets - 1}, () => []);

            for ( var node of this.nodes )
                node.tag_sub_base(this._sub_bases);
        }
    }

    get sub_bases()
    {
        this.ensure_sub_bases();
        return this._sub_bases;
    }
}


export class IKNode
{
    constructor(depth, config)
    {
        this.depth = depth;
        this.x = config.x;
        this.y = config.y;
        this.radius = config.radius ?? 15;
        this.parent = null;
        this.children = [];
        this.target = null;

        // Sub-base stuff
        this.descendant_targets = 0;
        this.possible_positions = [];
        this.sub_base_level = 0;
    }

    tag_sub_base(sub_bases)
    {
        this.sub_base_level = 0;

        if ( this.descendant_targets > 1 )
        {
            for ( var child of this.children )
            {
                if ( child.end.descendant_targets < this.descendant_targets && child.end.descendant_targets > 0 )
                {
                    this.sub_base_level = this.descendant_targets;
                    sub_bases[this.sub_base_level - 2].push(this);
                    return;
                }
            }
        }
    }

    mark_descendant_target()
    {
        this.descendant_targets += 1;
        if ( this.parent )
            this.parent.start.mark_descendant_target();
    }

    /**
     * \brief Call when the structure of the armature (rather than the pose) has changed
     */
    update_structure()
    {
        if ( this.parent )
            this.parent.update_structure();

        for ( var bone of this.children )
            bone.update_structure();
    }

    move_backward(pos, base_level, from_bone)
    {
        if ( this.sub_base_level && base_level < this.sub_base_level )
        {
            this.possible_positions.push(pos);
        }
        else
        {
            this.x = pos.x;
            this.y = pos.y;

            for ( var child of this.children )
            {
                if ( child !== from_bone && child.end.sub_base_level == 0 )
                    child.move_start()
            }

            if ( this.parent )
                this.parent.move_end(base_level);
        }
    }

    move_sub_base()
    {
        var cx = 0;
        var cy = 0;
        for ( var p of this.possible_positions )
        {
            cx += p.x;
            cy += p.y;
        }

        this.x = cx / this.possible_positions.length;
        this.y = cy / this.possible_positions.length;

        this.possible_positions = [];

        for ( var child of this.children )
            child.move_start()


        if ( this.parent )
            this.parent.move_end(this.sub_base_level);

    }


    move_forward(pos)
    {
        this.x = pos.x;
        this.y = pos.y;
        for ( var bone of this.children )
            bone.move_start();
    }
}

export class BoneConstraint
{
    /**
     * \brief Applies the constraint to the given bone
     * \param bone Bone the constraint is applied to
     * \param[in] p_target Point that should not be moved
     * \param[in,out] p_other Point that should be moved
     * \param angle_offset Angle difference between angles:
     *      0 for forward movement (start, end);
     *      2PI for backward movement (end, start);
     */
    apply(bone, p_target, p_other, angle_offset)
    {
    }
}


/**
 * \brief Contraint that limits the bone's angle relative to its parent
 */
export class MaxAngleDifferenceConstraint
{
    constructor(max_angle, offset=0)
    {
        this.max_angle = max_angle;
        this.offset = offset;
    }


    apply(bone, p_target, p_other, angle_offset)
    {
        if ( this.max_angle >= Math.PI )
            return;

        var parent = angle_offset == 0 ? bone.start.parent : bone.end.children[0];
        if ( parent )
        {
            var parent_angle = parent.angle + this.offset;
            var angle_delta = angle_difference(bone.angle, parent_angle);

            if ( Math.abs(angle_delta) > this.max_angle )
            {
                if ( angle_delta < 0 )
                    bone.angle = parent_angle + this.max_angle;
                else
                    bone.angle = parent_angle - this.max_angle;

                bone.apply_move_angle(p_target, p_other, angle_offset);
            }
        }
    }
}

/**
 * \brief Constraint that forces a fixed angle between a bone and its parent
 */
export class AngleOffsetConstraint
{
    constructor(angle, reference_bone)
    {
        this.angle = angle;
        this.reference_bone = reference_bone;
    }


    apply(bone, p_target, p_other, angle_offset)
    {
        var parent_angle = this.reference_bone.angle;
        bone.angle = parent_angle + this.angle;
        bone.apply_move_angle(p_target, p_other, angle_offset);
    }

    static fix_angle(bone, clear_constraints=true, sibling=false)
    {
        var parent = sibling ? bone.start.children[0] : bone.start.parent;
        if ( parent )
        {
            if ( clear_constraints )
                bone.constraints = [];
            bone.constraints.push(new AngleOffsetConstraint(bone.angle - parent.angle, parent));
        }
    }
}


export class IKBone
{
    constructor(solver, start, end)
    {
        this.solver = solver;
        this.start = start;
        this.end = end;
        this.constraints = [];
        this.update_structure();
    }

    get parent()
    {
        return this.start.parent;
    }


    has_constraint(type)
    {
        for ( var con of this.constraints )
            if ( con instanceof type )
                return true;
        return false;
    }

    /**
     * \brief Call when the structure of the armature (rather than the pose) has changed
     */
    update_structure()
    {
        var pd = polar_difference(this.start, this.end);
        this.angle = pd.angle;
        this.length = pd.length;
    }

    move_helper(p_target, p_other, angle_offset)
    {
        this.angle = Math.atan2(
            this.end.y - this.start.y,
            this.end.x - this.start.x
        );
        var pos = {};
        this.apply_move_angle(p_target, pos, angle_offset);
        this.apply_constraints(p_target, pos, angle_offset);
        return pos;
    }

    apply_move_angle(p_target, p_other, angle_offset)
    {
        var other = offset_point(p_target, this.length, this.angle + angle_offset);
        p_other.x = other.x;
        p_other.y = other.y;
    }

    move_end(base_level)
    {
        var pos = this.move_helper(this.end, this.start, Math.PI);
        this.start.move_backward(pos, base_level, this);
    }

    move_start()
    {
        var pos = this.move_helper(this.start, this.end, 0);
        this.end.move_forward(pos);
    }

    apply_constraints(p_target, p_other, angle_offset)
    {
        for ( var constraint of this.constraints )
            constraint.apply(this, p_target, p_other, angle_offset);
    }
}


export class IKTarget
{
    constructor(x, y, node)
    {
        this.x = x;
        this.y = y;
        this.node = node;
    }
}


export class IKGraphics
{
    constructor(render_order=0)
    {
        this.render_order = render_order;
        this.solver = null;
        this.parent = null;
    }

    attach(solver, parent)
    {
        this.solver = solver;
        this.parent = parent;
    }

    update()
    {
    }
}


export class IKAnimator
{
    constructor(solver)
    {
        this.solver = solver;
        this.playing = false;
        this.frame_callback = this.animation_frame.bind(this);
    }

    play()
    {
        this.playing = true;
        requestAnimationFrame(this.frame_callback);
    }

    pause()
    {
        this.playing = false;
    }

    animation_frame(t)
    {
        if ( !this.playing )
            return;

        this.on_frame(t);

        this.solver.solve();
        this.solver.update_graphics();

        requestAnimationFrame(this.frame_callback);
    }

    on_frame(t)
    {
    }
}
