var py_import;

async function load_python(module_path, on_load)
{
    let pyodide = await loadPyodide();
    py_import = async function(module, alias=null)
    {
        if ( alias == null )
            alias = module;
        await pyodide.runPythonAsync(`
            from pyodide.http import pyfetch
            response = await pyfetch("/media/scripts/${module_path}/${module}.py")
            with open("${alias}.py", "wb") as f:
                f.write(await response.bytes())
        `);
        return pyodide.pyimport(alias);
    };
    await on_load();
}
