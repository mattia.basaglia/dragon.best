// @license magnet:?xt=urn:btih:0b31508aeb0634b347b8270c7bee4d411b5d4109&dn=agpl-3.0.txt AGPLv3+
// See also number_writer server-side

class NumberWriterComponent
{
    constructor(factor, values)
    {
        this.factor = factor;
        this.values = values;
    }

    get_for(number)
    {
        if ( number < this.factor )
            return null;
        var remainder = number % this.factor;
        var multiplier = Math.floor(number / this.factor);
        var text = this.values[multiplier-1];

        if ( text === undefined || text === null )
        {
            text = this.values[0];
            if ( text === null )
                return null;
        }
        else if ( multiplier != 1 )
        {
            multiplier = null;
        }

        if ( this.factor == 1 )
        {
            remainder = 0;
            multiplier = 0;
        }

        return {
            "remainder": remainder,
            "multiplier": multiplier,
            "text": text,
        };
    }
}

class NumberWriter
{
    constructor(zero, components)
    {
        this.zero = zero;
        this.components = components;
    }

    format_number(number)
    {
        if ( number == 0 )
            return this.zero;
        if ( number < 0 )
            return this.format_number(-number);

        var result = "";

        for ( var comp of this.components )
        {
            var partial = comp.get_for(number);

            if ( partial === null )
                continue;

            number = partial.remainder;
            if ( partial.multiplier )
                result = this.join(result, this.format_number(partial.multiplier));
            result = this.join(result, partial.text);

            if ( !number )
                break;
        }

        return result;
    }

    join(result, str)
    {
        if ( result )
            result += " ";
        return result + str;
    }
}

function rawr_number_writer()
{
    return new NumberWriter("zerdurg", [
        new NumberWriterComponent(1000000000, ["dergbillion"]),
        new NumberWriterComponent(1000000, ["durgillion"]),
        new NumberWriterComponent(1000, ["clawsand"]),
        new NumberWriterComponent(100, ["hunderg"]),
        new NumberWriterComponent(10,
        [null, "twenderg", "thirderg", "fourderg", "fiftderg", "sixderg",
        "sevenderg", "eightderg", "ninederg"]),
        new NumberWriterComponent(1,
            ["dragone", "twurg", "three", "roar", "five", "six", "severg", "eight", "nine", "derg",
            "dergeven", "dergwelve", "dergeen", "fourdergeen", "fifdergeen", "sixdergeen",
            "sevendergeen", "eightdergeen", "ninedergeen"]),
    ]);
}

class RawrTransform
{
    constructor(element, clicker)
    {
        this.element = element;
        this.clicker = clicker;
        this.x = clicker.start_x;
        this.y = clicker.start_y;
        this.angle = Math.PI / 180 * rand_between(clicker.angle_range);
        this.opacity = 1.0;
        this.velocity = rand_between(clicker.speed_range);
        this.alpha_decay = this.velocity / (clicker.speed_range[1] - clicker.speed_range[0]) / clicker.alpha_range;
        this.adjust();
    }

    step(speed_factor)
    {
        var vel = this.velocity * speed_factor;
        this.x += vel / this.clicker.fps * Math.cos(this.angle);
        this.y += vel / this.clicker.fps * Math.sin(this.angle);
        this.opacity = Math.max(this.opacity - this.alpha_decay * speed_factor, 0);
    }

    adjust()
    {
        this.element.style.left = this.adjust_size(this.x) + 'px';
        this.element.style.top = this.adjust_size(this.y) + 'px';
        this.element.style["font-size"] = this.adjust_size(this.clicker.font_size) + 'px';
        this.element.style.transform = "rotate(" + this.angle + "rad)";
        this.element.style.opacity = this.opacity;
    }

    tick(speed_factor)
    {
        this.step(speed_factor);
        this.adjust();
        if ( this.opacity <= 0 )
        {
            this.element.parentNode.removeChild(this.element);
            return false;
        }

        return true;
    }

    adjust_size(size)
    {
        return size / 512.0 * this.clicker.dom.clickable.offsetWidth
    }
}

class RawrAnimator
{
    constructor(owner, fps)
    {
        this.owner = owner;
        this._fps = fps;
        this.last_frame = null;
        this.active_rawrs = [];
        this.timer = null;
    }

    start()
    {
        if ( !this.is_running )
        {
            this.last_frame = Date.now();
            this.timer = window.setInterval(this.tick.bind(this), 1000/this._fps);
        }
    }

    stop()
    {
        if ( this.is_running )
        {
            window.clearInterval(this.timer);
            this.timer = null;
        }
    }

    get is_running()
    {
        return this.timer !== null;
    }

    get fps()
    {
        return this._fps;
    }

    set fps(value)
    {
        this._fps = value;
        if ( this.is_running )
        {
            this.stop();
            this.start();
        }
    }

    add_rawr(text)
    {
        var rawrdiv = document.createElement("div");
        rawrdiv.classList.add("rawr");
        rawrdiv.textContent = text;
        rawrdiv.style.color = "hsl("+ rand_between([0, 360]) +", 100%, 60%)"
        var transform = new RawrTransform(rawrdiv, this.owner);
        this.owner.dom.parent.appendChild(rawrdiv);
        this.active_rawrs.push(transform);
    }

    tick()
    {
        var now = Date.now();
        var delta = now - this.last_frame;
        var acutal_fps = this.fps;
        if ( delta > 0 )
            acutal_fps = 1000 / delta;

        var speed_factor = 60 / acutal_fps; // use 60 fps as baseline

        this.last_frame = now;
        var keep = [];
        for ( var trans of this.active_rawrs )
        {
            if ( trans.tick(speed_factor) )
                keep.push(trans);
        }
        this.active_rawrs = keep;
    }
}

class RawrClicker
{
    constructor(config)
    {
        for ( var name in config )
            this[name] = config[name];

        this.number_writer = rawr_number_writer();

        this.dom = {
            clickable:document.getElementById("rawr"),
            parent:   document.getElementById("rawr_container"),
            counter:  document.getElementById("rawr_counter"),
            upgrades: document.getElementById("rawr_upgrades"),
        }

        this.rawrpower = new AutoRawr(
                "Rawr power",
                "The derg will make more rawrs",
                "Louder derg",
                0,
                1,

                0,
                0,

                1,
                1.3,
                5,
                1.5
        );

        this.autorawrs = [
            new AutoRawr(
                "Tiny derg",
                "Tiny dergs will join the rawrs",
                "Louder dergs",
                0,
                16,

                5,
                1.2,

                0.2,
                1.2,
                3,
                1.4
            ),
            new AutoRawr(
                "Party",
                "The derg goes to a party and rawrs",
                "Fancier party",
                3,
                128,

                100,
                1.5,

                0.75,
                1.3,
                1.5
            ),
        ];

        this.rawr_number = 0;
        this.rawr_animator = new RawrAnimator(this, this.fps);
        this.data = new PersistentData(this);
    }

    add_rawr(text)
    {
        this.rawr_animator.add_rawr(text, this.dom.parent);
        this.increase_rawr_number(this.rawrpower.rps);
    }

    add_random_rawr()
    {
        this.add_rawr(rand_sample(this.rawrs));
    }

    adjust_number()
    {
        this.dom.counter.textContent = this.number_writer.format_number(
            Math.floor(this.rawr_number)
        );
    }

    setup(callback)
    {
        this.data.recolor_controller.setup_page(
            document.getElementById("color_editor_preview"),
            function(){ this.on_setup(callback); }.bind(this)
        );
    }

    on_setup(callback)
    {
        this.data.recolor_controller.init_palette_editor(document.getElementById('palette_editor'));

        this.data.load();

        this.dom.upgrades.appendChild(this.rawrpower.make_buy_dom(this));
        for ( var autorawr of this.autorawrs )
            this.dom.upgrades.appendChild(autorawr.make_buy_dom(this));

        this.dom.clickable.addEventListener("click", this.add_random_rawr.bind(this));
        this.dom.parent.onselectstart = () => false;
        this.dom.parent.onmousedown = () => false;
        this.autosave();
        this.rawr_animator.start();
        window.setTimeout(this.assign_rps.bind(this), 1000);
        if ( callback )
            callback();
    }

    assign_rps()
    {
        for ( var auto of this.autorawrs )
        {
            this.rawr_number += this.rawrpower.total_rps;
        }

        this.adjust_number();
        window.setTimeout(this.assign_rps.bind(this), 1000);
    }

    autosave()
    {
        this.data.save();
        window.setTimeout(this.autosave.bind(this), this.data.autosave_interval);
    }

    increase_rawr_number(nrawrs)
    {
        this.rawr_number += nrawrs;
        this.adjust_number();
    }
}

class AutoRawr
{
    constructor(
        name,               ///< name of the item causing Rawrs
        description,        ///< description of how Rawrs are generated
        rps_description,    ///< description of how Rawrs are increased with upgrades
        dependency,         ///< number of the previous AutoRawrs needed to start purchasing this
        max_count,          ///< maximum number of items that can be purchased
        base_price,         ///< base price to increase count
        price_mult,         ///< how much the price increases every time you buy more items
        rps,                ///< base Rawrs per second
        rps_mult,           ///< rps multiplier when purchasing upgrades
        rps_price,          ///< base price for purchasing upgrades
        rps_price_mult)     ///< how much the price increases every time you buy more upgrades
    {
        this.name = name;
        this.dependency = dependency;

        this.count = 0;
        this.max_count = max_count;
        this.price = base_price;
        this.price_mult = price_mult;

        this.rps = rps;
        this.rps_count = 0;
        this.rps_mult = rps_mult;
        this.rps_price = rps_price;
        this.rps_price_mult = rps_price_mult;
    }

    increase(amount)
    {
        if ( this.max_count > 0 && this.count + amount > this.max_count )
            amount = this.max_count - this.count;

        if ( amount <= 0 )
            return;

        this.count += amount;
        var cost = 0;
        for ( let i = 0; i < amount; i++ )
            cost += this.increase_price(i+1)
        this.price = this.increase_price(amount);
        return cost;
    }

    increase_price(amount)
    {
        return this.price * Math.pow(this.price_mult, amount);
    }

    upgrade_price(amount)
    {
        return this.rps_price * Math.pow(this.rps_price_mult, amount);
    }

    upgrade(amount)
    {
        if ( amount <= 0 )
            return;

        this.rps_count += amount;

        var cost = 0;
        for ( let i = 0; i < amount; i++ )
            cost += this.increase_price(i+1)

        this.rps *= Math.pow(this.rps_mult, amount);
        this.rps_price = this.upgrade_price(amount);
        return cost;
    }

    get total_rps()
    {
        return this.rps * this.count;
    }

    to_simple_object()
    {
        return {
            count: this.count,
            rps_count: this.rps_count,
        }
    }

    /**
     * \pre It has not been increased or upgraded
     */
    from_simple_object(object)
    {
        if ( !object )
            return;
        this.increase(object.count);
        this.upgrade(object.rps_count);
    }

    make_buy_dom(clicker)
    {
        var container = document.createElement("div");
        container.classList.add('rawr_buy_container');

        var name = document.createElement("div");
        name.classList.add('rawr_buy_name');
        name.textContent = this.name;
        container.appendChild(name);

        var description = document.createElement("div");
        description.classList.add('rawr_buy_description');
        description.textContent = this.description;
        container.appendChild(description);

        function mkbutton(classname, text, onclick)
        {
            var button = document.createElement("div");
            button.classList.add(classname);
            button.textContent = text + " ";
            button.amount = document.createElement("span");
            button.amount.textContent = '1';
            button.appendChild(button.amount);
            button.cost = document.createElement("span");
            button.cost.textContent = '??';
            button.appendChild(button.cost);

            button.addEventListener("click", onclick);
            return button;
        }

        var ar = this;
        var button_buy = mkbutton('rawr_buy_button', "Buy", function()
        {
            var cost = ar.increase(Math.round(Number(this.amount.textContent)));
            clicker.increase_rawr_number(-cost);
            this.cost.textContent = Math.ceil(ar.price);
        });
        button_buy.cost.textContent = Math.ceil(this.price);
        container.appendChild(mkbutton('rawr_buy_button', "Buy", button_buy));


        var button_upgrade = mkbutton('rawr_upgrade_button', "Upgrade", function()
        {
            var cost = ar.upgrade(Math.round(Number(this.amount.textContent)));
            this.cost.textContent = ar.rps_price;
        });
        button_upgrade.cost.textContent = Math.ceil(this.rps_price);
        container.appendChild(button_upgrade);
        return container;
    }
}


class PersistentData
{
    constructor(owner)
    {
        this.save_version = 1;
        this.owner = owner;
        this.recolor_controller = new RecolorController({});
        this.storage_key = "rawr_settings";
        this.storage = window.localStorage;
        this.settings_toggle = document.getElementById('settings_title');
        this.settings_element = document.getElementById('settings');
        this.color_editor_preview = document.getElementById('color_editor_preview');
        this.settings_toggle.addEventListener("click", function(){
            this.settings_element.classList.toggle('hide');
        }.bind(this));
        this.autosave_interval = 10;
        this.colors = this.current_colors;
    }

    get current_colors()
    {
        return Object.assign({}, this.recolor_controller.recolor_state);
    }

    update_settings_inputs()
    {
        this.recolor_controller.apply_state(this.colors);
    }

    reset()
    {
        this.storage.removeItem(this.storage_key);
        location.reload();
    }

    cancel_settings()
    {
        this.update_settings_inputs();
        this.settings_element.classList.add('hide');
    }

    save_settings()
    {
        this.colors = this.current_colors;
        this.recolor_controller.frame_to_raster(
            color_editor_preview,
            "rawr",
            512,
            512,
            function(data_url){
                this.owner.dom.clickable.src = data_url;
                this.save();
            }.bind(this),
            "png"
        );
        this.settings_element.classList.add('hide');
    }

    reset_colors()
    {
        this.recolor_controller.reset_features();
    }

    randomize_colors()
    {
        this.recolor_controller.randomize_colors();
    }

    save()
    {
        var data = {
            save_version: this.save_version,
            rawr_number: this.owner.rawr_number,
            image_url: this.owner.dom.clickable.src,
            autosave_interval: this.autosave_interval,
            colors: this.colors,
            rawrpower: this.owner.rawrpower.to_simple_object(),
            autorawrs: [],
        };
        for ( var i = 0; i < this.owner.autorawrs.length; i++ )
        {
            data.autorawrs[i] = this.owner.autorawrs[i].to_simple_object();
        }
        this.storage.setItem(this.storage_key, JSON.stringify(data));
    }

    load_from_object(data)
    {
        if ( this.save_version != data.save_version )
        {
            console.log("Incompatible save version!");
            return;
        }

        this.owner.rawr_number = data.rawr_number;
        this.owner.adjust_number();
        this.owner.dom.clickable.src = data.image_url;
        this.autosave_interval = data.autosave_interval;
        this.owner.rawrpower.from_simple_object(data.rawrpower);
        for ( var i = 0; i < this.owner.autorawrs.length; i++ )
        {
            this.owner.autorawrs[i].from_simple_object(data.autorawrs[i]);
        }
    }


    load()
    {
        // Deferred hide so the script can fetch the offsetWidth
        this.settings_element.classList.add('hide');

        var data = this.storage.getItem(this.storage_key);
        if ( data )
        {
            this.load_from_object(JSON.parse(data));
        }
        this.update_settings_inputs();
    }
}

function rand_sample(array)
{
    return array[Math.floor(Math.random() * array.length)];
}

function rand_between(range)
{
    return Math.random() * (range[1]-range[0]) + range[0];
}

// @license-end
