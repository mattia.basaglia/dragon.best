class DurgControl extends ol.control.Control
{
    constructor(opt_options, classname, icon, title)
    {
        var options = opt_options || {};


        var button = document.createElement('button');
        button.classList.add("fas");
        button.classList.add(icon);

        var element = document.createElement('div');
        element.className = 'custom-ol-control ol-unselectable ol-control';
        element.classList.add(classname);
        element.appendChild(button);
        element.setAttribute("title", title);

        super({
            element: element,
            target: options.target
        });

        this.button = button;
        button.addEventListener('click', this.on_click.bind(this));
    }

    on_click(ev) {}

    set_title(str)
    {
        this.element.setAttribute("title", str);
    }
}

class DialogControl extends DurgControl
{
    constructor(opt_options, classname, icon, title)
    {
        super(opt_options, classname, icon, title);
        this.dialog = document.createElement('div');
        this.dialog.classList.add("custom-ol-control-dialog");
        this.element.insertBefore(this.dialog, this.element.firstChild);

        this.hide_event_handler = this.hide_dialog.bind(this);

    }

    on_click(ev)
    {
        this.element.classList.add("active");
        document.addEventListener("click", this.hide_event_handler, true);
        this._on_show_dialog(ev);
    }

    hide_dialog(ev)
    {
        if ( this.dialog.contains(ev.target) )
            return true;
        this.element.classList.remove("active");
        document.removeEventListener("click", this.hide_event_handler, true);
        ev.stopPropagation();
        this._on_hide_dialog(ev);
        return false;
    }

    _on_show_dialog(ev) {}
    _on_hide_dialog(ev) {}
}

class ShareMapControl extends DialogControl
{
    constructor(opt_options)
    {
        super(opt_options, "share-map", "fa-share-alt", "Share Map");

        this.show_link = document.createElement('p');
        this.show_link.classList.add("ol-selectable");
        this.dialog.appendChild(this.show_link);
        var msg = document.createElement('p');
        msg.innerText = "(copied to clipboard)";
        this.dialog.appendChild(msg);

    }

    _on_show_dialog(ev)
    {
        this.show_link.innerText = this.get_share_url().toString();
        var range = document.createRange();
        range.selectNode(this.show_link);
        window.getSelection().removeAllRanges();
        window.getSelection().addRange(range);
        document.execCommand('copy');
    }

    get_share_url()
    {
        var lonlat = ol.proj.toLonLat(map.getView().getCenter());
        var zoom = map.getView().getZoom();
        var url = new URL(window.location);

        var lon = lonlat[0].toFixed(4);
        var lat = lonlat[1].toFixed(4);
        url.hash = "#map=" + zoom + "/" + lon + "/" + lat;
        return url;
    }
}

class LayerToggleControl extends DialogControl
{
    constructor(opt_options)
    {

        super(opt_options, "layer-switcher", "fa-layer-group", "Toggle Layers");

        this.list_parent = document.createElement("ul");
        this.dialog.appendChild(this.list_parent);
    }

    setMap(map)
    {
        super.setMap(map);
        this._init_layers(map);
    }

    _init_layers(map)
    {
        while ( this.list_parent.firstChild )
            this.list_parent.removeChild(this.list_parent.firstChild);

        for ( var layer of map.getLayers().getArray() )
        {
            if ( layer.get("title") )
                this._make_checkbox(this.list_parent, layer);
        }

    }

    _make_checkbox(parent, layer)
    {
        var li = document.createElement("li");
        parent.appendChild(li);
        var cb = document.createElement("input");
        cb.setAttribute("type", "checkbox");
        cb.checked = true;
        cb.id = "_toggle_layer_" + layer.ol_uid;
        li.appendChild(cb);
        var label = document.createElement("label");
        label.innerText = layer.get("title");
        label.setAttribute("for", cb.id);
        li.appendChild(label);

        cb.addEventListener("change", function(e){
            layer.setVisible(e.target.checked);
        });
    }
}

class PlaceMarkerControl extends DurgControl
{
    constructor(opt_options)
    {
        super(opt_options, "place-marker", "fa-map-marker-alt", "Place your marker");

        this.dialog = document.createElement("div");
        this.dialog.classList.add("follow-mouse");
        var dico = document.createElement("span");
        this.dialog.appendChild(document.createTextNode(" Click on the map to set your location"));
        this.element.appendChild(this.dialog);

        this.hide_event_handler = this.hide_dialog.bind(this);
        this.move_event_handler = this.mouse_move.bind(this);
    }

    on_click(ev)
    {
        this.element.classList.add("active");
        document.addEventListener("click", this.hide_event_handler, true);
        document.addEventListener("mousemove", this.move_event_handler, true);
        this.mouse_move(ev);
    }

    hide_dialog(ev)
    {

        this.element.classList.remove("active");
        document.removeEventListener("click", this.hide_event_handler, true);
        document.removeEventListener("mousemove", this.mouse_move, true);
        ev.stopPropagation();

        var map = this.getMap();
        if ( map.getTargetElement().contains(ev.target) && !this.element.contains(ev.target) )
        {
            var pix = map.getEventPixel(ev);
            var coord = ol.proj.toLonLat(map.getCoordinateFromPixel(pix));
            var dragon = get_my_marker_prop("dragon");
            var _public = get_my_marker_prop("public");
            src_my_marker.clear();
            src_my_marker.addFeature(
                mk_marker(coord[0], coord[1], my_icon, dragon, _public)
            );
            save_marker({lon: coord[0], lat: coord[1], icon: my_icon});
        }
    }

    mouse_move(ev)
    {
        this.dialog.style.left = (ev.clientX + 16) + "px";
        this.dialog.style.top = (ev.clientY + 16) + "px";
        return true;
    }
}

class DeleteMarkerControl extends DurgControl
{
    constructor(opt_options)
    {
        super(opt_options, "delete-marker", "fa-trash-alt", "Remove your marker");
    }

    on_click(ev)
    {
        var xhr = new XMLHttpRequest();
        xhr.open("DELETE", api_url, true);
        xhr.onreadystatechange = function() {
            if ( this.readyState === XMLHttpRequest.DONE )
            {
                if ( this.status === 200 )
                {
                    src_my_marker.clear();
                    status_container.textContent = "Marker deleted";
                }
                else
                {
                    status_container.textContent = "Error while saving changes";
                }
            }
        }
        status_container.textContent = "Please wait";
        xhr.send(null);
    }
}

class ToggleableMarkerControl extends DurgControl
{
    constructor(opt_options, name, icon_enabled, icon_disabled, title)
    {
        super(opt_options, name, icon_enabled, title);
        this.icon_enabled = icon_enabled;
        this.icon_disabled = icon_disabled;
        this.initialize_checkbox();
    }

    initialize_checkbox()
    {
        this._checked = this.initial_checked();
        this._update_style(this._checked);
    }

    initial_checked()
    {
        return true;
    }

    get checked()
    {
        return this._checked;
    }

    set checked(val)
    {
        this._checked = val;
        this._update_style(val);
        this.on_change(this._checked);
    }

    _update_style(checked)
    {
        var classes = [this.icon_enabled, this.icon_disabled];
        if ( !checked )
            classes.reverse();
        this.button.classList.add(classes[0]);
        this.button.classList.remove(classes[1]);
    }

    on_click(ev)
    {
        this.checked = !this._checked;
    }

    on_change(checked) {}
}

class SetMarkerVisible extends ToggleableMarkerControl
{
    constructor(opt_options, name, icon_enabled, icon_disabled, title)
    {
        super(opt_options, "marker-visibility", "fa-eye", "fa-eye-slash", "")
    }

    initial_checked()
    {
        return get_my_marker_prop("public");
    }

    on_change(checked)
    {
        save_marker({public: checked});
    }

    _update_style(checked)
    {
        super._update_style(checked);
        if ( checked )
            this.set_title("Public marker - Click to make private (hide from non-logged-in users)");
        else
            this.set_title("Private marker - Click to make public (shows your marker to everyone)");
    }
}

class SetMarkerDragon extends ToggleableMarkerControl
{
    constructor(opt_options, name, icon_enabled, icon_disabled, title)
    {
        super(opt_options, "marker-dragon", "fa-dragon", "fa-paw", "")
    }

    initial_checked()
    {
        return get_my_marker_prop("dragon");
    }

    on_change(checked)
    {
        save_marker({dragon: checked});
    }

    _update_style(checked)
    {
        super._update_style(checked);
        if ( checked )
            this.set_title("You are a dragon - Click to make it non-dragon");
        else
            this.set_title("You are not a dragon - Click to make it a dragon");
    }
}
