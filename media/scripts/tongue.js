function lerp(a, b, f)
{
    return a * (1-f) + b * f;
}

function lerp_point(a, b, f)
{
    return {
        x: lerp(a.x, b.x, f),
        y: lerp(a.y, b.y, f)
    };
}

function offset_point(point, radius, angle)
{
    return {
        x: point.x + Math.cos(angle) * radius,
        y: point.y + Math.sin(angle) * radius
    };
}

function offset_point_cartesian(point, other)
{
    return {
        x: point.x + other.x,
        y: point.y + other.y
    };
}

function point_distance(a, b)
{
    var dx = a.x - b.x;
    var dy = a.y - b.y;
    return Math.hypot(dx, dy);
}

function point_reflection(p, tan_in)
{
    return { x: p.x * 2 - tan_in.x, y: p.y * 2 - tan_in.y};
}

function average_angle(a, b)
{
    var sin = Math.sin(a) + Math.sin(b);
    var cos = Math.cos(a) + Math.cos(b);
    var angle = Math.atan2(sin / 2, cos / 2);
    return angle;
}


function angle_difference(a, b)
{
    var diff = b - a + Math.PI;
    var tau = Math.PI * 2;
    return (diff % tau + tau) % tau - Math.PI;
}

function r2d(a)
{
    return a * 180 / Math.PI;
}

class FabrikSolver
{
    constructor(config)
    {
        this.element = config.element;
        this.start = config.start;
        this.config = config;

        this.segments = [];
        this.anchored = config.anchored ?? true;
        var segment_class = config.segment_class ?? TrapezoidSegment;
        var segment_constructor = config.segment_constructor ?? ((...a) => new segment_class(...a));
        var circle_parent = config.add_circles ? this.element : false;

        var size_lerp = config.size_lerp ?? lerp;

        var point = new IKPoint(config.start.x, config.start.y, size_lerp(config.start.radius, config.finish.radius, 0), circle_parent);

        for ( var i = 1; i <= config.segments; i++ )
        {
            var f = i / config.segments;
            var new_point = new IKPoint(
                lerp(config.start.x, config.finish.x, f),
                lerp(config.start.y, config.finish.y, f),
                size_lerp(config.start.radius, config.finish.radius, f),
                circle_parent
            );
            var segment = segment_constructor(this, i - 1, point, new_point);
            this.segments.push(segment);

            point = new_point;
        }

        for ( var segment of this.segments )
        {
            segment.build_path(this.element);
            segment.update_path();
        }
    }

    segment_index_from_percentage(pc)
    {
        return Math.floor(pc * this.config.segments);
    }

    add_segment(options)
    {
        var last_segment = this.segments[this.segments.length-1];

        var length = options.length ?? last_segment.length;
        var angle = options.angle ?? last_segment.angle;
        var radius = options.radius ?? length;
        var segment_class = options.segment_class ?? TrapezoidSegment;
        var segment_args = options.segment_args ?? [];

        var point = last_segment.end;
        var pos = offset_point(point, length, angle);
        var new_point = new IKPoint(pos.x, pos.y, radius);
        var segment = new segment_class(...segment_args, this, this.segments.length, point, new_point, this.element);
        this.segments.push(segment);
        segment.build_path(this.element);
        segment.update_path();
        return segment;
    }

    move(p)
    {
        for ( var i = this.segments.length - 1; i >= 0; i-- )
        {
            this.segments[i].move_end(p.x, p.y);
            p = this.segments[i].begin;
        }

        if ( this.anchored )
        {
            p = this.start;
            for ( var seg of this.segments )
            {
                seg.move_begin(p.x, p.y);
                seg.update_path();
                p = seg.end;
            }
        }
        else
        {
            for ( var seg of this.segments )
            {
                seg.update_path();
            }
        }

    }

    get last_segment()
    {
        return this.segments[this.segments.length - 1];
    }

    smooth_target(target, options, force_smooth_angle)
    {
        var current = this.last_segment.end;

        var pd = polar_difference(current, target);

        if ( pd.length <= options.speed )
            return target;

        if ( force_smooth_angle || pd.length > options.max_angle_distance )
        {
            var current_angle = this.last_segment.angle;
            var angle_delta = angle_difference(pd.angle, current_angle);
            if ( Math.abs(angle_delta) > options.max_angle )
            {
                if ( angle_delta < 0 )
                    pd.angle = current_angle + options.max_angle;
                else
                    pd.angle = current_angle - options.max_angle;
            }
        }

        return offset_point(current, options.speed, pd.angle);
    }
}

/**
 * \brief Calculates x and y from an event client position to SVG coordinates
 * \param x X in client coordinates
 * \param y Y in client coordinates
 * \param svg SVG root element
 * \param element Elements within the SVG to get coordinates relative to
 */
function client_to_svg_point(x, y, svg, element)
{
    var pt = svg.createSVGPoint();
    pt.x = x;
    pt.y = y;
    var global_pt = pt.matrixTransform(svg.getScreenCTM().inverse());
    var matrix = svg.getScreenCTM().inverse().multiply(element.getScreenCTM()).inverse();
    return global_pt.matrixTransform(matrix);
}

class IKPoint
{
    constructor(x, y, radius)
    {
        this.x = x;
        this.y = y;
        this.radius = radius;
        this.circle = null;
    }

    offset_point(angle)
    {
        return offset_point(this, this.radius, angle);
    }
}

function point_to_svg(p)
{
    return `${p.x} ${p.y}`;
}

function points_to_svg(points)
{
    return points.map(point_to_svg).reduce((a, b) => a + " " + b);
}

function polar_difference(p1, p2)
{
    var dx = p2.x - p1.x;
    var dy = p2.y - p1.y;
    return {
        angle: Math.atan2(dy, dx),
        length: Math.hypot(dx, dy)
    };
}

class EmptySegment
{
    constructor(parent, index, begin, end)
    {
        this.parent = parent;
        this.index = index;
        this.begin = begin;
        this.end = end;
        var pd = polar_difference(this.begin, this.end);
        this.angle = pd.angle;
        this.length = pd.length;
        this.max_angle = parent.config.max_angle ?? null;
    }

    build_path(element)
    {
    }

    move_helper(x, y, p_target, p_other, angle_offset)
    {
        p_target.x = x;
        p_target.y = y;
        this.angle = Math.atan2(
            this.end.y - this.begin.y,
            this.end.x - this.begin.x
        );
        this.apply_move_angle(p_target, p_other, angle_offset);
    }

    apply_move_angle(p_target, p_other, angle_offset)
    {
        var other = offset_point(p_target, this.length, this.angle + angle_offset);
        p_other.x = other.x;
        p_other.y = other.y;
    }

    move_end(x, y)
    {
        this.move_helper(x, y, this.end, this.begin, Math.PI);
        this.apply_constraints();
    }

    apply_constraints()
    {
        // Constrain angle between segments
        if ( this.max_angle !== null && this.index < this.parent.segments.length - 1)
        {
            var parent_angle = this.parent.segments[this.index + 1].angle;
            var angle_delta = angle_difference(this.angle, parent_angle);

            if ( Math.abs(angle_delta) > this.max_angle )
            {
                if ( angle_delta < 0 )
                    this.angle = parent_angle + this.max_angle;
                else
                    this.angle = parent_angle - this.max_angle;

                this.apply_move_angle(this.end, this.begin, Math.PI);
            }
        }
    }

    move_begin(x, y)
    {
        this.move_helper(x, y, this.begin, this.end, 0);
    }

    update_path()
    {
    }
}

class TrapezoidSegment extends EmptySegment
{
    build_path(element)
    {
        if ( this.index == 0 )
            this.circle_begin = this.build_circle(this.begin, element);

        this.circle_end = this.build_circle(this.end, element);

        this.polygon = document.createElementNS("http://www.w3.org/2000/svg", "polygon");
        element.appendChild(this.polygon);

    }

    build_circle(point, element)
    {
        var circle = document.createElementNS("http://www.w3.org/2000/svg", "circle");
        element.appendChild(circle);
        circle.setAttribute("r", point.radius);
        return circle;
    }

    update_circle(point, circle)
    {
        circle.setAttribute("cx", point.x);
        circle.setAttribute("cy", point.y);
    }

    update_path()
    {
        if ( this.index == 0 )
            this.update_circle(this.begin, this.circle_begin);
        this.update_circle(this.end, this.circle_end);

        var points = [
            this.begin.offset_point(this.angle-Math.PI/2),
            this.end.offset_point(this.angle-Math.PI/2),
            this.end.offset_point(this.angle+Math.PI/2),
            this.begin.offset_point(this.angle+Math.PI/2),
        ];
        this.polygon.setAttribute("points", points_to_svg(points));
    }
}


class ImageTransformer
{
    constructor(url, offset={x: 256, y: 256})
    {
        this.image = document.createElementNS("http://www.w3.org/2000/svg", "image");
        this.image.setAttributeNS("http://www.w3.org/1999/xlink", "href", url);
        this.offset = offset;
    }

    add_to_element(element, before=null)
    {
        element.insertBefore(this.image, before);
        var svg = element.closest("svg");
        this.transform = svg.createSVGTransform();
        this.image.transform.baseVal.appendItem(this.transform);
    }


    update(pos, angle)
    {
        var x = pos.x - this.offset.x;
        var y = pos.y - this.offset.y;
        this.image.x.baseVal.value = x;
        this.image.y.baseVal.value = y;
        this.transform.setRotate(angle * 180 / Math.PI, this.offset.x + x, this.offset.y + y);
    }
}

class ImageSegment extends EmptySegment
{
    constructor(url, offset, from_end, ...args)
    {
        super(...args);
        this.from_end = from_end;
        this.image = new ImageTransformer(url, offset);
    }

    build_path(element)
    {
        this.image.add_to_element(element);
        this.under_element = this.image.image;
    }


    update_path()
    {
        this.image.update(this.from_end ? this.end : this.begin, this.angle);
    }
}
