import pathlib
import argparse
from xml.dom.minidom import parse

import yipyap
from xml_helper import mkcell, setup_ids


def cell(tr, *a, **kw):
    td = mkcell(document, tr, *a, **kw)
    tr.appendChild(document.createTextNode("\n"))
    return td


parser = argparse.ArgumentParser()
parser.add_argument("infile")
parser.add_argument("outfile")
args = parser.parse_args()

document = parse(args.infile)

setup_ids(document)

instructions = document.getElementById("instructions")
for instr in yipyap.instructions:
    tr = instructions.appendChild(document.createElement("tr"))
    cell(tr, instr.token, "unbreakable")
    cell(cell(tr, ""), instr.c_like, tag="code")
    cell(tr, instr.description)


with open(args.outfile, "wb") as file:
    file.write(document.documentElement.toxml("utf8"))
