import io
import traceback

import js
import pyodide.ffi
from pyodide.http import pyfetch

import yipyap
from xml_helper import mkcell


class WebMachine(yipyap.Machine):
    def __init__(self, environment):
        self.env = environment
        super().__init__()

    def reset(self):
        self.env.prepare_inspect()
        self.dirty_bytes = set()
        super().reset()

    def print(self, ch):
        self.env.output_element.innerText += ch
        self.env.output_scroll.scrollTop = self.env.output_scroll.scrollHeight

    def swap(self):
        super().swap()
        self.dirty_bytes.add(self.stack)

    def allocate_byte(self):
        self.dirty_bytes.add(len(self.memory))
        super().allocate_byte()


class WebEnvironment:
    def __init__(self, input_element, output_element, output_scroll, memory):
        self.last_instruction = None
        self.running = False
        self.timer_callback = None
        self.examples = {}
        self.input_element = input_element
        self.output_element = output_element
        self.output_scroll = output_scroll
        self.memory = memory
        self.machine = WebMachine(self)
        self.interpreter = yipyap.Interpreter(yipyap.instructions, self.machine)

    @property
    def halted(self):
        return self.interpreter.halted

    def web_load_program(self):
        try:
            file = io.StringIO(self.input_element.value)
            self.output_element.innerText = ""
            self.prepare_inspect()
            self.interpreter.init_execution(file)
            self.last_instruction = None
            self.running = False
        except yipyap.YipYapError as e:
            self.output_element.innerText += "\n%s" % e
        except Exception:
            self.output_element.innerText += "\nVM Error"
            traceback.print_exc()

    def web_run(self):
        if self.running:
            return
        if self.halted:
            self.web_load_program()
        self.running = True
        self.play_speed = int(js.document.getElementById("run-speed").value)
        if self.timer_callback is None:
            self.timer_callback = pyodide.ffi.create_proxy(self.web_run_tick)
        js.setTimeout(self.timer_callback, 0)

    def web_run_tick(self):
        self.run_step()
        if not self.halted and self.running:
            js.setTimeout(self.timer_callback, self.play_speed)
        self.update_inspect()

    def web_pause(self):
        self.running = False

    def run_step(self):
        try:
            instr = self.interpreter.fetch_instruction()
            self.last_instruction = instr
            self.interpreter.exec_instruction(instr)
        except yipyap.YipYapError as e:
            self.output_element.innerText += "\n%s" % e
            self.interpreter.halt()
        except Exception:
            self.output_element.innerText += "\nVM Error"
            self.interpreter.halt()
            traceback.print_exc()

        if self.halted:
            self.running = False
        self.update_inspect()

    def web_step(self):
        if not self.running and not self.halted:
            self.run_step()

    def web_example(self, ev):
        self.input_element.value = self.examples[ev.target.value]
        self.interpreter.halt()

    def register_meta(self):
        return [
            ("hold", "Hold Register"),
            ("SV", "Stack Value"),
            ("SP", "Stack Pointer"),
        ]

    def register_data(self):
        return [
            (self.machine.hold, 1),
            (self.machine.memory[self.machine.stack]
                if self.machine.stack < len(self.machine.memory)
                else 0,
            1),
            (self.machine.stack, 2),
        ]

    def prepare_inspect(self):
        # Memory
        self.inspect_cells = []
        self.memory.innerHTML = ""
        item = self.make_memory_cell()
        self.stack_pointer = cell(item[0], "SP", "pointer", "span", title="Stack Pointer")

        # Registers
        self.inspect_regs = []
        registers = js.document.getElementById("inspect-registers")
        registers.innerHTML = ""
        for short, long in self.register_meta():
            tr = registers.appendChild(js.document.createElement("tr"))
            row = []
            center_cell(tr, short).setAttribute("title", long)
            row.append(center_cell(tr, ""))
            row.append(cell(tr, "0", "number"))
            row.append(cell(tr, ""))
            self.inspect_regs.append(row)

        # Instruction
        self.inspect_instr = [
            js.document.getElementById("last-instruction-name"),
            js.document.getElementById("last-instruction-pseudocode"),
            js.document.getElementById("last-instruction-desc"),
        ]

    def update_inspect(self):
        # Registers
        for i, reg in enumerate(self.register_data()):
            row = self.inspect_regs[i]
            value, size = reg
            row[1].textContent = str(value)
            if size == 1:
                row[0].textContent = "%02x" % value
                row[2].textContent = yipyap.byte_to_char(value)
            else:
                row[0].textContent = "%04x" % value

        # Instruction
        if self.last_instruction:
            instr = self.last_instruction.instruction
            cells = [instr.token, instr.c_like, instr.description]
            for i, text in enumerate(cells):
                self.inspect_instr[i].textContent = text
            # Bit hackish but it does the job
            self.input_element.focus()
            tok = self.last_instruction.token
            self.input_element.setSelectionRange(tok.start, tok.end)

        # Memory
        for i in self.machine.dirty_bytes:
            if i == len(self.inspect_cells):
                mcell = self.make_memory_cell()
            elif i > len(self.inspect_cells):
                continue
            else:
                mcell = self.inspect_cells[i]

            newval = self.machine.memory[i]
            mcell[1].textContent = "%02x" % newval
            mcell[2].textContent = yipyap.byte_to_char(newval)
        self.machine.dirty_bytes = set()

        if self.machine.stack < len(self.inspect_cells):
            self.stack_pointer.parentElement.removeChild(self.stack_pointer)
            mcell = self.inspect_cells[self.machine.stack]
            mcell[0].appendChild(self.stack_pointer)

    def make_memory_cell(self):
        table = cell(self.memory, tag="table", cls="table")
        address = len(self.inspect_cells)
        title = cell(cell(table, tag="tr"), "%02x" % address, tag="th", cls="xmem")
        hex = cell(cell(table, tag="tr"), "00", cls="xmem")
        chr = cell(cell(table, tag="tr"), "")
        item = (title, hex, chr)
        self.inspect_cells.append(item)
        return item


def cell(*a, **kw):
    return mkcell(js.document, *a, **kw)


def center_cell(tr, text):
    return cell(tr, text, "centered")


examples = [
    ("empty", "Empty"),
    ("hello", "Hello World"),
    ("99bottles", "99 Bottles of Beer"),
    ("fizzbuzz", "Fizz Buzz"),
]


async def setup_playground():
    # Elements
    output_element = js.document.getElementById("playground-output")
    output_scroll = js.document.getElementById("playground-output-p")
    input_element = js.document.getElementById("playground-editor")
    memory = js.document.getElementById("inspect-memory")

    # Env
    env = WebEnvironment(input_element, output_element, output_scroll, memory)

    # Events
    event_listener(js.document.getElementById("btn-load"), "click", lambda ev: env.web_load_program())
    event_listener(js.document.getElementById("btn-play"), "click", lambda ev: env.web_run())
    event_listener(js.document.getElementById("btn-step"), "click", lambda ev: env.web_step())
    event_listener(js.document.getElementById("btn-pause"), "click", lambda ev: env.web_pause())

    # Examples
    base_path = "/media/scripts/yipyap/examples/"

    examples_element = js.document.getElementById("examples")
    event_listener(examples_element, "input", env.web_example)

    for slug, title in examples:
        code = await (await pyfetch("%s/%s.yip" % (base_path, slug))).text()
        env.examples[slug] = code
        cell(examples_element, title, None, "option", value=slug)


async def init():
    try:
        await setup_playground()
        js.document.getElementById("main-loading").style = "display: none"
        js.document.getElementById("main-loaded").style = "display: block"
    except Exception:
        js.document.getElementById("main-loading").innerText = "Loading failed x_x"
        raise


def event_listener(element, event, callback):
    proxy = pyodide.ffi.create_proxy(callback)
    element.addEventListener(event, proxy)
    return proxy
