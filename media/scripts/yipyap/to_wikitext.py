import re
import sys

line_replace = re.compile("owo (.*)")

sys.stdout.write('<div style="border: 1px solid #ddd;padding:1em;background-color:#f9f9f9;font-family: monospace,Courier;white-space: pre-wrap";\n>')
with open(sys.argv[1]) as f:
    for line in f:
        if line and line[0] == ' ':
            line = "&nbsp;" + line[1:]
        elif line == '\n':
            line = "&nbsp;\n"
        sys.stdout.write(line_replace.sub("<font color='#ccc'>owo</font> <font color='#888'>\\1</font>", line))

sys.stdout.write('</div>\n')
