// @license magnet:?xt=urn:btih:0b31508aeb0634b347b8270c7bee4d411b5d4109&dn=agpl-3.0.txt AGPLv3+

function lerp(a, b, f)
{
    return a * (1-f) + b * f;
}

function int_lerp(a, b, f)
{
    return Math.round(lerp(a, b, f));
}

function num_to_2hex(num)
{
    return num.toString(16).padStart(2, "0");
}

class Color
{
    constructor(r, g, b, a=1)
    {
        this.r = r;
        this.g = g;
        this.b = b;
        this.a = 1;
    }

    to_css()
    {
        return 'rgba(' + this.r + "," + this.g + "," + this.b + "," + this.a + ")";
    }

    to_html()
    {
        return "#" + num_to_2hex(this.r) + num_to_2hex(this.g) + num_to_2hex(this.b);
    }

    blend(other, f=0.5)
    {
        return new Color(
            int_lerp(this.r, other.r, f),
            int_lerp(this.g, other.g, f),
            int_lerp(this.b, other.b, f),
            lerp(this.a, other.a, f)
        );
    }

    static from_html(hex)
    {
        var r = parseInt(hex.slice(1, 3), 16);
        var g = parseInt(hex.slice(3, 5), 16);
        var b = parseInt(hex.slice(5, 7), 16);
        return new Color(r, g, b)
    }
}

class Gradient
{
    constructor(colors=[])
    {
        this.colors = colors;
    }

    color(factor)
    {
        if ( !this.colors.length )
            return new Color(0, 0, 0, 0);

        var multiplied = factor * (this.colors.length-1);
        var lower = Math.floor(multiplied);
        var upper = Math.ceil(multiplied);

        if ( lower == upper )
            return this.colors[lower];

        return this.colors[lower].blend(this.colors[upper], multiplied - lower);
    }
}

class DragonPoint
{
    constructor(x, y, factor)
    {
        this.x = x;
        this.y = y;
        this.factor = factor;
    }

    angle_to(other)
    {
        return Math.atan2(other.y - this.y, other.x - this.x);
    }

    distance_to(other)
    {
        return Math.sqrt((other.y - this.y) ** 2 + (other.x - this.x) ** 2);
    }
}

class DragonSegment
{
    constructor(container, gradient, p1, p2)
    {
        this.container = container;
        this.gradient = gradient;
        this.p1 = p1;
        this.p2 = p2;
        this.factor = lerp(this.p1.factor, this.p2.factor, 0.5);
        this.line = this.spawn_element();
        this.update_color();
    }

    spawn_element()
    {
        var line = document.createElementNS("http://www.w3.org/2000/svg", "line");
        line.setAttribute("x1", this.p1.x);
        line.setAttribute("y1", this.p1.y);
        line.setAttribute("x2", this.p2.x);
        line.setAttribute("y2", this.p2.y);
        line.classList.add("durg-segment");
        return line;
    }

    update_color()
    {
        this.line.style["stroke"] = this.gradient.color(this.factor).to_css();
    }

    show()
    {
        this.container.appendChild(this.line);
    }

    hide()
    {
        this.container.removeChild(this.line);
    }

    split()
    {
        var angle = this.p1.angle_to(this.p2) + Math.PI / 4;
        var length = this.p1.distance_to(this.p2) * Math.SQRT1_2;
        var mid = new DragonPoint(
            this.p1.x + Math.cos(angle) * length,
            this.p1.y + Math.sin(angle) * length,
            this.factor,
        );


        return [
            new DragonSegment(this.container, this.gradient, this.p1, mid),
            new DragonSegment(this.container, this.gradient, this.p2, mid),
        ]
    }
}

class DragonLevel
{
    constructor(segments)
    {
        this.segments = segments;
    }

    show()
    {
        for ( var segment of this.segments )
            segment.show();
    }

    hide()
    {
        for ( var segment of this.segments )
            segment.hide();
    }

    split()
    {
        var segments = [];
        for ( var segment of this.segments )
            segments = segments.concat(segment.split());
        return new DragonLevel(segments);
    }

    update_color()
    {
        for ( var segment of this.segments )
            segment.update_color();
    }
}

class DragonCurve
{
    constructor(container)
    {
        this.container = container;
        this.level = 0;
        this.gradient = new Gradient([]);
        this.levels = [
            new DragonLevel([
                new DragonSegment(
                    this.container,
                    this.gradient,
                    new DragonPoint(120, 256-64, 0),
                    new DragonPoint(512-64, 256-64, 1),
                )
            ])
        ];
        this.levels[0].show();
    }

    go_down()
    {
        this.levels[this.level].hide();
        this.level++;
        if ( this.level >= this.levels.length )
            this.levels.push(this.levels[this.level - 1].split());
        this.levels[this.level].show();
    }

    go_up()
    {
        if ( this.level <= 0 )
            return;
        this.levels[this.level].hide();
        this.level--;
        this.levels[this.level].show();

    }

    jump_to(level)
    {
        if ( level >=0 && level < this.levels.length )
        {
            this.levels[this.level].hide();
            this.level = level;
            this.levels[this.level].show();
        }
    }

    update_gradient(colors)
    {
        this.gradient.colors = colors;
        for ( var level of this.levels )
            level.update_color();
    }
}

class ColorSelector
{
    constructor(presets, on_change=null)
    {
        this.presets = presets;
        this.id_prefix = "color-";
        this.on_change = on_change;

    }

    create_dom(parent)
    {
        this.create_preset_dom(parent);
        this.create_list_dom(parent);
    }

    create_preset_dom(parent)
    {
        var preset_parent = document.createElement("div");
        var preset_label = document.createElement("label");
        preset_label.setAttribute("for", this.id_prefix + "preset-select");
        preset_label.textContent = "Presets ";
        preset_parent.appendChild(preset_label);

        this.preset_select = document.createElement("select");
        preset_parent.appendChild(this.preset_select);
        this.preset_select.id = this.id_prefix + "preset-select";

        var preset_custom = document.createElement("option");
        preset_custom.textContent = "Custom";
        preset_custom.disabled = true;
        preset_custom.value = "";
        this.preset_select.appendChild(preset_custom);

        for ( var preset_name in this.presets )
        {
            var preset_option = document.createElement("option");
            preset_option.textContent = preset_name;
            preset_option.value = preset_name;
            this.preset_select.appendChild(preset_option);
        }

        parent.appendChild(preset_parent);

        this.preset_select.addEventListener("change", function(){
            this.select_preset(this.preset_select.value);
        }.bind(this));
    }

    create_list_dom(parent)
    {
        this.list = document.createElement("ul");
        this.list.classList.add("color-selector-list");
        parent.appendChild(this.list);

        var btn_list = document.createElement("ul");
        btn_list.classList.add("buttons");
        parent.appendChild(btn_list);

        var btn_add_li = document.createElement("li");
        btn_list.appendChild(btn_add_li);

        var btn_add = document.createElement("button");
        btn_add.textContent = "Add color";
        btn_add.addEventListener("click", function(event){
            event.preventDefault();
            this.preset_select.value = "";
            this.append_color(new Color(0, 0, 0));
            this.trigger_update();
        }.bind(this));
        btn_add_li.appendChild(btn_add);
    }

    append_color(color)
    {
        var row = document.createElement("div");
        this.list.appendChild(row);

        var input = document.createElement("input");
        input.type = "color";
        input.value = color.to_html();
        row.appendChild(input);
        input.addEventListener("change", function(){
            this.preset_select.value = "";
            this.trigger_update();
        }.bind(this));

        var delet_this = document.createElement("button");
        delet_this.textContent = " - ";
        delet_this.title = "Remove color";
        delet_this.addEventListener("click", function(){
            this.preset_select.value = "";
            this.list.removeChild(row);
            this.trigger_update();
        }.bind(this));
        row.appendChild(delet_this);
    }

    select_preset(name)
    {
        this.preset_select.value = name;
        while ( this.list.firstChild )
            this.list.removeChild(this.list.firstChild);
        var colors = this.presets[name];
        for ( var color of colors )
            this.append_color(color);
        this.trigger_update();
    }

    get_colors()
    {
        if ( this.preset_select.value )
        {
            return this.presets[this.preset_select.value];
        }

        var colors = [];
        for ( var input of this.list.querySelectorAll("input") )
            colors.push(Color.from_html(input.value));
        return colors;
    }

    trigger_update()
    {
        if ( !this.on_change )
            return;
        this.on_change(this.get_colors());
    }
}
