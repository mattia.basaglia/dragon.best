<!DOCTYPE html>
<html prefix="og: http://ogp.me/ns#">
<head>
<?php $this->head($render_args); ?>
</head>
<body vocab="http://schema.org/" typeof="WebPage">
<?php $this->body($render_args); ?>
</body>
</html>

