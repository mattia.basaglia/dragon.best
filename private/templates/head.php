<?php

include($this->template_path."/page_meta.php");
?>

<link rel="stylesheet" type="text/css" href="https://use.fontawesome.com/releases/v6.4.0/css/all.css" />
<link href='https://unpkg.com/boxicons@2.1.4/css/boxicons.min.css' rel='stylesheet'>

<?php
    foreach ( $this->styles as $styles )
    {
        echo mkelement([
            "link",
            array(
                "rel" => "stylesheet",
                "type" => "text/css",
                "href" => href($styles)
            )
        ]);
    }

    foreach ( $this->scripts as $script )
    {
        echo mkelement([
            "script",
            array("src" => href($script)),
            ""
        ]);
    }

    $this->extra_head($render_args);
