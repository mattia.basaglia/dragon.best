<aside class='copyright'>
    Copyright &copy;
    <span property="copyrightYear"><?php echo $this->copy_range(); ?></span>
    <span property="author" typeof="Person">
        <span property="name"><?php echo $this->copy_author; ?></span>
    </span>
    <br />
    <?php
        echo $this->license_object()->as_link(), " ", new Link("/license/", "License info");
        echo $this->extra_footer($render_args);
    ?>
</aside>
