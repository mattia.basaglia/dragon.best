<meta name="viewport" content="width=device-width, initial-scale=1.0" />

<?php
    $sizes = [
        "16x16" => "16.png",
        "24x24" => "24.png",
        "32x32" => "32.png",
    ];
//         "any" => ["scalable.svg", "image/svg+xml"]
    foreach ( $sizes as $size => $file )
        echo mkelement(["link", [
            "rel" => "icon",
            "href" => href("/media/img/icon/$file"),
            "sizes" => $size,
            "type" => "image/png",
        ]]);

    $ogmeta = $this->opengraph_metadata($render_args);
    $show_twitter = $this->show_social_media_card($render_args, $ogmeta);

    if ( $ogmeta["type"] == "photo" )
    {
        $show_twitter = true;
        echo '<meta name="twitter:card" content="summary_large_image" />';
    }

    foreach ( $ogmeta as $type => $content )
    {
        echo mkelement(["meta", ["property"=>"og:$type", "content" => $content]]);
        echo "\n";

        if ( $type == "description" )
            echo mkelement(["meta", ["name"=>$type, "content"=>$content]]) . "\n";

        if ( $show_twitter && in_array($type, ["image", "title", "description", "url"]) )
            echo mkelement(["meta", ["property"=>"twitter:$type", "content" => $content]]) . "\n";

    }
?>

<title><?php echo escape($this->title($render_args)); ?></title>


