<nav class="menu">
<?php
    $navlinks = [
        new SimpleElement("li", ["class"=>"logo"],
            new PlainLink("/", "",  ["title"=>"Glax is best dragon"])
        ),
        new Menu("Fun", [
            Link::nav_icon("Choir",                 "/choir",       "navicon fa-fw fas fa-music"),
            Link::nav_icon("Noodle",                "/noodle",      "navicon bx bxs-bowl-hot"),
            Link::nav_icon("Tongue",                "/tongue/",     "navicon fa-fw fas fa-grin-tongue-squint"),
            Link::nav_icon("Generator",             "/random",      "navicon fa-fw fas fa-dice"),
            Link::nav_icon("Weather",               "/weather/",    "navicon fa-fw fas fa-cloud-sun-rain"),
            Link::nav_icon("Are dragons awesome?",  "/awesome/",    "navicon fa-fw fas fa-dragon"),
            Link::nav_icon("GlaxGPT",               "/glaxgpt",     "navicon fa-fw fas fa-comment-dots"),
            Link::nav_icon("Cookie",                "/cookie/",     "navicon fa-fw fas fa-cookie-bite"),
            Link::nav_icon("Captcha",               "/captcha/",    "navicon fa-fw fas fa-robot"),
            Link::nav_icon("Dragon Test",           "/test/",       "navicon fa-fw far fa-question-circle"),
            Link::nav_icon("Durg Time",             "/time/",       "navicon fa-fw far fa-clock"),
//             Link::nav_icon("Membership",            "/shady-scales/","navicon fa-fw fas fa-money-bill-wave"),
            Link::nav_icon("Dragon Curve",          "/curve/",      "navicon fa-fw fas fa-chart-line"),
            // Link::nav_icon("Angle Dragons",         "/angle/",      "navicon fa-fw fas fa-shapes"),
        ]),
        new Menu("Games", [
            Link::nav_icon("Glaxle",                "/games/glaxle/?nav=1", "navicon fa-fw far fa-square"),
            Link::nav_icon("Derg Flight",           "/games/derg-flight",   "navicon fa-fw fas fa-plane"),
            Link::nav_icon("Rawr!",                 "/rawr/",       "navicon fa-fw fas fa-bullhorn"),
        ]),
        new Menu("Art", [
            Link::nav_icon("Sticker Editor",        "/editor/",     "navicon fa-fw fas fa-pencil-alt"),
            Link::nav_icon("Vectors",               "/vectors/",    "navicon fa-fw fas fa-bezier-curve"),
            Link::nav_icon("Lottie Animations",     "/lottie/",     "navicon fa-fw fas fa-video"),
            Link::nav_icon("3D Models",             "/3d/",         "navicon fa-fw fas fa-cubes"),
            Link::nav_icon("Ascii Art",             "/ascii/",      "navicon fa-fw fas fa-font"),
            Link::nav_icon("Gifs",                  "/gifs/",       "navicon fa-fw fas fa-video"),
            Link::nav_icon("Glaxmoji",              "/glaxmoji/",   "navicon fa-solid fa-face-laugh-beam"),
            Link::nav_icon("Fur Noises",            "/fur-noises/", "navicon fa-fw fas fa-bullhorn"),
            Link::nav_icon("Commission",            "/commission/", "navicon fa-fw fas fa-money-bill-wave"),
            Link::nav_icon("Telegram Stickers",     "/stickers/",   "navicon fa-fw fab fa-telegram"),
            Link::nav_icon("Refs",                  "/refs/",       "navicon fa-fw fas fa-dragon"),
            Link::nav_icon("Fursuiting",            "/fursuit/",    "navicon fa-fw fas fa-mask"),
            Link::nav_icon("Contrib",               "/contrib/",    "navicon fa-fw far fa-images"),
        ]),
        new Menu("Tech", [
            Link::nav_icon("Glaxverse",             "/glaxverse/",  "navicon fa-fw fas fa-meteor"),
            Link::nav_icon("API",                   "/api/",        "navicon fa-fw fas fa-code"),
            Link::nav_icon("ConCat [RFC9402]", "https://datatracker.ietf.org/doc/html/rfc9402", "navicon fa-fw fas fa-cat"),
            Link::nav_icon("COBOLD",                "/yipyap",      "navicon fa-fw fas fa-y"),
            Link::nav_icon("Exemoji",               "/exemoji",     "navicon fa-fw fas fa-microchip"),
            Link::nav_icon("Copilot",               "/copilot",     "navicon bx bxs-plane-alt"),
        ]),
        new Menu("Dragons", [
            Link::nav_icon("List",                      "/dragons/","navicon fa-fw fas fa-list", [], false),
//             Link::nav_icon("Points",             "/dragons/points/","navicon fa-fw fas fa-coins"),
            Link::nav_icon("Map",                   "/dragons/map/","navicon fa-fw fas fa-map-marked"),
            Link::nav_icon("Size Chart",           "/dragons/size/","navicon fa-fw fas fa-ruler-combined"),
        ]),
        new Menu("Personal", [
            Link::nav_icon("Contact",               "/contact/",    "navicon fa-solid fa-address-card"),
            Link::nav_icon("Conventions",           "/cons/",       "navicon fa-solid fa-calendar-days"),
            Link::nav_icon("Telegram",              "/telegram/",   "navicon fa-fw fab fa-telegram-plane"),
        ]),
        new SimpleElement("li", ["class"=>"separator"], ""),
    ];


    global $auth;

    if ( $auth->user )
    {
        $usermenu = new Menu(
            mkelement(
                ["span", [], [$auth->user->display_name]],
                ["img", ["src"=>$auth->user->photo_url, "class"=>"profile-pic"]]
            ), [
                Link::nav_icon("Logout", "/auth/logout/?next={$_SERVER['REQUEST_URI']}", "navicon fa-fw fas fa-sign-out-alt")
            ]
        );

        if ( $auth->is_admin() )
        {
            global $site;
            foreach ( $site->settings->alt_uris as $name => $prefix )
            {
                $usermenu->link_list->add(Link::nav_icon(
                    $name,
                    $prefix.$_SERVER["REQUEST_URI"],
                    "navicon fa-fw fas fa-globe"
                ));
            }
        }

        $navlinks[] = $usermenu;
    }
    else
    {
        $navlinks["Login"] = new PlainLink(
            "/auth/login/?next={$_SERVER['REQUEST_URI']}",
            "Login",
            ["rel" => "nofollow"]
        );
    }

    echo new LinkList($navlinks);
?>
</nav>
