<?php
    global $site;
    if ( $site->settings->track )
    {
        include(__dir__.'/track.php');
    }

?>
<?php $this->nav($render_args); ?>
<main id='content' property="mainContentOfPage">
<?php $this->main($render_args); ?>
</main>
<?php $this->footer($render_args); ?>
<?php

    if ( $site->settings->debug )
    {
        echo "<script>";
        foreach ( $site->php_errors as $error )
        {
            $js_str = str_replace(["\n", "'"], ["\\n", "\\'"], $error);
            echo "console.log('$js_str');";
        }
        echo "</script>";
    }
