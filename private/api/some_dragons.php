<?php

require_once(__dir__."/../lib/api/base.php");

class SomeDragonsPage extends MemeImageBasePage
{
    public $formats = ["png", "jpg"];
    public $description = "Renders a \"Some X. Get over it\" image.";
    public $params = [
        ["what", "string", "String to render", "text", "DRAGONS ARE CUTE"]
    ];

    function fetch_data($format)
    {
        $margin = 10;
        $font_size = 64;
        $font = ApiParameter::font_path("Heebo-Black-Emoji");
        $width = 512;

        $what = strtoupper($this->get_parameter("what"));

        $image = new Imagick();
        $draw = new ImagickDraw();
        $draw->setFont($font);
        $draw->setFontSize($font_size);

        $string = "SOME $what.";

        $tw1 = new ImagickTextWrapper(
            $image,
            $draw,
            $margin,
            $margin,
            $width - $margin
        );
        $tw1->wrap_text($string, 0, 0.66);

        $tw2 = ImagickTextWrapper::continue_from($tw1);
        $tw2->wrap_text("GET OVER IT!", 0, 0.66);

        $image->newImage($width, $tw2->bounds->y2 + $margin, "rgb(244, 58, 45)");
        $draw->setFillColor("white");
        $tw1->draw();
        $draw->setFillColor("black");
        $tw2->draw();

        return $image;
    }
}

$api_page = new SomeDragonsPage();
