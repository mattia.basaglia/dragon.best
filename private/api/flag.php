<?php

require_once(__dir__."/../lib/api/base.php");

function regional_indicator_symbol($symbol)
{
    # Regional Indicator Symbol A is 1F1E6
    $letter = $symbol - 0x1F1E6;
    if ( $letter < 0 || $letter >= 26 )
        return "";
    # 'a' is 97
    return chr(97 + $letter);
}

function latin_tag($symbol)
{
    # TAG LATIN SMALL LETTER A is E0061
    $letter = $symbol - 0xE0061;
    if ( $letter < 0 || $letter >= 26 )
        return "";
    # 'a' is 97
    return chr(97 + $letter);
}

class GlaxFlagPage extends MemeImageBasePage
{
    public $formats = ["png", "jpg"];
    public $description = "Renders a Glax holding a flag.";
    public $params = [
        ["flag", "string", "Country / Flag", "text", "eu"],
        ["background", "color", "Image background color", null, "transparent"],
    ];

    function __construct()
    {
        parent::__construct();

        $this->asset_abs_path = __dir__ . "/assets/countries/";
        $this->img_abs_path = __dir__ . "/assets/countries/flags/";
        $this->flags = [];
        foreach ( scandir($this->img_abs_path) as $filename )
            if ( strlen($filename) > 4 && substr($filename, -4) == ".png" )
                $this->flags[] = substr($filename, 0, -4);

        include("{$this->asset_abs_path}countries.php");
        $this->countries = $countries;

        $this->flag_colors = [
            "rgba(0, 170, 0, 0.6)",
            "rgba(255, 200, 0, 0.6)",
        ];
        $this->flag_color = -1;
    }

    protected function is_flag($text)
    {
        return preg_match("~^([a-z]{2}/)?[a-z]+$~", $text) && file_exists($this->img_abs_path . strtolower($text) . ".png");
    }

    protected function parse_flags($text)
    {
        $text = trim($text);

        // Direct flag name: nl nl/nb
        if ( $this->is_flag($text) )
            return [$text];

        // Empty
        $len = mb_strlen($text);
        if ( $len == 0 )
            return ["none"];

        // text command: color:red color:red+netherlands netherlands
        if ( $len == strlen($text) || mb_stripos($text, " ") !== false |
            mb_stripos($text, ":") !== false || mb_stripos($text, "+") !== false )
        {
            return array_map([$this, "find_flag_text"], explode("+", $text));
        }

        // Emoji
        $flags = [];
        $unisplit = array_map("mb_ord", unicode_split($text));
        while ( sizeof($unisplit) > 0 )
        {
            # Crossed flags
            if ( $unisplit[0] == 0x1F38C )
            {
                array_shift($unisplit);
                $flags []= "jp";
                $flags []= "jp";
            }
            else
            {
                $flags []= $this->find_flag_emoji($unisplit);
            }
        }

        return $flags;
    }

    protected function find_flag_text($text)
    {
        $text = trim($text);

        // Render text: text:hello
        if ( substr($text, 0, 5) == "text:" )
            return $text;

        $low_text = mb_strtolower($text);

        // Direct flag name: nl nl/nb
        if ( $this->is_flag($low_text) )
            return $low_text;

        // Blazon: blazon:red
        if ( substr($low_text, 0, 7) == "blazon:" )
            return $text;

        // Verbose country name: Netherlands
        foreach ( $this->countries as $name => $code )
        {
            if ( mb_strtolower($name) == $low_text )
                return strtolower($code);
        }

        return "blazon:$text";
    }

    protected function find_flag_emoji(&$unisplit)
    {
        # Country Flag
        if ( regional_indicator_symbol($unisplit[0]) && sizeof($unisplit) > 1 )
        {
            $country = regional_indicator_symbol(array_shift($unisplit));
            $country .= regional_indicator_symbol(array_shift($unisplit));

            if ( !$this->is_flag($country) )
                return "none";
            return $country;
        }

        # White flag 0x1F3F3
        if ( $unisplit[0] == 0x1F3F3 )
        {
            array_shift($unisplit);

            # Variation selector FE0F
            if ( sizeof($unisplit) >= 1 && $unisplit[0] == 0xFE0F )
            {
                array_shift($unisplit);

                # zero width joiner 200D
                if ( sizeof($unisplit) >= 2 && $unisplit[0] == 0x200D )
                {
                    array_shift($unisplit);
                    $flag = array_shift($unisplit);
                    if ( count($unisplit) && $unisplit[0] == 0xFE0F )
                        array_shift($unisplit);

                    if ( $flag == 0x1F308 )
                        return "rainbow";
                    if ( $flag == 0x26A7 )
                        return "trans";
                }
            }

            return "none";
        }
        # Black flag
        else if ( $unisplit[0] == 0x1F3F4 )
        {
            array_shift($unisplit);
            $tags = "";
            while ( sizeof($unisplit) > 0 && latin_tag($unisplit[0]) )
            {
                $tags .= latin_tag(array_shift($unisplit));
            }
            # Ends with cancel tag
            if ( strlen($tags) > 0 )
            {
                if ( sizeof($unisplit) > 0 && $unisplit[0] == 0xE007F && strlen($tags) >= 3 )
                {
                    array_shift($unisplit);
                    $flag = substr($tags, 0, 2) . "/" . substr($tags, 2);
                    if ( $this->is_flag($flag) )
                        return $flag;
                }
                return "black";
            }
            # zero width joiner 200D + skull and bones 2620 + variation selector FE0F
            else if ( sizeof($unisplit) >= 3 && $unisplit[0] == 0x200D && $unisplit[1] == 0x2620 && $unisplit[2] == 0xfe0f )
            {
                array_shift($unisplit);
                array_shift($unisplit);
                array_shift($unisplit);
                return "pirate";
            }

            return "black";
        }
        # Chequered 1F3C1
        else if ( $unisplit[0] == 0x1F3C1 )
        {
            array_shift($unisplit);
            return "chequered";
        }
        # Triangular red flag 1F6A9
        else if ( $unisplit[0] == 0x1F6A9 )
        {
            array_shift($unisplit);
            return "red";
        }
        # Hammer and Sickle 262D
        else if ( $unisplit[0] == 0x262D )
        {
            array_shift($unisplit);
            return "su";
        }
        # Carp streamer 1f38f
        else if ( $unisplit[0] == 0x1f38f )
        {
            array_shift($unisplit);
            return "carp";
        }

        return "text:".mb_chr(array_shift($unisplit));
    }

    function render_text($text, $w, $h, $bg, $fg)
    {
        $font_size = 256;
        $font_family = __dir__."/assets/fonts/Heebo-Black-Emoji.ttf";

        # Text sizing
        $image_text = new Imagick();
        $draw = new ImagickDraw();
        $draw->setFont($font_family);
        $draw->setFontSize($font_size);
        $tw = new ImagickTextWrapper($image_text, $draw, 0, 0, null);
        $tw->wrap_text($text);

        $atw = $tw->bounds->width();
        $ath = $tw->bounds->height();
        $ratio = (float)$h / $w;
        $fw = max($w, $tw->bounds->width()+$font_size/2);
        $fh = $fw * $ratio;
        $image_text->newImage($fw, $fh, $bg);

        $atx = ($fw - $atw) / 2;
        $aty = ($fh - $ath) / 2;

        # Text (out)
        $draw->setFillColor($fg);
        $draw->setStrokeColor("transparent");
        $tw->draw($atx, $aty);

        $image_text->scaleImage($w, $h);

        return $image_text;
    }

    private function generated_color()
    {
        $this->flag_color = ($this->flag_color + 1) % sizeof($this->flag_colors);
        return $this->flag_colors[$this->flag_color];
    }


    protected function render_blazon($text)
    {
        $root = dirname(__dir__) . "/external/drawshield";

        global $chg_data_cache;
        global $dom;
        global $messages;
        global $options;
        global $placementData;
        global $scale;
        global $scale_factor;
        global $subArg;
        global $svg_chief;
        global $svg_context;
        global $svg_region;
        global $targetColours;
        global $toReverse;
        global $trace;
        global $version;
        global $xpath;
        global $xScale;

        require_once("$root/version.inc");
        require_once("$root/parser/utilities.inc");
        $options["blazon"] = strip_tags($text);
        $options["size"] = 550;
        $options["shape"] = "flag";
        $options["flagHeight"] = 600; // based on width being 1000
        $options["asFile"] = "1";
        $options['useWebColours'] = true;
        $options['customPalette'] = [
            "heraldic/marshalling-stroke" => "none",
            "heraldic/azure" => "#0f47af",
            "heraldic/rose" => "#ff218c",
            "heraldic/celestial-azure" => "#21b1ff",
            "heraldic/purpure" => "#750787",
        ];
//         $options["palette"] = "drawshield";
        require_once("$root/parser/parser.inc");
        $p = new parser('english');
        $dom = $p->parse($options["blazon"], 'dom');

        $xpath = new DOMXPath($dom);
        $blazonOptions = $xpath->query('//instructions/child::*');
        if ( !is_null($blazonOptions) )
        {
            for ($i = 0; $i < $blazonOptions->length; $i++)
            {
                $blazonOption = $blazonOptions->item($i);
                switch ($blazonOption->nodeName)
                {
                    case blazonML::E_PALETTE:
                        $options['palette'] = $blazonOption->getAttribute('keyterm');
                        break;
                    case blazonML::E_ASPECT:
                        $ar = calculateAR($blazonOption->getAttribute('keyterm'));
                        $options["flagHeight"] = (int)(round($ar * 1000));
                        break;
                }
            }
        }

        unset($p);
        require_once("$root/analyser/utilities.inc");
        require_once("$root/analyser/references.inc");
        $references = new references($dom);
        $dom = $references->setReferences();
        unset($references);
        require_once("$root/svg/draw.inc");

        $output = draw();
        unset($options);
        unset($version);
        unset($xpath);
        unset($dom);
        unset($targetColours);

        $im = new Imagick();
        $im->setBackgroundColor(new ImagickPixel('transparent'));
        $im->readImageBlob($output);
        return $im;
    }

    private function render_flag($image, $flag, $flag_x, $flag_y, $flag_w, $flag_h_max, $flag_angle, $flag_wave)
    {
        if ( substr($flag, 0, 6) == "color:" )
        {
            $flag_image = new Imagick();
            $flag_image->newImage(550, 367, substr($flag, 6));
        }
        else if ( substr($flag, 0, 5) == "text:" )
        {
            $flag_image = $this->render_text(substr($flag, 5), 550, 367, $this->generated_color(), "black");
        }
        else if ( substr($flag, 0, 7) == "blazon:" )
        {
            $flag_image = $this->render_blazon(substr($flag, 7));

        }
        else
        {
            $filename = "{$this->img_abs_path}{$flag}.png";
            $flag_image = new Imagick($filename);
        }

        $w = $flag_image->getImageWidth();
        $shading = new Imagick();
        $shading->newImage($flag_image->getImageWidth(), $flag_image->getImageHeight(), "white");
        $draw = new ImagickDraw();
        $draw->setStrokeColor("transparent");
        $draw->setFillColor("#ccc");
        $draw->rectangle($w/3.5, 0, $w-$w/5.2, $flag_image->getImageHeight());
        $shading->drawImage($draw);
        $shading->blurImage(128, 64);
        $flag_image->setImageBackgroundColor("transparent");
        $flag_image->compositeImage($shading, imagick::COMPOSITE_MULTIPLY, 0, 0, imagick::CHANNEL_RED|imagick::CHANNEL_GREEN|imagick::CHANNEL_BLUE);

        $flag_image->setImageBackgroundColor("transparent");
        $flag_image->waveImage(-$flag_wave, $flag_image->getImageWidth());
        $flag_scale = (float)$flag_w / $flag_image->getImageWidth();
        $flag_h = $flag_scale * $flag_image->getImageHeight();
        if ( $flag_h > $flag_h_max )
        {
            $flag_h = $flag_h_max;
            $flag_scale = (float)$flag_h / $flag_image->getImageHeight();
            $flag_w = $flag_image->getImageWidth() * $flag_scale;
        }
        $flag_image->scaleImage($flag_w, $flag_h);
        $flag_image->rotateImage("transparent", $flag_angle);
        $flag_dy = $flag_scale * $flag_wave * -cos($flag_angle*M_PI/180);
        $flag_dx = ($flag_scale * $flag_wave - $flag_h) * sin($flag_angle*M_PI/180);
        $image->compositeImage($flag_image, imagick::COMPOSITE_DEFAULT, $flag_x + $flag_dx, $flag_y + $flag_dy);
    }

    function fetch_data($format)
    {
        # Params
        $width = 512;
        $height = 512;
        $background = $this->get_parameter("background");


        $flags = $this->parse_flags($this->get_parameter("flag"));

        $image = new Imagick();
        $image->newImage($width, $height, $background);


        if ( sizeof($flags) == 1 )
        {
            $flag_size = 285;
            $flag_wave = 48;
            $this->render_flag($image, $flags[0], 235, 5, $flag_size, $flag_size, 25, $flag_wave);

            $base_image = new Imagick("{$this->asset_abs_path}flag-empty.png");
            $image->compositeImage($base_image, imagick::COMPOSITE_DEFAULT, 0, 0);
        }
        else
        {
            $flag_size = 250;
            $flag_h = 235;
            $flag_wave = 38;
            $this->render_flag($image, $flags[0], 155, 12, $flag_size, $flag_h, 7, $flag_wave);
            $this->render_flag($image, $flags[1], 355, 80, $flag_size, $flag_size, 57, $flag_wave);

            $base_image = new Imagick("{$this->asset_abs_path}flag2.png");
            $image->compositeImage($base_image, imagick::COMPOSITE_DEFAULT, 0, 0);
        }

        return $image;
    }
}

$api_page = new GlaxFlagPage();
