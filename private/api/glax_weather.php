<?php

require_once(__dir__."/../lib/api/base.php");

class GlaxWeatherPage extends JsonBasePage
{
    public $formats = ["json", "png"];
    public $description = "Shows Glax in the current weather.";
    public $params = [
        ["location", "string", "Weather location", "city,country"],
        ["lon", "float", "Longitude (only used if location isn't given)", null, null],
        ["lat", "float", "Latitude (only used if location isn't given)", null, null],
        ["units", "string", "Measure units", null, "metric", ["default", "metric", "imperial"]],
        ["forecast", "bool", "Show weather forecast"],
    ];
    public $params_example = "location=london,uk&units=metric";
    public $main_page = "/weather/";
    public $credits = "Weather data by OpenWeatherMap.org CC BY-SA 4.0";
    public $api_url = "https://api.openweathermap.org/data/2.5/";

    const Day = 'd';
    const Night = 'n';
    const Clear = 1;
    const PartialCloud = 2;
    const Cloud = 3;
    const Rain = 9;
    const PartialRain = 10;
    const Thunderstorm = 11;
    const Snow = 13;
    const Fog = 50;

    function __construct()
    {
        parent::__construct();
        global $site;
        $this->api_key = $site->settings->open_weather_map_api_key;
    }

    function fetch_data($format)
    {
        $weather_api = new GlaxWeatherPage();
        $location = $this->get_parameter("location");

        $lon = $this->get_parameter("lon");
        $lat = $this->get_parameter("lat");

        if ( $lon !== null && $lat !== null )
            $location_query = [
                "lon" => $lon,
                "lat" => $lat
            ];
        else
            $location_query = ["q" => $location];

        $units = $this->get_parameter("units");
        $data = $weather_api->fetch_weather($location_query, $units);

        if ( is_array($data) )
        {
            $this->set_status_code($data["code"]);
            return $data;
        }

        $data = $this->format_partial($data, $data->name);

        if ( !$this->get_parameter("forecast") || $format != "json" )
            return $data;

        $data_forecast = $weather_api->fetch_forecast($location_query, $units);
        if ( is_array($data_forecast) )
        {
            $this->set_status_code($data_forecast["code"]);
            return $data_forecast;
        }

        $forecast = [];
        if ( $data_forecast->city && $data_forecast->city->name )
            $city = $data_forecast->city->name;
        else
            $city = "the given location";

        foreach ( $data_forecast->list as $forecast_point )
        {
            $forecast[] = $this->format_partial($forecast_point, $city);
        }

        return [
            "current" => $data,
            "forecast" => $forecast,
        ];
    }

    private function format_partial($data, $city)
    {
        $temperature = round($data->main->temp, 1);
        $temperature_unit = $data->temperature_unit;
        $humidity = $data->main->humidity;
        $weather = $data->weather[0]->description;
        $icon = $data->dragon->icon;
        if ( !$city )
            $city = "the given location";
        $message = "Weather in $city is $temperature $temperature_unit, $humidity% Humidity, $weather";

        return [
            "speed_unit" => $data->speed_unit,
            "temperature_unit" => $temperature_unit,
            "temperature" => $temperature,
            "humidity" => $humidity,
            "weather" => $weather,
            "icon" => $data->dragon->icon,
            "city" => $city,
            "message" => $message,
            "credits" => $this->credits,
            "time" => gmdate("c", $data->dt),
            "timestamp_utc" => $data->dt,
            "wind_speed" => round($data->wind->speed),
        ];
    }

    function encode_data($data, $format)
    {
        if ( $format == "png" )
        {
            header("Content-type: image/png");
            global $site;
            echo file_get_contents("{$site->settings->root_dir}{$data['icon']}");
        }
        else
        {
            $data["icon"] = href($data["icon"]);
            return parent::encode_data($data, $format);
        }
    }

    function icon_filename($icon)
    {
        return "/media/img/weather/$icon.png";
    }

    function get_icon($cond, $dn, $temperature)
    {
        switch ( $cond )
        {
            case GlaxWeatherPage::Clear:
                if ( $temperature >= 25 )
                    return "sunny-hot";
                else if ( $temperature < 0 )
                    return "freezing";
                else if ( $dn == GlaxWeatherPage::Night )
                    return "night";
                else
                    return "sunny";
            case GlaxWeatherPage::Cloud:
            case GlaxWeatherPage::PartialCloud:
                if ( $temperature >= 25 )
                    return "sunny-hot";
                else if ( $temperature < 0 )
                    return "freezing";
                else if ( $dn == GlaxWeatherPage::Night )
                    return "cloudy-night";
                else
                    return "cloudy";
            case GlaxWeatherPage::Rain:
                return "rain-00";
            case GlaxWeatherPage::PartialRain:
                if ( $dn == GlaxWeatherPage::Night )
                    return "rain-02-moon";
                else
                    return "rain-01-sun";
            case GlaxWeatherPage::Thunderstorm:
                return "rain-10-thunder";
            case GlaxWeatherPage::Snow:
                return "snow";
            case GlaxWeatherPage::Fog:
                return "fog";
            default:
                return "not-found";
        }
    }

    function get($method, $query)
    {
        $url = "{$this->api_url}$method?".http_build_query($query);
        $opts = [
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_TIMEOUT => 1,
        ];
        list($result, $response_code) = curl_get($url, $opts);
        return [json_decode($result), $response_code];
    }

    function fetch_weather($location_query, $units)
    {
        list($json, $response_code) = $this->get("weather",[
            "appid" => $this->api_key,
            "units" => $units,
        ] + $location_query);

        if ( $json === null || $response_code >= 400 )
            return $this->error($json, $response_code);


        return $this->dragonify($json, $units);
    }

    function fetch_forecast($location_query, $units)
    {
        list($json, $response_code) = $this->get("forecast",[
            "appid" => $this->api_key,
            "units" => $units,
        ] + $location_query);

        if ( $json === null || $response_code >= 400 )
            return $this->error($json, $response_code);

        foreach ( $json->list as &$forecast )
        {
            $this->dragonify($forecast, $units);
        }

        return $json;
    }

    private function dragonify(&$json, $units)
    {
        $c_temp = $json->main->temp;
        if ( $units == "metric" )
        {
            $json->temperature_unit = "C";
            $json->speed_unit = "km/h";
            $json->wind->speed *= 3.6;
        }
        else if ( $units == "default" )
        {
            $json->temperature_unit = "K";
            $json->speed_unit = "m/s";
            $c_temp += 273.15;
        }
        else if ( $units == "imperial" )
        {
            $json->temperature_unit = "F";
            $json->speed_unit = "mph";
            $c_temp = ($c_temp - 32) / 9 * 5;
        }

        $json->dragon = new stdClass;
        $json->dragon->day_night = $json->weather[0]->icon[2];
        $json->dragon->condition = $this->id2condition($json->weather[0]->id);
        $json->dragon->icon = $this->icon_filename($this->get_icon(
            $json->dragon->condition, $json->dragon->day_night, $c_temp
        ));

        return $json;
    }

    private function id2condition($id)
    {
        if ( $id < 300 )
            return GlaxWeatherPage::Thunderstorm;
        if ( $id < 400 )
            return GlaxWeatherPage::Rain;
        if ( $id < 510 )
            return GlaxWeatherPage::PartialRain;
        if ( $id == 511 )
            return GlaxWeatherPage::Snow;
        if ( $id < 600 )
            return GlaxWeatherPage::Rain;
        if ( $id < 700 )
            return GlaxWeatherPage::Snow;
        if ( $id < 800 )
            return GlaxWeatherPage::Fog;
        if ( $id == 800 )
            return GlaxWeatherPage::Clear;
        if ( $id == 801 )
            return GlaxWeatherPage::PartialCloud;
        return GlaxWeatherPage::Cloud;
    }

    private function error($json, $response_code)
    {
        if ( $json === null )
        {
            return [
                "error" => true,
                "message" => "Invalid API response",
                "code" => 500,
                "icon" => $this->icon_filename("not-found"),
            ];
        }
        else if ( $response_code >= 400 )
        {

            return [
                "error" => true,
                "message" => $json->message,
                "code" => $response_code,
                "icon" => $this->icon_filename("not-found"),
            ];
        }
    }
}

$api_page = new GlaxWeatherPage();
