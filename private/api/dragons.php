<?php

require_once(__dir__."/../lib/api/base.php");


class DragonsApiPage extends JsonBasePage
{
    public $description = "Lists the best dragons in JSON format.";
    public $main_page = "/dragons/";
    public $params = [
        ["q", "string", "Search term"],
    ];

    function fetch_data($format)
    {
        include(__dir__."/../../dragons/index.php");
        $results = [];
        global $site;
        $search = strtolower($this->get_parameter("q"));
        foreach ( $page->dragons as $dragon )
        {
            if ( !$search || strpos(strtolower($dragon->name), $search) !== False )
                $results []= [
                    "name" => $dragon->name,
                    "image" => href($dragon->image),
                    "description" => $dragon->description(),
                    "page" => href("/dragons/{$dragon->id}/"),
                ];
        }
        return $results;
    }
}

$api_page = new DragonsApiPage();
