<?php

require_once(__dir__."/../lib/api/base.php");
require_once(__dir__."/../models.php");
require_once(__dir__."/../lib/forms/models.php");


class DragonsSizeApiPage extends JsonBasePage
{
    public $description = "Data for the dragon size chart.";
    public $main_page = "/dragons/size/";
    public static $units = [
        "cm" => 1,
        "m" => 100,
        "ft" => 30.48,
    ];

    private function form($dragon, $input)
    {
        $form = new ModelForm($dragon, ["type", "name", "height", "length", "color"], [], $input);

        if ( !isset($input["type"]) || !isset(DragonSize::$types[$input["type"]]) )
        {
            $form->add_error("type", "Invalid type");
        }
        else
        {

            $type = DragonSize::$types[$input["type"]];

            $input["height"] = @(float)$input["height"];
            if ( isset($input["height-unit"]) && isset(self::$units[$input["height-unit"]]) )
                $input["height"] *= self::$units[$input["height-unit"]];

            $input["length"] = @(float)$input["length"];
            if ( isset($input["length-unit"]) && isset(self::$units[$input["length-unit"]]) )
                $input["length"] *= self::$units[$input["length-unit"]];


            if ( $input["height"] == 0 && $input["length"] == 0 )
            {
                if ( strlen($type->height) )
                    $form->add_error("height", "Missing value");
                if ( strlen($type->length) )
                    $form->add_error("length", "Missing value");
            }

            $form->raw_data = $input;
        }
        return $form;
    }

    function fetch_data($format)
    {
        global $auth;
        $is_authed = $auth->user;
        $tg_id = $is_authed && $auth->user instanceof TelegramAuthedUser ? $auth->user->user_id: null;

        $method = $_SERVER['REQUEST_METHOD'];
        if ( $method != "GET" && !$is_authed )
        {
            http_response_code(401);
            return [];
        }
        else
        {
            $input = $this->get_body();
        }

        if ( $method == "DELETE" )
        {
            if ( !isset($input["id"]) )
            {
                http_response_code(400);
                return ["id" => "Missing ID"];
            }
            DragonSize::query()
                ->where("telegram_id", "=", $tg_id)
                ->where("id", "=", $input["id"])
                ->delete();
            return [];
        }
        else if ( $method == "POST" || $method == "PUT" )
        {
            $dragon = new DragonSize(["telegram_id"=>$tg_id]);
            $dragon->icon_url = $auth->user->photo_url;
            $form = $this->form($dragon, $input);

            if ( $form->is_valid() )
            {
                $form->save();
                return [];
            }
            else
            {
                http_response_code(400);
                return $form->get_errors();
            }
        }
        else if ( $method == "PATCH" )
        {
            if ( !isset($input["id"]) )
            {
                http_response_code(400);
                return ["id" => "Missing ID"];
            }
            $dragon = DragonSize::query()
                ->where("telegram_id", "=", $tg_id)
                ->where("id", "=", $input["id"])
                ->first();

            if ( $dragon === null )
            {
                http_response_code(404);
                return ["id" => ["Not found"]];
            }

            $form = $this->form($dragon, $input);
            if ( $form->is_valid() )
            {
                $dragon->icon_url = $auth->user->photo_url;
                $form->save();
                return [];
            }
            else
            {
                http_response_code(400);
                return $form->get_errors();
            }
        }

        $query = DragonSize::query();

        $results = [];
        foreach ( $query->select() as $dragon )
        {
            $results []= [
                "id" => $dragon->id,
                "type" => $dragon->type,
                "icon_url" => $dragon->icon_url,
                "owned" => $tg_id === $dragon->telegram_id,
                "name" => $dragon->name,
                "height" => $dragon->height,
                "length" => $dragon->length,
                "color" => $dragon->color,
            ];
        }
        return $results;
    }
}

$api_page = new DragonsSizeApiPage();


