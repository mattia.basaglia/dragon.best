<?php

require_once(__dir__."/../lib/api/base.php");
require_once(__dir__."/../lib/number_writer.php");


class RawrNumbersApiPage extends JsonBasePage
{
    public $description = "Rawrify a number.";
    public $main_page = "/rawr/";
    public $formats = ["json", "txt"];
    public $params = [
        ["number", "int", "Number to display", "number"]
    ];
    public $params_example = "number=621";

    function fetch_data($format)
    {
        $number = $this->get_parameter("number");
        if ( $number === null )
            return "";
        return rawr_number_writer()->format_number($number);
    }

    function encode_data($data, $format)
    {
        if ( $format == "txt" )
        {
            header("Content-type: text/plain");
            echo "$data\n";
        }
        else
        {
            parent::encode_data(["result"=>$data], $format);
        }
    }
}

$api_page = new RawrNumbersApiPage();

