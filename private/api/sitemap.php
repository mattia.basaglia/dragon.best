<?php

require_once(__dir__."/../lib/api/base.php");

class SitemapApiPage extends JsonBasePage
{
    public $description = "Find pages on dragon.best.";
//     public $main_page = "";
    public $formats = ["json", "xml"];
    public $params = [
        ["q", "string", "Search term"]
    ];
    protected $items = ["title", "description", "url"];
    protected $disallowed = ["api", "robots.txt.php", "media.php"];

    function fetch_data($format)
    {
        return $this->find_pages(strtolower($this->get_parameter("q") ?? ""));
    }

    function find_pages($query)
    {
        global $site;
        $output = [];
        return $this->find_pages_recursive($site->base_path, "/", $query, $output);
    }

    protected function find_pages_recursive($file_path, $uri_path, $query, &$output)
    {
        $flags = FilesystemIterator::CURRENT_AS_FILEINFO |
                 FilesystemIterator::SKIP_DOTS |
                 FilesystemIterator::UNIX_PATHS |
                 0
        ;
        $iter = new FilesystemIterator($file_path, $flags);

        foreach ( $iter as $file )
        {
            $filename = $file->getFilename();
            if ( in_array($filename, $this->disallowed) )
                continue;

            if ( $file->isFile() && substr($filename, -4) == ".php" )
            {
                $file_uri_path = $uri_path;
                if ( $filename != "index.php" )
                    $file_uri_path .= $file->getBasename(".php");

                $page = $this->load_page($file->getPathname(), $file_uri_path);
                if ( $page !== null && $this->matches_query($page, $query) )
                {
                    $output []= $this->make_absolute($page);
                }
            }
            else if ( $file->isDir() && $filename !== "rfc" )
            {
                $this->find_pages_recursive($file->getPathname(), "$uri_path$filename/", $query, $output);
            }
        }

        return $output;
    }

    protected function matches_query($result, $query)
    {
        if ( !$query )
            return true;

        foreach ( $this->items as $item )
        {
            if ( strpos(strtolower($result[$item]), $query) !== false )
                return true;
        }
        return false;
    }

    /**
     * Split so matches_query doesn't see the domain name
     */
    protected function make_absolute(&$result)
    {
        $result["image"] = href($result["image"]);
        $result["url"] = href($result["url"]);
        return $result;
    }

    protected function load_page($file, $uri_path)
    {
        include($file);
        if ( !isset($page) || !($page instanceof Page) )
            return null;

        return [
            "title" => $page->get_meta_title([]),
            "description" => $page->get_meta_description([]),
            "image" => $page->get_meta_image([]),
            "author" => $page->copy_author,
            "license" => $this->format_license($page->license_object()),
            "url" => $uri_path,
        ];
    }

    protected function format_license(License $license)
    {
        return [
            "name" => $license->name,
            "url" => $license->url,
        ];
    }

    function encode_data($data, $format)
    {
        if ( $format == "xml" )
        {
            header("Content-type: application/xml");
            $xw = new XMLWriter();
            $xw->openMemory();
            $xw->setIndent(true);
            $xw->startDocument("1.0", "utf-8");
            $xw->startElement("urlset");
            $xw->startAttribute("xmlns");
            $xw->text("http://www.sitemaps.org/schemas/sitemap/0.9");
            $xw->endAttribute();
            foreach ( $data as $url )
            {
                $xw->startElement("url");
                $xw->startElement("loc");
                $xw->text($url["url"]);
                $xw->endElement();
                $xw->endElement();
            }
            $xw->endElement();
            $xw->endDocument();
            echo $xw->outputMemory();
        }
        else
        {
            parent::encode_data(["result"=>$data], $format);
        }
    }
}

$api_page = new SitemapApiPage();
