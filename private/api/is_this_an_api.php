<?php

require_once(__dir__."/../lib/api/base.php");

class IsThisASetting
{
    function __construct($name, BoundingBox $is_this_area, BoundingBox $object_area)
    {
        $this->name = $name;
        $this->is_this_area = $is_this_area;
        $this->object_area = $object_area;
    }

    function image_path()
    {
        return __dir__ . "/assets/is_this_an_api/{$this->name}.jpg";
    }
}

class IsThisAPage extends MemeImageBasePage
{
    public $formats = ["png", "jpg"];
    public $description = "Renders a fursuit meme.";
    public $params = [
        ["text", "string", "String to render", "text", "Is this a thing?"],
        ["object", "string", "String to render above the hand", "text", ""],
        ["font", "font", "Font to use", "name", "DejaVuEmoji"],
        ["width", "int", "Maximum image width"],
    ];

    function __construct()
    {
        parent::__construct();
        $this->fursuits = [
            "beeps" => new IsThisASetting(
                "beeps",
                new BoundingBox(
                    0, 930,
                    1340, 1030
                    // max: 1919, 1079
                ),
                new BoundingBox(
                    1000, 244,
                    1820, 532
                )
            ),
            "cd" => new IsThisASetting(
                "cd",
                new BoundingBox(
                    0, 948,
                    958, 1130
                ),
                new BoundingBox(
                    0, 250,
                    470, 470
                )
            ),
        ];
        $this->params["who"] = new ApiParameter("who", "raw", "Suiter to use", null, "beeps", array_keys($this->fursuits));
    }

    function render_rext(
        $string,
        BoundingBox $box,
        $font,
        $font_size,
        $font_bold,
        $font_border,
        $line_height
    )
    {
        # Text sizing
        $image = new Imagick();
        $draw = new ImagickDraw();
        $draw->setFont($font);

        $font_scale = 1;

        $draw->setFontSize($font_size);
        $font_scale = determine_font_scale(
            $image,
            $draw,
            $string,
            $box->width() - 20,
            $box->height(),
            $font_bold + $font_border,
            1,
            3
        );
        $draw->setFontSize($font_size*$font_scale);

        $half_border = ($font_bold+$font_border)*$font_scale / 2;

        $text_x = $half_border;
        $text_y = $half_border;
        $text_w = $box->width();
        $tw = new ImagickTextWrapper(
            $image,
            $draw,
            $half_border,
            $half_border,
            $text_w
        );
        $tw->wrap_text($string, 0.5, $line_height);
        $image->newImage($box->width(), $box->height(), 'transparent');
//         $image->newPseudoImage($box->width(), $box->height(), "pattern:checkerboard");

        # Text (out)
        $fill = 'white';
        $outline = 'black';
        $y = ($box->height() - $tw->bounds->height()) / 2;
        $draw->setFillColor($outline);
        $draw->setStrokeColor($outline);
        $draw->setStrokeWidth(($font_bold+$font_border)*$font_scale);
        $tw->draw(-1, $y);

        # Text (fill)
        $draw->setFillColor($fill);
        $draw->setStrokeColor($fill);
        $draw->setStrokeWidth($font_bold*$font_scale);
        $tw->draw(0, $y);

        return $image;
    }

    function parse_args()
    {
        $params = new stdClass();

        $params->who = $this->fursuits[$this->get_parameter("who")];
        $params->object = $this->get_parameter("object");

        $string = $this->get_parameter("text");

        if ( $params->object == "" && strpos($string, "?") !== false )
        {
            $i = strrpos($string, "?");
            $params->object = trim(substr($string, $i+1));
            $string = substr($string, 0, $i+1);
        }

        $params->string = $string;

        $params->font = $this->get_parameter("font");

        $params->max_width = $this->get_parameter("width");

        return $params;
    }

    function fetch_data($format)
    {
        # Config
        $font_size = 64;
        $line_height = 1;
        $font_bold = 0;

        # Params
        $params = $this->parse_args();

        # sizing
        $filename = $params->who->image_path();
        $image = new Imagick($filename);
        $width = $image->getImageWidth();
        $font_border = $width / 640.0;


        $img_text = $this->render_rext(
            $params->string,
            $params->who->is_this_area,
            $params->font,
            $font_size,
            $font_bold,
            $font_border,
            $line_height
        );

        if ( $params->object )
        {
            $object_img_text = $this->render_rext(
                $params->object,
                $params->who->object_area,
                $params->font,
                $font_size,
                $font_bold,
                $font_border,
                $line_height
            );
        }

        $image->compositeImage(
            $img_text,
            imagick::COMPOSITE_DEFAULT,
            $params->who->is_this_area->x1,
            $params->who->is_this_area->y1
        );

        if ( $params->object )
        {
            $image->compositeImage(
                $object_img_text,
                imagick::COMPOSITE_DEFAULT,
                $params->who->object_area->x1,
                $params->who->object_area->y1
            );
        }

        if ( $params->max_width && $params->max_width < $width )
            $image->scaleImage($params->max_width, 0);

        return $image;
    }
}

$api_page = new IsThisAPage();

