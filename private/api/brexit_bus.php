<?php

require_once(__dir__."/../lib/api/base.php");


class Transline
{
    function __construct(
        $tl_x, $tl_y,
        $bl_x, $bl_y,
        $tr_x, $tr_y,
        $br_x, $br_y,
        $pre_width,
        $pre_height
    )
    {
        $this->tl_x = $tl_x;
        $this->tl_y = $tl_y;
        $this->bl_x = $bl_x;
        $this->bl_y = $bl_y;
        $this->tr_x = $tr_x;
        $this->tr_y = $tr_y;
        $this->br_x = $br_x;
        $this->br_y = $br_y;
        $this->pre_width = $pre_width;
        $this->pre_height = $pre_height;
    }

    function apply(Imagick $overlay, Imagick $dest)
    {
//         $overlay->transformImage();
        $cp = [
            0, 0,
            $this->tl_x, $this->tl_y,

            0, $overlay->getImageHeight(),
            $this->bl_x, $this->bl_y,

            $overlay->getImageWidth(), 0,
            $this->tr_x, $this->tr_y,

            $overlay->getImageWidth(), $overlay->getImageHeight(),
            $this->br_x, $this->br_y,
        ];
        $overlay->setImageVirtualPixelMethod(Imagick::VIRTUALPIXELMETHOD_TRANSPARENT);
        $overlay->distortImage(Imagick::DISTORTION_PERSPECTIVE, $cp, true);
        $dest->compositeImage($overlay, imagick::COMPOSITE_DEFAULT, $this->tl_x, $this->tl_y);
    }
}

class BrexitBusPage extends MemeImageBasePage
{
    public $formats = ["jpg"];
    public $description = "Renders custom text on the brexit bus.";
    public $params_example = "text=We send the EU £350 million a week&bottom_text=If we leave we can still eat BLT sandwiches";
    public $params = [
        ["text", "string", "String to render", "text", "Brexit costs £440 million a week"],
        ["bottom_text", "string", "String to render at the bottom", "text", ""],
        ["font", "font", "Font to use", "name", "Helvetica-Narrow-Bold"],
        ["colorize", "color", "Shift colors to match this", "color", "transparent"],
        ["width", "int", "Maximum image width"],
    ];

    function __construct()
    {
        parent::__construct();
        $this->img_path = __dir__ . "/assets/brexit_bus/";
    }

    function parse_args()
    {
        $params = new stdClass();

        $params->colorize = $this->get_parameter("colorize");
        $params->bottom_string = $this->get_parameter("bottom_text");

        $string = $this->get_parameter("text");

        while ( $string && $string[0] == "-" )
        {
            if ( unprefix($string, "--colorize") )
                parse_colorize($string, $params->colorize);
            else if ( unprefix($string, "--spaced") )
                $params->spaced = true;
            else
                break;
        }

        if ( !$string )
        {
            $string = "...";
        }
        else if ( $params->bottom_string == "" && strpos($string, "::") !== false )
        {
            list($string, $params->bottom_string) = array_map("trim", explode("::", $string, 2));
        }

        $params->string = $string;

        $params->font = $this->get_parameter("font");

        $params->dest_width = $this->get_parameter("width");

        return $params;
    }

    function render_text($string, Transline $line, $font, $font_size)
    {
        $img = new Imagick();
//         $img->newPseudoImage($line->pre_width, $line->pre_height, "pattern:checkerboard");
        $img->newImage($line->pre_width, $line->pre_height, 'transparent');
        $padding = 10;

        $draw = new ImagickDraw();
        $draw->setFillColor('#ddd');
        $draw->setFillAlpha(1);
        $draw->setStrokeColor('#400');
        $draw->setStrokeWidth(1);
        $draw->setFont($font);
        $draw->setFontSize($font_size);

        $font_scale = determine_font_scale(
            $img,
            $draw,
            $string,
            $line->pre_width - 8,
            $line->pre_height,
            0,
            0.6,
            3
        );

        $draw->setFontSize($font_size * $font_scale);
        $tw = new ImagickTextWrapper($img, $draw, 0, 0, $line->pre_width);
        $tw->wrap_text($string, 0.5, 0.8);
        $tw->draw(0, ($line->pre_height - $tw->bounds->height()) / 2);

        return $img;
    }

    function fetch_data($format)
    {
        # Config
        $font_size = 64;
        $lines = [
            new Transline(
                586, 98,
                635, 225,
                1400, 207,
                1436, 278,
                840 * 2,
                80 * 2
            ),
            new Transline(
                619, 238,
                656, 354,
                1436, 292,
                1452, 351,
                844 * 2,
                80 * 2
            ),
        ];
        $main_color = "#aa1c08";

        # Params
        $params = $this->parse_args();

        # Sizing
        $filename = "{$this->img_path}bus.jpg";
        $bus_image = new Imagick($filename);
        $bus_w = $bus_image->getImageWidth();
        $bus_h = $bus_image->getImageHeight();

        # Draw text
        $img_text = $this->render_text(
            $params->string,
            $lines[0],
            $params->font,
            $font_size
        );
        $lines[0]->apply($img_text, $bus_image);

        if ( $params->bottom_string )
        {
            $bottom_img_text = $this->render_text(
                $params->bottom_string,
                $lines[1],
                $params->font,
                $font_size
            );
            $lines[1]->apply($bottom_img_text, $bus_image);
        }

        # Colorize
        if ( $params->colorize->getColorValue(Imagick::COLOR_ALPHA) )
        {
            list($im_deltabri, $im_deltasat, $im_deltahue) = colorize_params(
                new ImagickPixel($main_color),
                $params->colorize
            );
            $bus_image->modulateImage($im_deltabri, $im_deltasat, $im_deltahue);
        }

        # Scale
        if ( $params->dest_width && $params->dest_width < $bus_image->getImageWidth() )
            $bus_image->scaleImage($params->dest_width, 0);

        return $bus_image;
    }
}

$api_page = new BrexitBusPage();

