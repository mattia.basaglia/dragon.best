<?php

require_once(__dir__."/../lib/api/base.php");


class LayoutDebugPage extends MemeImageBasePage
{
    public $formats = ["png"];
    public $description = "Debugging Text layout.";
    public $params = [
        ["text", "string", "String to render", "text", "foo barq"],
        ["font", "font", "Font to use", "name", "DejaVuEmoji"],
        ["nowrap", "bool", "Whether to skip auto wrapping"],
    ];

    function fetch_data($format)
    {
        # Config
        $font_size = 64;
        $font_scale = 1;
        $font_border = 8;
        $font_bold = 0;

        $margin = 32;
        $width = $height = 512;
        $text_box = new BoundingBox($margin, $margin, $width-$margin, $height-$margin);

        # Params
        $string = str_replace("\n", "", $this->get_parameter("text"));
        $font = $this->get_parameter("font");
        $nowrap = $this->get_parameter("nowrap");

        # Text sizing
        $image_text = new Imagick();
        $image_text->newImage($width, $height, 'transparent');
        $draw = new ImagickDraw();
        $draw->setFont($font);
        $draw->setFontSize($font_size);

        # Determine font scale
        if ( strpos($string, "\n") === false )
        {
            $font_scale = determine_font_scale(
                $image_text,
                $draw,
                $string,
                $text_box->width(),
                $text_box->height(),
                $font_bold + $font_border
            );
        }

        # Determine actual text size / position
        $draw->setFontSize($font_size*$font_scale);
        $full_border = ($font_bold+$font_border)*$font_scale;
        $text_x = $text_box->x1 + $full_border / 2;
        $text_y = $text_box->y1 + $full_border / 2;
        $text_w = $text_box->width() - $full_border;
        $tw = new ImagickTextWrapper(
            $image_text,
            $draw,
            $text_x,
            $text_y,
            $nowrap ? null : $text_x + $text_w
        );
        $tw->wrap_text($string);

        $actual_text_box = $tw->bounds;

        # Text (out)
        $fill = "#ccc";
        $outline = "#888";
        $draw->setFillColor($outline);
        $draw->setStrokeColor($outline);
        $draw->setStrokeWidth(($font_bold+$font_border)*$font_scale);
        $tw->draw(-1, 0);

        $draw->setFillColor($fill);
        $draw->setStrokeColor($fill);
        $draw->setStrokeWidth($font_bold*$font_scale);
        $tw->draw();

        // Combine
        $image = new Imagick();
        $image->newImage($width, $height, 'transparent');
        $image->setImageType(Imagick::IMGTYPE_TRUECOLORMATTE);
        $image->setImageColorSpace(Imagick::COLORSPACE_SRGB);
        $image->compositeImage($image_text, imagick::COMPOSITE_DEFAULT, 0, 0);

        // Bounding boxes
        $draw = new ImagickDraw();
        $draw->setStrokeWidth(1);
        $draw->setFontSize(12);

        function annotate_rect($image, $draw, $box, $text, $color, $to = 0)
        {
            $draw->setFillColor($color);
            $draw->setStrokeColor($color);

            $draw->setFillOpacity(1);
            $draw->setStrokeOpacity(0);
            $draw->setStrokeWidth(0);
            $image->annotateImage($draw, $box->x1, $box->y2 + 12 + $to, 0, "$text $box");

            $draw->setStrokeWidth(1);
            $draw->setFillOpacity(0.2);
            $draw->setStrokeOpacity(1);
            $draw->rectangle($box->x1, $box->y1, $box->x2, $box->y2);
        }

        function annotate_metrics($image, $draw, $x, $y, $m, $text, $color, $to = 0)
        {
            annotate_rect($image, $draw, $m->bounds->translated($x, $y), $text, $color, $to);

            $draw->setStrokeWidth(2);
            $draw->line(
                $m->bounds->x1 + $x,
                $y,
                $m->bounds->x1 + $m->bounds->width() + $x,
                $y
            );

            $draw->setFillColor('black');
            $draw->setStrokeColor('black');
            $draw->setFillOpacity(1);
            $draw->setStrokeOpacity(0);
            $draw->setStrokeWidth(0);
            $image->annotateImage($draw, $x, $m->baseline + $y + 12 + $to, 0, "{$m->baseline}");
        }

        annotate_rect($image, $draw, $text_box, "avail", "red");

        $tw->bounds_limit->y2 = $text_box->y2 - $full_border / 2;
        if ( $nowrap )
            $tw->bounds_limit->x2 = $text_x + $text_w;
        annotate_rect($image, $draw, $tw->bounds_limit, "effective avail", "purple", -12);

        annotate_rect($image, $draw, $actual_text_box, "used-global", "green", 24);

        foreach ( $tw->lines as $line )
        {
            annotate_rect($image, $draw, $line->bounds, "line", "#088", 12);
            $draw->line($line->bounds->x1, $line->baseline, $line->bounds->x2, $line->baseline);

            $x = $line->x1;
            $y = $line->baseline;
            $i = 0;
            foreach ( $line->char_metrics as $cm )
            {
                annotate_rect($image, $draw, $cm->bounds->translated($x, $y), $cm->char, "yellow", $i % 2 * 12);
                $i++;
                $x += $cm->right;
            }
        }

        $image->drawImage($draw);

        return $image;
    }
}

global $site;

if ( $site->settings->debug )
    $api_page = new LayoutDebugPage();


