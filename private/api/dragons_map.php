<?php

require_once(__dir__."/../lib/api/base.php");
require_once(__dir__."/../models.php");
require_once(__dir__."/../lib/forms/models.php");


class DragonsMapApiPage extends JsonBasePage
{
    public $description = "Data for the dragon map.";
    public $main_page = "/dragons/map/";

    function fetch_data($format)
    {
        global $auth;
        $is_authed = $auth->user;
        $tg_id = $is_authed && $auth->user instanceof TelegramAuthedUser ? $auth->user->user_id: null;

        $method = $_SERVER['REQUEST_METHOD'];
        if ( $method != "GET" && !$is_authed )
        {
            http_response_code(401);
            return [];
        }
        else
        {
            $input = $this->get_body();
        }

        if ( $method == "DELETE" )
        {
            DragonLocation::query()->where("telegram_id", "=", $tg_id)->delete();
            return [];
        }
        else if ( $method == "POST" || $method == "PUT" )
        {
            $marker = DragonLocation::query()->where("telegram_id", "=", $tg_id)->first();
            if ( $marker === null )
                $marker = new DragonLocation(["telegram_id"=>$tg_id]);

            $marker->icon_url = $auth->user->photo_url;
            $form = new ModelForm($marker, ["dragon", "public", "lon", "lat"], [], $input);
            if ( $form->is_valid() )
            {
                $form->save();
                return [];
            }
            else
            {
                http_response_code(400);
                return $form->get_errors();
            }
        }
        else if ( $method == "PATCH" )
        {
            $marker = DragonLocation::query()->where("telegram_id", "=", $tg_id)->first();
            if ( $marker === null )
            {
                if ( !isset($input["lon"]) || !isset($input["lat"]) )
                {
                    http_response_code(404);
                    return [];
                }
                $marker = new DragonLocation(["telegram_id"=>$tg_id]);
            }

            if ( isset($input["dragon"]) )
                $marker->dragon = (bool)$input["dragon"];

            if ( isset($input["public"]) )
                $marker->public = (bool)$input["public"];

            if ( isset($input["lon"]) )
                $marker->lon = (float)$input["lon"];

            if ( isset($input["lat"]) )
                $marker->lat = (float)$input["lat"];

            if ( isset($input["icon"]) )
                $marker->icon_url = $input["icon"];

            $marker->save();
            return $input;
        }

        $query = DragonLocation::query()->where("shadowed", "=", false);
        # TODO shadowed = false or telegram_id = $tg_id
        if ( !$is_authed )
            $query->where("public", "=", true);
        $results = [];
        foreach ( $query->select() as $dragon )
        {
            $results []= [
                "lon" => $dragon->lon,
                "lat" => $dragon->lat,
                "icon" => $dragon->icon_url,
                "current" => $tg_id === $dragon->telegram_id,
                "dragon" => $dragon->dragon,
                "public" => $dragon->public,
            ];
        }
        return $results;
    }
}

$api_page = new DragonsMapApiPage();

