<?php

require_once(__dir__."/../lib/api/base.php");


class ADRagonPage extends MemeImageBasePage
{
    public $formats = ["png", "jpg"];
    public $description = "Renders a dragon ADR sign.";
    public $params = [
        ["text", "string", "Text to show"],
    ];

    public $data = [
        "1" => ["#e77817", "#ffffff", "explosives"],
        "2" => ["#da251d", "#ffffff", "flammable"],
        "3" => ["#da251d", "#ffffff", "flammable"],
        "4" => ["#0093dd", "#ffffff", "dangerous when wet"],
        "5" => ["#f5f014", "#000000", "oxidizing"],
        "6" => ["#ffffff", "#000000", "poison"],
    ];

    function __construct()
    {
        parent::__construct();
        $this->img_path = __dir__ . "/assets/adragon/";
        $this->params["class"] = new ApiParameter("class", "string", "Type of hazard", null, "2", array_keys($this->data));
    }

    function text($text, $text_box, $image, $fgcolor)
    {
        $font_size = 64;
        $font_bold = 0;

        $draw = new ImagickDraw();
        $draw->setFont(ApiParameter::font_path("Heebo-Black-Emoji"));
        $draw->setFontSize($font_size);
        $draw->setFillColor($fgcolor);

        $font_scale = determine_font_scale(
            $image,
            $draw,
            $text,
            $text_box->width(),
            $text_box->height(),
            $font_bold,
            0,
            2
        );

        $draw->setFontSize($font_size*$font_scale);
        $tw = new ImagickTextWrapper($image, $draw, 0, 0, null);
        $tw->wrap_text($text, 0.5);
        $tw->draw(
            $text_box->x1 + $text_box->width() / 2,
            $text_box->y1 + $text_box->height() / 2 - $tw->m_metrics["textHeight"] * sizeof($tw->lines) / 4
        );
    }

    function fetch_data($format)
    {
        $input = new SimpleSvgImage();
        $input->load($this->img_path . "adr.svg");

        $class = $this->get_parameter("class");
        $text = $this->get_parameter("text");
        list($bgcolor, $fgcolor, $deftext) = $this->data[$class];
        $input->getElementById("back")->setAttribute("style", "fill: $bgcolor");
        $input->getElementById("durg")->setAttribute("style", "fill: $fgcolor");
        $input->getElementById("border_in")->setAttribute("style", "fill:none;stroke:$fgcolor;stroke-width:4.5px;stroke-linejoin:round;");
        if ( $fgcolor == "#000000" )
            $input->getElementById("border_out")->setAttribute("style", "fill:$fgcolor");
        else
            $input->getElementById("border_out")->setAttribute("style", "fill:none;");

        if ( strlen($text) == 0 )
            $text = $deftext;

        $image = $input->to_imagick();

        $xmargin = 70;
        $ymargin = 220;
        $text_box = new BoundingBox($xmargin, $ymargin, 512-$xmargin, 512-$ymargin);
        $this->text(strtoupper($text), $text_box, $image, $fgcolor);

        $xmargin = 220;
        $text_box = new BoundingBox($xmargin, 370, 512-$xmargin, 440);
        $this->text($class, $text_box, $image, $fgcolor);

        return $image;
    }
}

$api_page = new ADRagonPage();
