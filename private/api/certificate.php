<?php

require_once(__dir__."/../lib/api/base.php");

class CertificatePage extends MemeImageBasePage
{
    public $formats = ["png", "jpg", "mp4", "gif"];
    public $description = "Renders a Glax certificate.";
    public $params = [
        ["title", "string", "Title", "text", "Certificate"],
        ["text", "multiline", "Contents", "text", "Contents"],
    ];
    private $text_area_width = 360;
    private $text_area_height = 280;

    function __construct()
    {
        parent::__construct();

        $this->image_path = __dir__ . "/assets/svg/certificate.svg";
    }

    function render_text($image, $font, $font_size, $string, $y, $scale)
    {
        $width = $this->text_area_width;
        $height = $this->text_area_height - $y;
        $draw = new ImagickDraw();
        $draw->setFont(ApiParameter::font_path($font));
        $font_scale = 1;
        $draw->setFontSize($font_size);

        if ( $scale && strpos($string, "\n") === false )
        {
            $font_scale = determine_font_scale(
                $image,
                $draw,
                $string,
                $width,
                $height,
                0,
                0.5,
                1.5
            );
        }
        $draw->setFontSize($font_size*$font_scale);

        $tw = new ImagickTextWrapper($image, $draw, 0, $y, $width);
        $tw->wrap_text($string, 0.5);

        $draw->setFillColor("#726450");
        $tw->draw();

        return $tw->bounds->y2;
    }

    function fetch_data($format)
    {
        # Params
        $title = $this->get_parameter("title");
        $contents = $this->get_parameter("text");

        $image = new Imagick();
        $image->setBackgroundColor(new ImagickPixel('transparent'));
        $image->readimage($this->image_path);

        $image_text = new Imagick();
        $image_text->newImage($this->text_area_width, $this->text_area_height, 'transparent');
        $y = $this->render_text($image_text, "UnifrakturCook_Bold", 54, $title, 0, true);
        $this->render_text($image_text, "firstorderplain", 32, $contents, $y + 10, false);
        $image_text->rotateImage("transparent", 9.7);
        $image->compositeImage($image_text, imagick::COMPOSITE_DEFAULT, 30, 83);

        return $image;
    }
}

$api_page = new CertificatePage();

