<?php

require_once(__dir__."/../lib/api/base.php");

class SplitLine
{
    function __construct($space_w)
    {
        $this->space_w = $space_w;
        $this->width = 0;
        $this->words = [];
    }

    function push(TextWrapperLine $word)
    {
        $this->width += $word->right + $this->space_w;
        $this->words []= $word;
    }

    function is_empty()
    {
        return sizeof($this->words) == 0;
    }

    function move($x, $y, $width, $align=0.5)
    {
        $x += ($width - $this->width) * $align;
        foreach ( $this->words as &$metrics )
        {
            $metrics->move_x($x);
            $x = $metrics->right + $this->space_w;
            $metrics->move_y($y);
        }
    }

    function draw(Imagick $image, ImagickDraw $draw, $offset_x=0, $offset_y=0)
    {
        foreach ( $this->words as $word )
        {
            $word->draw($image, $draw, $offset_x, $offset_y);
        }
    }

    function shake($height)
    {
        $target = -$height / 2;
        foreach ( $this->words as $word )
        {
            $off = (((float)rand() / getrandmax()) * 0.8 + 0.2) * $target;
            $target = -$target;
            $word->move_y($off);
        }
    }
}

class SplitLines
{
    function __construct(Imagick $image, ImagickDraw $draw, $x, $y, $width, $line_spacing)
    {
        $this->image = $image;
        $this->draw = $draw;
        $this->x = $x;
        $this->y = $y;
        $this->width = $width;
        $m_metrics = $image->queryFontMetrics($draw, "M");
        $this->line_height = $m_metrics["textHeight"] + $line_spacing;
        $this->lines = [];
        $s_metrics = $image->queryFontMetrics($draw,  " ");
        $this->space_w = $m_metrics["originX"];
    }

    function draw($offset_x=0, $offset_y=0)
    {
        foreach ( $this->lines as $line )
            $line->draw($this->image, $this->draw, $offset_x, $offset_y);
    }

    function split_words($string, $align=0)
    {
        $lines = [];
        $ws = new WordSplitter($this->image, $this->draw);
        $ws->split_words($string);
        $line = new SplitLine($this->space_w);
        while ( $ws->words )
        {
            $word = array_shift($ws->words);
            if ( $line->width + $word->right > $this->width )
            {
                $this->push($line, $align);
                $line = new SplitLine($this->space_w);
            }
            $line->push($word);
        }

        $this->push($line, $align);

        return $lines;
    }

    function push(SplitLine $line, $align=0)
    {
        if ( $line->is_empty() )
            return;
        $line->move($this->x, $this->y, $this->width, $align);
        $this->lines []= $line;
        $this->y += $this->line_height;
    }

    function shake($height)
    {
        foreach ( $this->lines as $line )
            $line->shake($height);
    }
}


class YouWouldntPage extends MemeImageBasePage
{
    public $formats = ["png", "jpg"];
    public $description = "Renders a \"You Wouldn't X\" image.";
    public $params = [
        ["what", "string", "String to render", "text", "Use this bot"]
    ];


    function fetch_data($format)
    {
        $margin = 10;
        $margin_h = 64;
        $font_size = 64;
        $font = ApiParameter::font_path("XBAND Rough");
        $width = 512;

        $what = $this->get_parameter("what");

        $image = new Imagick();
        $draw = new ImagickDraw();
        $draw->setFont($font);
        $draw->setFontSize($font_size);

        $lines = new SplitLines($image, $draw, $margin, $margin_h, $width - $margin * 2, $margin_h);
        $lines->split_words("You Wouldn't", 0.5);
        $lines->split_words($what, 0.5);

        $image->newImage($width, $lines->y, "black");
        $draw->setFillColor("white");
        $lines->shake($margin_h / 2);
        $lines->draw();

        return $image;
    }
}

$api_page = new YouWouldntPage();
