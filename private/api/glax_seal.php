<?php

require_once(__dir__."/../lib/api/base.php");

class GlaxSealPage extends MemeImageBasePage
{
    public $formats = ["png", "jpg", "mp4", "gif"];
    public $description = "Renders a Glax sticker with text around it.";
    public $params = [
        ["text", "string", "String to render", "text", ""],
        ["bottom_text", "string", "String to render at the bottom", "text", ""],
        ["background", "color", "Image background color", null, "transparent"],
        ["font", "font", "Font to use", "name", "DejaVuEmoji"],
        ["colorize", "color", "Shift colors to match this", "color", "transparent"],
        ["width", "int", "Maximum image width", 0],
        ["spaced", "bool", "Space words to fill the circle"],
    ];

    function __construct()
    {
        parent::__construct();

        $this->face_abs_path = __dir__ . "/assets/glax_says/";
        $faces = [];
        foreach ( scandir($this->face_abs_path) as $filename )
            if ( strlen($filename) > 4 && substr($filename, -4) == ".png" )
                $faces[] = substr($filename, 0, -4);
        $this->params["face"] = new ApiParameter("face", "raw", "Face to use", null, "derp", $faces);
    }

    function create_text_picture(
        $string,

        $font,
        $font_size,
        $font_bold,
        $font_border,

        $text_height,
        $text_width
    )
    {
        # Text sizing
        $img_text = new Imagick();
        $draw = new ImagickDraw();
        $draw->setFont($font);

        $draw->setFontSize($font_size);
        $font_scale = determine_font_scale(
            $img_text,
            $draw,
            $string,
            $text_width,
            $text_height,
            $font_bold + $font_border,
            0,
            3
        );
        $draw->setFontSize($font_size*$font_scale);

        $img_text->newImage($text_width, $text_height, 'transparent');
        $img_text->setImageVirtualPixelMethod(Imagick::VIRTUALPIXELMETHOD_TRANSPARENT);
        $img_text->setImageMatte(true);

        return [$img_text, $draw, $font_scale];

    }

    function render_text(
        $string,

        $font,
        $font_size,
        $font_bold,
        $font_border,

        $line_height,
        $text_height,
        $text_width
    )
    {
        # Text
        global $palette;

        list($img_text, $draw, $font_scale) = $this->create_text_picture(
            $string,

            $font,
            $font_size,
            $font_bold,
            $font_border,

            $text_height,
            $text_width
        );

        $half_border = ($font_bold+$font_border)*$font_scale / 2;

        $tw = new ImagickTextWrapper(
            $img_text,
            $draw,
            $half_border,
            $half_border,
            null
        );
        $tw->wrap_text($string, 0, $line_height);

        # Text (out)
        $fill = $palette->iris->to_html();
        $outline = $palette->iris_dark->to_html();
        $draw->setFillColor($outline);
        $draw->setStrokeColor($outline);
        $draw->setStrokeWidth(($font_bold+$font_border)*$font_scale);
        $x = ($text_width - $tw->bounds->x2) / 2;
        $tw->draw($x-1);

        # Text (fill)
        $draw->setFillColor($fill);
        $draw->setStrokeColor($fill);
        $draw->setStrokeWidth($font_bold*$font_scale);
        $tw->draw($x);

        return $img_text;
    }

    function render_text_spaced(
        $string,

        $font,
        $font_size,
        $font_bold,
        $font_border,

        $line_height,
        $text_height,
        $text_width
    )
    {
        global $palette;

        list($img_text, $draw, $font_scale) = $this->create_text_picture(
            $string,

            $font,
            $font_size,
            $font_bold,
            $font_border,

            $text_height,
            $text_width
        );

        $ws = new WordSplitter($img_text, $draw);
        $ws->split_words($string);
        $space = $text_width;
        foreach ( $ws->words as $word )
            $space -= $word->bounds->width();
        $space /= sizeof($ws->words);

        # Text (out)
        $half_border = ($font_bold+$font_border)*$font_scale / 2;
        $fill = $palette->iris->to_html();
        $outline = $palette->iris_dark->to_html();
        $draw->setFillColor($outline);
        $draw->setStrokeColor($outline);
        $draw->setStrokeWidth($half_border * 2);


        $x = 0;
        foreach ( $ws->words as $word )
        {
            $word->draw($img_text, $draw, $x + $half_border, $half_border);
            $x += $word->bounds->width() + $space;
        }

        # Text (fill)
        $draw->setFillColor($fill);
        $draw->setStrokeColor($fill);
        $draw->setStrokeWidth($font_bold*$font_scale);

        $x = 0;
        foreach ( $ws->words as $word )
        {
            $word->draw($img_text, $draw, $x + $half_border, $half_border);
            $x += $word->bounds->width() + $space;
        }

        if ( $ws->words )
            $img_text->rollImage(-$ws->words[0]->bounds->width()/2 + $text_width / 2, 0);

        return $img_text;
    }

    function parse_args()
    {
        $params = new stdClass();

        $params->spaced = $this->get_parameter("spaced");
        $params->face = $this->get_parameter("face");
        $params->colorize = $this->get_parameter("colorize");
        $params->bottom_string = $this->get_parameter("bottom_text");
        $params->background = $this->get_parameter("background");

        $string = $this->get_parameter("text");

        while ( $string && $string[0] == "-" )
        {
            if ( unprefix($string, "--altfont") )
                $this->params["font"]->default = "Maya-Emoji";
            else  if ( unprefix($string, "--colorize") )
                parse_colorize($string, $params->colorize);
            else if ( unprefix($string, "--spaced") )
                $params->spaced = true;
            else
                break;
        }

        if ( !$string )
        {
            $string = "Glax's seal of {$params->face}";
        }
        else if ( $params->bottom_string == "" && strpos($string, "::") !== false )
        {
            list($string, $params->bottom_string) = array_map("trim", explode("::", $string, 2));
        }

        $params->string = $string;

        $params->font = $this->get_parameter("font");

        $params->scale_width = $this->get_parameter("width");
        if ( $params->scale_width == null )
            $params->scale_width = 512;

        return $params;
    }

    function fetch_data($format)
    {
        # Config
        $font_size = 48;
        $font_border = 8;
        $text_height = (int)round($font_size * 1.25);
        $line_height = 1;
        $font_bold = 0;
        $rainbow_offset = rand() / getrandmax() * 2 - 1;
        $inner_width = 512;

        # Params
        $params = $this->parse_args();

        if ( substr($params->font, -14, -4) == "Maya-Emoji" )
        {
            $font_bold = 2;
            $line_height = 0.66;
        }

        # Face sizing
        $filename = "{$this->face_abs_path}{$params->face}.png";
        $face_image = new Imagick($filename);
        $face_image->trimImage(0);
        $face_w = $face_image->getImageWidth();
        $face_h = $face_image->getImageHeight();
        $face_dw = $inner_width;
        $face_dh = (int)round($face_dw / $face_w * $face_h);
        $face_image->scaleImage($face_dw, $face_dh);
        # full_sz is so the corners of the face image fall in the middle of the text height
        # $text_height * 2 would make the coners of the image fall inside the circle
        $full_sz = (max($face_dh, $face_dw) + $text_height) * M_SQRT2;
        $face_x = ($full_sz - $face_dw) / 2;
        $face_y = ($full_sz - $face_dh) / 2;

        # Text
        global $palette;

        # Text sizing
        $top_radius = $full_sz / 2;
        $text_width = (int)round($top_radius * M_PI);

        if ( $params->spaced )
        {
            $params->string .= "\n{$params->bottom_string}";
            $params->bottom_string = "";

            $img_text = $this->render_text_spaced(
                $params->string,

                $params->font,
                $font_size,
                $font_bold,
                $font_border,

                $line_height,
                $text_height,
                floor($text_width)
            );
        }
        else
        {
            if ( $params->bottom_string )
                $text_width /= 2;

            $img_text = $this->render_text(
                $params->string,

                $params->font,
                $font_size,
                $font_bold,
                $font_border,

                $line_height,
                $text_height,
                floor($text_width)
            );

            if ( $params->bottom_string )
            {
                $bottom_img_text = $this->render_text(
                    $params->bottom_string,

                    $params->font,
                    $font_size,
                    $font_bold,
                    $font_border,

                    $line_height,
                    $text_height,
                    ceil($text_width)
                );

                $bottom_img_text->flipImage();
                $bottom_img_text->flopImage();

                $img_text->setImageExtent($text_width * 2, $text_height);
                $img_text->compositeImage($bottom_img_text, imagick::COMPOSITE_COPY, floor($text_width), 0);
                $img_text->rollImage($text_width / 2, 0);

                $text_width *= 2;
            }
        }

        # Colorize
        if ( $params->colorize->getColorValue(Imagick::COLOR_ALPHA) )
        {
            list($im_deltabri, $im_deltasat, $im_deltahue) = colorize_params(
                new ImagickPixel($palette->body_main),
                $params->colorize
            );
            $face_image->modulateImage($im_deltabri, $im_deltasat, $im_deltahue);
        }

        # Animation
        $duration = 2;
        $nframes = 1;
        $roll_offset = 0;
        if ( $this->is_animated($format) )
        {
            $nframes = 36;
            $rainbow_offset = -1/$nframes;
            $roll_offset = -360 / $nframes;
        }

        $text_rainbowizer = new Rainbowizer($text_width, $text_height, "#05f-#f5f", $rainbow_offset);

        $img_text->modulateImage(150, 100, 100);
        $text_rainbowizer->add_image($img_text);

        $image = new Imagick();
        for ( $i = 0; $i < $nframes; $i++ )
        {
            # Base Image
            $image->newImage($full_sz, $full_sz, $params->background);

            # Face
            $image->compositeImage(
                $face_image,
                imagick::COMPOSITE_DEFAULT,
                $face_x,
                $face_y
            );

            # Text
            $text_rainbowizer->gradient->rollImage($text_width * abs($rainbow_offset), 0);
            $text_rainbowizer->apply($img_text, 0, 0);

            // Text distortion
            $img_text_circle = clone $img_text;
            $img_text_circle->distortImage(
                Imagick::DISTORTION_ARC,
                [360, $i * $roll_offset, $top_radius],
                false
            );
            $image->compositeImage($img_text_circle, imagick::COMPOSITE_DEFAULT, 0, 0);

            $image->setImageDispose(2);
            $image->setImageDelay($duration * 100 / $nframes);

            if ( $params->scale_width < $full_sz )
                $image->scaleImage($params->scale_width, 0);
        }

        return $image;
    }
}

$api_page = new GlaxSealPage();
