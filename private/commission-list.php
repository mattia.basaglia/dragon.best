<?php

require_once(__dir__."/lib/pages/gallery.php");

class MediaType
{
    function __construct($media_path, $class)
    {
        $this->media_path = $media_path;
        $this->class = $class;

        $this->copy_author = "Glax";
    }

    function copy_range()
    {
        return "";
    }

    function license_object()
    {
        return License::find_license("CC BY-NC-SA");
    }

    function media_path()
    {
        return $this->media_path;
    }
}

class CommissionType
{
    public static $force_closed;

    function __construct($name, $price, $type, $example, $description, $open)
    {
        $this->name = $name;
        $this->price = $price;
        $cls = $type->class;
        $this->example = new $cls($type, $example, "", $name, "");
        $this->description = $description;
        $this->open = $open && !self::$force_closed;
    }

    function render()
    {
        echo "<div class='comm-type'>";
        echo $this->example->render("comm-preview", [], []);
        echo mkelement(["div", ["class"=>"comm-details"], [
            new HeadingAnchor("h3", $this->name),
            $this->open ?
                ["span", ["class"=> "comm-open"], [fa_icon("check"), "Available"]] :
                ["span", ["class"=> "comm-closed"], [fa_icon("times"), "Not Available"]],
            ["p", [], $this->description],
            ["p", [], ["Base price: ", ["span", ["class"=>"comm-price"], new HtmlString("{$this->price}&euro;")]]],
        ]]);
        echo "</div>";
    }

}

function commission_types()
{
    $lottie = new MediaType("/media/img/lottie/", "LottieImageInfo");
    $misc = new MediaType("/media/img/pages/commissions/", "ImageInfo");

    CommissionType::$force_closed = false;

    $br = new SimpleElement("br");

    return [
        new CommissionType(
            "Telegram Sticker (Simple)",
            15,
            $misc,
            "meeesh-mace.png",
            "Telegram sticker with simple contents (eg: Headshots, simple objects, etc)",
            false
        ),
        new CommissionType(
            "Telegram Sticker (Complex)",
            30,
            $misc,
            "zeebdeer.png",
            "Telegram stickers but with more complex subjects (eg: full body, multiple characters, etc)",
            false
        ),
        new CommissionType(
            "Animated Sticker (Simple)",
            20,
            $lottie,
            "commissions/hondra-frills.json",
            "Simple animation (up to 3 seconds) with simple movements and subject matter.",
            false
        ),
        new CommissionType(
            "Animated Sticker (Complex)",
            40,
            $lottie,
            "commissions/mipsy-float.json",
            "More complex animations (up to 3 seconds) with more advanced effects, fullbody characters, etc.",
            false
        ),
        new CommissionType(
            "Telegram Bot",
            60,
            $lottie,
            "codeterm.json",
            [
                "You get your own bot on telegram to generate images, animations, or other things.", $br,
                "For some examples of my bots see ", new Link("/telegram#bots", "the list of my own bots"), ".", $br,
            ],
            false
        ),
    ];
}
