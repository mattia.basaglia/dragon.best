<?php
require(__dir__ . '/vendor/autoload.php');
set_include_path(get_include_path() . PATH_SEPARATOR . __dir__."/vendor/simpletest/");

require_once('simpletest/autorun.php');
require_once('simpletest/web_tester.php');


class TestLoadPages extends WebTestCase
{
    public $paths = [
        "/",
        "/refs/",
        "/refs/palette/",
        "/ascii/",
        "/color/",
        "/cute/",
        "/fluff/",
        "/license/",
        "/media/",
        "/vectors/",
    ];

    function __construct($label = false)
    {
        parent::__construct($label);
        $this->base = getenv("TEST_HOST");
        if ( !$this->base )
            $this->base = "https://dragon.best";
        echo "Testing {$this->base}\n";
    }

    function test_get_200()
    {
        foreach ( $this->paths as $path )
        {
            $this->get("{$this->base}$path");
            $this->assertResponse(200, "$path %s");
        }
    }

    function test_redirects()
    {
        foreach ( $this->paths as $path )
        {
            if ( $path == "/" )
                continue;
            $path = rtrim("{$this->base}$path", "/");
            $this->setMaximumRedirects(0);
            $this->get($path);
            $this->assertResponse(301, "$path %s");
            $this->assertText("Redurgecting");
        }
    }
}
