<?php

require_once(__dir__."/init.php");

global $site;
$pdo = $site->connect_db()->pdo;


return [
    "environments" => [
        "default_database" => "development",
        "development" => [
            "connection" => $pdo,
            "name" => "dragon_best",
        ]
    ],
    "paths" => [
        "migrations" => __dir__ . "/db/migrations"
    ]
];
