<?php
class FurryEvent
{
    public static $icons = [
        "Furry" => "🐺",
        "Comic" => "🦸",
        "MLP" => "🦄",
        "Board Game" => "♟️",
        "Fantasy" => "🧚",
        "Convention" => "🏨",
        "Event" => "🎉",
        "Camping" => "⛺️",
        "Meet" => "🗓",
        "Considering" => "💭",
        "Looking for Roomie" => "🔎",
        "Waiting List" => "⏳",
        "Registered" => "✅",
        "Attended" => "☑️",
        "Fursuiter1" => "🐲1️⃣",
        "Fursuiter2" => "🐲2️⃣",
        "Fullsuiter2" => "🐉2️⃣",
        "Staff" => "🈺",
        "Sponsor" => "💸",
    ];

    public static $fursuits = [
        "Fursuiter1",
        "Fursuiter2",
        "Fullsuiter2",
    ];

    public static $alias = [
        "Sponsor" => "Sponsor, Supersponsor, or similar",
        "Fursuiter1" => "Fursuiter (v1 Partial)",
        "Fursuiter2" => "Fursuiter (v2 Partial)",
        "Fullsuiter2" => "Fursuiter (v2 Full)",
        "Staff" => "Staff, Crew, Volunteer, etc"
    ];

    public static $lon_lat = [
        "Baarlo" => [6.0967255, 51.3319545],
        "Berlin" => [13.4105, 52.5244],
        "Birmingham" => [-1.9026911, 52.4796992],
        "Bristol" => [-2.5790334025708415, 51.47091115],
        "Brussels" => [4.3488, 50.8504],
        "Brussels" => [4.3488, 50.8504],
        "Cavalese" => [11.4594, 46.2915],
        "Combe Martin" => [-4.006006739519693, 51.203165049999996],
        "Croydon" => [-0.1, 51.3833],
        "Delft" => [4.3556, 52.0067],
        "Deurne" => [5.7915, 51.4612],
        "Essen" => [7.0158171, 51.4582235],
        "Glasgow" => [-4.2488787, 55.8609825],
        "Groesbeek" => [5.9167, 51.7667],
        "Haarlem" => [4.643559696559635, 52.383705750000004],
        "Hauenstein" => [13.019183, 50.3468696],
        "Köln" => [6.95, 50.9333],
        "Krásný Les" => [13.9352, 50.7677],
        "Livingston" => [-3.5149461, 55.8831932],
        "London" => [-0.1276474, 51.5073219],
        "Ludwigsburg" => [9.1895147, 48.8953937],
        "Lyon" => [4.5833, 45.75],
        "Malmö" => [13.0001566, 55.6052931],
        "Manchester" => [-2.2451148, 53.4794892],
        "Marl" => [7.0833, 51.65],
        "Prague" => [14.446459273258009, 50.0596288],
        "Preston" => [-2.7167, 53.7667],
        "Reutlingen" => [9.2043, 48.4914],
        "San Marino" => [12.4167, 43.9333],
        "Southampton" => [-1.4043, 50.904],
        "Suhl" => [10.7, 50.6],
        "Upplands Väsby" => [17.9113, 59.5184],
        "Uusimaa" => [22.5763464, 60.737775],
        "Zoetermeer" => [4.4931, 52.0575],
        "Antwerp" => [4.4035, 51.2199],
        "Hamburg" => [10, 53.55],
        "Gdańsk" => [18.6464, 54.3521],
        "Alicante" => [-0.4815, 38.3452],
        "Hasliberg" => [8.2078, 46.7332],
        "Vienna" => [16.3721, 48.2085],
        "Rotterdam" => [4.4792, 51.9225],
        "Haarzuilens" => [4.9981, 52.121],
        "Ankaran" => [13.7361, 45.5786],
        "Pordenone" => [12.65, 45.95],
        "Egmond aan Zee" => [4.6271, 52.6204],
        "Eindhoven" => [5.4667, 51.4333],
        "Ghent" => [3.7167, 51.05],
    ];

    public static $brand_colors = [
        "BUCK LDN" => "#bf1f4e",
        "BUCK" => "#bf1f4e",
        "Galacon" => "#6d4b44",
        "BronyScot" => "#0ec3bf",
        "Londonfurs" => "#d60f00",
        "LFM Summer Party" => "#d60f00",
        "LFM Winter Party" => "#d60f00",
        "Hearth's Warming Con" => "#5595aa",
        "JFTW" => "#32124e",
        "Eurofurence" => "#24615c",
        "Flüüfff" => "#81542a",
        "Nordic FuzzCon" => "#64b3e8",
        "Reffurence" => "#de4f25",
    ];

    public $year, $date_string, $date_start, $duration, $date_end, $name, $url, $city, $country, $status, $tags, $type, $special, $suit, $color, $date_announced, $topic;

    function __construct($name, $year, $month, $day, $duration, $city, $country, $status, $topic, $type, $special, $url)
    {
        $this->year = abs($year);
        $this->date_string = sprintf("%04d-%02d-%02d", $this->year, $month, $day);
        $this->date_start = new DateTimeImmutable($this->date_string);
        $this->duration = $duration;
        $duration -= 1;
        $this->date_end = $this->date_start->add(new DateInterval("P{$duration}D"));
        $this->name = $name;
        $this->url = $url;
        $this->city = $city;
        $this->country = $country;
        $this->status = is_array($status) ? $status : [$status];
        $this->topic = $topic;
        $this->type = $type;
        $this->tags = [$topic, $type];
        $this->special = $special;
        $this->suit = null;
        $this->color = self::$brand_colors[$this->name] ?? $this->auto_color();
        $this->date_announced = $year > 0;
        foreach ( static::$fursuits as $fs )
        {
            $key = array_search($fs, $this->special);
            if ( $key !== false )
            {
                $this->suit = $fs;
                unset($this->special[$key]);
            }
        }
    }

    function auto_color()
    {
        $color_hash = 0;
        foreach ( str_split($this->name) as $ch )
            $color_hash += ord($ch);
        $color_count = 6;
        $hue = 360 / $color_count * ($color_hash % $color_count);
        return "hsl($hue, 70%, 70%)";

    }

    function date_range_string($format = "l, F jS Y", $separator = " to ")
    {
        return $this->date_start->format("l, F jS Y") . $separator . $this->date_end->format("l, F jS Y");
    }

    function full_date_range()
    {
        $day = $this->date_start;
        $delta = new DateInterval("P1D");
        $dates = [];
        for ( $i = 0; $i < $this->duration; $i++ )
        {
            $dates[] = $day;
            $day = $day->add($delta);
        }

        return $dates;
    }

    function to_html($current_date)
    {
        $cells = [];

        $cells[] = $this->url ? [["a", ["href" => $this->url], $this->name]] :  $this->name;
        $cells[] = [["span", ["title" => $this->date_range_string()], $this->date_start->format("M")]];
        $country = $this->full_flag();
        $cells[] = [["span", ["class" => "emoji"], $country], " " . $this->city];
        $cells[] = array_map([$this, "type_icon"], $this->status, [$current_date]);
        $cells[] = array_map([$this, "type_icon"], $this->tags);
        $cells[] = array_map([$this, "type_icon"], $this->special);
        if ( $this->suit )
            $cells[sizeof($cells)-1][] = $this->type_icon($this->suit, null, " suit");

        $attrs = [
            "class" => implode(" ", array_map(function($item) { return "type-" . strtolower($item); }, $this->tags)),
        ];

        return mkelement(["tr", $attrs, array_map(
            function($cell) { return ["td", [], $cell]; },
            $cells
        )]);
    }

    function full_flag()
    {
        return static::emoji_flag($this->country[0]) . static::emoji_flag($this->country[1]);
    }

    static function emoji_flag($letter)
    {
        // 65 is 'A'
        return mb_chr(0x1F1E6 + ord($letter) - 65, "utf-8");
    }

    function type_icon($what, $current_date = "", $extra_class = "")
    {
        if ( $what == "Confirmed" )
            $what = $this->date_string <= $current_date ? "Attended" : "Registered";
        return mkelement(["span", ["title" => static::icon_title($what), "class" => "emoji$extra_class"], static::$icons[$what]]);
    }

    static function icon_title($name)
    {
        return static::$alias[$name] ?? $name;
    }

    function skip($filter)
    {
        foreach ( $this->tags as $type )
        {
            if ( !($filter[$type] ?? true) )
            {
                return true;
            }
        }

        return false;
    }
}

global $convention_list;

$convention_list = [//                      yyyy  mm  dd dur city          country status       topic       type          role                        url
    new FurryEvent("BUCK LDN",              2015, 11, 28, 1, "London",      "GB", "Confirmed", "MLP",       "Event",       [],                         "https://mlpfanart.fandom.com/wiki/BUCK#BUCK_LDN_.28November_28.2C_2015.29"),

    new FurryEvent("BUCK",                  2016, 04, 9,  2, "Manchester",  "GB", "Confirmed", "MLP",       "Convention",  ["Sponsor"],                "https://mlpfanart.fandom.com/wiki/BUCK#BUCK_2016_.28April_9.E2.80.9310.2C_2016.29"),
    new FurryEvent("Galacon",               2016, 07, 30, 2, "Ludwigsburg", "DE", "Confirmed", "MLP",       "Convention",  ["Sponsor"],                "https://www.galacon.eu/"),
    new FurryEvent("BronyScot",             2016, 10, 19, 3, "Glasgow",     "GB", "Confirmed", "MLP",       "Convention",  ["Sponsor"],                "https://twitter.com/BronyScot"),
    new FurryEvent("Lancashire Bronies",    2016, 12, 17, 1, "Preston",     "GB", "Confirmed", "MLP",       "Meet",        [],                         "https://ukofequestria.co.uk/threads/preston-surrounding-area-meets.17559/"),

    new FurryEvent("Lancashire Bronies",    2017, 01, 21, 1, "Preston",     "GB", "Confirmed", "MLP",       "Meet",        [],                         "https://ukofequestria.co.uk/threads/preston-surrounding-area-meets.17559/"),
    new FurryEvent("Lancashire Bronies",    2017, 03, 25, 1, "Preston",     "GB", "Confirmed", "MLP",       "Meet",        [],                         "https://ukofequestria.co.uk/threads/preston-surrounding-area-meets.17559/"),
    new FurryEvent("Lancashire Bronies",    2017, 05, 20, 1, "Preston",     "GB", "Confirmed", "MLP",       "Meet",        [],                         "https://ukofequestria.co.uk/threads/preston-surrounding-area-meets.17559/"),
    new FurryEvent("Londonfurs",            2017, 06, 03, 1, "London",      "GB", "Confirmed", "Furry",     "Meet",        [],                         "https://www.londonfurs.org.uk/"),
    new FurryEvent("Lancashire Bronies",    2017, 06, 17, 1, "Preston",     "GB", "Confirmed", "MLP",       "Meet",        [],                         "https://ukofequestria.co.uk/threads/preston-surrounding-area-meets.17559/"),
    new FurryEvent("Londonfurs",            2017, 06, 24, 1, "London",      "GB", "Confirmed", "Furry",     "Meet",        [],                         "https://www.londonfurs.org.uk/"),
    new FurryEvent("LFM Summer Party",      2017, 07, 14, 2, "London",      "GB", "Confirmed", "Furry",     "Event",       [],                         "https://www.londonfurs.org.uk/"),
    new FurryEvent("Lancashire Bronies",    2017, 07, 22, 1, "Preston",     "GB", "Confirmed", "MLP",       "Meet",        [],                         "https://ukofequestria.co.uk/threads/preston-surrounding-area-meets.17559/"),
    new FurryEvent("Galacon",               2017, 07, 28, 2, "Ludwigsburg", "DE", "Confirmed", "MLP",       "Convention",  ["Sponsor"],                "https://www.galacon.eu/"),
    new FurryEvent("Londonfurs",            2017,  8,  5, 1, "London",      "GB", "Confirmed", "Furry",     "Meet",        [],                         "https://www.londonfurs.org.uk/"),
    new FurryEvent("Czequestria",           2017,  8, 18, 3, "Prague",      "CZ", "Confirmed", "MLP",       "Convention",  ["Sponsor"],                "https://www.czequestria.cz/"),
    new FurryEvent("Londonfurs",            2017,  9, 16, 1, "London",      "GB", "Confirmed", "Furry",     "Meet",        [],                         "https://www.londonfurs.org.uk/"),
    new FurryEvent("Londonfurs",            2017, 10, 07, 1, "London",      "GB", "Confirmed", "Furry",     "Meet",        [],                         "https://www.londonfurs.org.uk/"),
    new FurryEvent("UK Ponycon",            2017, 10, 22, 2, "Bristol",     "GB", "Confirmed", "MLP",       "Convention",  ["Sponsor"],                "https://www.ukponycon.co.uk/"),
    new FurryEvent("Londonfurs",            2017, 10, 28, 1, "London",      "GB", "Confirmed", "Furry",     "Meet",        [],                         "https://www.londonfurs.org.uk/"),
    new FurryEvent("Londonfurs",            2017, 11, 18, 1, "London",      "GB", "Confirmed", "Furry",     "Meet",        [],                         "https://www.londonfurs.org.uk/"),
    new FurryEvent("BronyScot",             2017, 11, 25, 3, "Glasgow",     "GB", "Confirmed", "MLP",       "Convention",  ["Sponsor"],                "https://twitter.com/BronyScot"),
    new FurryEvent("LFM Winter Party",      2017, 12,  9, 1, "London",      "GB", "Confirmed", "Furry",     "Event",       [],                         "https://www.londonfurs.org.uk/"),
    new FurryEvent("Lancashire Bronies",    2017, 12, 16, 1, "Preston",     "GB", "Confirmed", "MLP",       "Meet",        [],                         "https://ukofequestria.co.uk/threads/preston-surrounding-area-meets.17559/"),

    new FurryEvent("Londonfurs",            2018, 01, 13, 1, "London",      "GB", "Confirmed", "Furry",     "Meet",        [],                         "https://www.londonfurs.org.uk/"),
    new FurryEvent("Lancashire Bronies",    2018, 01, 20, 1, "Preston",     "GB", "Confirmed", "MLP",       "Meet",        [],                         "https://ukofequestria.co.uk/threads/preston-surrounding-area-meets.17559/"),
    new FurryEvent("Londonfurs",            2018, 02, 03, 1, "London",      "GB", "Confirmed", "Furry",     "Meet",        [],                         "https://www.londonfurs.org.uk/"),
    new FurryEvent("Hearth's Warming Con",  2018, 02, 16, 3, "Haarlem",     "NL", "Confirmed", "MLP",       "Convention",  ["Sponsor"],                "http://www.hwcon.nl/"),
    new FurryEvent("Londonfurs",            2018, 02, 24, 1, "London",      "GB", "Confirmed", "Furry",     "Meet",        [],                         "https://www.londonfurs.org.uk/"),
    new FurryEvent("Londonfurs",            2018, 03, 10, 1, "London",      "GB", "Confirmed", "Furry",     "Meet",        [],                         "https://www.londonfurs.org.uk/"),
    new FurryEvent("Lancashire Bronies",    2018, 03, 24, 1, "Preston",     "GB", "Confirmed", "MLP",       "Meet",        [],                         "https://ukofequestria.co.uk/threads/preston-surrounding-area-meets.17559/"),
    new FurryEvent("JFTW",                  2018, 03, 30, 3, "Bristol",     "GB", "Confirmed", "Furry",     "Convention",  ["Sponsor"],                "https://jftw.org/"),
    new FurryEvent("Londonfurs",            2018, 04,  7, 1, "London",      "GB", "Confirmed", "Furry",     "Meet",        [],                         "https://www.londonfurs.org.uk/"),
    new FurryEvent("Londonfurs",            2018, 04, 28, 1, "London",      "GB", "Confirmed", "Furry",     "Meet",        [],                         "https://www.londonfurs.org.uk/"),
    new FurryEvent("Griffish Isles",        2018, 05, 18, 2, "Manchester",  "GB", "Confirmed", "MLP",       "Convention",  ["Staff"],                  "https://www.griffishisles.uk/"),
    new FurryEvent("ConFuzzled",            2018, 05, 25, 5, "Birmingham",  "GB", "Confirmed", "Furry",     "Convention",  ["Sponsor"],                "https://confuzzled.org.uk/"),
    new FurryEvent("Lancashire Bronies",    2018, 06, 16, 1, "Preston",     "GB", "Confirmed", "MLP",       "Meet",        [],                         "https://ukofequestria.co.uk/threads/preston-surrounding-area-meets.17559/"),
    new FurryEvent("Londonfurs",            2018, 06, 30, 1, "London",      "GB", "Confirmed", "Furry",     "Meet",        [],                         "https://www.londonfurs.org.uk/"),
    new FurryEvent("LFM Summer Party",      2018, 07, 20, 2, "London",      "GB", "Confirmed", "Furry",     "Event",       [],                         "https://www.londonfurs.org.uk/"),
    new FurryEvent("Londonfurs",            2018,  8, 11, 1, "London",      "GB", "Confirmed", "Furry",     "Meet",        ["Fursuiter1"],             "https://www.londonfurs.org.uk/"),
    new FurryEvent("Lancashire Bronies",    2018,  8, 18, 1, "Preston",     "GB", "Confirmed", "MLP",       "Meet",        [],                         "https://ukofequestria.co.uk/threads/preston-surrounding-area-meets.17559/"),
    new FurryEvent("Sotonfurs Summer Pary", 2018,  8, 25, 1, "Southampton", "GB", "Confirmed", "Furry",     "Event",       ["Fursuiter1"],             "https://twitter.com/SotonFurMeets"),
    new FurryEvent("Londonfurs",            2018,  9, 01, 1, "London",      "GB", "Confirmed", "Furry",     "Meet",        ["Fursuiter1"],             "https://www.londonfurs.org.uk/"),
    new FurryEvent("Sotonfurs",             2018,  9,  8, 1, "Southampton", "GB", "Confirmed", "Furry",     "Meet",        ["Fursuiter1"],             "https://twitter.com/SotonFurMeets"),
    new FurryEvent("Londonfurs",            2018,  9, 22, 1, "London",      "GB", "Confirmed", "Furry",     "Meet",        ["Fursuiter1"],             "https://www.londonfurs.org.uk/"),
    new FurryEvent("Lancashire Bronies",    2018,  9, 29, 1, "Preston",     "GB", "Confirmed", "MLP",       "Meet",        [],                         "https://ukofequestria.co.uk/threads/preston-surrounding-area-meets.17559/"),
    new FurryEvent("Furcation",             2018, 10, 11, 6, "Combe Martin","GB", "Confirmed", "Furry",     "Camping",     ["Sponsor", "Fursuiter1"],  "https://www.furcation.org.uk/"),
    new FurryEvent("BronyScot",             2018, 11,  1, 3, "Glasgow",     "GB", "Confirmed", "MLP",       "Convention",  ["Fursuiter1"],             "https://twitter.com/BronyScot"),
    new FurryEvent("ScotiaCon",             2018, 11,  9, 4, "Livingston",  "GB", "Confirmed", "Furry",     "Convention",  ["Sponsor", "Fursuiter1"],  "https://www.scotiacon.org.uk/"),
    new FurryEvent("Londonfurs",            2018, 11, 24, 1, "London",      "GB", "Confirmed", "Furry",     "Meet",        ["Fursuiter1"],             "https://www.londonfurs.org.uk/"),
    new FurryEvent("Lancashire Bronies",    2018, 12, 15, 1, "Preston",     "GB", "Confirmed", "MLP",       "Meet",        [],                         "https://ukofequestria.co.uk/threads/preston-surrounding-area-meets.17559/"),
    new FurryEvent("LFM Winter Party",      2018, 12,  8, 1, "London",      "GB", "Confirmed", "Furry",     "Event",       ["Fursuiter1"],             "https://www.londonfurs.org.uk/"),

    new FurryEvent("Londonfurs",            2019, 01, 12, 1, "London",      "GB", "Confirmed", "Furry",     "Meet",        ["Fursuiter1"],             "https://www.londonfurs.org.uk/"),
    new FurryEvent("Londonfurs",            2019, 02, 02, 1, "London",      "GB", "Confirmed", "Furry",     "Meet",        ["Fursuiter1"],             "https://www.londonfurs.org.uk/"),
    new FurryEvent("Barkade",               2019, 02, 23, 1, "Croydon",     "GB", "Confirmed", "Furry",     "Meet",        ["Fursuiter1"],             "http://www.barkade.org/"),
    new FurryEvent("Nordic FuzzCon",        2019, 02, 27, 5,"Upplands Väsby","SE","Confirmed", "Furry",     "Convention",  ["Sponsor", "Fursuiter1"],  "https://www.nordicfuzzcon.org/"),
    new FurryEvent("JFTW",                  2019, 04, 19, 3, "Bristol",     "GB", "Confirmed", "Furry",     "Convention",  ["Fursuiter1"],             "https://jftw.org/"),
    new FurryEvent("ConFuzzled",            2019, 05, 23, 7, "Birmingham",  "GB", "Confirmed", "Furry",     "Convention",  ["Sponsor", "Fursuiter1"],  "https://confuzzled.org.uk/"),
    new FurryEvent("FurSplashBowling",      2019,  7, 19, 3, "San Marino",  "SM", "Confirmed", "Furry",     "Camping",     ["Fursuiter1"],             "https://twitter.com/RSMFurmeet"),
    new FurryEvent("Eurofurence",           2019,  8, 13, 7, "Berlin",      "DE", "Confirmed", "Furry",     "Convention",  ["Sponsor", "Fursuiter1"],  "https://www.eurofurence.org/"),
    new FurryEvent("FinFur Animus",         2019, 10,  9, 6, "Uusimaa",     "FI", "Confirmed", "Furry",     "Convention",  ["Sponsor", "Fursuiter1"],  "https://animus.finfur.net/"),

    new FurryEvent("Nordic FuzzCon",        2020, 02, 18, 6, "Malmö",       "SE", "Confirmed", "Furry",     "Convention",  ["Fursuiter1"],             "https://www.nordicfuzzcon.org/"),
    new FurryEvent("Hearth's Warming Con",  2020, 02, 28, 1, "Haarlem",     "NL", "Confirmed", "MLP",       "Convention",  [],                         "http://www.hwcon.nl/"),

    new FurryEvent("Flüüfff",               2021, 10, 31, 6, "Brussels",    "BE", "Confirmed", "Furry",     "Convention",  ["Fursuiter2"],             "https://fluufff.org/"),

    new FurryEvent("Furry Weekend Holland", 2022, 04, 29, 4, "Baarlo",      "NL", "Confirmed", "Furry",     "Convention",  ["Fullsuiter2"],            "https://www.furryweekend.nl/"),
    new FurryEvent("Unifursity",            2022, 05, 14, 1, "Zoetermeer",  "NL", "Confirmed", "Furry",     "Event",       ["Fullsuiter2"],            "https://unifursity.nl/"),
    new FurryEvent("Furizon",               2022, 05, 28, 6, "Cavalese",    "IT", "Confirmed", "Furry",     "Convention",  ["Fullsuiter2"],            "https://furizon.net/"),
    new FurryEvent("Dutch Furcon",          2022, 07, 15, 4, "Groesbeek",   "NL", "Confirmed", "Furry",     "Convention",  ["Fullsuiter2"],            "https://dutchfurcon.com/"),
    new FurryEvent("EAST",                  2022, 07, 20, 4, "Suhl",        "DE", "Confirmed", "Furry",     "Convention",  ["Sponsor", "Fullsuiter2"], "https://www.sachsenfurs.de/en/east"),
    new FurryEvent("Eurofurence",           2022,  8, 24, 5, "Berlin",      "DE", "Confirmed", "Furry",     "Convention",  ["Fullsuiter2"],            "https://www.eurofurence.org/"),
    new FurryEvent("The Alfurnative",       2022,  9, 30, 3, "Delft",       "NL", "Confirmed", "Furry",     "Convention",  ["Sponsor", "Fullsuiter2"], "https://t.me/Alfurnative"),
    new FurryEvent("SPIEL",                 2022, 10,  9, 1, "Essen",       "DE", "Confirmed", "Board Game","Convention",  [],                         "https://www.spiel-messe.com/en/"),
    new FurryEvent("AnthroNRW",             2022, 11, 06, 1, "Marl",        "DE", "Confirmed", "Furry",     "Meet",        ["Fursuiter2"],             "https://anthro.nrw/"),
    new FurryEvent("Flüüfff",               2022, 11,  9, 7, "Brussels",    "BE", "Confirmed", "Furry",     "Convention",  ["Fullsuiter2", "Staff"],   "https://fluufff.org/"),
    new FurryEvent("Furvent",               2022, 12, 17, 2, "Köln",        "DE", "Confirmed", "Furry",     "Meet",        ["Fursuiter2"],             "https://www.furvent.eu/"),

    new FurryEvent("Nordic FuzzCon",        2023, 02, 21, 7, "Malmö",       "SE", "Confirmed", "Furry",     "Convention",  ["Fullsuiter2"],            "https://www.nordicfuzzcon.org/"),
    new FurryEvent("Furry Weekend Holland", 2023, 05, 18, 4, "Baarlo",      "NL", "Confirmed", "Furry",     "Convention",  ["Fursuiter2"],             "https://www.furryweekend.nl/"),
    new FurryEvent("Furizon",               2023, 05, 30, 4, "Cavalese",    "IT", "Confirmed", "Furry",     "Convention",  ["Fursuiter2"],             "https://furizon.net/"),
    new FurryEvent("Dutch Furcon",          2023, 07, 14, 4, "Groesbeek",   "NL", "Confirmed", "Furry",     "Convention",  ["Fursuiter2"],             "https://dutchfurcon.com/"),
    new FurryEvent("Reffurence",            2023,  8,  2, 5, "Antwerp",     "BE", "Confirmed", "Furry",     "Convention",  ["Fursuiter2", "Staff"],    "https://reffurence.com/"),
    new FurryEvent("Eurofurence",           2023,  9,  2, 5, "Hamburg",     "DE", "Confirmed", "Furry",     "Convention",  ["Fullsuiter2"],            "https://www.eurofurence.org/"),
    new FurryEvent("SPIEL",                 2023, 10,  5, 4, "Essen",       "DE", "Confirmed", "Board Game","Convention",  [],                         "https://www.spiel-messe.com/en/"),
    new FurryEvent("Golden Leaves Con",     2023, 10, 25, 4, "Hasliberg",   "CH", "Confirmed", "Furry",     "Convention",  ["Fursuiter2"],             "https://glc.furry.ch/"),
    new FurryEvent("Furvester",             2023, 12, 27, 6, "Reutlingen",  "DE", "Confirmed", "Furry",     "Convention",  ["Fursuiter2"],             "https://furvester.org/"),

    new FurryEvent("Nordic FuzzCon",        2024, 02, 20, 7, "Malmö",       "SE", "Confirmed", "Furry",     "Convention",  ["Staff", "Fursuiter2"],    "https://www.nordicfuzzcon.org/"),
    new FurryEvent("Elfia",                 2024, 04, 22, 2, "Haarzuilens", "NL", "Confirmed", "Fantasy",   "Convention",  ["Fursuiter2"],             "https://www.elfia.com/en/"),
    new FurryEvent("Furry Weekend Holland", 2024, 05,  2, 5, "Baarlo",      "NL", "Confirmed", "Furry",     "Convention",  ["Fursuiter2"],             "https://www.furryweekend.nl/"),
    new FurryEvent("Furizon",               2024, 06,  3, 6, "Cavalese",    "IT", "Confirmed", "Furry",     "Convention",  ["Fursuiter2"],             "https://furizon.net/"),
    new FurryEvent("Awoostria",             2024, 07, 24, 5, "Vienna",      "AT", "Confirmed", "Furry",     "Convention",  ["Fursuiter2", "Staff"],    "https://awoostria.at/"),
    new FurryEvent("VOX Cafe 54",           2024,  8, 12, 1, "Eindhoven",   "NL", "Confirmed", "Furry",     "Meet",        ["Fursuiter2"],             "https://voxevents.org"),
    new FurryEvent("Dutch Furcon",          2024,  8, 23, 4, "Groesbeek",   "NL", "Confirmed", "Furry",     "Convention",  ["Fursuiter2", "Sponsor"],  "https://dutchfurcon.com/"),
    new FurryEvent("Eurofurence",           2024,  9, 18, 4, "Hamburg",     "DE", "Confirmed", "Furry",     "Convention",  ["Fursuiter2"],             "https://www.eurofurence.org/"),
    new FurryEvent("SPIEL",                 2024, 10,  3, 4, "Essen",       "DE", "Confirmed", "Board Game","Convention",  [],                         "https://www.spiel-messe.com/en/"),
    new FurryEvent("Reffurence",            2024, 10, 23, 6, "Rotterdam",   "NL", "Confirmed", "Furry",     "Convention",  ["Fursuiter2", "Staff"],    "https://reffurence.com/"),
    new FurryEvent("Facts",                 2024, 11,  3, 1, "Ghent",       "BE", "Confirmed", "Comic",     "Convention",  ["Fursuiter2"],             "https://www.facts.be/en/"),
    new FurryEvent("Golden Horn",           2024, 11,  7, 3, "Ankaran",     "SI", "Confirmed", "Furry",     "Convention",  ["Fursuiter2"],             "https://slofurs.org/events/9bdac5d7-48b6-4ea4-8462-0202f79a4b72"),
    new FurryEvent("Games & Co",            2024, 11, 16, 2, "Pordenone",   "IT", "Considering", "Comic",   "Convention",  ["Fursuiter2"],             "https://www.gamesandco.net/"),
    new FurryEvent("Furvester",             2024, 12, 29, 5, "Reutlingen",  "DE", "Confirmed", "Furry",     "Convention",  ["Fursuiter2"],             "https://furvester.org/"),

    new FurryEvent("Nordic FuzzCon",        2025, 02, 18, 7, "Malmö",       "SE", "Confirmed", "Furry",     "Convention",  ["Staff", "Fursuiter2"],    "https://www.nordicfuzzcon.org/"),
    new FurryEvent("Furry Weekend Holland", 2025, 04, 24, 5, "Baarlo",      "NL", "Confirmed", "Furry",   "Convention",  ["Fursuiter2"],             "https://www.furryweekend.nl/"),
    new FurryEvent("Awoostria",             2025, 07, 24, 4, "Vienna",      "AT", "Confirmed", "Furry",     "Convention",  ["Fursuiter2", "Staff"],    "https://awoostria.at/"),
    new FurryEvent("Dutch Furcon",          2025,  8,  8, 4, "Groesbeek",   "NL", "Confirmed", "Furry",     "Convention",  ["Fursuiter2", "Sponsor"],  "https://dutchfurcon.com/"),
    new FurryEvent("Eurofurence",           2025,  9,  2, 5, "Hamburg",     "DE", "Confirmed", "Furry",     "Convention",  ["Fursuiter2"],             "https://www.eurofurence.org/"),
    new FurryEvent("Reffurence",            2025, 10, 29, 5, "Egmond aan Zee","NL","Considering", "Furry",  "Convention",  ["Fursuiter2", "Staff"],    "https://reffurence.com/"),

    /*
    NOTE: Remember to add the coordinates of the city:

        private/tools/get_lonlat.php

    Tags
    Status:
        Considering
        Looking for Roomie
        Waiting List
        Confirmed
    Type:
        Furry
        MLP
        Fantasy
        Board Game
        Convention
        Event
        Camping
    Roles:
        Fursuiter1
        Fursuiter2
        Fullsuiter2
        Staff
        Sponsor
    */

];

