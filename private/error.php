<?php
require_once(__dir__."/dragon.php");
require_once(__dir__."/lib/pages/error.php");


class ErrorPage extends DurgPage
{
    use ErrorTrait;
    public $title = "Oh noes!";

    function main($render_args)
    {
        $code = http_response_code();
        $path = $render_args["path"];
        echo "<h1>The page ". escape($path) . " had some issue D:</h1>";
        echo "<p>". escape($code) . " dragons are crying because of this.</p>";

        global $site;
        if ( $site->settings->debug )
        {
            echo "<h2>Details</h2><ul style='white-space: pre-wrap;'>";
            foreach ( $site->php_errors as $error )
            {
                echo "<li>". escape($error) . "</li>";
            }
            echo "</ul>";
        }
        echo mkelement(["img", [
                "src"=>href("/media/img/vectors/blargh.svg"),
                "alt"=>"a dragon vomiting rainbows",
                "style"=>"max-width: 512px; width:100%;"
            ]]
        );
    }
}

$page = new ErrorPage();
