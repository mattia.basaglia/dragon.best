<?php

require_once(__dir__."/lib/html.php");

class DragonPoints extends PageWidget
{
    function __construct($points, $description, $picture=null)
    {
        $this->points = $points;
        $this->picture = $picture;
        $this->description = $description;
    }

    function element()
    {
        $inner = [["p", [], $this->description]];
        if ( $this->picture )
            array_unshift($inner, mkelement(["img", [
                "src"=>href($this->picture),
                "alt"=>"{$this->points} points right there",
            ]]));

        return mkelement(
            ["h3", [], "{$this->points} points"],
            ["div", ["class"=>"points-description"], $inner]
        );
    }

}


class Dragon extends PageWidget
{
    function __construct($name, $image, $properties, $points=[], $desc_template="{name} is one of the best dragons")
    {
        $this->name = $name;
        $this->id = title_to_slug($name);
        $this->image = $image;
        $this->properties = $properties;
        $this->description_template = $desc_template;
        $this->points = $points;
    }

    function url()
    {
        return "/dragons/{$this->id}/";
    }

    function expand($template)
    {
        return preg_replace_callback(
            '/\{([a-z_]+)\}/',
            function($match){
                $prop = $match[1];
                if ( isset($this->$prop) )
                    return $this->$prop;
                return "";
            },
            $template
        );
    }

    function description()
    {
        if ( !isset($this->description) )
            $this->description = $this->expand($this->description_template);
        return $this->description;
    }

    function element()
    {
        return mkelement(["li",
            [
                "id"=>$this->id,
            ], [
                ["a",
                    [
                        "href"=>href($this->url()),
                        "class"=>"durg-title",
                    ],
                    [["h2", [], $this->name]]
                ],
                ["div", ["class"=>"durg-info"], [
                    new PlainLink($this->url(), $this->img_element()),
                    $this->list_elements(),
                ]]
        ]]);
    }

    function img_element()
    {
        return mkelement(["img", ["class"=>"durg-pic", "src"=>href($this->image)]]);
    }

    function list_elements()
    {
        $list = [];
        foreach ( $this->properties as $name => $value )
        {
            $list[] = new SimpleElement("dt", [], $name);
            $list[] = new SimpleElement("dd", [], $value);
        }
        return mkelement(["dl", ["class"=>"durg-props"], $list]);
    }

    function total_points()
    {
        $total = 0;
        foreach ( $this->points as $points )
        {
            $total += $points->points;
        }
        return $total;
    }
}

global $dragon_list;
$dragon_list = [
    new Dragon("Glax", "/media/img/rasterized/vectors/sitting.png", [
        "Color" => "Blue",
        "Breath" => "Fire, sometimes rainbows",
        "Cuteness level" => "Over 9000!",
    ], [], "{name} is the best dragon"
    ),
    new Dragon("Diamond", "/media/img/dragons/diamond.png", [
        "Color" => "Blue",
        "Breath" => "Fire",
        "Adorableness level" => new HtmlString("<span style='font-size:x-large'>&infin;</span>"),
    ]),
    new Dragon("Viper", "/media/img/dragons/viper.png", [
        "Colour" => "Purple/black and white",
        "Ears" => "Huge!",
        "Hoard" => "Pokemon Cards",
    ], [
        new DragonPoints(
            200,
            ["Awarded for letting ", new Link("/dragons/glax/", "Glax"),
            " wear his fursuit"],
            "/media/img/dragons/flipping-the-durg.jpg"
        ),
    ]),
    new Dragon("Doran", "/media/img/fur-noises/doran.png", [
        "Colour" => "Blue",
        "Cuteness level" => "Dependent upon floof geometry",
        "Breath" => "Laser beam",
    ]),
    new Dragon("Bax Gemhoarder", "/media/img/dragons/bax.jpg", [
        "Colour" => "Hot pink",
        "Occupation" => "Gem Miner",
    ]),
    new Dragon("Kai", "/media/img/dragons/kai.jpg", [
        "Colour" => "Grey",
        "Breath" => "Buffer Overflows, sometimes lasers",
        "Ability" => "Can look innocent until less than 5m away",
    ]),
    new Dragon("Anthrax", "/media/img/dragons/anthrax.png", [
        "Food" => "BK",
        "сука" => "блять",
    ], [
        new DragonPoints(
            20,
            "Awarded for making the UK Dragons starter pack",
            "/media/img/dragons/starter-pack.jpg"
        ),
    ]),
    new Dragon("Ninspry", "/media/img/dragons/ninspry.jpg", [
        "Colour" => "Blue",
        "Breath" => "Fire",
    ]),
    new Dragon("Arialana", "/media/img/fur-noises/arialana.png", [
        "Breath" => "Minty",
        "Kindness" => "OVER 9000!",
    ]),
    new Dragon("CD Seadragom", "/media/img/dragons/seadragom.png", [
        "Colour" => "Blue",
        "From the sea" => "Not",
        "Fluffy" => "Very",
    ], [
        new DragonPoints(
            100,
            'Awarded for wearing a suit on a train making a "how to train your dragon" joke.',
            "https://pbs.twimg.com/media/DTcuCBDW0AAx79y.jpg"
        ),
    ]),
    new Dragon("Dmitrii Wrathoss", "/media/img/dragons/dmitrii.png", [
        "Colour" => "Black/Red",
        "Tree of Meat" => "No",
        "Occupation" => "[REDACTED]",
    ], [
        new DragonPoints(
            200,
            ["Awarded for letting ", new Link("/dragons/glax/", "Glax"),
            " wear his fursuit"],
            "/media/img/dragons/dmitrii-suit.jpg"
        ),
    ]),
    new Dragon("Drian", "/media/img/dragons/drian.png", [
        "Colour" => "Black/Red",
        "Food" => ent("pi"),
        "Breath" => "[DATA EXPUNGED]",
    ]),
    new Dragon("Julibugs", "/media/img/dragons/julibugs.png", [
        "Colour" => "Blue",
        "Likes" => "Hugs, lewds, games",
        "^.=.^" => "I am cute derg, will snuggle",
    ]),
    new Dragon("Phyore", "/media/img/dragons/phyore.png", [
        "Colour" => "Black/Red",
        "Adorableness" => "Crank it to 11!",
        "Quote" => "Pew pew :3",
    ]),
    new Dragon("Ashdon", "/media/img/dragons/ashdon.jpg", [
        "Dragon status" => "Pretend. (Awful)",
        "Dragons" => "Exist 50% / don't exist 50%",
    ], [], "{name} is the best worst pretend dragon"
    ),
    new Dragon("Draks", "/media/img/dragons/draks.png", [
        "Colour" => "Red/black",
        "Secret Powers" => "Banter, lewds and music",
    ]),
    new Dragon("Exodus Arias", "/media/img/dragons/exodus.png", [
        "Color" => "Blue",
        "Breath" => "Leaves!",
        "Adorable level" => "Ultra cute and fluffy",
    ]),
    new Dragon("Beeps", "/media/img/dragons/beeps.png", [
        "Color" => "Burgundy",
        "Fluff" => "100%",
        "Ability" => "Still doesn't know what noises is making",
    ]),
    new Dragon("Kaarthurnax (Jeffery)", "/media/img/dragons/kaarthurnax.jpg", [
        "Smoothness" => "200%",
        "Breath" => "None, just angery hisses or rerrs",
    ]),
    new Dragon("Danza", "/media/img/dragons/danza.jpg", [
        "Colour" => "Blue (Best Colour)",
        "Alliances" => "KFC and Bargin Booze",
        "Hoard" => "Lots of drawn weenies",
    ]),
    new Dragon("Felix Ward", "/media/img/dragons/felix.jpg", [
        "Colour" => "Orange",
    ]),
    new Dragon("Prestonroo", "/media/img/dragons/prestonroo.jpg", [
        "Colour" => "Purple",
        "Type" => "Hungry",
        "Occupation" => "Lazy licker dragon",
    ], [
        new DragonPoints(
            20,
            [
                ["div", [], ["Awarded for writing the Hail Dergy prayer:",
                    ["blockquote", ["class"=>"quote"], [
                        ["p", [], "Our dergy that is in a cave somewhere,"],
                        ["p", [], "gold be thy name,"],
                        ["p", [], "thy noms be done,"],
                        ["p", [], "gimmie coins and sleps,"],
                        ["p", [], "give us them snugs and stuff,"],
                        ["p", [], "and deliver us pizza, amen."],
                    ]]
                ]],
            ]
        ),
    ]),
    new Dragon("Whip", "/media/img/dragons/whip.jpg", [
        "Colour" => "Gold",
        "Interests" => "Gold",
        "Hobbies" => "Gold",
        "Hoards" => "Gold",
        "Notes" => "Reclusive grumpy old dragon bastard with massive ego, avoid at all costs",
    ]),
    new Dragon("Paktani", "/media/img/dragons/paktani.jpg", [
        "Employment" => "Local villain, accepts Steaks as offerings",
        "Appearance" => "Floof anthro or feral. Awoos and borks when happy",
    ]),
    new Dragon("Heartlilly", "/media/img/dragons/heartlilly.jpg", [
        "Colour" => "Cookie dough",
        "Occupation" => "Mother of Dragons, Mama Hearts",
        "Hoard" => "Washi based items",
        "Furaffinity" => new Link("http://www.furaffinity.net/user/stefiheartlilly/", "stefiheartlilly"),
    ]),
    new Dragon("Benji", "/media/img/dragons/benji.jpg", [
        "Colour" => "Blue/Brown/Pink",
        "Breath" => "Plasma Beam",
        "Occupation" => "Bal'Kar P.R. Guy",
    ]),
    new Dragon("Saethwr", "/media/img/dragons/saethwr.jpg", [
        "Colours" => "Red and Purple",
        "Breath" => "Fire",
        "Hoard" => "Tea and fossils",
    ]),
    new Dragon("Chaos", "/media/img/dragons/chaos.jpg", [
        "Colour" => "Excessively blue",
        "Red panda level" => "43.2%",
        "Tail length" => "Excessive",
        "Noises" => "DRAGON",
    ]),
    new Dragon("Zephyr", "/media/img/dragons/zephyr.png", [
        "Colour" => "Pale Grey",
        "Wings" => "For Gliding Only",
        "Cuteness level" => "Rising!",
    ]),
    new Dragon("Arumar", "/media/img/dragons/arumar.png", [
        "Colour" => "Hacker",
        "Breath" => "Fire and Ice",
        "Hello" => "Yes, this derg",
    ]),
];
