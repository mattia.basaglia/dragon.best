<?php

require_once(__dir__."/../../dragon.php");

class GlaxlePage extends DurgPage
{
    public $title = "Glaxle";
    public $description = "Guess the glax stickers";
    public $stickers = [
        "bamboozled",
        "anguish",
        "bite",
        "blep",

        "innocent",
        "laughing",
        "noises",
        "tongue out",

        "boxhead",
        "facepalm",
        "flying",
        "freezing",

        "smirk",
        "thumbs-up",
        "rawrdical",
        "party",
    ];

    public $styles = [
        "/media/styles/durg.css",
        "/media/styles/glaxle.css"
    ];

    public $scripts = [
        "/media/scripts/glaxle.js",
    ];

    function daily_seed()
    {
        $date = getdate();
        return mktime(0, 0, 0, $date["mon"], $date["mday"], $date["year"]);
    }

    protected function init_patterns()
    {
        $this->patterns["^/check/$"] = "check";
        $this->patterns["^/solution/$"] = "show_solution";
    }

    protected function show_solution($match)
    {
        header("Content-Type: text/plain");

        $solution = $this->get_solution();
        foreach ( $solution as $item )
            print("$item\n");
    }

    protected function check($match)
    {
        header("Content-Type: application/json");

        $solution = $this->get_solution();
        $guess = explode(";", $_GET["guess"]);
        if ( sizeof($guess) != sizeof($solution) )
        {
            print(json_encode(["error" => true, "message"=> "Invalid guess"]));
            return;
        }

        $already_used = [];
        $status = [];
        for ( $i = 0; $i < sizeof($guess); $i++ )
        {
            $status[$i] = ["sticker" => $guess[$i]];
            if ( $solution[$i] == $guess[$i] )
            {
                $status[$i]["state"] = "correct";
                $already_used[] = $i;
            }
            else
            {
                $status[$i]["state"] = "";
            }
        }

        for ( $i = 0; $i < sizeof($guess); $i++ )
        {
            if ( $status[$i]["state"] == "" )
            {
                $status[$i]["state"] = "absent";
                for ( $j = 0; $j < sizeof($guess); $j++ )
                {
                    if ( !in_array($j, $already_used) && $solution[$j] == $guess[$i] )
                    {
                        $already_used[] = $j;
                        $status[$i]["state"] = "present";
                        break;
                    }
                }
            }
        }

        print(json_encode([
            "error" => false,
            "result" => $status,
        ]));
    }

    private function param($name, $default)
    {
        if ( isset($_GET[$name]) && is_numeric($_GET[$name]) )
            return (int)$_GET[$name];
        return $default;
    }

    private function seed()
    {
        $this->daily_seed = $this->daily_seed();
        return $this->param("seed", $this->daily_seed);
    }

    private function get_solution()
    {
        $this->seed = $this->seed();
        srand($this->seed);
        $this->columns = $this->param("columns", 5);
        $advanced = isset($_GET["advanced"]);
        $this->max_stickers = $advanced ? sizeof($this->stickers) : 8;
        $this->guesses = $this->param("guesses", 6);
        $this->custom = $advanced || $this->columns != 5 || $this->guesses != 6 || $this->seed != $this->daily_seed;
        $solution = [];
        for ( $i = 0; $i < $this->columns; $i++ )
            $solution[] = $this->stickers[rand() % $this->max_stickers];
        return $solution;
    }

    function nav($render_args)
    {
        if ( $this->param("nav", 0) )
            parent::nav($render_args);
    }

    function footer($render_args)
    {
        if ( $this->param("nav", 0) )
            parent::footer($render_args);
    }

    function main($render_args)
    {
        $this->get_solution();
        ?>
        <div id="game" typeof="Game">
            <header>
                <div class="icon-menu">
                    <span class="icon" title="help" aria-label="help" onclick="show('help');">
                        <i class="far fa-question-circle"></i>
                    </span>
                </div>
                <div class="title" property="name">Glaxle</div>
                <div class="icon-menu">
                    <?php
                        if ( $this->custom )
                            echo '<div class="description">Custom mode</div>';
                    ?>
                    <span class="icon" title="custom game" aria-label="custom game" onclick="show('custom');">
                        <i class="far fa-plus-square"></i>
                    </span>
                    <span class="icon" title="statistics" aria-label="statistics" onclick="show('stats');">
                        <i class="fas fa-chart-bar"></i>
                    </span>
                    <span class="icon" title="settings" aria-label="settings" onclick="show('settings');">
                        <i class="fas fa-cog"></i>
                    </span>
                </div>
            </header>
            <div id="board-container">
                <div id="board">
                <?php
                    for ( $y = 0; $y < $this->guesses; $y++ )
                    {
                        echo '<div class="row" data-state="empty">';
                        for ( $x = 0; $x < $this->columns; $x++ )
                            echo '<div class="tile" data-state="empty" data-animation="idle" data-sticker="" data-selected="false" onclick="select_tile(event.target);"></div>';
                        echo '</div>';

                    }
                ?>
                </div>
            </div>
            <div id="keyboard">
                <?php
                    $kb_columns = 4;
                    $kb_rows = $this->max_stickers / $kb_columns;
                    for ( $y = 0; $y < $kb_rows; $y++ )
                    {
                        echo '<div class="row">';
                        for ( $x = 0; $x < $kb_columns; $x++ )
                        {
                            $sticker = $this->stickers[$y * $kb_columns + $x];
                            echo "<div class='button' data-sticker='$sticker'>";
                            echo mkelement(["img", [
                                "src" => href("/media/img/rasterized/vectors/$sticker.png"),
                                "alt" => $sticker,
                                "aria-label" => $sticker,
                                "data-sticker" => $sticker,
                                "onclick" => "button_press(event);",
                            ]]);
                            echo "</div>";
                        }
                        echo '</div>';
                    }

                    echo '<div class="row">';
                    echo "<div class='button' onclick='button_enter();'>enter</div>";
                    echo "<div class='button' onclick='button_delete();' aria-label='delete'><i class='fas fa-backspace'></i></div>";
                    echo '</div>';
                ?>
            </div>
        </div>
        <div id="help" class="page">
            <header>
                <h1>How to play</h1>
                <span class="icon" title="close" aria-label="close" onclick="hide('help');">
                    <i class="fas fa-times"></i>
                </span>
            </header>
            <section>
                <p>Guess the Glax Stickers in 6 tries.</p>
                <p>Each guess must be a sequence of glax stickers. Hit the enter button to submit.</p>
                <p>After each guess, the color of the tiles will change to show how close your guess was to the word.</p>
                <div class="examples">
                    <p><strong>Examples</strong></p>
                    <div class="row">
                        <div class="tile" data-state="correct">
                            <img src="/media/img/rasterized/vectors/blep.png" alt="blep" />
                        </div>
                        <div class="tile" data-state="tbd">
                            <img src="/media/img/rasterized/vectors/laughing.png" alt="laughing" />
                        </div>
                        <div class="tile" data-state="tbd">
                            <img src="/media/img/rasterized/vectors/bite.png" alt="biting" />
                        </div>
                        <div class="tile" data-state="tbd">
                            <img src="/media/img/rasterized/vectors/bamboozled.png" alt="bamboozled" />
                        </div>
                        <div class="tile" data-state="tbd">
                            <img src="/media/img/rasterized/vectors/tongue%20out.png" alt="tongue out" />
                        </div>
                    </div>
                    <p>The <strong>glax blep</strong> is in the solution and in the correct spot.</p>
                    <div class="row">
                        <div class="tile" data-state="tbd">
                            <img src="/media/img/rasterized/vectors/blep.png" alt="blep" />
                        </div>
                        <div class="tile" data-state="present">
                            <img src="/media/img/rasterized/vectors/laughing.png" alt="laughing" />
                        </div>
                        <div class="tile" data-state="tbd">
                            <img src="/media/img/rasterized/vectors/bite.png" alt="biting" />
                        </div>
                        <div class="tile" data-state="tbd">
                            <img src="/media/img/rasterized/vectors/bamboozled.png" alt="bamboozled" />
                        </div>
                        <div class="tile" data-state="tbd">
                            <img src="/media/img/rasterized/vectors/tongue%20out.png" alt="tongue out" />
                        </div>
                    </div>
                    <p>The <strong>laughing glax</strong> is in the solution but in the wrong spot.</p>
                    <div class="row">
                        <div class="tile" data-state="tbd">
                            <img src="/media/img/rasterized/vectors/blep.png" alt="blep" />
                        </div>
                        <div class="tile" data-state="tbd">
                            <img src="/media/img/rasterized/vectors/laughing.png" alt="laughing" />
                        </div>
                        <div class="tile" data-state="absent">
                            <img src="/media/img/rasterized/vectors/bite.png" alt="biting" />
                        </div>
                        <div class="tile" data-state="tbd">
                            <img src="/media/img/rasterized/vectors/bamboozled.png" alt="bamboozled" />
                        </div>
                        <div class="tile" data-state="tbd">
                            <img src="/media/img/rasterized/vectors/tongue%20out.png" alt="tongue out" />
                        </div>
                    </div>
                    <p>The <strong>biting glax</strong> is not in the solution.</p>
                </div>
                <p><strong>A new set of stickers will be available each day!</strong></p>
            </section>
        </div>

        <div id="settings" class="page">
            <header>
                <h1>Settings</h1>
                <span class="icon" title="close" aria-label="close" onclick="hide('settings');">
                    <i class="fas fa-times"></i>
                </span>
            </header>
            <section>
                <div class="setting">
                    <label for="setting_dark-mode" class="text">
                        <div class="title">Dark Theme</div>
                    </label>
                    <div class="control">
                        <input type="checkbox" onchange="toggle_class(event.target.checked, 'dark-mode');" autocomplete="off" id="setting_dark-mode" />
                    </div>
                </div>
                <div class="setting">
                    <label for="setting_colorblind" class="text">
                        <div class="title">Color Blind Mode</div>
                        <div class="description">High contrast colors</div>
                    </label>
                    <div class="control">
                        <input type="checkbox" onchange="toggle_class(event.target.checked, 'colorblind');" autocomplete="off" id="setting_colorblind" />
                    </div>
                </div>
                <div class="setting">
                    <label for="setting_smolwidth" class="text">
                        <div class="title">Mobile View</div>
                        <div class="description">Reduce the game area to fit on moble devices</div>
                    </label>
                    <div class="control">
                        <input type="radio" onchange="toggle_class(event.target.checked, 'smolwidth', ['largewidth', 'fullwidth']);" autocomplete="off" id="setting_smolwidth" name="width" />
                    </div>
                </div>
                <div class="setting">
                    <label for="setting_largewidth" class="text">
                        <div class="title">Large View</div>
                        <div class="description">Larger game area</div>
                    </label>
                    <div class="control">
                        <input type="radio" onchange="toggle_class(event.target.checked, 'largewidth', ['smolwidth', 'fullwidth']);" autocomplete="off" id="setting_largewidth" name="width" />
                    </div>
                </div>
                <div class="setting">
                    <label for="setting_fullwidth" class="text">
                        <div class="title">Full Width</div>
                        <div class="description">Expand the game area to fit the browser window</div>
                    </label>
                    <div class="control">
                        <input type="radio" onchange="toggle_class(event.target.checked, 'fullwidth', ['largewidth', 'largewidth']);" autocomplete="off" id="setting_fullwidth" name="width" />
                    </div>
                </div>
            </section>
        </div>

        <div id="stats" class="page">
            <header>
                <h1>Statistics</h1>
                <span class="icon" title="close" aria-label="close" onclick="hide('stats');">
                    <i class="fas fa-times"></i>
                </span>
            </header>
            <section id="stats-not-solved">
                <p>You haven't solved the puzzle.</p>
            </section>
            <section id="stats-solved" style="display: none">
                <p>You solved the puzzle in <strong id="stats-count">0</strong> tries.</p>
                <div id="stats-preview"></div>
                <p>You can <a href="#" onclick="show('custom');">create a new random game</a> or come back tomorrow for a new puzzle.</p>
                <button class="large-button button-share" onclick="share();">
                    Share <i class="fas fa-share-alt"></i>
                </button>
            </section>
        </div>

        <div id="custom" class="page">
            <header>
                <h1>Custom Game</h1>
                <span class="icon" title="close" aria-label="close" onclick="hide('custom');">
                    <i class="fas fa-times"></i>
                </span>
            </header>
            <section>
                <form method="get" autocomplete="off">
                    <div class="setting">
                        <label for="custom_seed" class="text">
                            <div class="title">Seed</div>
                            <div class="description">Random seed used to generate the solution</div>
                        </label>
                        <div class="control">
                            <input type="number" name="seed" id="custom_seed" />
                            <button onclick="randomize_control_seed(); return false;" title="Randomize" aria-label="randomize" class="inline-button">
                                <i class="fas fa-dice"></i>
                            </button>
                        </div>
                    </div>
                    <div class="setting">
                        <label for="custom_columns" class="text">
                            <div class="title">Columns</div>
                            <div class="description">How long a guess should be</div>
                        </label>
                        <div class="control">
                            <input type="number" name="columns" value="<?php echo $this->columns; ?>" id="custom_columns"/>
                        </div>
                    </div>
                    <div class="setting">
                        <label for="custom_guesses" class="text">
                            <div class="title">Guesses</div>
                            <div class="description">Number of guesses the player can enter</div>
                        </label>
                        <div class="control">
                            <input type="number" name="guesses" value="<?php echo $this->guesses; ?>" id="custom_guesses" />
                        </div>
                    </div>
                    <div class="setting">
                        <label for="custom_advanced" class="text">
                            <div class="title">Extra Stickers</div>
                            <div class="description">If checked, more stickers are available for guesses</div>
                        </label>
                        <div class="control">
                            <input type="checkbox" name="advanced" id="custom_advanced" <?php
                                if ( $this->max_stickers == sizeof($this->stickers) )
                                    echo "checked='checked'";
                                ?>/>
                        </div>
                    </div>
                    <?php
                        if ( $this->param("nav", 0) )
                            echo "<input type='hidden' name='nav' value='1' />";
                    ?>
                    <div class="setting">
                        <button class="large-button button-newgame" type="submit">
                            Generate
                        </button>
                    </div>
                </form>
            </section>
        </div>

        <div id="toaster"></div>
        <script>
        var seed = "<?php echo $this->seed; ?>";
        var max_guesses = <?php echo $this->guesses; ?>;
        var summary_pic = "";
        var guess_count = 0;
        var finished = false;
        var preferences = load_settings();
        var saved_state = load_saved_state(seed);
        var selected_tile = null;

        randomize_control_seed();
        </script>
        <?php

    }
}

$page = new GlaxlePage();

