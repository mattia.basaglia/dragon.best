<?php

require_once(__dir__."/../../dragon.php");


class DergFlightPage extends DurgPage
{
    public $title = "Derg Flight";
    public $description = "Simple game where you're a derg and you fly.";
    public $default_image = "/media/games/derg_flight/splash.png";

    function get_meta_type($render_args)
    {
        return "photo";
    }

    function extra_head($ra)
    {
        ?><style>
        iframe {
            width: 1152px;
            height: 648px;
            margin: 0 auto;
            display: block;
            max-width: 100vw;
        }
        </style><?php
    }

    function main($render_args)
    {
        $this->body_title(null, $render_args);
        ?>
        <button onclick='document.getElementById("game").requestFullscreen()'>Fullscreen</button>
        <?php
        echo mkelement(["iframe", [
            "id" => "game",
            "src" => "/media/games/derg_flight/Derg Flight 3D.html",
            "sandbox" => "allow-scripts allow-same-origin",
        ], []]);
        ?>
        <p>Just a simple game I made to learn Godot.</p>
        <p>
        Credits: Most assets (except the dragon) are from <a href="https://kenney.nl/">kenney</a>.
        </p>
        <?php
    }
}

$page = new DergFlightPage();

