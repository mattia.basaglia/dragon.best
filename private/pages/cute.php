<?php

require_once(__dir__."/../dragon.php");

class CutePage extends DurgPage
{
    public $title = "All Dragons Are Cute!";
    public $default_image = "/media/img/rasterized/vectors/boxhead.png";

    function extra_head($render_args)
    {
        ?><style>
            h1 {
                margin-bottom: 200px;
            }
        </style><?php
    }

    function main($render_args)
    {
        $this->body_title(null, $render_args);
    }
};

$page = new CutePage();

