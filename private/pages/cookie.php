<?php

require_once(__dir__."/../dragon.php");


class CookiePage extends DurgPage
{
    public $title = "Cookie";
    public $description = "Get a cookie from your friendly dragon";
    public $cookie_duration = 1893417120; // 60 years (approx)
    public $default_image = "/media/img/rasterized/vectors/cookie.png";


    function extra_head($render_args)
    {
        ?><style>
        #cookie-img {
            max-height: 50vh;
            object-fit: contain;
        }
        #content {
            display: flex;
            flex-flow: row wrap;
        }
        #content li {
            list-style: none;
            margin-bottom: .5ex;
        }
        #content li::before,
        .cookie-before::before,
        .cookie-after::after,
        .cookie-both::before,
        .cookie-both::after
        {
            display: inline-block;
            background-image: url("/media/img/pages/cookie.png");
            width: 2.1ex;
            height: 2.2ex;
            content: "";
            display: inline-block;
            background-size: 2ex;
            background-repeat: no-repeat;
            background-position: center;
            margin-left: .5ex;
            margin-right: .5ex;
            vertical-align: middle;
        }
        #content button {
            font-size: larger;
        }
        </style><?php
    }

    function make_token($data)
    {
        return rtrim(
            strtr(
                base64_encode(
                    gzdeflate(
                        json_encode($data),
                        9
                    )
                ),
                '+/',
                '-_'
            ),
            "="
        );
    }

    function decode_token($token)
    {
        return @json_decode(
            gzinflate(
                base64_decode(
                    strtr($token, '-_', '+/'),
                    false
                )
            ),
            true
        );
    }

    function set_cookie($data)
    {
        setcookie(
            "cookies",
            $this->make_token($data),
            time() + $this->cookie_duration,
            "/cookie/",
            "",
            !empty($_SERVER['HTTPS']),
            true
        );
    }

    protected function init_patterns()
    {
        $this->patterns["^/(?P<token>[-_a-zA-Z0-9]+)/((?P<action>accept|reject)/)?$"] = "render";
    }

    function bake_cookie($from)
    {
        return $this->make_token(dechex(time())." $from");
    }

    function unbake_cookie($token)
    {
        $token_dec = $this->decode_token($token);
        if ( $token_dec === null or !is_string($token_dec) )
            return null;

        $token_arr = explode(" ", $token_dec, 2);
        if ( sizeof($token_arr) != 2 )
            return null;

        return [
            "xtime" => $token_arr[0],
            "time" => hexdec($token_arr[0]),
            "from" => $token_arr[1],
        ];
    }

    function get_meta_description($render_args=array())
    {
        $token = $render_args["token"] ?? "";
        if ( !$token )
            return $this->description;
        $token_data = $this->unbake_cookie($token);
        return "Get a cookie from {$token_data["from"]}";
    }

    function main($render_args)
    {
        echo "<div>";
        $token = $render_args["token"] ?? "";
        if ( !$token )
        {
            $this->main_notoken();
        }
        else
        {
            $token_data = $this->unbake_cookie($token);
            if ( $token_data === null )
            {
                $this->main_token_invalid();
            }
            else
            {
                $action = $render_args["action"] ?? "";
                if ( $action == "accept" )
                    $this->main_token_accept($token_data);
                else if ( $action == "reject" )
                    $this->main_token_reject($token_data);
                else
                    $this->main_token($token_data);
            }
        }
        echo "</div>";
        echo mkelement(["img", [
            "src"=>"/media/img/rasterized/vectors/cookie.png",
            "alt"=>"Glax nomming a cookie",
            "title"=>"Cookie!",
            "id"=>"cookie-img",
        ]]);
    }

    protected function get_cookies()
    {
        if ( !array_key_exists("cookies", $_COOKIE) )
            return [];

        $result = $this->decode_token($_COOKIE["cookies"]);
        if ( !$result )
            return [];

        return $result;
    }

    private function main_token_invalid()
    {
        echo mkelement(["h1", [], "You tampered with the cookie!"]);
        echo "<p>That's kind of sad...</p>";
    }

    private function main_token($token_data)
    {
        $from = $token_data["from"];
        echo  mkelement(['h1', [], "$from gave you a cookie"]);
        echo new LinkList([
            "Accept cookie" => "accept/",
            "Reject cookie" => "reject/",
        ]);
    }

    private function main_token_accept($token_data)
    {
        $from = $token_data["from"];
        echo  mkelement(["h1", [], "You got a cookie from $from!"]);
        $old = $this->get_cookies();

        if ( isset($old[$from]) && in_array($token_data["xtime"], $old[$from]) )
        {
            echo "<p>You already ate this cookie.</p>";
            $this->show_cookies("Cookies you received so far:");
            echo new Link("/cookie/", "Send a cookie to someone");
            return;
        }

        $baketime = $token_data["time"];
        $now = time();
        $timediff = $now - $baketime;
        if ( $timediff < 0 )
        {
            echo mkelement(["p", [], "This cookie was baked in the future, congratulation on time travel!"]);
        }
        else if ( $timediff < 60 * 5 ) # 5 minutes
        {
            echo mkelement(["p", [], "This cookie has just been baked, eat it while it's hot!"]);
        }
        else if ( $timediff <= 60 * 60 * 24 ) # 24 hours
        {
            echo mkelement(["p", [], "This cookie was baked earlier today."]);
        }
        else
        {
            $stale = $timediff > 60 * 60 * 24 * 365 ? " it might be a bit stale..." : ".";
            echo mkelement(["p", [],
                "This cookie was baked on ",
                date("l, jS", $baketime),
                " of ", date("F Y", $baketime),
                " at ", date("H:m", $baketime),
                " (UTC)$stale"
            ]);
        }

        $this->show_cookies("Cookies you received so far:");

        if ( isset($old[$from]) )
            $old[$from][] = $token_data["xtime"];
        else
            $old[$from] = [$token_data["xtime"]];

        $this->set_cookie($old);
        echo new Link("/cookie/", "Send a cookie to someone");
    }

    private function main_token_reject($token_data)
    {
        $from = $token_data["from"];
        echo mkelement(["h1", [], "You rejected the cookie!"]);
        echo mkelement(["p", [], "You are a grumpy pants but no matter, Glax will eat the cookie instead of you."]);

        $this->show_cookies("Cookies you haven't rejected:");
    }

    private function main_notoken()
    {
        echo mkelement(["h1", [], "Dragon cookies"]);

        $from = trim($_POST["from"] ?? "");
        if ( !$from )
        {
            ?>
            <p>Give someone a cookie!</p>
            <form method="post" action="<?php echo href("/cookie/"); ?>">
                <p><label for="from">Your name </label>
                    <input type="text" id="from" name="from"/>
                </p>
                <button type="submit" class="cookie-both">Bake cookie</button>
            </form>
            <?php
        }
        else
        {
            $token = $this->bake_cookie($from);
            echo mkelement(["p", [], "Cookie baked!"]);
            echo mkelement(["p", [], "Give your friend this URI to hand them the cookie:"]);
            echo mkelement(["blockquote", ["onclick" => "copy(this);"], href("./$token/")]);
            ?><script>
                function copy(dom)
                {
                    var text = dom.textContent;
                    navigator.clipboard.writeText(text);

                    var selection = window.getSelection();
                    var range = document.createRange();
                    range.selectNodeContents(dom);
                    selection.removeAllRanges();
                    selection.addRange(range)
                }
            </script><?php
        }

        $this->show_cookies("Cookies you received:");
    }

    private function show_cookies($prompt)
    {
        $cookie_list = $this->get_cookies();

        if ( $cookie_list )
        {
            echo "<p>$prompt</p><ul>";
            foreach ( $cookie_list as $name => $cookies )
            {
                $amount = sizeof($cookies);
                echo mkelement(["li", [], ["$amount from $name"]]);
            }
            echo "</ul>";
        }

    }
}

$page = new CookiePage();

