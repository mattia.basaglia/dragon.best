<?php

require_once(__dir__."/../dragon.php");

class StickersPage extends DurgPage
{
    public $title = "Glax's Telegram Stickers";
    public $default_image = "/media/img/pages/stickers.png";

    public $patterns = [
        "^/(?P<pack>[^/]+)/?$" => "render"
    ];

    public $scripts = [
        "https://unpkg.com/twemoji@latest/dist/twemoji.min.js",
        "https://cdnjs.cloudflare.com/ajax/libs/bodymovin/5.12.2/lottie.js"
    ];

    function __construct()
    {
        parent::__construct();
        $this->data = null;
        $this->base_uri = href("/stickers/");
        $this->base_image_uri = href("/media/img/stickers/");
    }

    function data()
    {
        if ( $this->data == null )
        {
            // this is to keep the top-level as array but inner objects as stdClass
            $this->data = get_object_vars(json_decode(file_get_contents(__dir__ . "/../../media/img/stickers/data.json")));
        }

        return $this->data;
    }

    function pack_data($pack)
    {
        return $this->data()[$pack];
    }

    function pack_image_path($pack_data, $image="")
    {
        $uri = $this->base_image_uri;
        if ( $pack_data->nsfw )
            $uri .= "nsfw/";
        return "$uri{$pack_data->short_name}/$image";
    }

    function extra_head($render_args)
    {
        ?><style>
        #content {
            padding: 0;
            margin: 0;
        }
        .pack-list img {
            max-width: 100px;
            vertical-align: middle;
        }
        ul#stickers {
            margin: 0;
            padding: 0;
            display: flex;
            flex-flow: row wrap;
            list-style: none;
        }
        li.sticker {
            display: flex;
            flex-flow: column;
            margin: 4px;
            border: 1px solid var(--shade-2);
        }
        .sticker p {
            font-size: xx-large;
            text-align: center;
            margin: 0;
            height: 1.2em;
            padding-bottom: 2px;
            border-bottom: 1px solid var(--shade-3);
        }
        .sticker-content {
            width: 230px;
            height: 230px;
        }
        .pack-emoji .sticker-content {
            width: 100px;
            height: 100px;
        }
        .sticker img {
            object-fit: contain;
        }
        .pack-list {
            margin: 0 auto;
            border-collapse: collapse;
        }
        .pack-list i {
            font-size: x-large;
            margin: 0 5px;
        }
        .pack-list td:last-child {
            text-align: right;
        }
        img.emoji {
            height: 1em;
            margin-top: 0.1em;
        }
        .rot90 {
            transform: rotate(90deg);
        }
        .pack-list tr:hover {
            background: var(--shade-4);
        }
        </style>
        <?php
    }

    function render($render_args=[])
    {
        if ( isset($render_args["pack"]) )
        {
            $pack = $this->pack_data($render_args["pack"]);
            $render_args["pack"] = $pack;
            $render_args["image_path"] = $this->pack_image_path($pack);
        }
        else
        {
            $render_args["pack"] = null;
        }

        parent::render($render_args);
    }

    function title($render_args=[])
    {
        if ( $render_args["pack"] )
            return $render_args["pack"]->title;
        return $this->title;
    }

    function get_meta_image($render_args)
    {
        if ( isset($render_args["pack"]) )
            return $render_args["image_path"] . "thumb.webp";
        return $this->default_image;
    }

    function main($render_args)
    {
        echo "<main>";

        if ( isset($render_args["pack"]) )
            $this->pack($render_args["pack"], $render_args["image_path"]);
        else
            $this->pack_list();


        ?></main>
        <script>
            twemoji.parse(document.querySelector("main"));
        </script><?php
    }

    function pack_list()
    {
        echo "<nav class='path'><span>{$this->title}</span></nav>";
        $nsfw = ($_GET["nsfw"] ?? "0") == "1";
        echo "<table class='pack-list'>";
        foreach ( $this->data() as $pack )
        {
            if ( $nsfw || !$pack->nsfw )
            {
                $sticker_count = sizeof($pack->stickers);
                if ( $pack->type == "emoji" )
                    $count_word = "emoji";
                else
                    $count_word = "sticker" . ($sticker_count == 1 ? "" : "");
                echo mkelement(
                    ["tr", [], [
                        ["td", [], [
                            ["a", ["href" => $this->base_uri . $pack->short_name . "/"], [
                                ["img", ["src" => $this->pack_image_path($pack, "thumb.webp"), "alt" => $pack->title]]
                            ]],
                        ]],
                        ["td", [], [
                            ["a", ["href" => $this->base_uri . $pack->short_name . "/"], [
                                $pack->title
                            ]],
                        ]],
                        ["td", [], [
                            ["a", ["href" => $pack->url, "title" => "Add Sticker Pack"], [
                                ["i", ["class" => "fa-solid fa-right-to-bracket rot90"], []]
                            ]]
                        ]],
                        ["td", [], ["$sticker_count $count_word"]]
                    ]]
                );
            }
        }
        echo "</table>";
    }

    function pack($pack, $image_path)
    {
        echo "<nav class='path'>";
        echo new Link($this->base_uri, $this->title);
        $separator = new SimpleElement("span", array("class"=>"path_sepatator"), "");
        echo $separator;
        echo mkelement(
            ["span", [], [
                $pack->title, " ",
                ["a", ["href" => $pack->url, "title" => "Add Sticker Pack"], [
                    ["i", ["class" => "fa-solid fa-right-to-bracket rot90"], []]
                ]]
            ]]
        );
        echo "</nav>";
        echo "<ul id='stickers' class='pack-{$pack->type}'>";
        echo "<script>bodymovin.useWebWorker(true);</script>";

        foreach ( $pack->stickers as $sticker )
        {
            echo '<li class="sticker">';
            echo mkelement(["p", [], $sticker->emoji]);
            if ( $sticker->mime_type == "image/webp" )
            {
                echo mkelement(["img",  ["src" => "$image_path{$sticker->id}.webp", "alt" => "", "class" => "sticker-content"]]);
            }
            else if ( $sticker->mime_type == "application/json" )
            {
                $id = "lottie_{$sticker->id}";
                echo mkelement(["div", ["class" => "sticker-content", "id"=> $id, "onclick" => "toggle_$id();"], ""]);
                echo mkelement(["script", [], [new HtmlString("
                    var anim_$id = bodymovin.loadAnimation({
                        container: document.getElementById('$id'),
                        renderer: 'canvas',
                        loop: true,
                        autoplay: true,
                        path: '$image_path{$sticker->id}.json',
                    });
                    function toggle_$id()
                    {
                        anim_$id.togglePause();
                        var togbutt = document.getElementById('pause_$id');
                        togbutt.children[0].classList.toggle('fa-play');
                        togbutt.children[0].classList.toggle('fa-pause');
                        togbutt.setAttribute('title', anim_$id.isPaused ? 'Play' : 'Pause');
                    }
                ")]]);
            }
            else if ( $sticker->mime_type == "video/webm" )
            {
                echo mkelement(["video", ["class" => "sticker-content", "autoplay" => "autoplay", "loop" => "loop"], [
                    ["source", ["src" => "$image_path{$sticker->id}.webm", "type" => $sticker->mime_type]]
                ]]);
            }
            echo '</li>';
        }

        echo "</ul>";
    }

}

$page = new StickersPage();

