<?php

require_once(__dir__."/../dragon.php");


class GlaxmojiPage extends DurgPage
{
    public $title = "Glaxmoji";
    public $description = "Emoji set for dragons";
    public $scripts = [
        "/media/scripts/external/emoji-mart-patched.js",
        // "https://cdn.jsdelivr.net/npm/emoji-mart@latest/dist/browser.js"
    ];
    public $styles = [
        "/media/styles/durg.css",
        "/media/styles/Glaxmoji.css",
    ];

    function extra_head($render_args)
    {
        ?><style>
          @font-face {
            font-family: 'EmojiMart';
            src: url(/media/styles/Glaxmoji.otf);
        }
        g-m {
            font-family: Glaxmoji;
            font-size: 48px;
        }
        #emoji_input {
            font-family: Glaxmoji;
            font-size: 128px;
            width: 100%;
            resize: vertical;
        }
        em-emoji-picker {
            max-width: 100%;
            min-height: 800px;
            width: 512px;
            margin: 0 auto;
        }
        </style><?php
    }

    function main($render_args)
    {
        $this->body_title(null, $render_args);
        ?>

        <p>Glaxmoji is an <a href="https://gitlab.com/mattbas/glaxmoji">open source</a> emoji set for dragons <g-m>😊</g-m></p>

        <h2>Intro</h2>

        <p>Glaxmoji is an emoji set where all the facial expressions are applied to a dragon's face <g-m>🥰</g-m></p>
        <p>It supports skin tones <g-m>👍🏽</g-m> and ZWJ sequences <g-m>🧑‍🔧</g-m>and combination thereof <g-m>👩🏾‍❤️‍💋‍👨🏻</g-m></p>

        <h2>Usage</h2>
        <p>Use can use Glaxmoji as a font, an <a href="/media/styles/Glaxmoji.otf?download">OTF file</a> is available for download.</p>
        <p>To use it on the web, you can reference <a href="/media/styles/Glaxmoji.css">Glaxmoji.css</a> and simply use <code>Glaxmoji</code> as <code>font-family</code>:</p>
        <?php
        $src = href("/media/styles/Glaxmoji.css");
        echo mkelement(["pre", [], [["code", [], '<link rel="stylesheet" src="'.$src.'" />']]]);
        ?>

        <h2>Customize</h2>
        <p>You can customize emoji pictures and create emoji-kitchen-like mashups at the
        <a href="/editor/emoji">Emoji editor</a>.</p>

        <h2>Demo</h2>
        <p>Try it below!</p>
        <textarea id="emoji_input">😛</textarea>
        <div id="emoji_selector"></div>
        <script>
            const emoji_input = document.getElementById("emoji_input");
            const selector = document.getElementById("emoji_selector");

            function on_emoji(data, ev)
            {
                emoji_input.value += data.native;
            }

            const size = 48;
            // https://github.com/missive/emoji-mart
            const picker = new EmojiMart.Picker({
                onEmojiSelect: on_emoji,
                maxFrequentRows: 0,
                skinTonePosition: "search",
                noCountryFlags: false,
                emojiButtonSize: size + 6,
                emojiSize: size,
                emojiButtonSize: size * 2,
                // perLine: 8,
                dynamicWidth: true
            });
            selector.appendChild(picker);
        </script>

        <?php
    }
}

$page = new GlaxmojiPage();
