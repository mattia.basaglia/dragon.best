<?php

require_once(__dir__."/../dragon.php");
require_once(__dir__."/../commission-list.php");

class HomePage extends DurgPage
{
    public $title = "🐉 Best Dragon 🐉";
    public $metadata = [
        "site_name" => "🐉 Best Dragon 🐉",
        "title" => "Glax is best Dragon!",
        "description" => "Glax is a dragon with blue scales",
    ];
    public $default_image = "/media/img/rasterized/vectors/sitting.png";

    function extra_head($render_args)
    {
        ?><style>
            .durg {
                background: url("/media/img/vectors/sitting.svg") no-repeat center center;
                width: 100%;
                height: 90vmin;
                background-size: contain;
                align-self: center;
                max-height: 512px;
                max-width: 512px;
            }
            h1 {
                margin: 0;
                padding: 0;
                height: 10%;
                width: 100%;
                background: transparent;
                display: flex;
                align-items: center;
                justify-content: center;
                font-size: 6vmin;
                text-shadow: 0 0 0.2ex var(--glax-body-main);
            }
            main#content {
                display: flex;
                flex-flow: column;
            }
            .item-links td {
                width: 1%;
                white-space: nowrap;
            }
            .item-links td:last-child {
                width: auto;
            }
        </style><?php
    }

    function main($render_args)
    {
        $this->body_title("Glax is best Dragon!", $render_args);

        $comm_types = commission_types();

        $open = false;
        foreach ( $comm_types as $type )
        {
            if ( $type->open )
            {
                $open = true;
                break;
            }
        }

        if ( $open )
        {
            echo mkelement(["p", ["style"=>"text-align: center; font-size: x-large;"], [
                "I'm open for commissions, check the ",
                new Link("/commission/", "commission page"),
                " for details."
            ]]);
        }

        function item($link, $icon, $description)
        {
            $icon_elem = mkelement(["i", ["class" => $icon], []]);
            return mkelement(["span", [], [$icon_elem, " ", new Link($link), "- $description"]]);
        }
        function item_table($items)
        {
            $rows = [];
            foreach ( $items as [$url, $icon, $description] )
            {
                $rows[] = mkelement(["tr", [], [
                    mkelement(["td", [], [["i", ["class" => $icon], []]]]),
                    mkelement(["td", [], new Link($url)]),
                    mkelement(["td", [], "- $description"]),
                ]]);
            }

            echo mkelement(["table", ["class" => "item-links"], $rows]);
        }


        echo '<div class="durg"></div>';
        // echo mkelement(["p", [], [
        //     "Glax is a ", new Link("/cute/", "dragon"), " with ",
        //     new Link("/color/", "blue"), " ", new Link("/fluff/", "scales"), "."
        // ]]);
        ?>
        <p>Welcome to Glax's website, this is a place where I add silly things and web toys.</p>
        <p>Some fun pages to get started:</p>
        <?php
            item_table([
                ["/choir", "fa-fw fas fa-music", "a choir of kobolds who can sing midi files"],
                ["/noodle", "bx bxs-bowl-hot", "noodle dragon that moves around the screen, following your mouse"],
                ["/editor/emoji", "fa-fw fas fa-pencil-alt", "an emoji editor that can export telegram stickers"],
                ["/random", "fa-fw fas fa-dice", "a random dragon generator"],
            ]);
        ?>
        <p>Or navigate using the menu at the top of this page for more stuff!</p>
        <?php
    }
}

$page = new HomePage();
