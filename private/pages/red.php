<?php

require_once(__dir__."/../dragon.php");

class RedPage extends DurgPage
{
    public $title = "Red dragons just as adorable as blue ones";

    function extra_head($render_args)
    {
        ?><style>
            html {
                background-color: #a82929;
                color: #f88;
            }
        </style><?php
    }

    function main($render_args)
    {
        $this->body_title(null, $render_args);
    }
};

$page = new RedPage();
