<?php

require_once(__dir__."/../dragon.php");
require_once(__dir__."/../commission-list.php");

class ShadyScalesPage extends DurgPage
{
    public $title = "Join Glaxo Shady Scales!";
    public $description = "Pay your hard-earned money for amazing rewards!";
    public $default_image = "/media/img/pages/shady-scales.png";

    private $rewards = [
        [
            "title" => "Plebs",
            "image" => "unimpressed",
            "cost" => "€100",
            "cost+" => "",
            "perks" => [
                "Nothing, you cheapo!"
            ]
        ],
        [
            "title" => "uwu",
            "image" => "thumbs-up",
            "cost" => "€200",
            "cost+" => "Your left kidney",
            "perks" => [
                "Early access to my stickers",
                "Ad-free access to this website",
                "Ability to send me more money",
                "Access to Glax art I don't even like",
            ]
        ],
        [
            "title" => "OwO",
            "image" => "smirk",
            "cost" => "€500",
            "cost+" => "Both your kidneys",
            "perks" => [
                "1 Free DMs every day",
                "1 Free shitpost per month",
                "Invisible trophy on your shelf",
                "Early access to my stickers",
                "Your existence acknowledged by me",
                "Ad-free access to this website",
                "Ability to send me more money",
                "Access to Glax art I find just OK",
            ]
        ],
        [
            "title" => "What's This?",
            "image" => "huff",
            "cost" => "€1000",
            "cost+" => "Your firstborn",
            "perks" => [
                "5 Min PRIVATE Chat",
                "2 Free DMs every day",
                "1 Free shitpost per week",
                "1 Virtual hug per month",
                "Invisible trophy on your shelf",
                "Early access to my stickers",
                "Your existence acknowledged by me",
                "Ad-free access to this website",
                "Ability to send me more money",
                "Access to my favourite Glax art",
                "Knowledge of the fact you have too much money to waste",
            ]
        ]
    ];

    function extra_head($render_args)
    {
        ?><style>

        h1, h2, h3, p {
            text-align: center;
        }
        .pfp {
            width: 128px;
            height: 128px;
            display: block;
            margin: 0 auto;
            border: 5px solid var(--glax-shade-1);
            border-radius: 128px;
        }
        #rewards {
            display: flex;
            margin: 2em 0;
            justify-content: space-around;
            gap: 1em;
            flex-flow: row wrap;
        }
        .reward_pic {
            width: 128px;
            height: 128px;
            display: block;
            margin: 0 auto 1em;
        }
        #reward {
            flex: 0 0 270px;
            display: flex;
            flex-flow: column;
            border: 1px solid var(--shade-3);
            border-radius: 3px;
            text-align: center;
            min-width: 200px;
        }
        .cost {
            font-size: larger;
            font-weight: bold;
        }
        .cost_month {
            font-weight: bold;
            color: var(--shade-2);
            text-transform: uppercase;
        }
        .cost_extra {
            font-weight: bold;
            color: var(--shade-3);
        }
        #reward ul {
            text-align: left;
            color: var(--shade-1);
        }
        .join {
            color: var(--shade-5);
            background: var(--glax-shade-2);
            padding: 1ex 5ex;
            margin: 1ex auto;
            border-radius: 2em;
            font-size: large;
            cursor: pointer;
            text-decoration: none;
        }
        </style><?php
    }

    function main($render_args)
    {
        $this->body_title("Glaxo Shady Scales");

        echo mkelement(["img", ["src"=>href("/media/img/rasterized/vectors/noises.png"), "class"=>"pfp"]]);

        echo mkelement(["p", [], $this->description]);
        echo mkelement(["h2", [], "Select a membership level"]);

        echo "<div id='rewards'>";

        foreach ( $this->rewards as $reward )
        {
            $image = $reward["image"];
            $cost_extra = $reward["cost+"];
            if ( $cost_extra )
                $cost_extra = "(+$cost_extra)";
            else
                $cost_extra = ent("nbsp");

            echo "<div id='reward'>";
            echo mkelement(["h3", [], $reward["title"]]);
            echo mkelement(["img", ["class"=>"reward_pic", "src" => href("/media/img/rasterized/vectors/$image.png")]]);
            echo mkelement(["span", ["class"=>"cost"], $reward["cost"]]);
            echo mkelement(["span", ["class"=>"cost_month"], "per month"]);
            echo mkelement(["span", ["class"=>"cost_extra"], $cost_extra]);
            echo mkelement(["a", ["href"=>href("/donate/"), "class"=>"join"], "Join"]);
            echo "<ul>";
            foreach ( $reward["perks"] as $perk )
                echo mkelement(["li", [], $perk]);
            echo "</ul>";
            echo "</div>";
        }

        echo "</div>";

    }
}

$page = new ShadyScalesPage();

