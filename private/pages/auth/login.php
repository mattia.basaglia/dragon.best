<?php

require_once(__dir__."/../../dragon.php");
require_once(__dir__."/../../lib/telegram.php");

class LogInPage extends DurgPage
{
    public $title = "Log In";

    function extra_head($render_args)
    {
        ?><style>
        </style><?php
    }

    function main($render_args)
    {
        $next = to_safe_uri($_GET["next"] ?? "/");
        global $auth;
        if ( $auth->user )
            redirect(href($next));

        $this->body_title(null, $render_args);
        echo Telegram::instance()->me()->login_button("/auth/check/?next=$next", false, "large", 5);
    }
}

$page = new LogInPage();
