<?php

require_once(__dir__."/../../dragon.php");
require_once(__dir__."/../../lib/telegram.php");

class LogOutPage extends DurgPage
{
    function render($render_args=[])
    {
        global $auth;
        $auth->logout();
        redirect(href(to_safe_uri($_GET["next"] ?? "/")));
    }
};

$page = new LogOutPage();
