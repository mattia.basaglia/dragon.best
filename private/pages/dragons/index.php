<?php

require_once(__dir__."/../../dragon.php");
require_once(__dir__."/../../dragon-list.php");


class DragonsPage extends DurgPage
{
    public $title = "All the best dragons";
    public $metadata = [
    ];

    function __construct()
    {
        parent::__construct();
        global $dragon_list;
        $this->dragons = $dragon_list;
    }

    protected function init_patterns()
    {
        $this->patterns["^/(?P<dragon>.*)/$"] = "details";
    }

    function find_durg($id)
    {
        foreach ( $this->dragons as $durg )
        {
            if ( $durg->id == $id )
                return $durg;
        }
        return null;
    }

    function title($render_args=array())
    {
        if ( isset($render_args["durg"]) )
            return $render_args["durg"]->name;
        return $this->title;
    }

    function get_meta_image($render_args)
    {
        if ( isset($render_args["durg"]) )
            return $render_args["durg"]->image;
        return parent::get_meta_image($render_args);
    }

    function get_meta_description($render_args=[])
    {
        if ( isset($render_args["durg"]) )
        {
            return $render_args["durg"]->description();
        }

        return null;
    }

    function extra_head($render_args)
    {
        if ( isset($render_args["durg"]) )
            $this->style_single();
        else
            $this->style_multi();
        ?><style>
        .durg-props > dt {
            font-weight: bold;
        }
        .durg-props dd {
            margin: 0 0 0.33em 1em;
        }
        </style><?php
    }

    function style_single()
    {
        ?><style>
        .durg-pic{
            max-width: 400px;
            max-height: 400px;
            width: 100%;
            object-fit: contain;
        }
        .durg-info {
            display: flex;
            flex-flow: row-reverse wrap;
            justify-content: space-around;
            font-size: larger;
        }
        .other-durgs img {
            max-width: 128px;
            max-height: 128px;
            align-self: center;
        }
        .other-durgs ul {
            list-style: none;
            margin: 0;
            padding: 0;
            display: flex;
            flex-flow: row wrap;
            justify-content: center;
        }
        .other-durgs a {
            width: 128px;
            height: 128px;
            padding: 5px;
            border: 1px solid black;
            display: flex;
            margin: 5px;
            border-radius: 5px;
            justify-content: center;
        }
        .other-durgs h2 {
            text-align: center;
        }
        .other-durgs a:hover {
            background: #ddd;
        }
        .points-description img {
            width: 50%;
            max-width: 256px;
            max-height: 256px;
            object-fit: contain;
            margin-right: 1em;
        }
        .points-description {
            display: flex;
        }
        </style><?php
    }

    function style_multi()
    {
        ?><style>
        .durg-list {
            margin: 0;
            padding: 0;
            list-style: none;
            display: flex;
            flex-flow: row wrap;
        }
        .durg-list > li {
            border: 1px solid black;
            padding: 1em;
            border-radius: 0.5em;
            margin: 0 1em 1em 0;
        }
        .durg-list > li:target {
            background-color: #ddd;
        }
        .durg-pic {
            max-width: 128px;
            max-height: 128px;
            margin-right: 1em;
            align-self: center;
        }
        .durg-info {
            display: inline-flex;
            flex-flow: row nowrap;
            align-items: center;
        }
        .durg-title {
            color: black;
            text-decoration: none;
        }
        .durg-title:hover {
            text-decoration: underline;
        }
        .durg-title > h2 {
            margin: 0 0 0.5em;
            text-align: center;
            border-bottom: 1px solid black;
            padding-bottom: 0.25em;
        }
        .durg-title:focus {
            outline: none;
        }
        </style><?php
    }

    function details($match)
    {
        $durg = $this->find_durg(urldecode($match["dragon"]));
        if ( $durg )
        {
            $this->render([
                "durg" => $durg,
            ]);
            return true;
        }

        throw new HttpStatus(404);
    }

    function main($render_args)
    {
        $this->body_title(null, $render_args);
        if ( isset($render_args["durg"]) )
            $this->render_durg_details($render_args["durg"]);
        else
            $this->render_durg_list();
    }

    function render_durg_details(Dragon $durg)
    {
        echo mkelement(["p", [], $durg->description()]);
        echo mkelement(["div", ["class"=>"durg-info"], [
            $durg->img_element(),
            $durg->list_elements(),
        ]]);
        echo mkelement([
            "p",
            [],
            "This dragon has been certified as a ".
            "Dragon of Unequivocally Recognized Greatness (DURG)."
        ]);

        if ( sizeof($durg->points) != 0 )
        {
            $points = $durg->total_points();
            echo mkelement(["section", [], [
                [
                    "header",
                    [],
                    [["h2", ["id"=>"durg-points"], new PlainLink("#durg-points", "Points")]],
                ],
                ["p", [], "This dragon has been awarded $points dragon points."]
            ]]);
            foreach ( $durg->points as $points );
                echo $points;
        }

        echo "<nav class='other-durgs'><h2>Other best dragons</h2><ul>";
        foreach ( $this->dragons as $other_durg )
        {
            if ( $durg !== $other_durg )
                echo mkelement(["li", [], [
                    ["a", ["href"=>href("/dragons/{$other_durg->id}/"), "title"=>$other_durg->name], [
                        ["img", ["src"=>href($other_durg->image)]],
                    ]]
                ]]);

        }
        echo "</ul></nav>";
    }

    function render_durg_list()
    {
        ?>
        <p>These are the unquestionably best dragons</p>
        <ul class="durg-list">
        <?php
            foreach ( $this->dragons as $durg )
            {
                echo $durg->element();
            }
        ?>
        </ul>
        <?php
    }
}

$page = new DragonsPage();
