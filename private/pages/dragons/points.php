<?php

require_once(__dir__."/../../dragon.php");
require_once(__dir__."/../../dragon-list.php");


class DragonsPointsPage extends DurgPage
{
    public $title = "The best points for the best dragons";
    public $description = "Points awarded for great feats of dragonosity.";

    function extra_head($render_args)
    {
        ?><style>
            .durg-pic {
                width: 64px;
                max-height: 64px;
                vertical-align: middle;
                margin-right: 1ex;
                object-fit: contain;
            }
            .durg-link {
                color: inherit;
                text-decoration: none;
            }
            .durg-link:hover {
                text-decoration: underline;
            }
            .durg-points-list tbody th {
                text-align: left;
            }
        </style><?php
    }

    function main($render_args)
    {
        $this->body_title(null, $render_args);
        echo mkelement(["p", [], $this->description]);

        global $dragon_list;
        $pointed = [];
        $total = 0;

        foreach ( $dragon_list as $durg )
        {
            if ( $durg->points )
            {
                $durg->tp = $durg->total_points();
                $total += $durg->tp;
                $pointed []= $durg;
            }
        }

        usort($pointed, function($a, $b){ return $b->tp - $a->tp; });

        ?><table class="durg-points-list">
            <tbody>
        <?php
            foreach ( $pointed as $durg )
                echo mkelement(["tr", [], [
                    ["th", [], [
                        new Link($durg->url(), mkelement(
                            ["img", ["class"=>"durg-pic", "src"=>href($durg->image)]],
                            ["span", ["class"=>"durg-name"], $durg->name]
                        ), ["class" => "durg-link"])
                    ]],
                    ["td", [], $durg->tp]
                ]]);
        ?></tbody></table><?php
    }
}

$page = new DragonsPointsPage();

