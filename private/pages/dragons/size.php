<?php

require_once(__dir__."/../../dragon.php");
require_once(__dir__."/../../models.php");


class DragonsSizePage extends DurgPage
{
    public $title = "Size chart of the best dragons";
    public $metadata = [
    ];
    public $scripts = [
        "https://hammerjs.github.io/dist/hammer.min.js",
        "/media/scripts/size.js",
    ];
    public $styles = [
        "/media/styles/durg.css",
    ];
    function extra_head($render_args)
    {
        ?><style>
        #sizechart {
            height: 500px;
            max-height: 90vh;
            width: 100%;
            border: 1px solid gray;
        }
        .details {
            position: fixed;
            border: 1px solid black;
            background: white;
            pointer-events: none;
            display: none;
        }
        .details header img {
            width: 64px;
            height: 64px;
        }
        .details header {
            font-weight: bold;
            font-size: large;
            border-bottom: 1px solid black;
            display: flex;
            align-items: center;
        }
        .details header span {
            flex-grow: 1;
            text-align: center;
            margin: 0 1ex;
        }
        .details th {
            text-align: right;
        }
        #sizetable td, #sizetable th {
            border: 1px solid gray;
        }
        #sizetable {
            border-collapse: collapse;
        }
        #sizetable td:nth-last-child(1), #sizetable td:nth-last-child(2) {
            text-align: right;
        }
        #sizetable svg {
            vertical-align: middle;
        }
        #sizetable img {
            width: 32px;
            height: 32px;
            vertical-align: middle;
        }
        </style><?php
    }

    function main($render_args)
    {
        $this->body_title(null, $render_args);
        global $auth;
        $icon = $auth->user ? htmlentities($auth->user->photo_url) : "";


        foreach ( DragonSize::$types as $key => $obj )
            $obj->slug = $key;
        echo "<script>const dragon_types = ".json_encode(DragonSize::$types).";</script>";

        echo '<svg id="sizechart" xmlns="http://www.w3.org/2000/svg"><defs>';
        foreach ( DragonSize::$types as $key => $obj )
            echo str_replace(
                '<?xml version="1.0" encoding="UTF-8" standalone="no"?>', "",
                file_get_contents(root_dir."/media/img/pages/size/$key.svg")
            );
        echo '</defs></svg>';

        if ( $auth->user )
        {
            ?>
                <ul id="form-start" class="buttons">
                    <li><button onclick="size_form.show('create');">Create</button></li>
                    <li><button onclick="size_form.show('modify');">Modify</button></li>
                    <li><button onclick="size_form.show('delete');">Delete</button></li>
                </ul>
                <form id="edit" style="display: none" onsubmit="size_form.submit(); return false;" autocomplete="off">
                    <table>
                        <tr id="form-row-dragon">
                            <td><label for="edit-id">Dragon</label></td>
                            <td><select name="id" id="edit-id" oninput="size_form.dragon();"></select></td>
                        </tr>
                        <tr id="form-row-name">
                            <td><label for="name">Name</label></td>
                            <td><input type="text" name="name" id="name" required="required" /></td>
                        </tr>
                        <tr id="form-row-type">
                            <td><label for="type">Type</label></td>
                            <td><?php echo new Select(
                                array_flip(array_map(function($x){return $x->name;}, DragonSize::$types)),
                                ["name"=> "type", "id" => "type", "oninput" => "size_form.type();"]
                            ); ?></td>
                        </tr>
                        <tr id="form-row-height">
                            <td><label for="height">Height</label></td>
                            <td><input type="number" name="height" id="height" step="any"/>
                                <?php echo new Select(
                                    ["cm", "m", "ft"],
                                    ["name"=> "height-unit", "id" => "height-unit"]
                                ); ?>
                            </td>
                        </tr>
                        <tr id="form-row-length">
                            <td><label for="length">Length</label></td>
                            <td><input type="number" name="length" id="length" step="any"/>
                                <?php echo new Select(
                                    ["cm", "m", "ft"],
                                    ["name"=> "length-unit", "id" => "length-unit"]
                                ); ?>
                            </td>
                        </tr>
                        <tr id="form-row-color">
                            <td><label for="color">Color</label></td>
                            <td><input type="color" name="color" id="color"/></td>
                        </tr>
                        <tr id="form-row-save">
                            <td colspan="2">
                                <button type="button" onclick="size_form.hide();">Cancel</button>
                                <button type="submit" id="button-save"  onclick="size_form.submit(); return false;">Save</button>
                            </td>
                        </tr>
                    </table>
                </form>
            <?php
        }


        echo "<div id='filters'>";
        foreach ( DragonSize::$types as $name => $type )
            echo mkelement(["div", [], [
                ["input", ["type"=>"checkbox", "autocomplete" => "off", "name"=> $name,
                "checked"=>"checked", "id" => "toggle_$name", "oninput" => "update_filters();"]],
                ["label", ["for" => "toggle_$name"], $type->name]
            ]]);
        echo "</div><table id='sizetable' display='none'></table>";
        ?>
        <script>
            let provider = new SizeChartApiData();
            let query_dragons = new URL(window.location.href).searchParams.get("dragons");
            if ( query_dragons !== null )
                provider = new SizeChartFixedData(query_dragons == "" ? [] : JSON.parse(query_dragons));

            let svg_chart = new SizeChart(
                document.getElementById("sizechart"),
                document.getElementById("sizetable"),
                provider
            );
            svg_chart.update();

            let filters = document.getElementById("filters");
            function update_filters()
            {
                svg_chart.set_filters(new Set(Array.from(
                    filters.querySelectorAll("input:checked")).map(x => x.name)
                ));
            }

            var size_form = new SizeChartForm(svg_chart, document.getElementById("form-start"), document.getElementById("edit"));
        </script>
        <?php
    }
}

$page = new DragonsSizePage();
