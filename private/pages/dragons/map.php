<?php

require_once(__dir__."/../../dragon.php");
require_once(__dir__."/../../models.php");
require_once(__dir__."/../../lib/forms/models.php");


class DragonsMapPage extends DurgPage
{
    public $title = "Map of the best dragons";
    public $metadata = [
    ];
    public $scripts = [
        "https://cdn.jsdelivr.net/gh/openlayers/openlayers.github.io@master/en/v5.3.0/build/ol.js",
        "/media/scripts/map.js",
    ];
    public $styles = [
        "/media/styles/durg.css",
        "https://cdn.jsdelivr.net/gh/openlayers/openlayers.github.io@master/en/v5.3.0/css/ol.css",
    ];
    function extra_head($render_args)
    {
        ?><style>
        #mapdiv {
            width: 100%;
            height: 100vh;
        }
        .fren {
            max-width: 32px;
            max-height: 32px;
            vertical-align: middle;
            margin-left: 1ex;
        }

        .ol-rotate {
            top: 0.5em;
            right: 2.5em;
        }
        .layer-switcher {
            top: 0.5em;
            right: 0.5em;
        }
        .share-map {
            top: 3em;
            right: 0.5em;
        }
        .place-marker {
            top: 5.5em;
            right: 0.5em;
        }
        .delete-marker {
            top: 8em;
            right: 0.5em;
        }
        .marker-visibility {
            top: 10.5em;
            right: 0.5em;
        }
        .marker-dragon {
            top: 13em;
            right: 0.5em;
        }

        .layer-switcher ul {
            margin: 0;
            padding: 0;
        }
        .layer-switcher li {
            list-style: none;
            margin: 1ex;
        }

        .custom-ol-control .follow-mouse {
            display: none;
            position: fixed;
            background: white;
        }
        .custom-ol-control.active .follow-mouse {
            display: block;
        }

        .custom-ol-control:hover, .custom-ol-control.active {
            opacity: 1;
            z-index: 1;
        }
        .custom-ol-control {
            opacity: 0.8;
        }
        .custom-ol-control button {
            display: inline-block;
            vertical-align: top;
        }
        .custom-ol-control .custom-ol-control-dialog {
            display: none;
            background-color: white;
            padding: 1ex;
            margin-right: 0.5em;
        }
        .custom-ol-control.active .custom-ol-control-dialog {
            display: inline-block;
        }
        .custom-ol-control-dialog p {
            margin: 0;
        }
        </style><?php
    }

    function main($render_args)
    {
        $this->body_title(null, $render_args);
        global $auth;
        $icon = $auth->user ? htmlentities($auth->user->photo_url) : "";

        if ( $auth->user )
        {
            ?>
            <p id="form_status">&nbsp;</p>
            <script>
                var status_container = document.getElementById("form_status");

                function save_marker(attrs)
                {

                    var feat = src_my_marker.getFeatures()[0];
                    if ( feat )
                    {
                        for ( var n in attrs )
                            feat.set(n, attrs[n]);
                    }
                    var xhr = new XMLHttpRequest();
                    xhr.open("PATCH", api_url, true);
                    xhr.onreadystatechange = function() {
                        if ( this.readyState === XMLHttpRequest.DONE )
                        {
                            if ( this.status === 200 )
                                status_container.textContent = "Changes saved successfully";
                            else
                                status_container.textContent = "Error while saving changes";
                        }
                    }
                    status_container.textContent = "Please wait";
                    xhr.send(JSON.stringify(attrs));
                }
            </script>
            <?php
        }
        ?>
        <div id="mapdiv"></div>
        <script>
            var icon_size = 48;
            var my_icon = "<?php echo $icon; ?>";
            var api_url = "/api/dragons_map.json";

            function mk_marker(lon, lat, icon, dragon, public)
            {
                var feature = new ol.Feature({
                    geometry: new ol.geom.Point(ol.proj.fromLonLat([lon, lat])),
                    lon: lon,
                    lat: lat,
                    dragon: dragon,
                    public: public,
                });
                var style = new ol.style.Style({
                    image: new ol.style.Icon({
                        src: icon,
                        scale: icon_size/320,
                    }),
                });
                feature.setStyle(style);
                return feature;
            }

            function get_my_marker_prop(prop)
            {
                var feat = src_my_marker.getFeatures()[0];
                if ( feat )
                    return feat.get(prop);
                return true;
            }

            function view_to_all()
            {
                var extent = ol.extent.extend(src_dragons.getExtent(), src_my_marker.getExtent());
                extent = ol.extent.extend(extent, src_non_dragons.getExtent());
                map.getView().fit(extent, {maxZoom: 6});
            }

            function update_markers(callback = null)
            {
                var xhr = new XMLHttpRequest();
                xhr.open("GET", api_url, true);
                xhr.onreadystatechange = function() {
                    if ( this.readyState === XMLHttpRequest.DONE && this.status === 200 )
                    {
                        src_my_marker.clear();
                        src_dragons.clear();
                        src_non_dragons.clear();
                        for ( let derg of JSON.parse(this.responseText) )
                        {
                            var marker = mk_marker(derg.lon, derg.lat, derg.icon, derg.dragon, derg.public);
                            var source = src_dragons;
                            if ( derg.current )
                                source = src_my_marker;
                            else if ( !derg.dragon )
                                source = src_non_dragons;
                            source.addFeature(marker);
                        }
                        update_closest();
                        if ( callback )
                            callback();
                    }
                }
                xhr.send(null);
            }

            function initial_view()
            {
                if ( window.location.hash )
                    view_from_url();
                else
                    view_to_all();
            }

            function view_from_url()
            {
                var hash = window.location.hash;
                var regex = /^#map=([0-9]+)\/(-?[0-9]+\.[0-9]+)\/(-?[0-9]+\.[0-9]+)$/;
                var match = hash.match(regex);
                if ( match )
                {
                    map.getView().setZoom(parseInt(match[1]));
                    map.getView().setCenter(ol.proj.fromLonLat([
                        parseFloat(match[2]),
                        parseFloat(match[3]),
                    ]));
                }
            }

            var src_non_dragons = new ol.source.Vector();
            var src_dragons = new ol.source.Vector();
            var src_my_marker = new ol.source.Vector();

            var map = new ol.Map({
                target: 'mapdiv',
                controls: ol.control.defaults().extend([
                    new ShareMapControl(),
                    new LayerToggleControl(),
                ]),
                layers: [
                    new ol.layer.Tile({
                        source: new ol.source.OSM()
                    }),
                    new ol.layer.Vector({
                        title: "Others",
                        source: src_non_dragons
                    }),
                    new ol.layer.Vector({
                        title: "Dragons",
                        source: src_dragons
                    }),
                    new ol.layer.Vector({
                        source: src_my_marker
                    })
                ],
                view: new ol.View({
                    center: ol.proj.fromLonLat([-6.174, 54.915]),
                    zoom: 6
                })
            });

            update_markers(initial_view);
            // Update markers every 2 minutes
            var update_period = 2*60*1000;
            var update_interval = window.setInterval(update_markers, update_period);

            window.addEventListener("hashchange", view_from_url);

            function update_closest(){}
        </script>
        <?php
        if ( $auth->user )
        {
        ?><div id="closest"/></div>
        <script>

            var drag_marker = new ol.interaction.Modify({
                source: src_my_marker,
                style: new ol.style.Style({
                    image: new ol.style.RegularShape({
                        radius: icon_size / 2 * Math.SQRT2,
                        points: 4,
                        angle: Math.PI / 4,
                        stroke: new ol.style.Stroke({color: 'rgb(120, 180, 255)', width: 1}),
                        fill: new ol.style.Fill({color: 'rgba(120, 180, 255, 0.4)'}),
                    })
                }),
                pixelTolerance: icon_size / 2 * Math.SQRT2
            });
            map.addInteraction(drag_marker);

            update_markers();
            map.addControl(new PlaceMarkerControl());
            map.addControl(new DeleteMarkerControl());
            var ctrl_visible = new SetMarkerVisible();
            map.addControl(ctrl_visible);
            var ctrl_dragon = new SetMarkerDragon();
            map.addControl(ctrl_dragon);

            drag_marker.on('modifystart', function(evt)
            {
                window.clearInterval(update_interval);
            });
            drag_marker.on('modifyend', function(evt)
            {
                var feature = evt.features.item(0);
                if ( feature )
                {
                    var props = feature.getProperties();
                    props.coords = ol.proj.toLonLat(feature.getGeometry().getCoordinates());
                    feature.setProperties(props, true)
                    update_closest();
                    save_marker({lon: props.coords[0], lat: props.coords[1]});
                }
                update_interval = window.setInterval(update_markers, update_period);
            });

            function update_closest()
            {
                var closest = document.getElementById("closest");
                while ( closest.firstChild )
                    closest.removeChild(closest.firstChild);

                if ( src_my_marker.getFeatures().length === 0 )
                    return;

                ctrl_visible.initialize_checkbox();
                ctrl_dragon.initialize_checkbox();

                var min = Infinity;
                var min_f = null;
                var my_coords = [get_my_marker_prop("lon"), get_my_marker_prop("lat")];
                for ( var feat of src_dragons.getFeatures().concat(src_non_dragons.getFeatures()) )
                {
                    var d = ol.sphere.getDistance(my_coords, [feat.get("lon"), feat.get("lat")]);
                    if ( d < min )
                    {
                        min = d;
                        min_f = feat;
                    }
                }

                if ( min_f === null )
                    return;

                var msg = min < 1000 ?
                    "Nearest fren is less than 1 km away" :
                    "Nearest fren is " + Math.round(min/1000) + " km away";
                closest.appendChild(document.createTextNode(msg));
                var img = document.createElement("img");
                img.setAttribute("src", min_f.getStyle().getImage().getSrc());
                img.classList.add("fren");
                closest.appendChild(img);

            }
        </script><?php
        }
    }
}

$page = new DragonsMapPage();
