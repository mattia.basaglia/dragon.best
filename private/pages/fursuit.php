<?php

require_once(__dir__."/../dragon.php");
require_once(__dir__."/../lib/pages/gallery.php");

class FursuitPage extends DurgPage
{
    use FileGalleryTrait;

    public $title = "Fursuiting";
    public $description = "Glax doing various shenanigans while wearing his durgsuit.";
    public $styles = [
        "/media/styles/durg.css",
        "/media/styles/gallery.css"
    ];

    public $picmeta = [
        "ceiling" => [
            "description" => "Ceiling durg is watching you...",
            "copyrightYear" => 2018,
            "maker" => "Cabbagebath Furstuff",
        ],
        "coming-out" => [
            "description" => "Glax is coming out of the closet.",
            "copyrightYear" => 2018,
            "maker" => "Cabbagebath Furstuff",
        ],
        "bar" => [
            "description" => "Glax at the bar.",
            "author" => "WolfStarHooves",
            "copyrightYear" => 2018,
            "maker" => "Cabbagebath Furstuff",
        ],
        "phone" => [
            "description" => "Using touch-screen phones is difficult when you have claws...",
            "author" => "Borealis",
            "copyrightYear" => 2018,
            "license" => "all rights reserved",
            "maker" => "Cabbagebath Furstuff",
        ],
        "middle-finger" => [
            "description" => "Glax is making the traditional dragon greeting gesture.",
            "author" => "Anuku",
            "copyrightYear" => 2018,
            "license" => "all rights reserved",
            "maker" => "Cabbagebath Furstuff",
        ],
        "world-domination" => [
            "description" => "Glax and Mini Glax are plotting world domination.",
            "author" => "IceDragon",
            "copyrightYear" => 2019,
            "license" => "all rights reserved",
            "maker" => "Cabbagebath Furstuff",
        ],
        "bubble" => [
            "description" => "Glax holding a bubble.",
            "author" => "Finbarzapek",
            "copyrightYear" => 2022,
            "license" => "all rights reserved",
            "maker" => "Mei Fursuits",
        ],
        "rooftop" => [
            "description" => "Glax on the roof.",
            "author" => "Raymond Feesh",
            "copyrightYear" => 2022,
            "license" => "all rights reserved",
            "link" => "https://twitter.com/raymondfeesh",
            "maker" => "Mei Fursuits",
        ],
        "mountains" => [
            "description" => "Glax on the mountains.",
            "author" => "Jinx",
            "copyrightYear" => 2023,
            "maker" => "Mei Fursuits",
        ],
    ];

    function base_uri()
    {
        return "/fursuit/";
    }

    function media_path()
    {
        return "/media/img/pages/fursuit/";
    }

    function server_image_path()
    {
        return dirname(dirname($this->self_dirname())) . $this->media_path();
    }

    protected function get_css_class($selected)
    {
        if ( $selected == 1 ) return "durgpic selected";
        if ( $selected == 2 ) return "bigdurg";
        return "durgpic";
    }

    function extra_head($render_args)
    {
        ?><style>
            .pic_copyright {
                font-size: x-small;
                text-align: right;
                display: block;
                margin: 1em 0 0;
            }
        </style>
        <?php
    }

    function list_directory($directory)
    {
        return array_map(function ($x) { return "$x.jpg"; }, array_keys($this->picmeta));
    }

    function humanmeta_template($image)
    {

        if ( isset($this->picmeta[$image->slug]["link"]) )
            $author = "<a href='%(link:html)'>%(author)</a>";
        else
            $author = "%(author)";

        return "%(description)<span class='pic_copyright'>Photo by $author &copy; %(copyrightYear) %(license:html)<br/>Fursuit by %(maker:nometa)</span>";
    }

    protected function load_image_metadata($image)
    {
        if ( isset($this->picmeta[$image->slug]) )
        {
            foreach ( $this->picmeta[$image->slug] as $meta => $value )
                $image->meta[$meta] = $value;
        }
    }

    function main($render_args)
    {
        $focus = $render_args["image"] ?? null;

        if ( !$focus )
        {
            $this->body_title(null, $render_args);
            echo "<p>{$this->description}</p>";
            echo "<h2>Pictures</h2>";
        }

        $this->render_gallery($focus, $render_args);
    }
}

$page = new FursuitPage();

