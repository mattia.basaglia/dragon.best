<?php

require_once(__dir__."/../dragon.php");

class PurplePage extends DurgPage
{
    public $title = "Purple dragons are the kinkiest";
    public $description = "Especially the wingless ones.";
    public $default_image = "/media/img/pages/purple.png";

    function extra_head($render_args)
    {
        ?><style>
            body {
                background-color: #5a29a8;
                color: #d8f;
            }
        </style><?php
    }

    function main($render_args)
    {
        $this->body_title(null, $render_args);
        echo mkelement(["p", [], $this->description]);
    }
};

$page = new PurplePage();
