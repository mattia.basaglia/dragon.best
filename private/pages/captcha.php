<?php

require_once(__dir__."/../dragon.php");

class CaptchaPage extends DurgPage
{
    public $title = "Solve this captcha";
    private $images = [];

    function extra_head($render_args)
    {
        ?><style>
        .captcha-container {
            border: 1px solid black;
            border-radius: 2px;
            margin: 0 auto;
            padding: 1em;
            max-width: 700px;
        }
        .captcha-grid {
            display: grid;
            grid-template-columns: auto auto auto;
            grid-auto-rows: 1fr;
        }
        .captcha-image {
            display: flex;
            align-items: center;
            margin: 1ex;
            border: 1px solid transparent;
            cursor: pointer;
            aspect-ratio: 1;
        }
        .captcha-clicked {
            background: #ddd;
        }
        .captcha-image:hover {
            background: #ccc;
            border-color: black;
        }
        .captcha-image img {
            max-width: 100%;
            max-height: 100%;
            margin: 0 auto;
        }
        .captcha-container h2 {
            font-weight: bold;
            font-size: x-large;
            text-align: center;
            margin-bottom: 0;
        }

        .captcha-grid::before {
            content: '';
            width: 0;
            padding-bottom: 100%;
            grid-row: 1 / 1;
            grid-column: 1 / 1;
        }

        .captcha-grid > *:first-child {
            grid-row: 1 / 1;
            grid-column: 1 / 1;
        }

        </style>

        <script>
            function captcha_click(element)
            {
                element.classList.toggle("captcha-clicked");
            }
        </script>
        <?php
    }

    private function image_path()
    {
        return "/media/img/rasterized/vectors/";
    }

    private function gather_images()
    {
        $path_base = $this->self_dirname() . "/../..";
        foreach ( scandir($path_base . $this->image_path()) as $file )
        {
            if ( substr($file, -4) != ".png" )
                continue;

            $basename = substr($file, 0, -4);
            $this->images[] = $basename;
        }
    }

    private function img($index)
    {
        $image = $this->images[$index];
        return ["div", ["class" => "captcha-image", "onclick" => "captcha_click(this);"],
            [["img", ["src" => $this->image_path() . "$image.png"]]]
        ];
    }

    function main($render_args)
    {
        $this->body_title(null, $render_args);
        $this->gather_images();

        $count = 9;
        $random = array_rand($this->images, $count);
        shuffle($random);

        $increments = 5;
        $page = max(1, (int)$_POST["page"] ?? 1);
        $n_pages = (intdiv($page, $increments) + 1) * $increments;

        echo "<form class='captcha-container' method='post'>";

        echo "<h2>Select all the Glaxes</h2>";

        echo mkelement(["div", ["class" => "captcha-grid"], array_map([$this, "img"], $random)]);

        echo "<button type='submit' style='float:right'>Verify</button>";
        echo mkelement(["p", [], "Page $page of $n_pages"]);
        echo mkelement(["input", ["type" => "hidden", "name"=>"page", "value" => $page + 1]]);

        echo "</form>";
    }
};

$page = new CaptchaPage();

