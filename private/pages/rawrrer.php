<?php

require_once(__dir__."/../dragon.php");

class RawrrerPage extends DurgPage
{
    public $title = "Rawrrer";
    public $description = "Dragon micro-blogging site";

    function extra_head($render_args)
    {
        ?><style>
.rawrrer-logo {
    height: 30px;
}

.rawrrer-link {
    cursor: pointer;
    background: var(--shade-5);
    display: inline-block;
    padding: 12px;
    border-radius: 30px;
}

.rawrrer-sidebar {
    width: 20%;
    margin-right: 20px;
}

.rawrrer-sidebar ul {
    display: flex;
    flex-flow: column;
    margin: 0;
    padding: 0;
    font-size: large;
}

.rawrrer-link:hover {
    background: var(--shade-4);
}

.rawrrer-feed {
    border: 1px solid var(--shade-4);
    border-top: none;
    border-bottom: none;
    padding: 0 20px;
    flex-grow: 2;
    max-width: 512px;
}

.rawrrer {
    display: flex;
}

.rawrrer-link i {
    margin-right: 20px;
}

.rawrrer h1 {
    margin: 0 0 60px;
}

.rawrrer-rawr {
    display: flex;
}

.rawrrer-pfp img {
    width: 40px;
    border-radius: 40px;
    border: 1px solid var(--shade-3);
}

ul.rawrrer-buttons {
    display: flex;
    list-style: none;
    padding: 0;
    justify-content: space-between;
    font-size: large;
}

.rawrrer-rawr-main {
    width: 100%;
}

.rawrrer-rawr-side {
    margin-right: 12px;
}

.rawrrer-icon {
    cursor: pointer;
    border-radius: 30px;
    padding: 10px;
    color: var(--shade-2);
    width: 16px;
    height: 16px;
    display: flex;
    align-items: center;
    justify-content: center;
}

li.rawrrer-icon:hover {
    color: var(--glax-body-main);
    background-color: var(--glax-belly);
}
.rawrrer-header {
    margin-bottom: 8px;
}
.rawrrer-user {
    font-weight: bold;
}
.rawrrer-verified {
    color: var(--glax-body-main);
}

@media (max-width: 830px) {
    .rawrrer-feed {
        max-width: 100%;
    }
    .rawrrer-wide {
        display: none;
    }
    .rawrrer-link {
        font-size: x-large;
        display: flex;
        justify-content: space-around;
    }
    .rawrrer-link i {
        margin: 0;
    }
    .rawrrer-sidebar:last-child {
        display: none;
    }
}

@media (max-width: 512px) {
    ul.rawrrer-buttons {
        margin-left: -52px;
    }
}

        </style><?php
    }

    function main($render_args)
    {
        ?>

        <div class="rawrrer">
            <div class="rawrrer-sidebar">
                <a class="rawrrer-link" href="."><img class="rawrrer-logo" src="/media/img/pages/modern_logo.svg"/></a>
                <ul>
                    <li class="rawrrer-link"><i class="fa-solid fa-house"></i><span class="rawrrer-wide"> Home</span></li>
                    <li class="rawrrer-link"><i class="fa-solid fa-magnifying-glass"></i><span class="rawrrer-wide"> Explore</span></li>
                    <li class="rawrrer-link"><i class="fa-regular fa-bell"></i><span class="rawrrer-wide"> Notifications</span></li>
                    <li class="rawrrer-link"><i class="fa-regular fa-envelope"></i><span class="rawrrer-wide"> Messages</span></li>
                    <li class="rawrrer-link"><i class="fa-regular fa-rectangle-list"></i><span class="rawrrer-wide"> Lists</span></li>
                    <li class="rawrrer-link"><i class="fa-regular fa-bookmark"></i><span class="rawrrer-wide"> Bookmarks</span></li>
                    <li class="rawrrer-link"><i class="fa-solid fa-certificate"></i><span class="rawrrer-wide"> Verified</span></li>
                    <li class="rawrrer-link"><i class="fa-regular fa-user"></i><span class="rawrrer-wide"> Profile</span></li>
                </ul>
            </div>
            <div class="rawrrer-feed">
                <h1>Home</h1>

                <div class="rawrrer-rawr">
                    <div class="rawrrer-rawr-side">
                        <div class="rawrrer-pfp">
                            <img src="/media/img/vectors/blep.svg"/>
                        </div>
                    </div>
                    <div class="rawrrer-rawr-main">
                        <div class="rawrrer-header">
                            <span class="rawrrer-user">Glax</span>
                            <i class="rawrrer-verified fa-solid fa-certificate"></i>
                        </div>
                        <div class="rawrrer-content">Rawr!</div>
                        <ul class="rawrrer-buttons">
                            <li class="rawrrer-icon" title="Reply"><i class="fa-regular fa-comment"></i></li>
                            <li class="rawrrer-icon" title="Rerawr"><i class="fa-solid fa-retweet"></i></li>
                            <li class="rawrrer-icon" title="Like"><i class="fa-regular fa-heart"></i></li>
                            <li class="rawrrer-icon" title="View"><i class="fa-solid fa-chart-column"></i></li>
                            <li class="rawrrer-icon" title="Share"><i class="fa-solid fa-arrow-up-from-bracket"></i></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="rawrrer-sidebar">
            </div>
        </div>

        <?php
    }
};

$page = new RawrrerPage();


