<?php

require_once(__dir__."/../dragon.php");

class GreenPage extends DurgPage
{
    public $title = "Green dragons are the punniest";
    public $description = "Green dragons make more dad jokes than a dutch angel dragon stand-up comedian";

    function extra_head($render_args)
    {
        ?><style>
            html {
                background-color: #29a829;
                color: #8f8;
            }
        </style><?php
    }

    function main($render_args)
    {
        $this->body_title(null, $render_args);
        echo mkelement(["p", [], $this->description]);
    }
};

$page = new GreenPage();
