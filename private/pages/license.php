<?php

require_once(__dir__."/../dragon.php");

class LicensePage extends DurgPage
{
    public $title = "License";
    public $description = "Content is CC BY-SA, code is AGPLv3+";

    function main($render_args)
    {
        global $site;
        $code_license = License::find_license($site->code_license);
        $content_license = License::find_license($site->content_license);
        $this->body_title(null, $render_args);
        echo "<p>All active code (both server side and client side) is under the ";
        echo "{$code_license->as_link()}.</p>";
        echo "<p>Static content (text and images) is uner the ";
        echo "{$content_license->as_link()}.</p>";
        echo "<p>Sourcecode can be found on ";
        echo new Link($site->source_code_link, $site->source_code_name);
        echo ".</p>";

    }
};

$page = new LicensePage();

