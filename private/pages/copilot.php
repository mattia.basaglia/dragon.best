<?php

require_once(__dir__."/../dragon.php");


class CopilotPage extends DurgPage
{
    public $title = "Code copilot";
    public $description = "Generate code with ease with the dragon copilot";
    public $scripts = [
        "https://cdnjs.cloudflare.com/ajax/libs/highlight.js/11.9.0/highlight.min.js",
        "https://cdnjs.cloudflare.com/ajax/libs/highlight.js/11.9.0/languages/javascript.min.js",
        "https://cdnjs.cloudflare.com/ajax/libs/highlight.js/11.9.0/languages/python.min.js",
        "https://cdnjs.cloudflare.com/ajax/libs/highlight.js/11.9.0/languages/c++.min.js",
        "https://cdnjs.cloudflare.com/ajax/libs/highlight.js/11.9.0/languages/php.min.js",
        "https://cdnjs.cloudflare.com/ajax/libs/highlight.js/11.9.0/languages/typescript.min.js",
    ];
    public $styles = [
        "/media/styles/durg.css",
        "https://cdnjs.cloudflare.com/ajax/libs/highlight.js/11.10.0/styles/base16/atelier-heath-light.min.css",
    ];

    function main($render_args)
    {
        $this->body_title(null, $render_args);
        ?>
        <p>Enter something in the prompt and press <em>Generate</em> to let the copilot generate code for you!</p>

        <input type="text" id="prompt" placeholder="Prompt" />
        <button id="button">Generate</button><select id="languages"></select>

        <pre><code id="output"></code></pre>

        <script type="module">
            import * as ast from  "/media/scripts/module/ast.js";
            let out = document.getElementById("output");
            let prompt_in = document.getElementById("prompt");
            let gen = new ast.CodeGenerator();
            let langs = {
                js: new ast.JsFormatter(),
                cxx: new ast.CxxFormatter(),
                php: new ast.PhpFormatter(),
                py: new ast.PythonFormatter(),
                ts: new ast.TsFormatter(),
            };
            let prog;
            let typewriter = new ast.TypeWriter(out);
            let select = document.getElementById("languages");
            let debug = false;

            function generate(prompt, lang)
            {
                prog = gen.generate();

                if ( prompt != "" )
                    prog.statements.unshift(new ast.LineComment(prompt));

                write(lang);
            }

            function write(lang)
            {
                typewriter.stop();

                let code = lang.format(prog);
                let formatted = hljs.highlight(code, { language: lang.name }).value;

                if ( debug )
                    out.innerHTML = formatted;
                else
                    typewriter.type(formatted);
            }

            function on_click()
            {
                let prompt = prompt_in.value.trim();
                prompt_in.value = "";
                generate(prompt, langs[select.value]);
            }

            function initial_code()
            {
                prog = new ast.Program([
                    new ast.LineComment("Print Hello World"),
                    new ast.Print(new ast.Literal("Hello, World!")),
                ]);

                write(langs[select.value]);
            }

            document.getElementById("button").addEventListener("click", on_click);

            for ( let [id, lan] of Object.entries(langs) )
            {
                let opt = document.createElement("option");
                opt.setAttribute("value", id);
                opt.appendChild(document.createTextNode(lan.title));
                select.appendChild(opt);
            }
            select.addEventListener("change", () => write(langs[select.value]));

            if ( debug )
                on_click();
            else
                initial_code();
            prog
        </script>
        <style>
            .hljs-keyword {
                font-weight: bold;
            }
            #content pre {
                border: 1px solid var(--shade-3);
                border-radius: 5px;
                padding: 1ex;
                overflow-x: scroll;
            }
        </style>
        <?php

    }
}

$page = new CopilotPage();


