<?php

require_once(__dir__."/../dragon.php");
require_once(__dir__."/../lib/pages/gallery.php");

class GifsPage extends DurgPage
{
    use FileGalleryTrait;

    public $title = "Gifs";
    public $styles = [
        "/media/styles/durg.css",
        "/media/styles/gallery.css"
    ];

    function base_uri()
    {
        return "/gifs/";
    }

    function media_path()
    {
        return "/media/img/gifs/";
    }

//     function raster_image_path()
//     {
//         return "/media/img/gifs/preview/";
//     }

    function extra_head($render_args)
    {
        ?><style>
        .durgpic, .bigdurg {
            image-rendering: pixelated;
            image-rendering: crisp-edges;
            image-rendering: -moz-crisp-edges;
        }
        </style><?php
    }

    function server_image_path()
    {
        return dirname(dirname($this->self_dirname())) . $this->media_path();
    }

    protected function get_css_class($selected)
    {
        if ( $selected == 1 ) return "durgpic selected";
        if ( $selected == 2 ) return "bigdurg";
        return "durgpic";
    }

    protected function image_extensions()
    {
        return array("gif");
    }

    protected function render_type($match, $type)
    {
        $image = $this->find_image(urldecode($match["image"]));
        if ( $image )
        {
            $this->render(array(
                "image" => $image,
                "type" => $type,
            ));
        }
        else
        {
            http_response_code(404);
            $this->render();
        }
    }

    function render_focused(MediaFileInfo $image, $render_args)
    {
        $links = [
            "Download" => "{$image->full_url()}?download",
        ];

        $image->render(
            $this->get_css_class(2),
            ["property"=>"image"],
            []
        );
        $this->print_humanmeta($image);

        echo new LinkList($links, "buttons");
    }

    function humanmeta_template($image)
    {
        return "%(name) %(character) by %(author) &copy; %(copyrightYear) %(license:html)";
    }

    protected function load_image_metadata($image)
    {
        $image->meta["character"] = "Glax";
    }
}

$page = new GifsPage();

