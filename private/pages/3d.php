<?php

require_once(__dir__."/../dragon.php");


class DDDPage extends DurgPage
{
    public $title = "3D Models";
    public $description = "Glax... but in 3D!";
    public $scripts = [];
    public $default_image = "/media/img/3d/glax.png";
    public $styles = [
        "/media/styles/durg.css",
        "/media/styles/gallery.css",
    ];

    function get_meta_type($render_args)
    {
        return "photo";
    }

    function main($render_args)
    {
        $this->body_title(null, $render_args);

        # https://modelviewer.dev/
        $items = [
            ["dragon", "Flying-loop", 70, "0m 3m 0m"],
            ["pepper", "", 45, "0m 3m 0m"],
        ];
        ?>
        <style>
            .durgpic {
                cursor: pointer;
            }
        </style>
        <script type="module" src="https://ajax.googleapis.com/ajax/libs/model-viewer/3.5.0/model-viewer.min.js"></script>
        <model-viewer
            id="viewer"
            autoplay
            shadow-intensity="1"
            camera-controls
            touch-action="pan-y"
            style="width: 100%; height: 512px"
        >
            <div>
                <p id="animation-selector-parent"><label for="animation-selector">Animation:</label> <select id="animation-selector"></select></p>
            </div>
        </model-viewer>
        <ul class="buttons">
            <li><a id="download-button-glb"><i class="fa-solid fa-download"></i> GLB</a></li>
            <li><a id="download-button-blend"><i class="fa-solid fa-download"></i> Blend</a></li>
        </ul>

        <div class="gallery-wrapper" typeof="ImageGallery">
            <div class="gallery">
                <?php
                    for ( $i = 0; $i < sizeof($items); $i++ )
                    {
                        $slug = $items[$i][0];
                        $title = $slug;
                        $license = $this->license_object()->meta_element();
                        echo mkelement([
                            "span",
                            [
                                "property" => "associatedMedia",
                                "typeof" => "ImageObject",
                                "class" => "durgpic",
                                "data-item-index" => $i,

                            ],
                            [
                                ["img", [
                                    "src" => href("/media/img/3d/$slug.png"),
                                    "alt" => $title,
                                    "title" => $title,
                                    "property" => "image"
                                ]],
                                html_meta("name", $title),
                                $license,
                                html_meta("author", $this->copy_author),
                                html_meta("copyrightYear", $this->copy_range()),
                            ]
                        ]);
                    }
                ?>
            </div>
        </div>


        <script type="module">
            const items = <?php echo json_encode($items); ?>;
            const model_viewer = document.getElementById("viewer");
            const animation_select = document.getElementById("animation-selector");
            const animation_p = document.getElementById("animation-selector-parent");
            const download_button_glb = document.getElementById("download-button-glb");
            const download_button_blend = document.getElementById("download-button-blend");

            function show_item(item)
            {
                let [name, animation, orbit, target] = item;
                model_viewer.src = `/media/img/3d/${name}.glb`;
                download_button_glb.href = model_viewer.src + "?download";
                download_button_blend.href = `/media/img/3d/${name}.blend?download`;
                model_viewer.animationName = animation;
                model_viewer.cameraTarget = target;
                model_viewer.cameraOrbit = `${orbit}deg`;
                animation_p.style.visibility = animation != "" ? "visible" : "hidden";
            }

            animation_select.addEventListener("change", () => {
                model_viewer.animationName = animation_select.value;
            })
            model_viewer.addEventListener("load", () => {
                animation_select.innerHTML = ""
                for ( let anim_name of model_viewer.availableAnimations )
                {
                    const opt = document.createElement("option")
                    opt.value = anim_name;
                    opt.innerText = anim_name.replace("-loop", "");
                    animation_select.appendChild(opt)
                }
                animation_select.value = model_viewer.animationName;

            });
            show_item(items[0]);

            function click_event(ev)
            {
                show_item(items[Number(ev.currentTarget.dataset.itemIndex)]);
            }

            document.querySelectorAll("[data-item-index]").forEach(e => e.addEventListener("click", click_event))

        </script>


        <?php

    }
}

$page = new DDDPage();

