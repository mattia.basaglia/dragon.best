<?php

require_once(__dir__."/../dragon.php");

class RawrPage extends DurgPage
{
    public $title = "Rawr";
    public $description = "Rawr rawr rawr. Rawr rawr? Rawr! Rawr Rawr.";
    public $default_image = "/media/img/rasterized/icon/scalable.png";
    public $scripts = [
        "/media/scripts/file_tools.js",
        "/media/scripts/recolor.js",
        "/media/scripts/rawr.js"
    ];

    function extra_head($render_args)
    {
        ?>
        <style>
            #rawr_container {
                position: relative;
                height: 40vw;
                width: 100%;
                max-height: 512px;
                overflow: hidden;
            }
            #rawr {
                position: absolute;
                top: 0;
                bottom: 0;
                width: 40vw;
                max-width: 512px;
                cursor: pointer;
            }
            .rawr {
                position: absolute;
                transform-origin: left center;
                font-family: sans;
                font-weight: bold;
            }
            #palette_editor {
                display: flex;
                flex-flow: row wrap;
                justify-content: center;
            }
            #palette_editor > input {
                width: 48px;
                height: 32px;
                border: none;
                padding: 0;
                margin: 0;
                cursor: pointer;
            }
            #color_editor_preview {
                width: 256px;
                height: 256px;
            }
            .hide {
                display: none;
            }
            #settings_title {
                cursor: pointer;
            }
            #settings {
                border: 1px solid silver;
                padding: 1ex;
                border-radius: 8px;
            }
            #settings h3 {
                margin: 0;
            }

            #rawr_upgrades {
                display: none;
            }

            .rawr_buy_container {
                border: 1px solid black;
                border-radius: 5px;
                margin: 5px 0;
                padding: 5px;
            }
            .rawr_buy_name {
                border-bottom: 1px solid black;
                margin-bottom: 0.5ex;
            }
            .rawr_buy_button {
                padding: 5px;
                border-radius: 5px;
            }
            .rawr_buy_button {
                background: #f45;
            }
            .rawr_buy_button {
                background: #4f5;
            }
        </style><?php
    }

    function main($render_args)
    {
        ?>
        <div class="game" typeof="Game">
            <h1 property="name">Rawr</h1>
            <p property="quest">Click on the derg for rawrs.</p>
            <p style="white-space: nowrap;">Derg rawred <span id="rawr_counter"></span> times.</p>
            <div id="rawr_container">
                <img property="image"
                    src="<?php echo href("/media/img/rasterized/icon/scalable.png"); ?>"
                    alt="Rawr" id="rawr"/>
            </div>
            <div id="rawr_upgrades"></div>
        </div>

        <h2 id="settings_title">Settings</h2>
        <div id="settings">
            <h3>Colors</h3>
            <div id='palette_editor'></div>
            <iframe id='color_editor_preview' src='<?php echo href("/media/img/icon/scalable.svg"); ?>'></iframe>
            <ul class="buttons">
                <li><button onclick='rawr_clicker.data.save_settings();'>Save</button></li>
                <li><button onclick='rawr_clicker.data.cancel_settings();'>Cancel</button></li>
                <li><button onclick='rawr_clicker.data.randomize_colors();'>Randomize Colors</button></li>
                <li><button onclick='rawr_clicker.data.reset_colors();'>Reset Colors</button></li>
                <li><button onclick='rawr_clicker.data.reset();'>Clear</button></li>
            </ul>
        </div>

        <script>
            var rawr_clicker = new RawrClicker({
                start_x: 340,
                start_y: 210,
                angle_range: [-20, 20],
                speed_range: [300, 1000],
                alpha_range: 60,
                font_size: 80,
                fps: 60,
                rawrs: [
                    "Rawr",
                    "Rewr",
                    "Mrawrr",
                    "Rwwwk",
                    "Rwr",
                    "Reow",
                    "Rowo",
                    "Rer",
                    "Rrrr",
                ]
            });
            <?php
            echo "let controller = rawr_clicker.data.recolor_controller;";
            global $palette;
            foreach ( $palette->colors as $name => $color )
            {
                $title = slug_to_title($name);
                $original_color = $color->to_rgb()->to_css();
                echo "controller.create_color_rule('$original_color', '$name', '$title');";
            }
            ?>
            rawr_clicker.setup();
        </script>
        <?php

    }
}

$page = new RawrPage();
