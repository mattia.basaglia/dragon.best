<?php

require_once(__dir__."/../dragon.php");

class AsciiPage extends DurgPage
{
    public $title = ">',==,--";

    function extra_head($render_args)
    {
        ?><style>
            #frames
            {
                font-family: monospace;
                position: relative;
            }
            #frames div
            {
                display: none;
                white-space: pre;
            }
        </style><?php
    }

    function main($render_args)
    {
        $this->body_title("Ascii art", $render_args);
        ?>
<div id="frames">
<div>
                      ___
                     /   /`
                   // ..-_\
                 ,((:'    /`
                   \\''._\          __
          __//      \\   /`  _____//-- >
        --o  -----_^//    \^^  ---'
       `----^----'( ._____( /--
                   \\_,____\\/__,
                    \-/     \---/



</div>
<div>


                                     __
          __//               _____//-- >
        --o  -----_^^^^^^^^^^  ---'
       `----^----'( \\   /( /--
                   \// _\,_\\/__,
                   //,' /   \---/
                 `((:,  _\,
                   \\ '' /
                     \___\
                          `

</div>
<div>
                        ____
              __//     / _  /`
            --o   \  //_- ._|
           `----^/ /((:_  |  `
                / / ||  --,      __
                \ \^//   \_____//-- >
                (  _        ---'
                 ||,-__( /--
                 `-/___||/
                       ||_,
                       `--/


</div>
<div>


                                       __
              __//_.-----^^^^^^^_____//-- >
           .--o   ___-(  //   |   ---'
          #:----^'     |//   / /--
        ###            ((:-._\|/
                       |||   /:_,
                        | \_\`--/
                         \__/`



</div>
<div>
                             ____
                            / _  /`
                          //_- ._|
                         ((:_  |  `
                         ||  --,      __
             __//_.-----^//   \_____//-- >
          .--o   ___-(  _        ---'
         #:----^'     ||,-__( /--
       ###            `-/___||/
     ######                 ||_,
      ####                  `--/


</div>
<div>



                                      __
             __//_.-----^^^^^^^_____//-- >
          .--o   ___-(  //   |   ---'
         #:----^'     |//   / /--
      ##0#            ((:-._\|/
    #000#             |||   /:_,
  #000000#             | \_\`--/
   #0000#               \__/`
    ####

</div>
<div>
                               ____
                            / _  /`
                          //_- ._|
                         ((:_  |  `
     #                   ||  --,      __
      #   #  __//_.-----^//   \_____//-- >
 #   #   #.--o   ___-(  _        ---'
#     ###0:----^'     ||,-__( /--
 #  ##00-0##          `-/___||/
  ##0---0## #               ||_,
 #0------0##                `--/
 ##0----0## #
####0000####
</div>
<div>



             __//                     __
           --o  --------^^^^^^^_____//-- >
    #     `----^----'(  //   |   ---'
 #   #  #             |//   / /--
  # ####              ((:-._\|/
 # #00-0#   #         |||   /:_,
##0--0##               | \_\`--/
 ##0---0## #            \__/`
##0---0##
####0000####
</div>
<div>
                         ___
                        /   /`
                      // ..-_\
                    ,((:'    /`
                      \\''._\          __
       #     __//      \\   /`  _____//-- >
    #      --o  -----_^//    \^^  ---'
 #  ##    `----^----'( ._____( /--
  # ###               \\_,____\\/__,
   #0-0#  #            \-/     \---/
    #0--0##
  ##0--0##
 ###000####
</div>
<div>


                                     __
           __//              _____//-- >
         --o  ----_^^^^^^^^^^  ---'
        `----^---'( \\   /( /--
                   \// _\,_\\/__,
    #              //,' /   \---/
   ##    #       `((:,  _\,
 # ###             \\ '' /
  #00#               \___\
 ##000#                   `
##000###
</div>
<div>
                      ___
                     /   /`
                   // ..-_\
                 ,((:'    /`
                   \\''._\          __
          __//      \\   /`  _____//-- >
        --o  -----_^//    \^^  ---'
       `----^----'( ._____( /--
  # #              \\_,____\\/__,
   ##               \-/     \---/
  ###
  ####
 #####
</div>
<div>


                                     __
          __//               _____//-- >
        --o  -----_^^^^^^^^^^  ---'
       `----^----'( \\   /( /--
                   \// _\,_\\/__,
                   //,' /   \---/
                 `((:,  _\,
   #               \\ '' /
    #                \___\
   #                      `
  ###
</div>
</div>

<script>
/// @license magnet:?xt=urn:btih:0b31508aeb0634b347b8270c7bee4d411b5d4109&dn=agpl-3.0.txt AGPLv3+
function Animation(frame_container, frame_ids, fps, loop, after=null)
{
    this.frame_container = frame_container;
    this.frame_ids = frame_ids;
    this.fps = fps;
    this.loop = loop;
    this.after = after;

    this.finished = false;
    this.frame_divs = frame_container.children;
    this.current_frame_div = null;
    this.frame_index = -1;
    this.timeout = null;

    this.set_frame = function(id)
    {
        this.hide_frame();
        this.current_frame_div = this.frame_divs[id];
        this.current_frame_div.style.display = "block";
    };

    this.hide_frame = function(id)
    {
        if ( this.current_frame_div )
            this.current_frame_div.style.display = "none";
    };

    this.next_frame = function()
    {
        this.frame_index += 1;
        if ( this.frame_index >= this.frame_ids.length )
        {
            if ( !this.loop )
            {
                this.pause();
                this.finished = true;
                if ( this.after )
                    this.after();
                return;
            }
            else if ( this.loop !== true )
            {
                this.loop -= 1;
            }

            this.frame_index = 0;
        }
        this.set_frame(this.frame_ids[this.frame_index]);
    };

    this.resume = function()
    {
        this.timeout = setTimeout(this.resume.bind(this), 1000/this.fps);
        this.next_frame();
    };

    this.pause = function()
    {
        if ( this.timeout )
        {
            clearTimeout(this.timeout);
            this.timeout = null;
        }
    };

    this.reset = function()
    {
        this.loop = loop;
        this.current_frame_div = null;
        this.frame_index = -1;
        this.timeout = null;
        this.finished = false;
    };

    this.play = function()
    {
        this.reset();
        this.resume();
    };
}

function AnimationSequence(animations, loop)
{
    this.animations = animations;
    this.loop = loop;

    this.current = -1;

    this.next_animation = function()
    {
        if ( this.current != -1 &&
            ( this.loop || this.current != this.animations.length - 1 )
           )
        {
            this.animations[this.current].hide_frame();
        }

        this.current += 1;
        if ( this.current >= this.animations.length )
        {
            if ( !this.loop )
                return;
            this.current = 0;
        }
        this.animations[this.current].play();
    };

    this.pause = function()
    {
        if ( this.current != -1 )
            this.animations[this.current].pause();
    };

    this.resume = function()
    {
        if ( this.current != -1 )
            this.animations[this.current].resume();
    };

    this.play = function()
    {
        this.next_animation();
    };

    for ( var i = 0; i < this.animations.length; i++ )
    {
        this.animations[i].after = this.next_animation.bind(this);
    }
}
var frame_container = document.getElementById("frames");
var animation = new AnimationSequence(
    [
        new Animation(frame_container, [0, 1], 5, 3),
        new Animation(frame_container, [2, 3, 4, 5, 6, 7, 8, 9, 10, 11], 5, false),
    ],
    true
);
animation.play();
// @license-end
</script>
        <?php

    }
};

$page = new AsciiPage();
