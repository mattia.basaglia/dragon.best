<?php

require_once(__dir__."/../dragon.php");
require_once(__dir__."/../lib/pages/gallery.php");

class LottiePage extends DurgPage
{
    use FileGalleryTrait {
        FileGalleryTrait::render_gallery_thumbnails as private _render_gallery_thumbnails;
    }
    const GALLERY = 0;
    const ZOOM = 1;
    const RECOLOR = 2;

    public $title = "Lottie";
    public $styles = [
        "/media/styles/durg.css",
        "/media/styles/gallery.css"
    ];
    public $scripts = [
        "https://cdnjs.cloudflare.com/ajax/libs/bodymovin/5.5.3/lottie.js"
    ];

    function base_uri()
    {
        return "/lottie/";
    }

    function media_path()
    {
        return "/media/img/lottie/";
    }

    function raster_image_path()
    {
        return "/media/img/rasterized/lottie/";
    }

    function server_image_path()
    {
        return dirname(dirname($this->self_dirname())) . $this->media_path();
    }

    protected function get_css_class($selected)
    {
        if ( $selected == 1 ) return "durgpic selected";
        if ( $selected == 2 ) return "bigdurg";
        return "durgpic";
    }

    protected function image_extensions()
    {
        return array("json");
    }

    function humanmeta_template($image)
    {
        return "%(name) by %(author) &copy; %(copyrightYear) %(license:html)";
    }

    protected function render_gallery_thumbnails($focus, $render_args)
    {
        echo mkelement(["div", ["style" => "text-align: center;"], [
         ["p", [], "You can install my animated stickers on telegram:"],
         ["ul", ["class" => "buttons"], [
            ["li", [], new Link("https://t.me/addstickers/GlaxAnimated", "Glax Animated")],
            ["li", [], new Link("https://t.me/addstickers/GlaxAnimated2", "Glax Animated 2")],
         ]]
        ]]);
        $this->_render_gallery_thumbnails($focus, $render_args);
    }

    protected function load_image_object($filename, $slug, $title, $extension)
    {
        return new LottieImageInfo($this, $filename, $slug, $title, $extension);
    }


    protected function load_image_metadata($image)
    {
        if ( strpos($image->filename, "/commissions/") === 0 )
            $image->meta["license"] = License::find_license("CC BY-NC-ND");
    }
}

$page = new LottiePage();
