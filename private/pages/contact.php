<?php

require_once(__dir__."/../dragon.php");
require_once(__dir__."/../lib/telegram.php");

class ContactPage extends DurgPage
{
    public $title = "Contact Information";
    public $description = "How to get in touch";

    function main_type($render_args)
    {
        return "Person";
    }

    function main($render_args)
    {
        $this->body_title(null, $render_args);
        ?>
        <div typeof="Person">
            <p><span property="owns" typeof="WebSite"><a property="url" href="https://dragon.best/">dragon.best</a></span>
            is by <span property="name">Mattia "Glax" Basaglia</span>.</p>

            <h2>Website issues</h2>
            <p>If something isn't working properly on this website, please
            <a href="https://gitlab.com/mattia.basaglia/dragon.best/issues">create an issue on GitLab</a>.</p>

            <h2>Telegram</h2>
            <p>Feel free to message me at <a property="sameAs" href="https://t.me/MattBas">@MattBas</a>,
                I don't bite (most of the times).</p>
            <p><?php echo new Link("/telegram/", "Best telegram groups and bots"); ?></p>
            <p><?php echo new Link("/stickers/", "Telegram sticker sets"); ?></p>

            <h2>Conventions</h2>
            <p>You can see which cons I've attended or I'm planning to attend in the <a href="/cons">Conventions</a> page.</p>

            <h2>Various accounts</h2>
            <table>
                <?php
                    function simple_link($site_name, $uri, $nickname="mattbas")
                    {
                        echo mkelement(
                            ["tr", [], [
                                ["th", [], $site_name],
                                ["td", [], new Link($uri, $nickname, ["property" => "sameAs"])]
                            ]]
                        );
                    }
//                     simple_link("Twitter", "https://twitter.com/GlaxDurg", "@GlaxDurg");
                    simple_link("Bluesky", "https://bsky.app/profile/glax.dragon.best", "@glax.dragon.best");
                    simple_link("Redbubble", "https://www.redbubble.com/people/mattbas");
                    simple_link("DeviantArt", "https://mattbas.deviantart.com/");
                    simple_link("FurAffinity", "https://www.furaffinity.net/user/mattbas/");
                    simple_link("GitLab", "https://gitlab.com/mattia.basaglia", "mattia.basaglia");

                ?>
            </table>

            <h2>Donate</h2>
            <ul>
                <?php
                    function donate_link($site_name, $uri)
                    {
                        echo mkelement(
                            ["li", ["typeof" => "DonateAction"], [
                                new Link($uri, $site_name, ["property" => "url"])
                            ]]
                        );
                    }
//                     donate_link("Paypal", "https://paypal.me/GlaxDragon");
                    donate_link("Ko-fi", "https://ko-fi.com/glaxdragon");
                    donate_link("Patreon", "https://www.patreon.com/glax");
                ?>
            </ul>
        </div>
        <?php
    }
}

$page = new ContactPage();
