<?php

require_once(__dir__."/../dragon.php");


class GlaxGPTPage extends DurgPage
{
    public $title = "Glax GPT";
    public $description = "AI Powered system that can answer any question";
//     public $scripts = [];
//     public $styles = [
//         "/media/styles/durg.css",
//     ];

    function extra_head($render_args)
    {
        ?><style>
        #chat_input {
            font-size: large;
            padding: 0.5ex;
            width: 100%;
        }
        .chat-pfp {
            border-radius: 8px;
            display: inline-block;
            width: 56px;
            height: 56px;
            margin-right: 16px;
            background-size: 48px;
            background-repeat: no-repeat;
            background-position: center center;
            vertical-align: middle;
        }
        .chat-pfp.user {
            background-color: var(--ych);
            background-image: url("/media/img/vectors/blep.svg");
        }
        .chat-pfp.bot {
            background-color: #00d77d;
            background-image: url("/media/img/vectors/cyborg.svg");
        }
        #log > div {
            margin: 1ex 0;
            font-size: larger;
            border-bottom: 2px solid var(--shade-3);
            padding: 1ex 0;
        }
        #input_parent {
            display: flex;
        }
        </style><?php
    }

    function main($render_args)
    {
        $this->body_title(null, $render_args);
        ?>
        <p>GlaxGPT is a powerful AI system that can answer any question you ask.</p>

        <p>Disclaimer: The model might show some bias towards dragons.</p>

        <div id="log"></div>

        <p id="input_parent">
            <span class="chat-pfp user"></span>
            <input id="chat_input" type="text" autocomplete="off" onkeydown="if ( event.key == 'Enter' ) submit_request();"/>
        </p>
<!--         <button onlick="submit_request()"> -->

        <script>
            let input = document.getElementById("chat_input");
            let output = document.getElementById("log");

            function add_log(pfp, text)
            {
                let div = output.appendChild(document.createElement("div"));
                div.appendChild(document.createElement("span")).setAttribute("class", "chat-pfp " + pfp);

                let span = div.appendChild(document.createElement("span"));
                span.classList.add("chat-text");
                span.appendChild(document.createTextNode(text));
            }

            function on_response(data)
            {
                let response;
                let score = data.sentences[data.sentences.length-1].score;
                if ( score < 0 )
                    response  = "Not dragons";
                else if ( score > 0 )
                    response  = "Dragons";
                else
                    response = "I don't know";

                add_log("bot", response);
            }

            function submit_request()
            {
                let text = input.value;
                if ( text.trim().length === 0 )
                {
                    input.value = "";
                    return
                }

                add_log("user", input.value);

                fetch("https://bots.dragon.best/api/sentiment/?q=" + encodeURIComponent(text))
                    .then(resp => resp.json())
                    .then(on_response)
                ;
                input.value = "";
            }

        </script>
        <?php
    }
}

$page = new GlaxGPTPage();

