<?php

require_once(__dir__."/../dragon.php");

class DragonBestPage extends DurgPage
{
    public $title = "Dragons are the best at everything";
    public $description = "Except at being the worst";

    function main($render_args)
    {
        $this->body_title(null, $render_args);
        echo mkelement(["p", [], $this->description]);
    }
};

$page = new DragonBestPage();
