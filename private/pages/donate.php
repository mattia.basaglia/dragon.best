<?php

require_once(__dir__."/../dragon.php");

class DonatePage extends DurgPage
{
    public $title = "Donate";
    public $description = "Send a donation to Glax";

    function main($render_args)
    {
        $this->body_title(null, $render_args);
        ?>
        <div typeof="TipAction">
            <meta property="recipient" typeof="Person" content="Glax" />
            <p>
                If you reached this page it means you got one of my free commissions
                or you really like my content.
            </p>
            <p>
                Everything I produce on my free time I do for free, and while I
                appreciate donations they are in no way required or expected.
            </p>
            <p>
                If you decided you want to leave a donation here are your options:
            </p>
            <?php

            echo new LinkList([
//                 new Link("https://paypal.me/GlaxDragon", "PayPal", ["property" => "url"]),
                new Link("https://ko-fi.com/glaxdragon", "Ko-fi", ["property" => "url"]),
                new Link("https://www.patreon.com/glax", "Patreon", ["property" => "url"]),
            ]);
            ?>
        </div>
        <?php
    }
}

$page = new DonatePage();
