<?php

require_once(__dir__."/../dragon.php");

class TonguePage extends DurgPage
{
    public $title = "Glax's Tongue";
//     public $description = "";
    public $default_image = "/media/img/rasterized/icon/scalable.png";
    public $scripts = [
        "/media/scripts/tongue.js",
    ];

    function extra_head($render_args)
    {
        ?>
        <style>
            #game_area {
                position: relative;
                width: 100%;
                height: 812px;
                overflow: hidden;

                user-select: none;
                touch-action: none;
            }
        </style><?php
    }

    function main($render_args)
    {
        ?>
        <div class="game" typeof="Game">
            <h1 property="name">Glax's Tongue</h1>

            <div>
                <p><input type="file" onchange="image_file_input(event);" /></p>
                <p>
                    <input type="url" placeholder="Image URL" id="url_input" />
                    <button onclick="image_url_input();">From URL</button>
                </p>
            </div>

            <svg
                id="game_area"
                xmlns="http://www.w3.org/2000/svg"
                xmlns:xlink="http://www.w3.org/1999/xlink"
            >
                </defs>
                <g transform="translate(0,150)">
                    <g id="scale_target">
                        <image id="target" xlink:href=""
                            x="512" width="512" height="512" />
                        <image xlink:href="/media/img/pages/tongue/tongue_bottom.svg"/>
                        <g
                            id="tongue"
                            stroke="transparent"
                            fill-rule="nonzero"
                        ></g>
                        <image xlink:href="/media/img/pages/tongue/tongue_top.svg"/>
                    </g>
                </g>
            </svg>
        </div>

        <script type="module">
            import {MouseAnimator, SkeletonViewer} from "/media/scripts/module/ik-svg.js";
            import {IKSolver, MaxAngleDifferenceConstraint} from "/media/scripts/module/ik.js";
            import {lerp, lerp_point} from "/media/scripts/module/ik-math.js";

            function on_resize()
            {
                var width = svg.clientWidth;
                var target = 1024;
                if ( width < target )
                {
                    var scale_target = document.getElementById("scale_target");
                    scale_target.setAttribute("transform", `scale(${width/target})`);
                }
            }

            function image_set_url(url)
            {
                document.getElementById("target").setAttributeNS("http://www.w3.org/1999/xlink", "href", url);
            }

            function image_file_input(ev)
            {
                image_receive_files(ev.target.files);
            }

            function image_receive_files(files)
            {
                for ( var i = 0; i < files.length; i++ )
                {
                    var file = files[i];
                    if ( file.type.match("image") )
                    {
                        var reader = new FileReader();

                        reader.onload = function(e2)
                        {
                            image_set_url(e2.target.result);
                        };

                        reader.readAsDataURL(file);
                        return;
                    }
                }
            }

            function image_url_input()
            {
                image_set_url(document.getElementById("url_input").value);
            }

            let tongue = new IKSolver({
                anchored: true,
                element: document.getElementById("tongue"),
                // element_top: document.getElementById("tongue_top"),
                root: {
                    x: 256,
                    y: 256,
                    radius: 44,
                },
                finish: {
                    x: 1024,
                    y: 256,
                    radius: 16,
                },
                styles: [
                    [6, "#b53147"],
                    [0, "#e84d66"],
                ],
                segments: 32
            });

            let svg = document.getElementById("game_area");
            let node = tongue.root;
            let bone;
            for ( let i = 1; i <= tongue.config.segments; i++ )
            {
                let f = i / tongue.config.segments;
                let pos = lerp_point(tongue.config.root, tongue.config.finish, f);
                pos.radius = lerp(tongue.config.root.radius, tongue.config.finish.radius, f);

                bone = tongue.add_bone(node, pos);
                node = bone.end;
            }
            let target = tongue.add_target(bone.end);

            for ( let [grow, color] of tongue.config.styles )
            {
                let skel = new SkeletonViewer(SkeletonViewer.BallBoxes);
                skel.grow = grow;
                skel.default_style = (shape) => { shape.style.fill = color };
                tongue.attach_graphics(skel, null);
                skel.add_to_element(tongue.config.element);
            }

            let animator = new MouseAnimator(target, svg, tongue.config.element, tongue);
            animator.play();

            window.addEventListener("resize", on_resize);
            on_resize();
            window.image_file_input = image_file_input;
            window.image_url_input = image_url_input;
        </script>
        <?php

    }
}

$page = new TonguePage();

