<?php

require_once(__dir__."/../dragon.php");

class DerpPage extends DurgPage
{
    public $title = "Derpy dragons are the best";
    public $description = "";
    public $default_image = "/media/img/rasterized/icon/scalable.png";

    function main($render_args)
    {
        $this->body_title(null, $render_args);
        echo mkelement(["p", [], $this->description]);
    }
};

$page = new DerpPage();
