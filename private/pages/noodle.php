<?php

require_once(__dir__."/../dragon.php");

class NoodlePage extends DurgPage
{
    public $title = "Noodle Derg";
    public $description = "Play with the noodle derg";
    public $default_image = "/media/img/pages/noodle/nood.png";
    public $scripts = [];

    function get_meta_type($render_args)
    {
        return "photo";
    }

    function extra_head($render_args)
    {
        ?>
        <style>
            body {
                touch-action: none;
                overscroll-behavior: contain;
            }
            .filler {
                min-height: 90vh;
            }
            #game_area {
                position: fixed;
                top: 0;
                left: 0;
                width: 100vw;
                height: 100vh;

                user-select: none;
                pointer-events: none;
            }
            button.button {
                border: 1px solid var(--shade-0);
                color: var(--shade-0);
                background: var(--shade-4);
                border-radius: 5px;
                font-size: inherit;
                padding: 5px;
                display: inline-flex;
                cursor: pointer;
            }
            button.button:hover {
                border-color: var(--shade-5);
                color: var(--shade-5);
                background: var(--shade-1);

            }
        </style><?php
    }

    function main($render_args)
    {
        ?>
        <div class="game" typeof="Game">
            <h1 property="name">Noodle Derg <button class="button" title="settings" onclick="show_settings()"><i class='bx bxs-bowl-hot'></i></button></h1>
            <div id="settings" style="display: none">
                <p>
                    <label for="noodle-length">Length</label>
                    <input type="number" autocomplete="off" min="3" value="32" id="noodle-length" />
                </p>
                <p>
                    <label for="noodle-speed">Speed</label>
                    <input type="number" autocomplete="off" min="1" value="10" id="noodle-speed" />
                </p>
                <p>
                    <label for="ponder-orb">Ponder Orb</label>
                    <input type="checkbox" id="ponder-orb" />
                </p>
                <p>
                    <label for="follow-mouse">Follow Mouse</label>
                    <input type="checkbox" id="follow-mouse" checked="checked" />
                </p>
                <details>
                    <summary>Holding Pattern</summary>
                    <p>
                        <label for="holding-preset">Preset</label>
                        <select id="holding-preset" autocomplete="off" onchange="document.getElementById('holding-pattern').value = this.value"></select>
                    </p>
                    <textarea id="holding-pattern" cols="40" rows="12"></textarea>
                </details>
                <p>
                    <button class="button" onclick="apply_settings()">
                        <i class='bx bxs-bowl-hot'></i> Noodle Away <i class='bx bxs-bowl-hot'></i>
                    </button>
                </p>
            </div>
            <p class="filler">
                WARNING!<br/>
                Do not feed the noodle any mice or fingers<br/>
                Do not tap on the glass
            </p>

            <svg
                id="game_area"
                xmlns="http://www.w3.org/2000/svg"
                xmlns:xlink="http://www.w3.org/1999/xlink"
            ><style>
                .crest {
                    fill: var(--glax-body-dark);
                    stroke: var(--glax-body-outline);
                }
                .body {
                    fill: var(--glax-body-main);
                    stroke: var(--glax-belly);
                }
                .belly {
                    fill: var(--glax-belly);
                    stroke: var(--glax-body-outline);
                }
                .snoodle {
                    stroke-width: 8;
                    stroke: var(--glax-body-dark);
                    fill: none;
                }
                path {
                    stroke-linecap: round;
                    stroke-linejoin: round;
                }
                .orb {
                    fill: var(--glax-iris);
                    stroke: var(--glax-iris-dark);
                    stroke-width: 4;
                }
            </style>
                <g id="scale_target">
                    <g
                        id="noodle"
                        fill="red"
                        stroke="green"
                        stroke-width="4"
                        fill-rule="nonzero"
                    ></g>
                </g>
            </svg>
        </div>

        <script type="module">
            import {Noodle, NoodleAnimator, figure_8} from "/media/scripts/module/noodle.js";

            function on_resize()
            {
                var width = game_area.clientWidth;
                var target = 800;
                if ( width < target )
                {
                    var scale_target = document.getElementById("scale_target");
                    scale_target.setAttribute("transform", `scale(${width/target})`);
                }
            }

            function renoodle(segments, speed, holding_pattern)
            {
                let element = document.getElementById("noodle");

                if ( noodle )
                {
                    element.innerHTML = "";
                    animator.pause();
                }

                segments = Math.max(3, segments);

                let config = {
                    segments: segments,
                    segment_length: 48,
                    start: {x: -segments * 48, y: 300},
                    scale_size: 12,
                    max_angle: Math.PI / 6,
                    tail: {
                        percent: 0.4,
                        tip: 8,
                        radius: 12,
                        segment_length: 48 * 0.6
                    },
                    belly: {
                        radius: 44
                    },
                    legs_percent: 0.5,
                    arms_percent: 0.8,
                    front_limb_offset: 2,
                    image_path: "/media/img/pages/noodle/",
                    svg_parent: element,
                    snoodle:{
                        segments: 8,
                        length: 300,
                        max_angle: Math.PI / 3,
                    },
                    animation: {
                        max_angle_distance: 300,
                        max_angle: Math.PI / 6,
                        speed: 10,
                    }
                };

                if ( segments < 20 )
                {
                    config.front_limb_offset = 1;
                    config.legs_percent = 0.45;
                    config.arms_percent = 0.7;
                }


                noodle = new Noodle(config);
                if ( animator )
                    animator.renoodle(noodle);
                else
                    animator = new NoodleAnimator(noodle, game_area, holding_pattern);
            }

            function show_settings()
            {
                animator.pause();
                document.getElementById("settings").style.display = "block";
            }


            function apply_settings()
            {
                document.getElementById("settings").style.display = "none";
                var hold;
                try {
                    hold = build_holding_pattern(document.getElementById("holding-pattern").value);
                    var p = hold(0, 100, 100);
                    if ( typeof p.x != "number" || typeof p.y != "number" )
                        hold = figure_8;
                } catch(e) {
                    hold = figure_8;
                }

                var length = document.getElementById("noodle-length").value;
                var speed = document.getElementById("noodle-speed").value;

                if ( !noodle || length != noodle.config.segments )
                {
                    renoodle(length, speed, hold);
                }
                else
                {
                    animator.holding_pattern = hold;
                    animator.speed = speed;
                }

                noodle.orb.shape.style.display = document.getElementById("ponder-orb").checked ? "inline" : "none";
                animator.follow_mouse = document.getElementById("follow-mouse").checked;
                animator.play()
            }

            function build_holding_pattern(code)
            {
                return new Function("t", "width", "height",
                    "var x = 0; var y = 0;\n" + code + "\n; return {x, y};"
                );
            }

            var holding_defaults = {
                "Figure 8": `
var radius = height / 2;
var scale = 2 / (3 - Math.cos(2 * t));
x = Math.sin(2 * t) * radius / 2;
y = Math.cos(t) * radius;
`,
                "Circle": `
var radius = Math.min(width, height) / 2;
x = Math.cos(t) * radius;
y = Math.sin(t) * radius;
`,
                "Loops": `
x = Math.cos(t * 3 / 2) * width / 2;
y = Math.sin(t) * height / 2;
`,
            };

            var game_area = document.getElementById("game_area");
            var noodle = null;
            var animator;

            var pattern_select = document.getElementById("holding-preset");
            for ( let [name, code] of Object.entries(holding_defaults) )
            {
                var opt = document.createElement("option");
                opt.value = code.trim();;
                opt.appendChild(document.createTextNode(name));
                pattern_select.appendChild(opt);
            }

            document.getElementById("holding-pattern").value = Object.values(holding_defaults)[0].trim();

            apply_settings();
            window.addEventListener("resize", on_resize);
            on_resize();

            window.show_settings = show_settings;
            window.apply_settings = apply_settings;

        </script>
        <?php

    }
}

$page = new NoodlePage();
