<?php

require_once(__dir__."/../dragon.php");


class AwesomeTestPage extends DurgPage
{
    public $title = "Are dragons awesome?";
    public $description = "Answer here!";

    function main($render_args)
    {
        $this->body_title(null, $render_args);

        if ( isset($_POST["answer"]) )
        {
            if ( $_POST["answer"] == "yes" )
                echo mkelement(["p", [], "They are indeed!"]);
            elseif ( $_POST["answer"] == "derg" )
                echo mkelement(["p", [], "How did you get here? Are you an evil haxx0r?"]);
            else
                echo mkelement(["p", [], "We are sorry, your answer is invalid"]);

            echo new PlainLink(".", "Answer again");
        }
        else
        {
            ?>
            <form method="post">
                <p>Are dragons awesome?</p>
                <button type="submit" name="answer" value="yes">Yes</button>
                <button type="submit" name="answer" value="no" id="running" tabindex="-1">No</button>
            </form>
            <script>
                var running_button = document.getElementById("running");

                function get_point(element, to_x, to_y, length)
                {
                    var width2 = element.offsetWidth / 2;
                    var height2 = element.offsetHeight / 2;
                    var start_x = element.offsetLeft + width2;
                    var start_y = element.offsetTop + height2;
                    var angle = Math.atan2(
                        to_y - start_y,
                        to_x - start_x
                    );
                    return [
                        start_x + length * Math.cos(angle) - width2,
                        start_y + length * Math.sin(angle) - height2
                    ];
                }

                function run_away(event)
                {
                    var dest_x, dest_y;
                    [dest_x, dest_y] = get_point(
                        event.target,
                        event.clientX,
                        event.clientY,
                        - event.target.offsetWidth / 2
                    );
                    event.target.style.position = "absolute";
                    event.target.style.left = dest_x + "px";
                    event.target.style.top = dest_y + "px";
                }

                running_button.addEventListener("mouseenter", run_away);
                running_button.addEventListener("touchstart", function(event){
                    var touch = event.touches[0];
                    touch.target = event.target;
                    run_away(touch);
                });
                running_button.addEventListener("touchmove", function(event){
                    var element = event.target;
                    var touch = event.touches[0];

                    var width2 = element.offsetWidth / 2;
                    var height2 = element.offsetHeight / 2;
                    var start_x = element.offsetLeft + width2;
                    var start_y = element.offsetTop + height2;
                    var distance = Math.sqrt(
                        (touch.clientX - start_x) ** 2,
                        (touch.clientY - start_y) ** 2
                    );
                    if ( distance <= element.offsetWidth / 2 )
                    {
                        touch.target = element;
                        run_away(touch);
                    }
                });
            </script>
            <?php
        }
    }
}

$page = new AwesomeTestPage();

