<?php

require_once(__dir__."/../dragon.php");

class ColorPage extends DurgPage
{
    public $title = "Blue dragons are the best";

    function extra_head($render_args)
    {
        ?><style>
            body {
                background-color: var(--glax-body-main);
                color: var(--glax-belly);
            }

            h1 {
                margin-bottom: 200px;
            }
        </style><?php
    }

    function main($render_args)
    {
        $this->body_title(null, $render_args);
    }
};

$page = new ColorPage();

