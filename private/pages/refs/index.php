<?php

require_once(__dir__."/../../dragon.php");

class RefsPage extends DurgPage
{
    public $title = "Glax's Reference Sheet";
    public $image_path = "/media/img/pages/refsheet/";

    function get_meta_type($render_args)
    {
        return "photo";
    }

    function image_type($render_args)
    {
        switch ( $render_args["nsfw"] ?? "" )
        {
            case "m":
                return "nsfw-m";
            case "f":
                return "nsfw-f";
            case "reeva":
                return "nsfw-reeva";
            default:
                return "sfw";
        }

    }


    public function image_basename($small, $render_args)
    {
        return ($small ? "small-" : "") . "ref-" . $this->image_type($render_args) . ".png";
    }

    protected function init_patterns()
    {
        $this->patterns["^/nsfw/(?:(m|f|reeva)/)?$"] = "nsfw";
    }

    protected function nsfw($match)
    {
        $this->render(["nsfw" => $match[1] ?? "m"]);
    }

    public function get_meta_image($render_args)
    {
        return $this->image_path . $this->image_basename(true, $render_args);
    }

    function main($render_args)
    {
        $this->body_title(null, $render_args);

        echo mkelement(["p", [], [
            "Here's the ",
            new Link("./palette/", "color palette"),
            ". You can look at my ", new Link("/vectors/", "vectors"), "."
        ]]);

        $image_path = href($this->image_path);
        $image = $this->image_basename(false, $render_args);

        ?>
         <figure typeof='ImageObject'>
            <a href="<?php echo $image_path . $image; ?>" rel="image" property="contentUrl">
                <img src="<?php echo "{$image_path}small-$image"; ?>"
                alt="Glax Refsheet" width="1230" height="836" style="max-width: 100%;height: auto;" />
            </a>
            <figcaption>
                <span property='character'>Glax</span>
                <span property='name'>reference sheet</span>.
            </figcaption>
        </figure>
        <?php
    }
};

$page = new RefsPage();
