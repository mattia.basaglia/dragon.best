<?php

require_once(__dir__."/../../dragon.php");

class PalettePage extends DurgPage
{
    public $title = "Colors";

    function extra_head($render_args)
    {
        ?><style>
            .palette {
                display: flex;
                flex-flow: row wrap;
            }
            .palette > .color {
                width: calc(180px - 2ex);
                height: calc(180px - 2ex);
                display: flex;
                flex-flow: column;
                justify-content: space-evenly;
                padding: 1ex;
                font-family: sans;
            }
            .palette > .color:hover {
                cursor: pointer;
            }
            .bg-dark {
                color: white;
            }
            .bg-light {
                color: black;
            }
            .color-name {
                text-align: center;
                font-size: larger;
                font-weight: bolder;
            }
        </style><?php
    }

    function main($render_args)
    {
        global $palette;
        $this->body_title("Color palette for Glax", $render_args);
        echo new LinkList([
            "GIMP Palette file" => "/media/styles/Palette.gpl?download",
        ], "buttons");
        echo "<div class='palette'>";
        foreach ( $palette->colors as $name => $color )
        {
            if ( $color->luma() < 0.5 )
                $style = "bg-dark";
            else
                $style = "bg-light";

            $human_name = slug_to_title($name);

            print new SimpleElement(
                "div",
                array(
                    "class"=>"color $style",
                    "data-style"=>"$style",
                    "style"=>"background-color: $color;",
                    "title"=>$human_name,
                    "onclick"=>"set_background(this);",
                ),
                array(
                    new SimpleElement("span", array("class"=>"color-name"), $human_name ),
                    new SimpleElement("span", array("class"=>"color-html"), $color->to_html() ),
                    new SimpleElement("span", array("class"=>"color-css"), $color->to_css() ),
                )
            );
        }
        echo "</div>";
        ?><script>
            function set_background(element) {
                document.body.style.backgroundColor = element.style.backgroundColor;
                document.body.classList.remove("bg-dark");
                document.body.classList.remove("bg-light");
                document.body.classList.add(element.dataset.style);
            }
        </script><?php
    }
};

$page = new PalettePage();

