<?php

require_once(__dir__."/../../dragon.php");


class ApiIndex extends DurgPage
{
    public $title = "Best dragon APIs";
    public $description = "APIs to access best dragon data";
    public $base_uri_path = "/api/";


    function endpoint_path($filename="")
    {
        global $site;
        return $site->settings->root_dir . "/private/api/$filename";
    }

    protected function init_patterns()
    {
        $this->patterns["^/(?P<filename>[^.]+)\.(?P<format>[a-z0-9]+)$"] = "endpoint";
        $this->patterns["^/(?P<filename>[^.]+)/$"] = "editor";
    }

    function endpoint($render_args=array())
    {
        $filename = $render_args["filename"];
        $format = $render_args["format"];
        $endpoint_filename = $this->endpoint_path("$filename.php");
        $ok = false;
        if ( is_file($endpoint_filename) )
        {
            # NOTE: filename cannot contain . (therefore no ..) so it's safe
            include_once($endpoint_filename);
            if ( isset($api_page) )
                $ok = $api_page->dispatch($format);
        }
        if ( !$ok )
            throw new HttpStatus(404);
    }

    function editor($render_args=array())
    {
        $this->render($render_args);
    }

    function editor_main($render_args)
    {
        $name = $render_args["filename"];
        $this->body_title($name, $render_args);
        $foo = [];
        $meta = $this->load_endpoint_metadata("$name.php", $foo);
        if ( !$meta )
            throw new HttpStatus(404);

        $this->render_endpoint_editor($name, $meta);

        ?><script>

        for ( let preview of document.querySelectorAll(".api-preview > img") )
        {
            let form = preview.closest(".api-container").querySelector(".api-form");

            let orig_uri = preview.src;
            let el = function(){
                preview.src = orig_uri + '?' + (new URLSearchParams(new FormData(form))).toString();
            }

            for ( let input of form.querySelectorAll("input, select, textarea") )
            {
                input.addEventListener("input", el);
            }
        }
        </script><?php
    }

    function load_endpoint_metadata($basename, &$out)
    {
        $path = $this->endpoint_path($basename);
        if ( is_file($path) && substr($basename, -4) == ".php" )
        {
            include($path);
            if ( isset($api_page) )
            {
                $out[basename($basename, ".php")] = $api_page;
                return $api_page;
            }
        }
        return null;
    }

    function extra_head($render_args)
    {
        ?><style>
            span.url-example {
                font-family: monospace;
            }
            span.url-placeholder {
                font-family: auto;
                font-style: italic;
            }
            .api-param {
                font-weight: bold;
            }
            .api-placehoder {
                font-style: oblique;
            }
            .api-placehoder::before {
                content: "<";
            }
                .api-placehoder::after {
                content: ">";
            }
            .api-form {
                border: 1px solid #ccc;
                display: inline-block;

                padding: 1em;
            }
            .api-form th {
                font-weight: normal;
                text-align: right;
            }
            .api-form table {
                width: 100%;
                table-layout: fixed;
            }
            .api-default, .api-param-choice {
                font-family: monospace;
            }
            .api-default::before, .api-param-choice:first-child::before {
                content: "[";
                font-family: sans;
            }
            .api-default::after, .api-param-choice:last-child::after {
                font-family: sans;
                content: "]";
            }
            .api-param-choice::before {
                font-family: sans;
                content: "|";
            }
            .api-container {
                width: 100%;
                padding: 0;
                margin: 0;
                display: flex;
                flex-flow: row wrap;
            }
            .api-desc {
                width: 50%;
            }
            .api-preview {
                width: 50%;
                align-self: end;
            }
            .api-preview img
            {
                max-width: 100%;
                max-height: 100%;
            }
            .api-preview img
            {
                max-width: 100%;
                max-height: 100%;
            }
            .api-form input, .api-form select {
                max-width: 100%;
                box-sizing: border-box;
            }
            @media screen and (max-width: 512px) {
                .api-desc {
                    width: 100%;
                }
                .api-preview {
                    width: 100%;
                }
            }
        </style><?php
    }

    function main($render_args)
    {
        if ( isset($render_args["filename"]) )
        {
            return $this->editor_main($render_args);
        }

        $this->body_title(null, $render_args);
        echo mkelement(["p", [], $this->description]);
        echo mkelement(["p", [], [
            "All these API endpoints can be fetched with simple GET requests, ",
            "each endpoint can be follows the pattern ",
            ["span", ["class"=>"url-example"], [
                href("/api/"),
                ["span", ["class"=>"url-placeholder"], "endpoint"],
                ".",
                ["span", ["class"=>"url-placeholder"], "format"],
            ]],
        ]]);
        echo mkelement(["p", [], [
            "For the endpoints that take a font parameter, you can check the ",
            new Link("/api/fonts/", "font list"),
            " to see a preview."
        ]]);

        $endpoints = [];
        foreach(scandir($this->endpoint_path()) as $basename )
        {
            if ( $basename == "." || $basename == ".." )
                continue;
            $this->load_endpoint_metadata($basename, $endpoints);
        }

        foreach ( $endpoints as $name => $meta )
        {
            $this->render_endpoint_summary($name, $meta);
        }
    }

    function render_endpoint_summary($name, $meta)
    {
        $section = mkelement(["section", ["id"=>"api_$name"], []]);
        echo $section->open();
        echo mkelement(["header", [], [["h2", [], [["a", ["href"=>"#api_$name"], $name]]]]]);
        $this->render_endpoint_description($name, $meta);
        echo mkelement(["p", [], new PlainLink("$name/", "Documentation for $name")]);
        echo $section->close();
    }

    function render_endpoint_description($name, $meta)
    {
        echo "<div class='api-container'><div class='api-desc'>";

        echo mkelement(["p", [], $meta->description]);

        if ( $meta->main_page )
            echo mkelement(["p", [], [
                "See also ",
                new PlainLink($meta->main_page, basename($meta->main_page)),
                "."
            ]]);
    }

    function render_endpoint_editor($name, $meta)
    {
        $this->render_endpoint_description($name, $meta);

        $fmt_string = [];
        foreach ( $meta->formats as $fmt )
        {
            $uri = "$this->base_uri_path$name.$fmt";
            if ( $meta->params_example )
                $uri .= "?{$meta->params_example}";
            $fmt_string []= (string)(new PlainLink($uri, $fmt));
        }
        $fmt_string = implode(", ", $fmt_string);


        echo mkelement(["p", [], ["Supported formats: ", $fmt_string]]);

        echo mkelement(["p", [], "Parameters:"]);
        if ( $meta->params )
        {
            echo "<dl>";
            foreach ( $meta->params as $param )
            {
                echo $param->description();
            }
            echo "</dl>";

            echo "<form class='api-form'>";

            echo "<table>";
            foreach ( $meta->params as $param )
            {
                echo "<tr><th>";
                echo $param->form_input_label($name);
                echo "</th><td>";
                echo $param->form_input($name);
                echo "</td></tr>";
            }
            echo "</table>";

            echo "<ul class='buttons'>";
            foreach ( $meta->formats as $fmt )
            {
                echo mkelement(["li", [], [["input", [
                    "type"=>"submit",
                    "formaction"=>"$this->base_uri_path$name.$fmt",
                    "value"=>strtoupper($fmt)
                ]]]]);
            }
            echo "</ul>";
            echo "</form>";
        }

        echo "</div>";

        $this->render_preview("png", $name, $meta) || $this->render_preview("jpg", $name, $meta);

        echo "</div>";
    }

    protected function dispatch_noslash($path)
    {
        if ( $path == "" )
            return parent::dispatch_noslash($path);
        return false;
    }

    protected function render_preview($format, $name, $meta)
    {
        if ( in_array($format, $meta->formats) )
        {
            $uri = href("$this->base_uri_path$name.$format");
            echo "<div class='api-preview'><img id='preview_$name' src='$uri'/></div>";
            return true;
        }
        return false;
    }
}

$page = new ApiIndex();
