<?php

require_once(__dir__."/../../dragon.php");
require_once(__dir__."/../../lib/api/base.php");


class ApiFonts extends DurgPage
{
    public $title = "Best dragon API Fonts";

    function extra_head($render_args)
    {
        ?><style>
        </style><?php
    }

    function row($font)
    {
        echo mkelement(["tr",[],[
            ["td", [], [$font]],
            ["td", [], [["img", ["src"=>href("/api/font_preview.png?font=$font")]]]],
        ]]);
    }

    function main($render_args)
    {
        $this->body_title(null, $render_args);

        echo "<table><thead><tr><th>Name</th><th>Preview</th></tr></thead><tbody>";

        $ass_dir = ApiParameter::font_path();
        foreach ( scandir($ass_dir) as $filename )
        {
            $full = "$ass_dir$filename";
            if ( is_file($full) && substr($filename, -4) == ".ttf" )
                $this->row(substr($filename, 0, -4));
        }

        foreach ( Imagick::queryFonts() as $font )
            $this->row($font);

        echo "</tbody></table>";
    }
}

$page = new ApiFonts();
