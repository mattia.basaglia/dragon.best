<?php

require_once(__dir__."/../dragon.php");

class FluffPage extends DurgPage
{
    public $title = "Scaly dragons are the best";
    public $description = "Scales are better than fur";

    function extra_head($render_args)
    {
        ?><style>
            body {
                background-color: var(--glax-body-main);
                color: var(--glax-belly);
            }

            main > p {
                margin-bottom: 200px;
            }
        </style><?php
    }

    function main($render_args)
    {
        $this->body_title(null, $render_args);
        echo mkelement(["p", [], $this->description]);
    }
};

$page = new FluffPage();
