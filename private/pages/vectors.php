<?php

require_once(__dir__."/../dragon.php");
require_once(__dir__."/../lib/pages/gallery.php");

class VectorsPage extends DurgPage
{
    use FileGalleryTrait;
    const GALLERY = 0;
    const ZOOM = 1;
    const RECOLOR = 2;

    public $title = "Vectors";
    public $styles = [
        "/media/styles/durg.css",
        "/media/styles/gallery.css"
    ];
    public $scripts = [
        "/media/scripts/file_tools.js",
        "/media/scripts/recolor.js",
    ];

    function base_uri()
    {
        return "/vectors/";
    }

    function media_path()
    {
        return "/media/img/vectors/";
    }

    function raster_image_path()
    {
        return "/media/img/rasterized/vectors/";
    }

    function server_image_path()
    {
        return dirname(dirname($this->self_dirname())) . $this->media_path();
    }

    protected function get_css_class($selected)
    {
        if ( $selected == 1 ) return "durgpic selected";
        if ( $selected == 2 ) return "bigdurg";
        return "durgpic";
    }

    protected function image_extensions()
    {
        return array("svg");
    }

    protected function init_patterns()
    {
        $this->patterns["^/(?P<image>.*)/zoom/$"] = "zoom";
        $this->patterns["^/(?P<image>.*)/recolor/$"] = "recolor";
    }

    protected function render_type($match, $type)
    {
        $image = $this->find_image(urldecode($match["image"]));
        if ( $image )
        {
            $this->render(array(
                "image" => $image,
                "type" => $type,
            ));
        }
        else
        {
            http_response_code(404);
            $this->render();
        }
    }

    protected function zoom($match)
    {
        $this->render_type($match, VectorsPage::ZOOM);
    }

    protected function recolor($match)
    {
        $this->render_type($match, VectorsPage::RECOLOR);
    }

    function render($render_args=[])
    {
        if ( !isset($render_args["type"]) )
            $render_args["type"] = VectorsPage::GALLERY;
        return parent::render($render_args);
    }

    function body($render_args)
    {
        switch ($render_args["type"])
        {
            case VectorsPage::ZOOM:
                $render_args["image"]->render("durgest", null, ["name", "license", "author", "copyrightYear"]);
                break;
            default:
                parent::body($render_args);
        }
    }
    function extra_head($render_args)
    {
        ?><style>
        #palette_editor input {
            width: 64px;
            height: 64px;
        }
        </style><?php
    }

    function render_focused(MediaFileInfo $image, $render_args)
    {
        $links = [
            "Fullscreen" => $image->details_url()."zoom/",
            "SVG" => "{$image->full_url()}?download",
            "PNG" => "{$image->thumbnail_url()}?download",
            "Recolor" => $image->details_url()."recolor/",
        ];

        if ( $render_args["type"] == VectorsPage::RECOLOR )
        {
            echo "<div id='palette_editor'></div>";
            echo "<iframe id='image_container' src='{$image->full_url()}' rel='image'></iframe>";
            $links["SVG"] = new Link("#", "SVG", ["id"=>"save_svg"]);
            $links["PNG"] = new Link("#", "PNG", ["id"=>"save_png"]);
            $links["Recolor"] = new PlainLink($image->details_url(), "Exit Recolor");
            echo new LinkList($links, "buttons");

            ?><script>
                var controller = new RecolorController({handle_query_string: true});
                <?php
                    global $palette;
                    foreach ( $palette->colors as $name => $color )
                    {
                        $title = slug_to_title($name);
                        $original_color = $color->to_rgb()->to_css();
                        echo "controller.create_color_rule('$original_color', '$name', '$title');";
                    }
                ?>
                controller.setup_page(
                    document.getElementById('image_container'),
                    () => {
                    controller.init_palette_editor(document.getElementById('palette_editor'));
                    controller.init_buttons({
                        "save_png": "save_frame_png",
                        "save_svg": "save_frame_svg",
                    });
                });
                controller.on_change = function(){
                    let query_string = (new URL(document.location.href)).search;
                    let gallery_links = document.querySelectorAll("a.durgpic");
                    for ( let link of gallery_links )
                    {
                        let href = new URL(link.href);
                        href.search = query_string;
                        link.href = href.href;
                    }
                };
                window.addEventListener("load", controller.on_change);
            </script><?php
        }
        else
        {
            $image->render(
                $this->get_css_class(2),
                ["property"=>"image"],
                []
            );
            $this->print_humanmeta($image);;
            echo new LinkList($links, "buttons");
        }

    }

    function humanmeta_template($image)
    {
        return "%(name) %(character) by %(author) &copy; %(copyrightYear) %(license:html)";
    }

    function render_thumbnail($image, $render_args)
    {
        $url = $image->details_url();
        if ( $render_args["type"] == VectorsPage::RECOLOR )
            $url .= "recolor/";
        $image->render_thumbnail(
            $this->get_css_class(0),
            $url,
            $this->thumbnail_meta($image)
        );
    }

    protected function load_image_metadata($image)
    {
        $image->meta["character"] = "Glax";
    }
}

$page = new VectorsPage();
