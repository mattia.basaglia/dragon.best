<?php

require_once(__dir__."/../dragon.php");
require_once(__dir__."/../api/glax_weather.php");

class WeatherPage extends DurgPage
{
    public $title = "Dragon Weather";
    public $description = "Get the weather forecast from your trusty dragon";
    public $default_image = "/media/img/weather/rain-11-thunder-sun.png";

    function extra_head($render_args)
    {
        ?><script>
        function update_location()
        {
            if ( !window.XMLHttpRequest )
                return true;
            call_api(
                document.getElementById("location").value,
                document.getElementById("lon").value,
                document.getElementById("lat").value,
                false
            );
            return false;
        }

        var parent_node;

        function erase_old_content()
        {
            while ( parent_node.firstChild )
                parent_node.removeChild(parent_node.firstChild);
        }

        function error_message(message)
        {
            erase_old_content();
            var cont = document.createElement("div");
            cont.classList.add("error");
            append_text_element(cont, "h2", "Error");
            append_text_element(cont, "p", message);
            parent_node.appendChild(cont);
        }

        function append_text_element(parent, tag, text)
        {
            var element = document.createElement(tag);
            element.appendChild(document.createTextNode(text));
            parent.appendChild(element);
            return element;
        }

        function weather_card(data, parent, title, title_tag)
        {
            var cont = document.createElement("div");
            cont.classList.add("result");
            append_text_element(cont, title_tag, title);
            var card = document.createElement("div");
            card.classList.add("card");
            var img_cont = document.createElement("div");
            img_cont.classList.add("weather-icon");
            var img = document.createElement("img");
            img.setAttribute("src", data.icon);
            img_cont.appendChild(img);
            card.appendChild(img_cont);
            var dl = document.createElement("dl");
            /*var date = new Date(data.timestamp_utc * 1000);
            append_text_element(dl, "dt", "Time");
            append_text_element(dl, "dd", date.toDateString());
            append_text_element(dl, "dd", date2timestring(date));*/
            append_text_element(dl, "dt", "Weather");
            append_text_element(dl, "dd", data.weather);
            append_text_element(dl, "dt", "Temperature");
            append_text_element(dl, "dd", data.temperature + "\u00b0" + data.temperature_unit);
            append_text_element(dl, "dt", "Humidity");
            append_text_element(dl, "dd", data.humidity + "%");
            append_text_element(dl, "dt", "Wind");
            append_text_element(dl, "dd", data.wind_speed + " " + data.speed_unit);
            card.appendChild(dl);
            cont.appendChild(card);
            parent.appendChild(cont);
        }

        function int02(int)
        {
            return String(int).padStart(2, "0")
        }
        function date2timestring(date)
        {
            return int02(date.getHours()) + ":" + int02(date.getMinutes());
        }

        function display_result(response)
        {
            weather_card(response.current, parent_node,
                "Current Weather in " + response.current.city, "h2");

            var cont;
            function push_day(date)
            {
                var super_cont = document.createElement("div");
                super_cont.classList.add("result-day");
                append_text_element(super_cont, "h2", date.toDateString());
                parent_node.appendChild(super_cont);

                cont = document.createElement("div");
                cont.classList.add("result-day-list");
                super_cont.appendChild(cont);
            }
            var day = -1;

            for ( var data of response.forecast )
            {
                var date = new Date(data.timestamp_utc * 1000);
                var ld = date.getDate();
                if ( ld != day )
                {
                    push_day(date);
                    day = ld;
                }
                weather_card(data, cont, date2timestring(date), "h3");
            }
        }

        function call_api(location, lon, lat, replace_state)
        {
            var units = document.getElementById("units").value;
            var query_string = "?";
            query_string += "&units=" + encodeURIComponent(units);

            if ( location )
            {
                query_string += "&location=" + encodeURIComponent(location);
            }
            else if ( lon !== null && lat !== null && lon !== "" && lat !== "" )
            {
                query_string += "&lon=" + encodeURIComponent(lon);
                query_string += "&lat=" + encodeURIComponent(lat);
            }
            else
            {
                return;
            }

            var url = '<?php echo href("/api/glax_weather.json"); ?>';

            var new_url = '<?php echo href("/weather/"); ?>' + query_string;
            if ( replace_state )
                window.history.replaceState(null, "", new_url);
            else
                window.history.pushState(null, "", new_url);

            erase_old_content();

            request = new XMLHttpRequest();
            request.onreadystatechange = function(){
                if ( this.readyState === XMLHttpRequest.DONE )
                {
                    try
                    {
                        var response = JSON.parse(this.responseText);
                    }
                    catch ( SyntaxError )
                    {
                        error_message("Unknown Error");
                        return;
                    }

                    if (this.status === 200)
                        display_result(response)
                    else
                        error_message(response.message);
                }
            };
            request.open('GET', url + query_string + "&forecast=1");
            request.send();
        }

        function round_coord(value, precision=10)
        {
            var factor = 10 ** precision;
            return Math.round(value * factor) / factor;
        }

        function get_current_location()
        {
            if (navigator.geolocation)
            {
                document.getElementById("location").value = "";
                document.getElementById("lon").value = "";
                document.getElementById("lat").value = "";
                navigator.geolocation.getCurrentPosition(
                    show_current_location,
                    current_location_fallback
                );
            }
        }

        function current_location_error()
        {
            error_message("Could not get location from the browser");
        }

        function current_location_fallback()
        {
            if ( window.iplocation )
            {
                call_api(null, window.iplocation.longitude, window.iplocation.latitude, true);
                return;
            }

            current_location_error();
            console.log("fallback");
            window.callback = function callback(data){
                window.iplocation = data;
                call_api(null, data.longitude, data.latitude, true);
            };
            var jsonpcall = document.createElement("script");
            jsonpcall.src = "https://geoip-db.com/jsonp/";
            parent_node.appendChild(jsonpcall);
        }

        function show_current_location(position)
        {
            document.getElementById("lon").value = round_coord(position.coords.longitude);
            document.getElementById("lat").value = round_coord(position.coords.latitude);
            call_api(null, position.coords.longitude, position.coords.latitude, true);
        }
        </script>
        <style>
            .card {
                font-size: small;
                display: flex;
            }
            .card dt {
                font-weight: bold;
            }
            .card dd {
                margin: 0;
            }
            .weather-icon {
                width: 150px;
                height: 150px;
                margin-right: 1ex;
            }
            .weather-icon img {
                width: 100%;
            }
            .result-day-list {
                display: flex;
                flex-flow: row wrap;
            }
            .nowrap {
                white-space: nowrap;
                display: inline-block;
            }
        </style><?php
    }

    function main($render_args)
    {
        $this->body_title(null, $render_args);
        echo mkelement(["p", [], $this->description]);
        $curr_units = $_GET["units"] ?? "metric";
        $units = ["default", "metric", "imperial"];

        function mkinput($name, $label, $type, $placeholder=null)
        {
            $attrs = [
                "id"=>$name,
                "name"=>$name,
                "type"=>$type,
            ];
            if ( $placeholder !== null )
                $attrs["placeholder"] = $placeholder;
            if ( isset($_GET[$name]) )
                $attrs["value"] = $_GET[$name];
            if ( $type == "number" )
                $attrs["step"] = "0.000000001";

            echo mkelement(
                ["span", ["class"=>"nowrap"], [
                    ["label", ["for"=>$name], $label],
                    ["input", $attrs]
            ]]);
        }

        ?><form onsubmit="return update_location();" autocomplete="off"><?php
            mkinput("location", "Location", "text", "City, Country");
            mkinput("lon", "Longitude", "number");
            mkinput("lat", "Latitude", "number");
            echo mkelement(["span", ["class"=>"nowrap"], [
                ["label", ["for"=>"units"], "Units"],
                new Select($units, ["id"=>"units", "name"=>"units"], $curr_units)
            ]]);
        ?>
                <button type="submit">Update</button>
                <br/><button type="button" onclick="get_current_location()">Weather for your current location</button>
            </form>

            <div id="result"></div>
        <script>
            parent_node = document.getElementById("result");
            var get_params = (new URL(document.location)).searchParams;
            if ( get_params.get("lon") !== null || get_params.get("location") !== null )
                call_api(get_params.get("location"), get_params.get("lon"), get_params.get("lat"), true);
            else
                get_current_location();
        </script>
        <?php
    }

    function extra_footer($render_args)
    {
        echo "<br/>";
        echo "Weather data by ",
            new Link("https://openweathermap.org/", "OpenWeatherMap"),
            " (",
            License::find_license("CC BY-SA"),
            ")"
        ;
    }
}

$page = new WeatherPage();
