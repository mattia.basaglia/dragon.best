<?php

require_once(__dir__."/../dragon.php");
require_once(__dir__."/../convention-list.php");

class ConventionAggregate
{
    public $label, $days, $cons, $added_events, $planned_days;

    function __construct($label)
    {
        $this->label = $label;
        $this->days = 0;
        $this->planned_days = 0;
        $this->cons = 0;
        $this->added_events = [];
    }

    function total_days()
    {
        return $this->days + $this->planned_days;
    }

    function to_html($total, $max)
    {
        $w = $max == 0 ? 0 : $this->days * 100. / $max;
        $pw = $max == 0 ? 0 : $this->planned_days * 100. / $max;
        $pc = $total == 0 ? 0 : round($this->total_days() * 100. / $total);

        return mkelement(["tr", [], [
            ["td", [], $this->label],
            ["td", [], $this->days],
            ["td", [], $this->cons],
            ["td", ["class" => "histogram", "title"=>"$pc%"], [
                ["div", ["style"=>"width: $w%"], []],
                ["div", ["style"=>"width: $pw%", "class"=>"planned"], []],
            ]],
        ]], "\n");
    }

    function add_event_days(FurryEvent $event, int $days, bool $planned)
    {
        $key = $event->name . $event->date_string;
        if ( !in_array($key, $this->added_events) )
        {
            array_unshift($this->added_events, $key);
            $this->cons += 1;
        }
        if ( $planned )
            $this->planned_days += $days;
        else
            $this->days += $days;
    }

    function add_event(FurryEvent $event, bool $planned)
    {
        $this->add_event_days($event, $event->duration, $planned);
    }

    static function cmp(ConventionAggregate $a, ConventionAggregate $b)
    {
        if ( $a->days != $b->days )
            return $b->days - $a->days;

        if ( $a->cons != $b->cons )
            return $b->cons- $a->cons;

        return strcmp($a->label, $b->label);
    }
}

class ConventionAggregateArray
{
    public $label_title, $data, $total;

    function __construct($label_title, $total_label = null)
    {
        $this->label_title = $label_title;
        $this->data = [];
        $this->total = null;
        if ( $total_label )
        {
            $this->total = new ConventionAggregate($total_label);
            $this->data[$total_label] = $this->total;
        }
    }

    function add_event($label, $event, bool $planned)
    {
        $this->get($label)->add_event($event, $planned);

        if ( $this->total )
            $this->total->add_event($event, $planned);
    }

    function sort()
    {
        uasort($this->data, ["ConventionAggregate", "cmp"]);
    }

    function to_html($total = null)
    {
        if ( sizeof($this->data) == 0 )
            $max = 0;
        else
            $max = $this->total ? $this->total->total_days() : max(array_map(function ($a) { return $a->total_days(); },  $this->data));
        if ( $total === null )
            $total = $max;

        echo "<table class='table centered-block table stats-table'>";
        echo mkelement(["thead", [], [
            ["tr", [], [
                ["th", [], $this->label_title],
                ["th", [], "Days"],
                ["th", [], "Events"],
                ["th", [], "Histogram"],
            ]]
        ]]);
        echo "<tbody>";
        foreach ( $this->data as $stats )
        {
            echo $stats->to_html($total, $max);
        }
        echo "</tbody></table>";
    }

    function add_labels($labels)
    {
        foreach ( $labels as $label )
            $this->data[$label] = new ConventionAggregate($label);
    }

    function get($label)
    {
        if ( !isset($this->data[$label]) )
            $this->data[$label] = new ConventionAggregate($label);
        return $this->data[$label];
    }
}

class ConventionsPage extends DurgPage
{
    public $styles = [
        "/media/styles/durg.css",
        "https://cdn.jsdelivr.net/npm/ol@v10.2.1/ol.css",
        "https://raw.githack.com/walkermatt/ol-layerswitcher/v4.1.2/src/ol-layerswitcher.css",
        "https://unpkg.com/js-year-calendar@latest/dist/js-year-calendar.min.css",
    ];
    public $scripts = [
        "https://cdn.jsdelivr.net/npm/ol@v10.2.1/dist/ol.js",
        "https://raw.githack.com/walkermatt/ol-layerswitcher/v4.1.2/dist/ol-layerswitcher.js",
        "https://unpkg.com/js-year-calendar@latest/dist/js-year-calendar.min.js",
    ];
    public $title = "Conventions";
    public $description = "Events Glax has attended or is planning to attend";

    function extra_head($render_args)
    {
        ?><style>
        .table td:nth-child(2),
        .table td:nth-child(4),
        .table td:nth-child(5),
        .stats-table td {
            text-align: center;
        }
        .table td span.emoji {
            margin: 0 2px;
        }
        .emoji.suit {
            float: right;
        }
        dl.legend > div {
            display: flex;
            margin: 1em;
            align-items: center;
        }
        dl.legend > div > dt {
            font-size: x-large;
        }
        dl.legend > div > dd {
            margin-left: 1ch;
        }
        .table-wrapper {
            width: max-content;
        }
        .ol-popup {
            position: absolute;
            background-color: var(--shade-5);
            box-shadow: 0 1px 4px rgba(0,0,0,0.2);
            padding: 15px;
            border-radius: 10px;
            border: 1px solid var(--shade-3);
            bottom: 12px;
            left: -50px;
            min-width: 280px;
        }
        .ol-popup:after, .ol-popup:before {
            top: 100%;
            border: solid transparent;
            content: " ";
            height: 0;
            width: 0;
            position: absolute;
            pointer-events: none;
        }
        .ol-popup:after {
            border-top-color: var(--shade-5);
            border-width: 10px;
            left: 48px;
            margin-left: -10px;
        }
        .ol-popup:before {
            border-top-color: var(--shade-3);
            border-width: 11px;
            left: 48px;
            margin-left: -11px;
        }
        .ol-popup-closer {
            text-decoration: none;
            position: absolute;
            top: 2px;
            right: 8px;
            cursor: pointer;
        }
        .ol-popup-closer:after {
            content: "✖";
        }
        .stats-table {
            margin: 0;
        }
        .stats-parent {
            display: flex;
            flex-flow: row wrap;
            justify-content: space-evenly;
            gap: 1em;
        }
        .histogram {
            width: 100px;
            text-align: left !important;
        }
        .histogram > div {
            background-color: var(--glax-body-main);
            height: 1em;
            display: inline-block;
        }
        .histogram > div.planned {
            background-color: var(--glax-belly);
        }
        .selections {
            margin: 1em;
        }
        .selections > div {
            display: flex;
            flex-flow: row wrap;
            justify-content: space-between;
        }
        #mapdiv {
            width: 100%;
            aspect-ratio: 16 / 9;
        }
        </style><?php
    }

    function main($render_args)
    {
        global $convention_list;
        $this->body_title(null, $render_args);
        echo mkelement(["p", [], $this->description]);

        echo "<div class='table-wrapper centered-block'>";
        echo "<div class='selections'>";

        $filter = [
            "Meet" => $_GET["meets"] ?? "0" != "0",
            "Event" => $_GET["events"] ?? "1" != "0",
            "Convention" => $_GET["conventions"] ?? "1" != "0",
        ];

        $checks = [
            ["Conventions",     "Convention",   "conventions",  "1"],
            ["Events",          "Event",        "events",       "1"],
            ["Regular meets",   "Meet",         "meets",        "0"],
            [],
            ["MLP",             "MLP",          "mlp",          "1"],
            ["Furry",           "Furry",        "furry",        "1"],
            ["Board Games",     "Board Game",   "board-games",  "1"],
            ["Fantasy",         "Fantasy",      "fantasy",      "1"],
        ];
        echo "<div>";
        foreach ( $checks as $check )
        {
            if ( sizeof($check) == 0 )
            {
                echo "</div><div>";
                continue;
            }

            $label = $check[0];
            $filter_name = $check[1];
            $get_name = $check[2];
            $default = $check[3];
            $enabled = $_GET[$get_name] ?? $default != "0";
            $filter[$filter_name] = $enabled;
            $checked = $enabled ? "checked='checked'" : "";
            $emoji = FurryEvent::$icons[$filter_name];
            echo "<label><input type='checkbox' name='$get_name' autocomplete='off' $checked onclick='toggle_data(this);'/> $emoji$label</label>";
        }
        echo "</div>";
        echo "</div>";

        echo "<table class='table'>";
        echo mkelement(["thead", [], [
            ["tr", [], [
                ["th", [], "Name"],
                ["th", [], "Month"],
                ["th", [], "Location"],
                ["th", [], "Status"],
                ["th", [], "Type"],
                ["th", [], "Roles"],
            ]]
        ]]);
        echo "<tbody>";

        $year = null;
        $planned = false;
        $current_date = date("Y-m-d");
        $topics = new ConventionAggregateArray("Topic");
        $countries = new ConventionAggregateArray("Country", "🌍");
        $months = new ConventionAggregateArray("Month");
        $months->add_labels(["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]);
        $week_days = new ConventionAggregateArray("Week Day");
        $week_days->add_labels(["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"]);
        $years = new ConventionAggregateArray("Year");
        $scheduler_data = [];
        $names_visited = [];
        $count_planned = 0;

        foreach ( $convention_list as $event )
        {
            if ( $event->skip($filter) )
                continue;

            if ( $current_date < $event->date_string && !$planned)
            {
                $planned = true;
                echo mkelement(["tr", [], [["th", ["colspan" => "6"], "Planned"]]]);
            }

            $countries->add_event($event->full_flag(), $event, $planned);
            $topics->add_event(FurryEvent::$icons[$event->topic] . " {$event->topic}", $event, $planned);

            foreach ( $event->full_date_range() as $day )
            {
                $months->get($day->format("F"))->add_event_days($event, 1, $planned);
                $week_days->get($day->format("l"))->add_event_days($event, 1, $planned);
                $years->get($day->format("Y"))->add_event_days($event, 1, $planned);
            }

            if ( !$planned )
            {
                $names_visited[] = $event->name;
            }
            else
            {
                $count_planned += 1;
            }

            if ( $year != $event->year )
            {
                $year = $event->year;
                echo mkelement(["tr", [], [["th", ["colspan" => "6"], $year]]]);
            }

            $scheduler_data[] = [
                "startDate" => $event->date_string,
                "endDate" => $event->date_end->format("Y-m-d"),
                "name" => $event->name,
                "location" => $event->city,
                "emoji" => implode("", array_map([$event, "type_icon"], $event->tags)),
                "flag" => $event->full_flag(),
                "color" => $event->color,
                "announced" => $event->date_announced,
            ];

            echo $event->to_html($current_date);
        }

        echo "</tbody>";
        echo "</table>";

        echo "<details><summary>Legend:</summary>";
        echo "<dl class='legend'>";

        foreach ( FurryEvent::$icons as $name => $emoji )
        {
            $name = FurryEvent::icon_title($name);
            echo "<div><dt>$emoji</dt><dd>{$name}</dd></div>";
        }
        echo "</dl></details>";

        echo "</div>";

        global $palette;

        ?>

        <h2>Map</h2>
        <div id="mapdiv"></div>
        <div id="popup" class="ol-popup">
            <a id="popup-closer" class="ol-popup-closer"></a>
            <div id="popup-content"></div>
        </div>

        <script>
            var city_data = <?php
                $city_data = [];
                $max = 1;

                foreach ( FurryEvent::$lon_lat as $name => $pos )
                {
                    $cons = array_values(
                        array_filter(
                            $convention_list,
                            function($con) use ($name, $filter) {
                                return $con->city == $name && !$con->skip($filter);
                            }
                        )
                    );
                    $weight = 0;
                    $cons_data = [];
                    foreach ( $cons as $item )
                    {
                        $weight += $item->duration;
                        $cons_data[] = [
                            "name" => $item->name,
                            "year" => $item->year,
                            "meet" => "Meet" == $item->type,
                            "duration" => $item->duration,
                            "attended" => $item->date_string <= $current_date
                        ];
                    }
                    $item = [
                        "city" => $name,
                        "lon" => $pos[0],
                        "lat" => $pos[1],
                        "cons" => $cons_data,
                    ];

                    $item["weight"] = $weight;

                    if ( sizeof($cons) > 0 )
                    {
                        if ( $weight > $max )
                            $max = $weight;

                        $con = $cons[0];
                        $item["flag"] = $con->full_flag();

                        $city_data[] = $item;
                    };
                }
                echo json_encode($city_data);
            ?>;

            var max_weight = <?php echo $max; ?>;

            const container = document.getElementById('popup');
            const content = document.getElementById('popup-content');
            const closer = document.getElementById('popup-closer');

            var src_cons = new ol.source.Vector();

            const style = new ol.style.Style({
                /*text: new ol.style.Text({
                    font: "16px sans",
                    text: "<?php echo FurryEvent::$icons["Attended"]; ?>"
                })*/
                image: new ol.style.Circle({
                    fill: new ol.style.Fill({
                        color: '<?php echo $palette->body_main; ?>',
                    }),
                    stroke: new ol.style.Stroke({
                        color: '<?php echo $palette->body_outline; ?>',
                        width: 1,
                    }),
                    radius: 6,
                })
            });

            const style_future = new ol.style.Style({
                image: new ol.style.Circle({
                    fill: new ol.style.Fill({
                        color: '<?php echo $palette->belly; ?>',
                    }),
                    stroke: new ol.style.Stroke({
                        color: '<?php echo $palette->body_outline; ?>',
                        width: 1,
                    }),
                    radius: 6,
                })
            });


            for ( let city of city_data )
            {
                var feature = new ol.Feature({
                    geometry: new ol.geom.Point(ol.proj.fromLonLat([city.lon, city.lat])),
                    city: city,
                    attended: city.cons.filter(c => !c.attended).length == 0,
                    weight: city.weight / max_weight,
                });
//                 feature.setStyle(city.cons.filter(c => !c.attended).length ? style_future : style);
                src_cons.addFeature(feature);
            }
            const overlay = new ol.Overlay({
                element: container,
                autoPan: {
                    animation: {
                    duration: 250,
                    },
                },
            });
            closer.onclick = function () {
                overlay.setPosition(undefined);
                closer.blur();
                return false;
            };

            var map = new ol.Map({
                target: 'mapdiv',
                controls: ol.control.defaults.defaults().extend([
                    new ol.control.LayerSwitcher(),
                    new ol.control.FullScreen(),
                ]),
                layers: [
                    new ol.layer.Tile({
                        source: new ol.source.OSM()
                    }),
                    new ol.layer.Heatmap({
                        title: "Heatmap",
                        gradient: ["#800", "#f00", "#f80", "#ff0", "#fff"],
                        radius: 12,
                        source: src_cons,
                    }),
                    new ol.layer.Vector({
                        title: "Locations",
                        style: feature => feature.get("attended") ? style : style_future,
                        source: src_cons,
                        renderBuffer: 1400,
                    })
                ],
                overlays: [overlay],
                view: new ol.View({
                    center: ol.proj.fromLonLat([-6.174, 54.915]),
                    zoom: 6
                })
            });
            map.on('click', function(evt) {
                if ( !map.forEachFeatureAtPixel(evt.pixel,
                    function(feature) {
                        var data = feature.get("city");
                        var html = `<p><strong>${data.flag} ${data.city}</strong></p><ul>`;
                        var meets = new Set();
                        for ( var con of data.cons )
                        {
                            if ( con.meet )
                            {
                                if ( !meets.has(con.name) )
                                {
                                    meets.add(con.name);
                                    html += `<li>${con.name}</li>`;
                                }
                            }
                            else
                            {
                                html += `<li>${con.name} ${con.year}</li>`;
                            }
                        }
                        html += "</ul>";
                        content.innerHTML = html;
                        overlay.setPosition(feature.getGeometry().flatCoordinates);
                        return true;
                }) )
                {
                    overlay.setPosition(undefined);
                }
            });
            map.on("pointermove", evt => {
                if ( !evt.dragging )
                    map.getTargetElement().style.cursor = map.hasFeatureAtPixel(map.getEventPixel(evt.originalEvent)) ? "pointer" : "";
            });

            var extent = src_cons.getExtent();
            map.getView().fit(extent, {maxZoom: 6});

            function toggle_data(checkbox)
            {
                let url = new URL(window.location.href);
                url.searchParams.set(checkbox.name, Number(checkbox.checked));
                window.location.href = url;
            }
        </script>

        <h2>Calendar</h2>
        <div id="calendar" style='width: 100%;height: 500px;margin: auto;'></div>
        <div id="calendat_tooltip" style="display: none; position: fixed"></div>
        <style>
            div#calendat_tooltip {
                background: var(--background);
                padding: 1ex;
                border: 1px solid var(--shade-3);
                border-radius: 3px;
                margin-left: 1ex;
                margin-top: 3px;
                flex-flow: column;
                gap: 1em;
            }
            div#calendat_tooltip::before {
                content: " ";
                position: absolute;
                left: -10px;
                top: 0;
                border-style: solid;
                border-width: 10px 10px 10px 0;
                border-color: transparent var(--shade-3) transparent transparent;
            }
            .event-header {
                height: 3px;
                position: absolute;
                left: -1ex;
                right: -1ex;
                top: -1ex;
            }
            .event-tooltip-content:first-child .event-header {
                border-radius: 3px 3px 0 0;
            }
            .event-tooltip-content {
                position: relative;
            }
            .event-name {
                font-weight: bold;
            }
            .day-with-outline {
                margin: 3px !important;
                padding: 2px 3px !important;
                border-radius: 0 !important;
            }
        </style>
        <div id="calendar"></div>
        <script>
            let scheduler_data = <?php echo json_encode($scheduler_data); ?>;
            let calendat_tooltip = document.getElementById("calendat_tooltip");

            function make_gradient(colors)
            {
                let size = 100 / colors.length;
                let stops = colors.map((color, i) => {
                    let pc_start = size * i;
                    let pc_end = size * (i+1);
                    return `${color} ${pc_start}%, ${color} ${pc_end}%`;
                }).join(", ");
                return `linear-gradient(0deg, ${stops})`;
            }

            let calendar = new Calendar("#calendar", {
                style: "custom",
                weekStart: 1,
                mouseOnDay: function(e)
                {
                    if ( e.events.length == 0 )
                        return;

                    var content = '';

                    for ( let ev of e.events )
                    {
                        content += `<div class="event-tooltip-content">
                            <div class="event-header" style="background-color: ${ev.color}"></div>
                            <div class="event-name">${ev.name} ${ev.emoji}</div>
                            <div class="event-location">${ev.flag} ${ev.location}</div>
                        </div>`;
                    }

                    calendat_tooltip.innerHTML = content;
                    calendat_tooltip.style.display = "flex";
                    let rect = e.element.getBoundingClientRect();
                    calendat_tooltip.style.top = rect.top + "px";
                    calendat_tooltip.style.left = rect.right + "px";
                },
                mouseOutDay: function(e) {
                    if ( e.events.length > 0 )
                        calendat_tooltip.style.display = "none";
                },
                dataSource: scheduler_data.map( e => ({
                    ...e,
                    startDate: new Date(e.startDate),
                    endDate: new Date(e.endDate),
                })),
                customDataSourceRenderer(elt, date, events) {
                    const parent = elt.parentElement;
                    if ( events.length == 1 )
                    {
                        if ( events[0].announced )
                        {
                            parent.style.background = events[0].color;
                        }
                        else
                        {
                            parent.style.outline = `3px solid ${events[0].color}`;
                            parent.style.outlineOffset = "-3px";
                        }
                    }
                    else if ( events.length > 1 )
                    {
                        let outline = false;
                        let outline_colors = [];
                        let fill_colors = [];
                        for ( let ev of events )
                        {
                            if ( !ev.announced )
                            {
                                outline = true;
                                fill_colors.push("white");
                            }
                            else
                            {
                                fill_colors.push(ev.color);
                            }

                            outline_colors.push(ev.color);
                        }

                        if ( outline )
                        {
                            elt.classList.add("day-with-outline");
                            parent.style.background = make_gradient(outline_colors);
                            elt.style.background = make_gradient(fill_colors);
                        }
                        else
                        {
                            parent.style.background = make_gradient(fill_colors);
                        }
                    }
                }
            });
        </script>

        <?php
        echo "<h2>Stats</h2>";
        $count_total = sizeof($names_visited);
        sort($names_visited);
        $count_unique = sizeof(array_unique($names_visited));
        echo "<p>So far Glax has visited $count_total events ($count_unique unique ones) and has planned for $count_planned more.</p>";
        echo "<div class='stats-parent'>";
        $countries->sort();
        $countries->to_html();
        $months->to_html($countries->total->days);
        $week_days->to_html($countries->total->days);
        $years->to_html($countries->total->days);
        $topics->sort();
        $topics->to_html();
        echo "</div>";
    }
}

$page = new ConventionsPage();

