<?php

require_once(__dir__."/../dragon.php");

class RandomDragonPage extends DurgPage
{
    public $title = "Random Dragon";
    public $description = "Generate a random dragon";
    public $default_image = "/media/img/rasterized/vectors/sitting.png";
    public $scripts = [
        "/media/scripts/recolor.js",
        "/media/scripts/file_tools.js",
    ];

    function extra_head($render_args)
    {
        ?><style>
        .color-block {
            display: inline-block;
            width: 32px;
            height: 32px;
            vertical-align: middle;
        }
        iframe {
            display: block;
            margin: 0 auto;
        }
        </style><?php
    }

    function main($render_args)
    {
        $this->body_title($this->title, $render_args);
        ?>
        <div>
            <p>
                <label for="dragon_type">Type</label>
                <select id="dragon_type">
                    <option value="-1">Random</option>
                </select>
            </p>
            <button type="submit" id="submit">Generate</button>
        </div>
        <div id="output"></div>

        <div id="output_picture" style="visibility:hidden">
        <iframe id='image_container' src='/media/img/pages/editor/emoji.svg' rel='image'></iframe>
        <?php
            echo new LinkList([
                new Link("#", "SVG", ["id"=>"save_svg"]),
                new Link("#", "PNG", ["id"=>"save_png"]),
                new Link("#", "WebP", ["id"=>"save_webp"]),
                new Link("#", "JSON", ["id"=>"save_json"]),
                new Link("#", "Palette", ["id"=>"save_gpl"]),
            ], "buttons", ["id"=>"buttons"]);
        ?>
        </div>

        <script type="module">
            import {DragonElement, ElementFlag} from "/media/scripts/module/dragon_element.js";
            import {random_item, random_shuffle} from "/media/scripts/module/utils.js";

            let output = document.getElementById("output");
            let dragon_type = document.getElementById("dragon_type");

            for ( let [name, val] of Object.entries(ElementFlag) )
            {
                let opt = dragon_type.appendChild(document.createElement("option"));
                opt.value = val;
                opt.appendChild(document.createTextNode(name));
            }

            function color_div(color)
            {
                let div = document.createElement("div");
                div.classList.add("color-block");
                div.style.backgroundColor = color.toString();
                return div;
            }

            function generate()
            {
                let flag = Number(dragon_type.value);
                if ( flag == -1 )
                    flag = random_item(Object.values(ElementFlag));
                let element = new DragonElement(flag);
                let elem_name = Object.entries(ElementFlag).find(e => e[1] == flag)[0];
                let data = element.full_random();
                let name = data.name[0].toUpperCase() + data.name.substr(1);

                while ( output.firstChild )
                    output.removeChild(output.firstChild);

                output.appendChild(document.createElement("p")).appendChild(
                    document.createTextNode(
                        `${name} the ${elem_name} dragon.`
                    )
                );

                let color_p = output.appendChild(document.createElement("p"));
                color_p.appendChild(document.createTextNode("Colors: "));

                for ( let color of data.colors )
                    color_p.appendChild(color_div(color));

                color_p = output.appendChild(document.createElement("p"));
                color_p.appendChild(document.createTextNode("Eye color: "));
                color_p.appendChild(color_div(data.eye_color));


                controller.reset_features();
                controller.set_current_state(data.state);


                document.getElementById("output_picture").style.visibility = "visible";
            }

            <?php
                global $site;
                echo "var feature_meta = ";
                echo file_get_contents("{$site->settings->root_dir}/media/data/editor/features/emoji-features.json");
                echo ";\n";
            ?>

            let controller = new RecolorController({
                handle_query_string: false,
                advanced_rules: FeatureMode.INKSCAPE_LABEL,
            });
            controller.load_feature_meta(feature_meta);
            controller.setup_page(document.getElementById('image_container'), () => {
                controller.init_buttons({
                    "save_png": "save_frame_png",
                    "save_svg": "save_frame_svg",
                    "save_webp": "save_frame_webp",
                    "save_gpl": "save_data_gpl",
                });
                generate();
            });

            document.getElementById("submit").addEventListener("click", generate);

        </script>
        <?php

    }
}

$page = new RandomDragonPage();

