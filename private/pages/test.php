<?php

require_once(__dir__."/../dragon.php");


class TestPage extends DurgPage
{
    public $title = "Dragon Test";
    public $description = "Find what kind of dragon you are.";
    public $colors = [
        "black" => [
            "kinky" => +0,
            "awesome" => +0,
            "edgy" => +1,
            "unique" => +0,
        ],
        "red" => [
            "kinky" => +0,
            "awesome" => +0,
            "edgy" => +1,
            "unique" => +0,
        ],
        "green" => [
            "kinky" => +0,
            "awesome" => +0,
            "edgy" => +0,
            "unique" => +0,
        ],
        "yellow" => [
            "kinky" => +0,
            "awesome" => +0,
            "edgy" => +0,
            "unique" => +0,
        ],
        "blue" => [
            "kinky" => +0,
            "awesome" => +1,
            "edgy" => +0,
            "unique" => +0,
        ],
        "purple" => [
            "kinky" => +1,
            "awesome" => +0,
            "edgy" => +0,
            "unique" => +0,
        ],
        "white" => [
            "kinky" => +0,
            "awesome" => +0,
            "edgy" => +0,
            "unique" => +0,
        ],
        "other" => [
            "kinky" => +0,
            "awesome" => +0,
            "edgy" => +0,
            "unique" => +1,
        ],
    ];
    public $skin = [
        "skin" => [
            "kinky" => +0,
            "awesome" => +0,
            "edgy" => +1,
            "unique" => +0,
        ],
        "scales" => [
            "kinky" => +0,
            "awesome" => +2,
            "edgy" => +0,
            "unique" => +0,
        ],
        "fur" => [
            "kinky" => +2,
            "awesome" => +0,
            "edgy" => +0,
            "unique" => +0,
        ],
        "other" => [
            "kinky" => +0,
            "awesome" => +0,
            "edgy" => +0,
            "unique" => +2,
        ],
    ];
    public $wings = [
        0 => [
            "kinky" => +2,
            "awesome" => +0,
            "edgy" => +0,
            "unique" => +0,
        ],
        1 => [
            "kinky" => +0,
            "awesome" => +0,
            "edgy" => +3,
            "unique" => +0,
        ],
        2 => [
            "kinky" => +0,
            "awesome" => +2,
            "edgy" => +0,
            "unique" => +0,
        ],
        3 => [
            "kinky" => +0,
            "awesome" => +0,
            "edgy" => +0,
            "unique" => +2,
        ],
    ];
    public $descriptors = [
        0 => ["not", ""],
        1 => ["almost", ""],
        2 => ["slightly", ""],
        3 => ["", ""],
        4 => ["quite", ""],
        5 => ["very", ""],
        6 => ["", "af"],
        7 => ["", "af"],
    ];
    public $attributes = [
        "awesome", "unique", "kinky", "edgy",
    ];

    function get($name, $arr, &$output, $multiplier=1)
    {
        if ( !isset($_GET[$name]) )
            return false;
        $key = $_GET[$name];
        if ( !isset($arr[$key]) )
            return false;
        $values = $arr[$key];
        foreach ( $values as $name => $value )
            $output[$name] += $value * $multiplier;
        return true;
    }

    function get_scores()
    {
        if ( sizeof($_GET) == 0 )
            return false;
        $scores = [
            "kinky" => 0,
            "awesome" => 0,
            "edgy" => 0,
            "unique" => 0
        ];
        if ( !$this->get("primary", $this->colors, $scores, 2) )
            return false;
        if ( !$this->get("secondary", $this->colors, $scores) )
            return false;
        if ( !$this->get("skin", $this->skin, $scores) )
            return false;
        if ( !$this->get("wings", $this->wings, $scores) )
            return false;
        return $scores;
    }

    function get_quality($quality, $value)
    {
        list($before, $after) = $this->descriptors[$value];
        $out = "";
        if ( strlen($before) > 0 )
            $out .= "$before ";
        $out .= $quality;
        if ( strlen($after) > 0)
            $out .= " $after";
        return $out;
    }

    function get_qualities($score)
    {
        $qualities = [];
        foreach ( $this->attributes as $attr )
        {
            $value = $score[$attr];
            if ( $value > 0 )
                $qualities[] = $this->get_quality($attr, $value);
        }
        if ( sizeof($qualities) == 0 )
            return "normal";
        return implode($qualities, ", ");
    }

    function main($render_args)
    {
        $this->body_title(null, $render_args);

        $scores = $this->get_scores();

        if ( $scores !== false )
        {
            $type = $this->get_qualities($scores);

            $vowels = ['a', 'e', 'i', 'o', 'u'];
            if ( sizeof($type) != 0 && in_array(strtolower($type[0]), $vowels) )
                $article = "an";
            else
                $article = "a";

            echo mkelement(["p", [], [
                "Congratulations, you are $article $type dragon!"
            ]]);
        }
        else
        {
            echo "<p>{$this->description}</p>";
            echo mkelement(["form", ["autocomplete"=>"off"], [
                new Table([
                    [
                        ["label", ["for"=>"primary"], "Primary Color "],
                        new Select(
                            array_keys($this->colors),
                            ["name"=>"primary", "id"=>"primary", "required"=>"required"],
                            $_GET["primary"] ?? "other"
                        ),
                    ],
                    [
                        ["label", ["for"=>"secondary"], "Secondary Color "],
                        new Select(
                            array_keys($this->colors),
                            ["name"=>"secondary", "id"=>"secondary", "required"=>"required"],
                            $_GET["secondary"] ?? "other"
                        ),
                    ],
                    [
                        ["label", ["for"=>"skin"], "Skin "],
                        new Select(
                            array_keys($this->skin),
                            ["name"=>"skin", "id"=>"skin", "required"=>"required"],
                            $_GET["skin"] ?? "scales"
                        ),
                    ],
                    [
                        ["label", ["for"=>"wings"], "Wings "],
                        new Select(
                            [
                                "None" => 0,
                                "Just one" => 1,
                                "A pair" => 2,
                                "More than 2 wings" => 3,
                            ],
                            ["name"=>"wings", "id"=>"wings", "required"=>"required"],
                            $_GET["wings"] ?? 2,
                            true
                        ),
                    ],
                ]),
                ["button", ["type"=>"submit"], "Next"],
            ]]);
            echo "<form>";
        }
    }
}

$page = new TestPage();

