<?php

require_once(__dir__."/../dragon.php");
require_once(__dir__."/../lib/number_writer.php");


class TimePage extends DurgPage
{
    public $title = "Dragon Time";
    public $scripts = [
        "/media/scripts/rawr.js"
    ];

    function __construct()
    {
        parent::__construct();
        $this->number_formatter = rawr_number_writer();
    }

    private function derg_number($number)
    {
        return $this->number_formatter->format_number($number);
    }

    function get_meta_image($render_args)
    {
        return "/api/clock.png?time=" . date("H:i:s");
    }

    function get_meta_description($render_args=array())
    {
        $date = new DateTime();
        $minutes = (int) $date->format("i");
        $seconds = (int) $date->format("s");
        $hours_txt = $this->derg_number((int) $date->format("H"));
        $result = ucfirst($hours_txt) . " ";
        if ( $minutes == 0 )
        {
            $result .= "o'durgclock ";
        }
        else
        {
            if ( $minutes < 10 )
                $result .= "oh ";
            $result .= $this->derg_number($minutes);
        }
        $result .= " and " . $this->derg_number($seconds) . " second";
        if ( $seconds != 1 )
            $result .= "s";
        return $result;
    }

    function main($render_args)
    {
        $this->body_title(null, $render_args);
        ?>
        <div id="rawrtime"></div>
        <style>
            iframe
            {
                width: 512px;
                height: 512px;
                max-width: 100vw;
                max-height: 100vw;
            }
        </style>
        <iframe src="/media/img/vectors/clock.svg" id="clock" onload="setup_hands();"></iframe>

        <script>
            var container = document.getElementById("rawrtime");
            var number_formatter = rawr_number_writer();
            var clock_dom = null;
            var hand_minutes = null;
            var hand_hours = null;
            var tx = 181.00151;
            var ty = 330.94797;

            function rotate_hand(hand, fraction)
            {
                var angle = 360 * fraction;
                hand.setAttribute(
                    "transform",
                    "rotate(" + angle + "," + tx + "," + ty + ")"
                );
            }

            function ucfirst(text)
            {
                return text.charAt(0).toUpperCase() + text.slice(1);
            }

            function derg_number(number)
            {
                return number_formatter.format_number(number);
            }

            function update_time()
            {
                var date = new Date();
                var minutes = date.getMinutes();
                var seconds = date.getSeconds();
                var hours = date.getHours();
                var hours_txt = derg_number(hours);
                container.textContent = ucfirst(hours_txt) + " ";
                if ( minutes == 0 )
                {
                    container.textContent += "o'durgclock ";
                }
                else
                {
                    if ( minutes < 10 )
                        container.textContent += "oh ";
                    container.textContent += derg_number(minutes);
                }
                container.textContent += " and " + derg_number(seconds) + " second";
                if ( seconds != 1 )
                    container.textContent += "s";
                container.textContent += ".";

                if ( clock_dom )
                {
                    var fmin = minutes + seconds / 60;
                    var fhours = hours + fmin / 60;
                    rotate_hand(hand_hours, fhours / 12);
                    rotate_hand(hand_minutes, fmin / 60);
                }
            }

            function setup_hands()
            {
                clock_dom = document.getElementById("clock").contentDocument;
                hand_minutes = clock_dom.getElementById("hand_minutes");
                hand_hours = clock_dom.getElementById("hand_hours");
            }

            update_time();
            window.setInterval(update_time, 1000);
        </script>
        <?php

    }
}

$page = new TimePage();
