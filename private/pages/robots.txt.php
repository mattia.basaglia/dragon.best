<?php

require_once(__dir__."/../dragon.php");

class RobotsTxtPage extends PageController
{
    protected function dispatch_to_self($path)
    {
        $this->render();
        return true;
    }

    protected function dispatch_noslash($path)
    {
        return $this->dispatch_to_self($path);
    }

    protected function render()
    {
        header("Content-Type: text/plain");
        echo "User-agent: *\n";
        echo "Disallow: /auth/\n";
        echo "Disallow: /api/*.json\n";
        echo "Sitemap: ".href("/api/sitemap.xml")."\n";
    }

    function base_path()
    {
        return $this->self_dirname();
    }
}

$page = new RobotsTxtPage();
