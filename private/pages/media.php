<?php

require_once(__dir__."/../dragon.php");
require_once(__dir__."/../lib/pages/file.php");


class MediaListPage extends DurgPage
{
    use DirListTrait;

    public $title = "Listing durgectory";

    function path_prefix()
    {
        return "/media/";
    }

    function base_path()
    {
        return path_join(dirname(dirname($this->self_dirname())), $this->path_prefix());
    }

    function extra_head($render_args)
    {
        ?><style>
            ul.file_list > li{
                list-style: none;
            }

            ul.file_list > li > a::before {
                width: 32px;
                height: 32px;
                display: inline-block;
                content: "";
                vertical-align: middle;
                background-size: 32px 32px;
                margin: 4px;
            }

            ul.file_list > li.file_image_svg > a::before {
                background-image: url('/media/img/vectors/icons/icon_image_svg.svg');
            }

            ul.file_list > li.file_parent > a::before {
                background-image: url('/media/img/vectors/icons/icon_directory_up.svg');
            }

            ul.file_list > li.file_directory > a::before {
                background-image: url('/media/img/vectors/icons/icon_directory.svg');
            }

            ul.file_list > li.file_image > a::before {
                background-image: url('/media/img/vectors/icons/icon_image.svg');
            }

            ul.file_list > li.file_text > a::before {
                background-image: url('/media/img/icon/icon_text.svg');
            }

            ul.file_list > li.file_file > a::before {
                background-image: url('/media/img/icon/icon_file.svg');
            }

            ul.file_list > li.file_audio > a::before {
                background-image: url('/media/img/icon/icon_audio.svg');
            }
        </style><?php
    }

    function skip_item($dirname, $filename)
    {
        return strpos($filename, "nsfw") !== false;
    }

    function get_file_type($path)
    {
        $extension = pathinfo($path, PATHINFO_EXTENSION);
        if ( $extension == "svg" )
            return "image_svg";

        list($group, $type) = explode("/", mime_content_type($this->full_path($path)));
        if ( $group == "image" || $group == "text" || $group == "audio" )
            return $group;

        return "file";
    }

    function get_meta_image($render_args)
    {
        return "/media/img/rasterized/vectors/icons/icon_directory.png";
    }

    function get_meta_url_path($render_args)
    {
        return path_join($this->path_prefix(), $render_args["path"]);
    }
}

$page = new MediaListPage();
