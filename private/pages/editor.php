<?php

require_once(__dir__."/../dragon.php");
require_once(__dir__."/../lib/pages/gallery.php");

class EditorPage extends DurgPage
{
    use FileGalleryTrait;
    public $title = "Make your own dragon picture";
    public $styles = [
        "/media/styles/durg.css",
        "/media/styles/gallery.css",
        "/media/styles/emoji-demo.css",
        "https://cdn.jsdelivr.net/npm/choices.js@9.0.1/public/assets/styles/choices.min.css",
    ];
    public $description = "Customizable dragon sticker editor";
    public $scripts = [
        "/media/scripts/file_tools.js",
        "/media/scripts/recolor.js",
        "/media/scripts/editor/feature_encoding.js",
        "/media/scripts/external/emoji-mart-patched.js",
        // "https://cdn.jsdelivr.net/npm/emoji-mart@latest/dist/browser.js",
        "https://cdn.jsdelivr.net/npm/choices.js@9.0.1/public/assets/scripts/choices.min.js",
    ];

    function init_patterns()
    {
        $this->patterns["^/webapp/(?P<image>.*[^/])/?$"] = "webapp";
    }

    function base_uri()
    {
        return "/editor/";
    }

    function media_path()
    {
        return "/media/img/pages/editor/";
    }

    function server_image_path()
    {
        return dirname(dirname($this->self_dirname())) . $this->media_path();
    }

    protected function get_css_class($selected)
    {
        if ( $selected == 1 ) return "durgpic selected";
        if ( $selected == 2 ) return "bigdurg";
        return "durgpic";
    }

    protected function image_extensions()
    {
        return array("svg");
    }

    function extra_head($render_args)
    {
        ?><style>
        .editor-container {
            display: flex;
            flex-flow: row wrap;
            justify-content: space-around;
            align-items: start;
        }
        #editors td:first-child {
            text-align: right;
        }
        .editor-group td:nth-child(1), .editor-group td:nth-child(2) {
            min-width: 160px;
        }
        .editor-group input[type="checkbox"], .editor-group input[type="color"] {
            /* Copy dropdown max height */
            /*          icon    border*2    top     bottom*/
            height: calc(48px + 1px + 1px + 3px + 7.5px);
            /* aspect-ratio: 1; doesn't work for color */
            width: 100%;
        }
        .editor-group td {
            min-width: calc(48px + 1px + 1px + 3px + 7.5px);
        }
        #editors select {
            width: 100%;
        }
        #editors > div {
            border: 1px solid var(--shade-3);
            border-radius: 3px;
        }
        #editors table {
            height: max-content;
        }
        #editors table button {
            height: 100%;
            width: 100%;
            font-size: x-large;
        }
        #button-ops {
            display: flex;
            justify-content: space-evenly;
            flex-flow: row wrap;
        }
        input[type="color"], select {
            block-size: 27px;
        }
        .emoji-demo {
            background-image: url("/media/styles/emoji-demo.png");
            vertical-align: middle;
        }
        .choices .choices__list--dropdown {
            min-width: fit-content;
            background-color: var(--background);
            color: var(--text);
        }
        .choices .choices__list--dropdown .choices__item {
            display: flex;
            justify-content: start;
            align-items: center;
            white-space: pre;
            padding: 3px;
        }
        .choices__list--dropdown .choices__item--selectable.is-highlighted {
            background-color: var(--shade-3);
        }
        .choices .choices__list--single {
            padding: 0;
        }
        .choices .choices__inner {
            text-align: left;
            box-sizing: border-box;
            padding: 3px;
            padding-right: 16px;
            background-color: var(--background);

        }
        .choices[data-type*=select-one]:after {
            right: 5px !important;
            border-color: var(--text) transparent transparent;
        }
        .choices[data-type*=select-one].is-open:after {
            border-color: transparent transparent var(--text);
        }
        .palette {
            display: flex;
            list-style: none;
            padding: 0;
            justify-content: center;
            flex-flow: row wrap;
        }
        .palette li {
            display: inline-block;
            width: 32px;
            aspect-ratio: 1;
            margin: 1px;
            border-radius: 3px;
        }
        </style><?php

        if ( $render_args["webapp"] )
        {
            ?>
            <script src="https://telegram.org/js/telegram-web-app.js?56"></script>
            <script>

                function setup_telegram_ui()
                {
                    function delete_element(e)
                    {
                        e.parentElement.removeChild(e);
                    }
                    delete_element(document.querySelector('.editor-toggle[data-editor="all"]').parentElement);

                    let page_width = window.innerWidth;
                    let is_mobile = window.screen.width < 600;

                    if ( is_mobile )
                        page_width = window.screen.width;

                    let doc_width = document.body.clientWidth;
                    if ( doc_width > page_width )
                    {
                        let scale = page_width / doc_width;
                        document.body.style.transform = `scale(${scale})`;
                        document.body.style.transformOrigin = "0 0"
                    }

                    window.addEventListener("keyup", ev => {
                        if ( ev.key == "Escape" )
                        {
                            Telegram.WebApp.exitFullscreen();
                        }
                        else if ( ev.key == "F11" )
                        {
                            if ( Telegram.WebApp.isFullscreen )
                                Telegram.WebApp.exitFullscreen();
                            else
                                Telegram.WebApp.requestFullscreen();

                        }
                    });

                    let btn_parent = document.getElementById("button-ops");
                    let btn_send = document.createElement("button");
                    btn_parent.parentElement.insertBefore(btn_send, btn_parent);
                    btn_send.classList.add("telegram-button");

                    let btn_icon = document.createElement("i");
                    btn_icon.classList.add("fa-solid", "fa-paper-plane");

                    btn_send.appendChild(document.createTextNode("Send Sticker "));
                    btn_send.appendChild(btn_icon);

                    btn_send.addEventListener("click", ev => {
                        let code = encode_controller_state();
                        let chat = Telegram.WebApp.initData == '' ? undefined : ["users", "bots", "groups", "channels"];
                        Telegram.WebApp.switchInlineQuery(code, chat);
                    });

                    controller.load_from_storage(image_slug + "_1");

                    Telegram.WebApp.expand();
                    Telegram.WebView.postEvent("web_app_setup_swipe_behavior", null, {allow_vertical_swipe: false});
                    if ( is_mobile )
                        Telegram.WebApp.requestFullscreen();
                }

            </script>
            <style>
                button, .buttons a, input {
                    font-size: large !important;
                    /*font-weight: bold;*/
                }
                .buttons {
                    flex-flow: row;
                }
                input[type='checkbox'], input[type='color'], button {
                    cursor: pointer;
                }
                #content {
                    padding: 0;
                }
                .telegram-button {
                    background: var(--tg-theme-button-color);
                    color: var(--tg-theme-button-text-color);
                    font-size: x-large !important;
                    font-weight: bold;
                    padding: 1ex;
                    margin: 1ex;
                    border: 1px solid var(--button-border);
                    border-radius: 5px;
                }
            </style><?php
        }

    }

    function raster_image_path()
    {
        return "/media/img/rasterized/vectors/";
    }

    function render_focused(MediaFileInfo $image, $render_args)
    {
        global $site;

        $palette_html = '<div><p>Used colors</p><ul id="palette" class="palette"></ul></div>';

        ?>
        <input type="file" id="file_input" style="display:none" />

        <div id="toggler-parent"></div>

        <?php if ( $render_args["webapp"] ) echo $palette_html; ?>

        <div class='editor-container'>
            <div id='editors' class='editor-container'></div>
            <iframe id='image_container' src='<?php echo $image->full_url(); ?>' rel='image'></iframe>
        </div>

        <?php if ( !$render_args["webapp"] ) echo $palette_html; ?>

        <script>
            function options_cmp(a, b)
            {
                if ( a.value == "none" )
                    return -1;
                if ( b.value == "none" )
                    return 1;
                return a.value.localeCompare(b.value);
            }

            function make_better_select(input)
            {
                // https://github.com/Choices-js/Choices?tab=readme-ov-file
                let choices = new Choices(input, {
                    itemSelectText: "",
                    searchEnabled: false,
                    sorter: options_cmp
                });

                choices.containerOuter.element.style.minWidth = (
                    choices.dropdown.element.clientWidth
                ) + "px";

                input.addEventListener("internal-change", (ev) => {
                    choices.setChoiceByValue(ev.detail);
                });
            }

            function finish_setup()
            {
                if ( image_slug == "blep" )
                    editor_meta = controller.auto_editor_meta();

                controller.init_advanced_editor(editor_meta, document.getElementById('editors'), document.getElementById('toggler-parent'));
                controller.init_buttons({
                    "save_png": "save_frame_png",
                    "save_svg": "save_frame_svg",
                    "save_webp": "save_frame_webp",
                    "save_gpl": "save_data_gpl",
                });
                update_palette();

                for ( let element of document.querySelectorAll(".switcher-input") )
                {
                    make_better_select(element);
                }

                <?php if ( $render_args["webapp"] ) echo "setup_telegram_ui();"; ?>
            }

            let uploader = setup_file_upload("json", (data) => {
                controller.set_current_state(data);
            });

            function request_upload()
            {
                let elem = document.getElementById("file_input");
                let ev = document.createEvent("MouseEvents");
                ev.initEvent("click", true, false);
                elem.dispatchEvent(ev);
            }

            function update_palette()
            {
                let palette = (
                    Object.values(controller.features)
                    .filter(f => f instanceof ColorFeature && f.enabled)
                    .map(f => [f.value, f.label])
                    .sort()
                    .filter((v, i, arr) => i == 0 || v[0] != arr[i-1][0])
                );

                palette_parent.innerHTML = "";
                for ( let [color, name] of palette )
                {
                    let swatch = palette_parent.appendChild(document.createElement("li"));
                    swatch.style.background = color;
                    swatch.title = name;
                }
            }

            function suggested_palette(data)
            {
                let parent = document.createElement("ul");
                parent.classList.add("palette");

                let og_palette = document.getElementById("palette");
                og_palette.insertAdjacentElement("afterend", parent);

                let step = -1;
                let re_color = /^\s*([0-9]+)\s+([0-9]+)\s+([0-9]+)\s+(.*)$/;

                for ( let line of data.split("\n") )
                {
                    if ( line.startsWith("#") )
                    {
                        step += 1;
                    }
                    else if ( step > 0 )
                    {
                        let match = line.match(re_color);
                        if ( match )
                        {
                            let swatch = parent.appendChild(document.createElement("li"));
                            swatch.style.background = "rgb(" + match.slice(1, 4).join(", ") + ")";
                            swatch.title = match[4];
                        }
                    }
                }
            }

            function init_suggested_palette()
            {
                fetch("/media/data/editor/emoji.gpl")
                    .then(r => r.text())
                    .then(suggested_palette)
                ;
            }

            function encode_controller_state()
            {
                if ( controller.feature_meta )
                    return controller.image_id() + ":" + encode_editor_data(controller.recolor_state, controller.feature_meta);
                return "??";
            }

            function copy_share_code()
            {
                let code = encode_controller_state();
                if ( navigator.clipboard )
                    navigator.clipboard.writeText(code);
                console.log(code);
            }

            function copy_as_parts()
            {
                let state = controller.features_to_dict(null, true);
                let raw = controller.state_to_pretty_json(state);
                let ind = " ".repeat(4);
                let value = "\n" + ind + `"${document.getElementById("emoji_input").value}": `;
                let lines = raw.split("\n");
                value += lines[0] + "\n";
                value += lines.slice(1, -1).map(l => ind + l).join("\n");
                value += "\n" + ind + lines.slice(-1) + ",\n"

                if ( navigator.clipboard )
                    navigator.clipboard.writeText(value);
                else
                    console.log(value);
            }

            <?php
                function assign_json($slug, $var, $filename)
                {
                    global $site;
                    echo "var $var = ";
                    if ( $slug != "" )
                    {
                        echo file_get_contents("{$site->settings->root_dir}/media/data/editor/images/$slug/$filename");
                    }
                    else
                    {
                        echo "null";
                    }
                    echo ";\n";
                }

                $slug = $image->slug != "blep" ? $image->slug : "";
                assign_json($slug, "feature_meta", "$slug-features.json");
                assign_json($slug, "editor_meta", "$slug-editors.json");
                echo "var image_slug = '$image->slug';\n";
            ?>

            function fetch_json(url, callback)
            {
                fetch(url)
                    .then(r => r.status == 200 ? r.json() : null)
                    .then(data => {
                        if ( data )
                            callback(data);
                    })
                ;
            }

            let emoji_parts = null;
            fetch_json(
                `/media/data/editor/images/${image_slug}/${image_slug}-parts.json`,
                data => {
                    emoji_parts = new EmojiPartsData(data);
                }
            );
            fetch_json(
                `/media/data/editor/images/${image_slug}/${image_slug}-variants.json`,
                data => {
                    controller.variants_data = data;
                }
            );

            let palette_parent = document.getElementById("palette");
            let controller = new RecolorController({
                handle_query_string: true,
                advanced_rules: image_slug == "blep" ? FeatureMode.DATA_ATTR : FeatureMode.INKSCAPE_LABEL,
                table_layout: true,
                on_change: update_palette,
                show_icons: feature_meta != null,
                feature_meta:feature_meta
            });
            controller.load_feature_meta(feature_meta);
            controller.setup_page(document.getElementById('image_container'), finish_setup);
        </script>

        <?php
        if ( $image->slug != "blep" )
        {
            ?>
            <script type="module">
                import {random_element} from "/media/scripts/module/dragon_element.js";

                window.random_dragon_style = function()
                {
                    let element = random_element();
                    controller.randomize_colors();
                    controller.set_current_state(element.full_random().state);
                };
            </script>
            <div style="margin-top: 1em;">
                <input oninput='controller.apply_emoji(this.value, emoji_parts)' placeholder='emoji' type="text" id="emoji_input"/>
                <button id="emoji_trigger"><i class="fa-regular fa-face-smile"></i></button>
                <button id="emoji_delete"><i class="fa-solid fa-delete-left"></i></button>
            </div>
            <div id="emoji_selector" style="display:none; justify-content: center;"></div>
            <script>
                const emoji_input = document.getElementById("emoji_input");
                const selector = document.getElementById("emoji_selector");

                function on_emoji(data, ev)
                {
                    if ( ev.shiftKey )
                        emoji_input.value = "";
                    emoji_input.value += data.native;
                    controller.apply_emoji(emoji_input.value, emoji_parts);
                }

                const size = 48;
                // https://github.com/missive/emoji-mart
                const picker = new EmojiMart.Picker({
                    onEmojiSelect: on_emoji,
                    maxFrequentRows: 0,
                    previewPosition: "none",
                    skinTonePosition: "none",
                    noCountryFlags: false,
                    emojiButtonSize: size + 6,
                    emojiSize: size,
                    perLine: 8,
                    // "set": "google",
                });
                selector.appendChild(picker);

                document.getElementById("emoji_trigger").addEventListener("click", () => {
                    selector.style.display = selector.style.display == "none" ? "flex" : "none";
                });
                document.getElementById("emoji_delete").addEventListener("click", () => {
                    let emo = split_emoji(emoji_input.value);
                    emoji_input.value = emo.slice(0, -1).join("");
                    controller.apply_emoji(emoji_input.value, emoji_parts);
                });

                function request_share_code()
                {
                    let input = document.getElementById("code-input");
                    input.style.display = "inline-block";
                    input.value = "";
                    input.focus();
                    document.getElementById("code-request").style.display = "none";
                    document.getElementById("code-confirm").style.display = "inline-block";
                }

                function confirm_share_code()
                {
                    let input = document.getElementById("code-input");
                    if ( input.value != "" )
                    {
                        let match = input.value.match(/^([-a-z]+):([-a-zA-Z0-9=:.]+)$/);
                        input.value = "";

                        if ( match )
                        {
                            let encoded = match[2];
                            let decoded = decode_editor_data(encoded, controller.feature_meta);
                            controller.set_current_state(decoded);
                        }
                        else
                        {
                            let place = input.placeholder;
                            input.placeholder = "Invalid code!";
                            input.focus();
                            window.setTimeout(() => { input.placeholder = place; }, 2000);
                            return;
                        }

                    }

                    input.style.display = "none";
                    document.getElementById("code-request").style.display = "inline-block";
                    document.getElementById("code-confirm").style.display = "none";
                }
            </script>
            <?php
        }
        else
        {
            echo "<script>function random_dragon_style() { controller.randomize_colors(); }</script>";
        }


        function button($text, $icon, $attrs)
        {
            return new SimpleElement("button", $attrs, [fa_icon($icon), $text]);
        }

        function button_list($title, $buttons)
        {
            $id = title_to_slug($title);
            echo "<div id='buttons-$id'>";
            echo mkelement(["p", [], $title]);
            echo new LinkList($buttons, "buttons");
            echo "</div>";
        }

        echo "<div id='button-ops'>";

        if ( $render_args["webapp"] )
        {

            button_list("Character", [
                button("Random", "dice", ["onclick"=>"random_dragon_style()"]),
                button("Reset", "clock-rotate-left", ["onclick"=>"controller.reset_features(['style'])"]),
            ]);

            button_list("Pose", [
                button("Random", "dice", ["onclick"=>"controller.randomize_pose()"]),
                button("Reset", "clock-rotate-left", ["onclick"=>"controller.reset_features(['pose', 'transform'])"]),
            ]);

            button_list("Save Slots", [
                button("Save", "floppy-disk", ["onclick"=>"controller.save_to_storage(image_slug + '_1')"]),
                button("Load", "file-import", ["onclick"=>"controller.load_from_storage(image_slug + '_1')"]),
                button("2", "floppy-disk", ["onclick"=>"controller.save_to_storage(image_slug + '_2')"]),
                button("2", "file-import", ["onclick"=>"controller.load_from_storage(image_slug + '_2')"]),
                button("3", "floppy-disk", ["onclick"=>"controller.save_to_storage(image_slug + '_3')"]),
                button("3", "file-import", ["onclick"=>"controller.load_from_storage(image_slug + '_3')"]),
            ]);
        }
        else
        {
            button_list("Character", [
                button("Random", "dice", ["onclick"=>"random_dragon_style()"]),
                button("Save", "floppy-disk", ["onclick"=>"controller.save_to_storage('character', ['style'])"]),
                button("Load", "file-import", ["onclick"=>"controller.load_from_storage('character')"]),
                button("Reset", "clock-rotate-left", ["onclick"=>"controller.reset_features(['style'])"]),
            ]);

            button_list("Pose", [
                button("Random", "dice", ["onclick"=>"controller.randomize_pose()"]),
                button("Save", "floppy-disk", ["onclick"=>"controller.save_to_storage('pose', ['pose', 'transform'])"]),
                button("Load", "file-import", ["onclick"=>"controller.load_from_storage('pose')"]),
                button("Reset", "clock-rotate-left", ["onclick"=>"controller.reset_features(['pose', 'transform'])"]),
            ]);
        }

        $icon_dload = fa_icon("download");

        button_list("Export", [
            button("SVG", "download", ["id"=>"save_svg"]),
            button("PNG", "download", ["id"=>"save_png"]),
            button("WebP", "download", ["id"=>"save_webp"]),
            button("JSON", "download", ["onclick"=>"controller.save_to_file(controller.image_id(), null, true)"]),
            button("Palette", "download", ["id"=>"save_gpl"]),
            button("Copy Code", "copy", ["onclick"=>"copy_share_code()"]),
        ]);

        button_list("Import", [
            button("JSON", "upload", ["onclick"=>"uploader.request_upload()"]),
            mkelement(
                ["input", [
                    "type"=>"text", "id"=>"code-input", "placeholder"=>"Share Code",
                    "style"=>"display:none"
                ]]
            ),
            button("Confirm Code", "file-import", ["onclick"=>"confirm_share_code()", "id"=>"code-confirm", "style"=>"display:none"]),
            button("Enter Code", "paste", ["onclick"=>"request_share_code()", "id"=>"code-request"]),
        ]);

        if ( !$render_args["webapp"] )
        {
            button_list("Advanced Stuff", [
                button("Palette", "palette", ["onclick"=>"init_suggested_palette()"]),
                button("Copy as Parts", "copy", ["onclick"=>"copy_as_parts()"]),
                button("Copy JSON", "copy", ["onclick"=>"controller.save_to_clipboard(null, true)"]),
                button("Reset", "clock-rotate-left", ["onclick"=>"controller.reset_features()"]),
            ]);
        }

        echo "</div>";

        $this->attribution_instructions();
    }

    function nav($render_args)
    {
        if ( !$render_args["webapp"] )
            parent::nav($render_args);
    }

    function attribution_instructions()
    {
        $cc = License::find_license("CC BY-SA");
        $link = new Link(href("/"), href("/"));
        $credits = new Link(href("/media/img/rasterized/vectors/editor-credits.png"), "credits sticker");
        echo "<p>Feel free to use the images generated, but do give credit.<br/>" .
            "It's enough to link to this website ($link) for that.<br/>" .
            "You can use the $credits in packs.<br/>" .
            "Images are licensed under the $cc.</p>";
    }

    function webapp($match)
    {
        $image = $this->find_image(urldecode($match["image"]));
        if ( !$image )
        {
            http_response_code(404);
        }
        $this->render(array("image" => $image, "webapp"=> true));
    }

    function render($render_args=[])
    {
        if ( !isset($render_args["webapp"]) )
            $render_args["webapp"] = false;
        parent::render($render_args);
    }


    function main($render_args)
    {
        $focus = $render_args["image"] ?? null;

        if ( !$focus )
        {
            $this->body_title(null, $render_args);
            echo "<p>Here you can personalize some of the images to match other characters.</p>";
            echo mkelement(["p", [], [
                "A simplified version of this is available on more images as the \"Recolor\" feature in the ",
                new PlainLink("/vectors/", "vectors"), " pages."
            ]]);
            $this->attribution_instructions();
            echo "<p>Click on one of the images below to start.</p>";
        }

        if ( $render_args["webapp"] )
            $this->render_gallery_focus($focus, $render_args);
        else
            $this->render_gallery($focus, $render_args);
    }
};

$page = new EditorPage();


