<?php

require_once(__dir__."/../dragon.php");

class GlaxversePage extends DurgPage
{
    public $title = "Glaxverse";
    public $description = "Discover this revolutionary innovation";

    public $styles = [
        "/media/styles/durg.css",
        "/media/styles/glaxverse.css"
    ];

    public $scripts = [
        "/media/scripts/glaxverse.js",
    ];


    function main($render_args)
    {
        ?>
        <article id="section-container">

        <section class="fullwidth">
            <h1>The Glaxverse</h1>
            <p id="motto"></p>
            <p id="go-down"><i class="fa-solid fa-angles-down"></i></p>
        </section>

        <section id="first-thing">
            <div class="background bg2"></div>
            <div class="container reveal">
                <h2>Our mission</h2>
                <div class="text-container" id="our_mission"></div>
            </div>
        </section>

        <section>
            <div class="container reveal">
                <h2>Core values</h2>
                <div class="text-container" id="core_values"></div>
            </div>
        </section>

        <section>
            <div class="background bg3"></div>
            <div class="container reveal">
                <h2>Making a difference</h2>
                <div class="text-container" id="difference"></div>
            </div>
        </section>

        <section>
            <div class="container reveal">
                <h2>Working for the future</h2>
                <div class="text-container" id="future"></div>
            </div>
        </section>

        <section>
            <div class="background bg2"></div>
            <div class="container reveal">
                <h2>What is the Glaxverse?</h2>
                <div class="text-container" id="what_is"></div>
            </div>
        </section>

        <section>
            <div class="container reveal">
                <h2>Why Glaxverse?</h2>
                <div class="text-container" id="why"></div>
            </div>
        </section>

        <section>
            <div class="background bg3"></div>
            <div class="container reveal">
                <h2>A new era</h2>
                <div class="text-container" id="new_era"></div>
            </div>
        </section>

        </article>

        <script>
            var loading_more = false;

            function load_more()
            {
                if ( loading_more )
                    return;

                loading_more = true;
                new_section(parent);
                loading_more = false;
            }

            function reveal()
            {
                var reveals = document.querySelectorAll(".reveal");

                for (var i = 0; i < reveals.length; i++)
                {
                    var windowHeight = window.innerHeight;
                    var elementTop = reveals[i].getBoundingClientRect().top;
                    var elementVisible = 150;

                    if (elementTop < windowHeight - elementVisible)
                    {
                        reveals[i].classList.add("active");
                        if ( i == reveals.length - 1 )
                            load_more();
                    }
                    else
                    {
                        reveals[i].classList.remove("active");
                    }
                }
            }

            window.addEventListener("scroll", reveal);
            nonsense_to_element(document.getElementById("motto"), null, "<verb>. <verb>. <verb>.");

            nonsense_blocks(document.getElementById("our_mission"), "<verb>", null, nonsense_functor(0));
            nonsense_blocks(document.getElementById("core_values"), "<adjective>", null, nonsense_functor(0));
            nonsense_blocks(document.getElementById("difference"), "<noun>", null, null);
            nonsense_blocks(document.getElementById("future"), "<verb>", null, nonsense_functor(0));

            nonsense_blocks(document.getElementById("what_is"), "<adjective>", null, nonsense_functor(0));

            nonsense_blocks(document.getElementById("why"), "<noun>", "Because the Glaxverse");

            nonsense_blocks(document.getElementById("new_era"), random_title, null, nonsense_functor(0.3));

            var parent = document.getElementById("section-container");

            document.getElementById("go-down").addEventListener("click", () => {
                document.getElementById("first-thing").scrollIntoView({behavior: "smooth"})
            });
        </script>
        <?php
    }
}

$page = new GlaxversePage();


