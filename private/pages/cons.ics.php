<?php

require_once(__dir__."/../dragon.php");
require_once(__dir__."/../convention-list.php");
require_once(__dir__."/../lib/ical.php");

class ConventionsICalPage extends DurgPage
{
    private $one_day;

    public function __construct()
    {
        parent::__construct();
        $this->one_day = new DateInterval("P1D");
    }

    protected function dispatch_noslash($path)
    {
        $this->render([]);
        return true;
    }

    function event_to_ical(FurryEvent $event, ICalendar $calendar, $dtstamp)
    {
        $ical = new ICalendarObject();
        $ical->add("SUMMARY", $event->name);
        $ical->add_uid($event->name . "/" . $event->date_string, $calendar);
        $ical->add("DTSTAMP", $dtstamp);
        $ical->add_date("DTSTART", $event->date_start);
        $ical->add_date("DTEND", $event->date_end->add($this->one_day));
        list($lon, $lat) = FurryEvent::$lon_lat[$event->city];
        $ical->add("GEO", "$lat;$lon");
        $ical->add("LOCATION", "{$event->city}, {$event->country}");

        if ( in_array("Confirmed", $event->status) )
            $ical->add("STATUS", "CONFIRMED");
        else
            $ical->add("STATUS", "TENTATIVE");
        $ical->add("URL", $event->url);
        $ical->add("CATEGORIES", implode(",", $event->types));
        return $ical;
    }

    function render($render_args = [])
    {
        global $convention_list;
        header("Content-Type:text/calendar");


        $dtstamp = max(filemtime(__dir__."/../convention-list.php"), filemtime(__file__));
        $dtstamp = date("Ymd\\This\\Z", $dtstamp);
        $calendar = new ICalendar("Glax Convention List");
        $url = href("/".basename(__file__, ".php"));
        $calendar->add("URL", $url);
        $calendar->add("LAST-MODIFIED", $dtstamp);
        $calendar->add("SOURCE;VALUE=URI", $url);
        $calendar->add("REFRESH-INTERVAL;VALUE=DURATION", "P1D");
        $calendar->add("COLOR", "mediumslateblue");

        $current_date = date("Y-m-d");

        foreach ( $convention_list as $event )
        {
            // if ( $event->date_string >= $current_date )
            {
                $ev = $this->event_to_ical($event, $calendar, $dtstamp);
                $calendar->add_event($ev);
            }
        }

        $calendar->render();
    }
}

$page = new ConventionsICalPage();


