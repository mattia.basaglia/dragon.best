<?php

require_once(__dir__."/../dragon.php");

class ExemojiPage extends DurgPage
{
    public $title = "Exemoji";
    public $description = "Emoji-based bytecode architecture";
    public $scripts = [
        "https://cdn.jsdelivr.net/pyodide/v0.25.1/full/pyodide.js",
        "/media/scripts/python_glue.js",
    ];

    public $styles = [
        "/media/styles/durg.css",
        "/media/styles/vm.css"
    ];

    function extra_head($render_args)
    {
        ?><style>
        .key-row {
            display: flex;
            flex-flow: row;
        }
        #keyboard {
            font-size: 32px;
            margin: 5px 0;
        }
        .key {
            cursor: pointer;
        }
        #playground-graphics {
            width: 255px;
            height: 240px;
        }
        .grid-form {
            display: grid;
            grid-template-columns: auto auto auto;
            grid-gap: 5px;
        }
        tr.mem-bytes td {
            border-bottom: 1px solid var(--shade-4);
        }
        tr.mem-text {
            height: 1.2em;
        }
        tr.mem-text td {
            border-top: none;
        }
        td.xmem {
            padding-top: 10px;
        }
        </style><?php
    }

    function main($render_args)
    {
        $this->body_title(null, $render_args);
        ?>
            <h2>Specs</h2>

            <p><em>Exemoji</em> is an 8bit computer architecture based on emoji.</p>
            <p>Instructions are sequences of utf8-encoded emoji, making executables valid text files.</p>
        <?php
            global $site;
            echo file_get_contents($site->settings->root_dir . "/media/scripts/exemoji/specs.htm")
        ?>

        <h2>Playground</h2>
        <p>Here you can execute and debug <em>Exemoji</em> binaries.
        Note that since this is a debug view loading a program takes some time and execution is rather slow.
        For running the code faster, you should use the <a href="/media/scripts/exemoji/exemoji.py">python script</a>.
        </p>

        <div id="main-loading">
            Initializing virtual machine, please wait...
        </div>
        <div id="main-loaded" style="display: none">
            <table class="table" id="playground-explain" style="display: none"></table>

            <div id="playground-controls">
                <p><label for="examples">Example</label> <select id="examples"></select></p>
                <ul class="buttons">
                    <li><button id="btn-load" title="Load Code"><i class="fa-solid fa-download"></i></li>
                    <li><button id="btn-play" title="Play"><i class="fa-solid fa-play"></i></button></li>
                    <li><button id="btn-pause" title="Pause"><i class="fa-solid fa-pause"></i></button></li>
                    <li><button id="btn-step" title="Run Step"><i class="fa-solid fa-forward-step"></i></button></li>
                    <li><button title="Toggle Keyboard" onclick="toggle('keyboard');"><i class="fa-regular fa-keyboard"></i></li></button>
                    <li><button title="Toggle Terminal" onclick="toggle('playground-output-p');"><i class="fa-solid fa-terminal"></i></li></button>
                    <li><button title="Toggle Graphics" onclick="toggle('playground-graphics');"><i class="fa-solid fa-display"></i></li></button>
                    <li><button id="btn-explain" title="Explain Code"><i class="fa-solid fa-circle-info"></i></button></li>
                </ul>
                <p class="grid-form">
                    <label for="run-speed">Instruction delay</label>
                    <input type="number" min="0" step="100" max="1000" id="run-speed" value="0"/>
                    <span>ms</span>

                    <label for="stack-size">Stack size</label>
                    <input type="number" min="0" step="16" max="1024" id="stack-size" value="32"/>
                    <span>bytes</span>
                </p>
            </div>
            <div id="playground-area">
                <textarea id="playground-editor"></textarea>
                <div id="playground-output-p" class="mono border"><div id="playground-output"></div></div>
                <canvas id="playground-graphics" class="mono border" style="display: none" width="255" height="240"></canvas>
            </div>
            <div id="keyboard"></div>

            <section id="inspect">
                <div>
                    <table class="mono table">
                        <caption>Registers</caption>
                        <thead>
                            <tr>
                                <th>Register</th>
                                <th>Hex</th>
                                <th>Dec</th>
                                <th>Char</th>
                            </tr>
                        </thead>
                        <tbody id="inspect-registers"></tbody>
                    </table>
                    <table class="table" style="table-layout:fixed; width: 260px;">
                        <caption>Last instruction</caption>
                        <tr>
                            <th style="width: 6ex">Opcode</th>
                            <th>Name</th>
                            <th colspan="3" style="width: 9ex">Operands</th>
                        </tr>
                        <tr style="height: 1.2em">
                            <td id="last-instruction-emoji" class="centered"></td>
                            <td id="last-instruction-name"></td>
                            <td id="last-instruction-op0" class="centered"></td>
                            <td id="last-instruction-op1" class="centered"></td>
                            <td id="last-instruction-op2" class="centered"></td>
                        </tr>
                        <tr style="height: 5em"><td id="last-instruction-desc" colspan="5"></td></tr>
                    </table>
                </div>
                <table class="mono table" id="inspect-memory" style="max-width: 800px"></table>
            </section>

        </div>
        <script>
            function toggle(id, vis="block") {
                let elem = document.getElementById(id);
                elem.style.display = elem.style.display == "none" ? vis : "none";
            }
        </script>
        <script type="text/javascript">
            load_python("exemoji", async () => {
                await py_import("exemoji");
                await py_import("xml_helper");
                await py_import("exemoji_docs");
                let web = await py_import("exemoji_web");
                web.init();
            });
        </script>
        <?php
    }
};

$page = new ExemojiPage();
