<?php

require_once(__dir__."/../dragon.php");


class ChoirPage extends DurgPage
{
    public $title = "Kobold Choir";
    public $description = "Make the kobolds play music";
    public $scripts = [
        "https://cdnjs.cloudflare.com/ajax/libs/tone/15.1.2/Tone.min.js",
        "https://unpkg.com/@tonejs/midi@2.0.28/build/Midi.js",
    ];
    public $default_image = "/media/img/pages/choir/preview.png";

    public function show_social_media_card($render_args, $ogmeta)
    {
        return true;
    }

    function extra_head($ra)
    {
        ?>

        <meta name='twitter:card' content='player' />
        <meta name='twitter:player' content='<?php
            echo href("/choir/") . "?" . http_build_query([
                "embed" => "1",
                "url" => $_GET["url"] ?? "",
                "t" => $_GET["t"] ?? "0",
            ]);
        ?>' />
        <meta name='twitter:player:width' content='800' />
        <meta name='twitter:player:height' content='640' />


        <style>
        @import url('https://fonts.googleapis.com/css2?family=Dongle:wght@700&display=swap');

        h1 {
            text-align: center;
        }
        #content {
            padding: 0;
            max-width: 800px;
            width: 100%;
            margin: 0 auto;
        }
        .piano {
            position: relative;
            width: 100%;
            height: 400px;
            max-width: 720px;
            margin: 20px auto;
            user-select: none;
        }
        .piano > div {
            display: flex;
            width: 100%;
            height: 100%;
            pointer-events: none;
        }
        .key {
            border: 4px solid #000;
            border-radius: 0 0 8px 8px;
            display: inline-flex;
            box-sizing: border-box;
            align-items: center;
            pointer-events: auto;
            flex-flow: column;
            justify-content: end;
            padding-bottom: 10px;
            gap: 10px;
        }
        .natural .key {
            width: 14%;
            height: 100%;
            background: #fff;
            color: #000;
        }
        .sharp {
            padding: 0 7%;
            position: absolute;
            top: 0;
            left: 0;
        }
        .sharp > div {
            width: 8%;
            margin: 0 3%;
        }
        .sharp .key {
            height: 60%;
            background: #000;
            color: #eee;
        }

        .key-note {
            font-size: 30px;
            font-weight: bold;
            opacity: 40%;
        }

        .key-map {
            font-size: 40px;
            font-weight: bold;
        }

        .key-map5 {
            font-size: 20px;
            font-weight: bold;
        }

        .natural .key.pressed {
            border-color: var(--glax-body-main);
            box-shadow: 0 0 8px var(--glax-body-main);
            background: var(--glax-belly);
            color: var(--glax-body-main);
        }
        .sharp .key.pressed {
            border-color: var(--glax-body-main);
            box-shadow: 0 0 8px var(--glax-body-main);
            background: var(--glax-body-main);
            color: var(--glax-belly);
        }

        .buttons i {
            font-size: 32px;
        }

        .hidden {
            display: none !important;
        }

        #midi-drop {
            border: 1px solid black;
            margin: 20px auto;
            padding: 60px;
            box-sizing: border-box;
            text-align: center;
            border-radius: 5px;
            position: relative;
        }

        #midi-drop input {
            position: absolute;
            width: 100%;
            height: 100%;
            opacity: 0;
            left: 0px;
            top: 0px;
            cursor: pointer;
        }

        #svg-container {
            position: relative;
            display: flex;
            align-items: center;
            overflow: hidden;
            border-radius: 5px;
        }

        #svg-container.embed {
            position: fixed;
            left: 0;
            top: 0;
            width: 100vw;
            height: 100vh;
            background: black;
        }

        #svg-container.recording-mode {
            border-radius: 0;
            width: 1024px;
            max-width: "none";
        }

        #svg-container:fullscreen {
            background: #c2bab4;
        }

        #fullscreen-button {
            position: absolute;
            right: 10px;
            bottom: 10px;
            color: #000;
            cursor: pointer;
            opacity: 20%;
        }
        #fullscreen-button:hover {
            opacity: 100%;
        }

        #midi-input > p {
            display: flex;
        }

        #midi-url {
            flex-grow: 1;
        }

        #big-play-button {
            color: white;
            font-size: 150px;
            position: absolute;
            width: 100%;
            height: 100%;
            display: flex;
            align-items: center;
            justify-content: center;
            cursor: pointer;
        }

        #big-play-button i {
            background: #0004;
            border-radius: 100%;
            padding: 10px;
        }

        #share {
            display: flex;
            flex-flow: column;
            padding: 10px 0;
            gap: 5px;
        }

        #share-url {
            padding: 3px;
        }

        label[aria-disabled="true"] {
            color: var(--shade-3);
        }

        #player-controls {
            position: absolute;
            left: 0;
            bottom: 0;
            right: 0;
            height: 40px;
            opacity: 0%;
            background: linear-gradient(0deg, #fff6, transparent);
        }

        #player-controls:hover {
            opacity: 100%;
        }

        #svg-container.recording-mode #player-controls {
            display: none;
        }

        #playhead-track {
            position: absolute;
            left: 0;
            bottom: 0;
            right: 0;
            height: 10px;
            background: #0004;
            cursor: pointer;
        }

        #playhead {
            height: 100%;
            aspect-ratio: 1;
            background: black;
            position: absolute;
            left: 0;
            margin-left: -5px;
        }

        /* SVG */

        svg {
            width: 100%;
            display: block;
            height: auto;
            pointer-events: none;
            user-select: none;
        }

        text.yip {
            font-weight: 800;
            font-size: 64px;
            font-family: Dongle;
            text-align: center;
            text-anchor: middle;
            fill-rule: evenodd;
            stroke: #292929;
            stroke-width: 1.2px;
            stroke-linecap: round;
            stroke-linejoin: round;
            animation-name: yipanim;
            --animation-distance: 0px;
        }

        text.tiny_yip {
            transform: translate(17px, 8px);
            font-size: 24px;
            stroke-width: 0.4px;
            animation-name: yipanim_tiny;
        }
        text.tiny_yip.left {
            transform: translate(-17px, 6px);
        }

        @keyframes yipanim {
            0%   {transform: translate(0px, 0px);  opacity: 80%}
            50%   {opacity: 80%}
            100% {transform: translate(0px, var(--animation-distance)); opacity: 0%}
        }

        @keyframes yipanim_tiny {
            0%   {opacity: 80%}
            50%   {opacity: 80%}
            100% {opacity: 0%}
        }

        .kobold {
            pointer-events: auto;
            cursor: pointer;
        }

        .kobold_c {
            fill: hsl(0, 80%, 60%);
        }
        .kobold_cs {
            fill: hsl(20, 80%, 60%);
        }
        .kobold_d {
            fill: hsl(40, 80%, 60%);
        }
        .kobold_ds {
            fill: hsl(60, 80%, 60%);
        }
        .kobold_e {
            fill: hsl(80, 80%, 60%);
        }
        .kobold_f {
            fill: hsl(100, 80%, 60%);
        }
        .kobold_fs {
            fill: hsl(200, 80%, 60%);
        }
        .kobold_g {
            fill: hsl(220, 80%, 60%);
        }
        .kobold_gs {
            fill: hsl(240, 80%, 60%);
        }
        .kobold_a {
            fill: hsl(260, 80%, 60%);
        }
        .kobold_as {
            fill: hsl(300, 80%, 60%);
        }
        .kobold_b {
            fill: hsl(320, 80%, 60%);
        }
        .kobold_bs {
            fill: hsl(340, 80%, 60%);
        }

        :root {
            --rotate-arm-r: none;
            --rotate-arm-l: none;
            --rotate-jaw: none;
            --rotate-head: none;
            --rotate-tail: none;
        }
        #arm-left {
            transform-origin: 240px 273px;
            transform: var(--rotate-arm-l);
        }
        #arm-right {
            transform-origin: 300px 270px;
            transform: var(--rotate-arm-r);
        }
        #jaw {
            transform-origin: 256px 210px;
            transform: var(--rotate-jaw);
        }
        #head {
            transform-origin: 280px 204px;
            transform: var(--rotate-head);
        }
        #tail {
            transform-origin: 260px 320px;
            transform: var(--rotate-tail);
        }
        .singing {
            --rotate-arm-r: rotate(-27deg);
            --rotate-arm-l: rotate(27deg);
            --rotate-jaw: rotate(11deg);
            --rotate-head: rotate(-10deg);
            --rotate-tail: rotate(11deg);
        }


    </style>
    <?php

    }

    function main($render_args)
    {
        $this->body_title(null, $render_args);
        ?>
        <div id="controls">
            <ul class="buttons">
                <li><button title="Show help" onclick="document.getElementById('help').classList.toggle('hidden')"><i class="fa-regular fa-circle-question"></i></button></li>
                <li><button title="Upload MIDI file" onclick="document.getElementById('midi-input').classList.toggle('hidden')"><i class="fa-solid fa-upload"></i></button></li>
                <li><button title="Show keyboard" onclick="document.getElementById('piano').classList.toggle('hidden')"><i class='bx bxs-piano' ></i></button></li>
                <li><button title="Share" onclick="toggle_share()"><i class="fa-solid fa-share"></i></button></li>
                <li><button title="Toggle Fullscreen" onclick="toggle_fullscreen()"><i class="fa-solid fa-expand"></i></button></li>
                <li><button title="Play" onclick="controller.play_pause()" id="play-button"><i class="fa-solid fa-play"></i></button></li>
                <li><button title="Stop playback" onclick="controller.stop()"><i class="fa-solid fa-stop"></i></button></li>
            </ul>
            <ul class="buttons">
                <li><select id="song-select" autocomplete="off" onchange="update_share_url()"></select></li>
                <li><button title="Play selected song" onclick="controller.play_current()"><i class="fa-solid fa-play"></i></button></li>
            </ul>
        </div>

        <div id="help" class="hidden">
            <p>You can click on the Kobolds to make them sing, or press buttons on your keyboard to play the associated note. <br/>
                To see keyboard bindings toggle the keyboard display. <br/>
                Right clicking a kobold results in the kobold singing an octave higher.</p>
            <p>Holding Shift while pressing a button or cliking a kobold will make the kobold sing one octave higher. <br/>
            Holding Ctrl or Alt will make the kobold sing one octave lower.</p>
            <p>You can also make them sing a full song by selecting one on the drop down above and clicking the play button. <br/>
            You can upload MIDI files (or type a MIDI URL) and the kobolds will sing that.<br/>
            If you want to find MIDI files to play, <a href="https://bitmidi.com/">BitMidi</a> is a good place for it.</p>
        </div>

        <div id="midi-input" class="hidden">
            <p><input type="url" id="midi-url" placeholder="MIDI file URL"/><button title="Play selected song" onclick="load_url_input()"><i class="fa-solid fa-play"></i></button></p>
            <div id="midi-drop">
                <p>Drop MIDI file or click to browse files</p>
                <input id="midi-in" type="file" accept="audio/midi" />
            </div>
        </div>

        <div id="share" class="hidden">
            <input type="url" readonly="readonly" onclick="copy_clipboard()" id="share-url" />
            <label><input type="checkbox" autocomplete="off" id="share-check-song" onchange="update_share_url()" /> Autoplay current song</label>
            <label>Start at
                <input type="number" autocomplete="off" min="0" value="0" id="share-start" onchange="update_share_url()" style="width: 7ex;" />
                seconds
                <button title="Set to current time" onclick="update_share_url_time()"><span class="bx bx-time-five"></span></button>
                </label>
        </div>

        <div id="piano" class="piano hidden">
            <div class="natural">
                <div data-key5="KeyW" data-key="KeyZ" class="key" data-note="C"></div>
                <div data-key5="KeyE" data-key="KeyX" class="key" data-note="D"></div>
                <div data-key5="KeyR" data-key="KeyC" class="key" data-note="E"></div>
                <div data-key5="KeyT" data-key="KeyV" class="key" data-note="F"></div>
                <div data-key5="KeyY" data-key="KeyB" class="key" data-note="G"></div>
                <div data-key5="KeyU" data-key="KeyN" class="key" data-note="A"></div>
                <div data-key5="KeyI" data-key="KeyM" class="key" data-note="B"></div>
            </div>

            <div class="sharp">
                <div data-key5="Digit3" data-key="KeyS" class="key" data-note="C#"></div>
                <div data-key5="Digit4" data-key="KeyD" class="key" data-note="D#"></div>
                <div class="sharp-spacer"></div>
                <div data-key5="Digit6" data-key="KeyG" class="key" data-note="F#"></div>
                <div data-key5="Digit7" data-key="KeyH" class="key" data-note="G#"></div>
                <div data-key5="Digit8" data-key="KeyJ" class="key" data-note="A#"></div>
            </div>
        </div>

        <div id="svg-container">
            <div id="player-controls">
                <div id="playhead-track" onclick="playhead_click(this, event)"><div id="playhead"></div></div>
                <a id="fullscreen-button" title="Toggle Fullscreen" onclick="toggle_fullscreen()">
                    <i class="fa-solid fa-expand"></i>
                </a>
            </div>
            <div id="big-play-button" class="hidden" title="Play">
                <i class="fa-regular fa-circle-play"></i>
            </div>
        <?php
        echo file_get_contents(__DIR__ . "/../../media/img/pages/choir/kobolds.svg");
        ?>
        </div>

        <script>
            function decrease_yip(note)
            {
                yipping[note] -= 1;
                if ( yipping[note] <= 0 )
                {
                    yipping[note] = 0;
                    var elems = key_elements[note];
                    elems.key.classList.remove("pressed");
                    elems.kobold.classList.remove("singing");
                    elems.kobold.classList.remove("yip");
                }
            }

            function increase_yip(note)
            {
                yipping[note] += 1;
                if ( yipping[note] > 0 )
                {
                    var elems = key_elements[note];
                    elems.key.classList.add("pressed");
                    elems.kobold.classList.add("singing");
                }
            }

            function play_note(code, octave, duration)
            {
                var key = note_elements[code];

                if ( key )
                {
                    increase_yip(code);
                    sampler.triggerAttackRelease(code + octave, duration / 1000);
                    setTimeout(() => decrease_yip(code), duration);
                }
            }

            function clear_manual_yip(note)
            {
                var data = manual_yips[note];
                if ( data )
                {
                    delete manual_yips[note];
                    data.destroy();
                }
            }

            function key_note_down(key, octave)
            {
                var note = key.getAttribute("data-note");
                increase_yip(note);
                clear_manual_yip(note);
                manual_yips[note] = new YipText(note, 2, 1);
                sampler.triggerAttack(note + octave);
                return true;
            }

            function key_note_up(key, octave)
            {
                var note = key.getAttribute("data-note");
                decrease_yip(note);
                clear_manual_yip(note);
                sampler.triggerRelease(note + octave);
                sampler_p.triggerAttackRelease(note + octave, p_len);
            }

            function note_down(e)
            {
                if ( e.target.tagName.toLowerCase() == "input" )
                {
                    if ( e.key == "Enter" && e.target.id == "midi-url" )
                        load_url_input();
                    return;
                }

                var key_oct = key_notes[e.code];
                if ( key_oct )
                {
                    var [key, octave] = key_oct;

                    if ( e.shiftKey )
                        octave += 1;
                    if ( e.ctrlKey || e.altKey )
                        octave -= 1;
                    e.stopPropagation();
                    e.preventDefault();

                    if ( octaves[e.code] === undefined )
                    {
                        octaves[e.code] = octave;
                        key_note_down(key, octave);
                    }
                }
            }

            function note_up(e)
            {
                if ( e.target.tagName.toLowerCase() == "input" )
                    return;

                var key_oct = key_notes[e.code];
                if ( key_oct )
                {
                    e.stopPropagation();
                    e.preventDefault();
                    var key = key_oct[0];
                    var octave = octaves[e.code];
                    key_note_up(key, octave);
                    delete octaves[e.code];
                }
            }

            function note_mouse_down(key, e)
            {
                if ( mouse_note === null )
                    note_mouse_up();

                e.preventDefault();

                mouse_note = true;
                var octave = 4;
                if ( e.shiftKey )
                    octave += 1;
                if ( e.ctrlKey )
                    octave -= 1;
                if ( e.button == 2 )
                    octave += 1;

                mouse_note = [key, octave];
                key_note_down(key, octave);
            }

            function note_mouse_up()
            {
                if ( mouse_note )
                {
                    var [key, octave] = mouse_note;
                    mouse_note = null;
                    key_note_up(key, octave);
                }
            }

            function schedule_note(time, note, duration, velocity=1)
            {
                if ( duration < 0.05 )
                    return;

                sampler.triggerAttackRelease(note, duration, time, velocity);
                sampler_p.triggerAttackRelease(note, p_len, time + duration, velocity);
                var base = note.substr(0, note.length - 1);

                Tone.getDraw().schedule(() => {
                    var key = note_elements[base];
                    increase_yip(base);
                    if ( YipText.count < 40 )
                        new YipText(base, duration, velocity);
                }, time);
                Tone.getDraw().schedule(() => {
                    decrease_yip(base);
                }, time + duration);
            }

            function sequence_from_notes(notes, bpm, sustain=1)
            {
                var duration = 60 / bpm;
                var sequence = new Tone.Sequence((time, note) => {
                    schedule_note(time, note, duration * sustain - p_len);
                }, notes, duration);
                sequence.loop = false;
                return sequence;
            }

            function parse_midi_file(file)
            {
                const reader = new FileReader();
                reader.onload = function (e) {
                    array_buffer_to_midi(e.target.result, file.name, null, true, true);
                    document.getElementById('midi-input').classList.add('hidden');
                };
                reader.readAsArrayBuffer(file);
            }

            function midi_track_to_part(track)
            {
                var part = new Tone.Part(
                    (time, value) => {schedule_note(time, value.name.replace("-", ""), value.duration, value.velocity);},
                    track.notes
                );
                part.kobold_time = new TimeRange();
                for ( var note of track.notes )
                {
                    var start = note.time;
                    var end = note.time + note.duration;
                    part.kobold_time.add(start, end);
                }

                return part;
            }

            function midi_to_part(midi, name_hint)
            {
                var multipart = new Multipart(midi.tracks.filter(t => t.notes.length > 0).map(midi_track_to_part), midi.name || name_hint);
                multipart.midi = midi;
                return multipart;
            }

            function array_buffer_to_midi(data, title, url, autoplay, add_example)
            {
                var midi = new Midi(data);
                var part = midi_to_part(midi, title);
                if ( autoplay )
                    controller.play(part);
                if ( add_example )
                {
                    var id = "custom-" + part.name;
                    examples[id] = new Example(part, url, part);
                    var opt = select_song.appendChild(document.createElement("option"));
                    opt.setAttribute("value", id);
                    opt.appendChild(document.createTextNode(part.name));
                    select_song.value = id;
                    if ( autoplay )
                    {
                        controller.example = examples[id];
                        update_share_url();
                    }
                }
                return part;
            }

            function toggle_fullscreen()
            {
                if ( document.fullscreenElement )
                    document.exitFullscreen();
                else
                    document.getElementById("svg-container").requestFullscreen();
            }

            function screen_recording_mode()
            {
                document.getElementById("svg-container").classList.add("recording-mode");
            }

            function load_url_input()
            {
                var input = document.getElementById("midi-url");
                load_url(input.value, true);
                input.value = "";
            }

            function load_url(url, autoplay)
            {
                let basename = new URL(url).pathname.split("/").pop();
                fetch(url).then(r => r.arrayBuffer()).then(ab => {
                    array_buffer_to_midi(ab, basename, url, autoplay, true);
                    document.getElementById('midi-input').classList.add('hidden');
                });
            }

            class YipText
            {
                constructor(note, duration, velocity)
                {
                    var id = "kobold_" + note.toLowerCase().replace("#", "s");
                    let target_kobold = document.getElementById(id);
                    var yip_text = create_yip_element(id);

                    var x = Number(target_kobold.getAttribute("x")) + 696;
                    var y = Number(target_kobold.getAttribute("y")) + 170;
                    var facing_left = target_kobold.hasAttribute("transform");
                    if ( !facing_left )
                        x += 71;

                    yip_text.style.animationDuration = duration + "s";

                    if ( velocity < 0.6 && duration <= 0.3 )
                    {
                        yip_text.classList.add("tiny_yip", facing_left ? "left" : "right");
                    }
                    else
                    {
                        yip_text.style.setProperty("--animation-distance", (-70 * duration) + "px");
                    }

                    yip_text.setAttribute("x", x);
                    yip_text.setAttribute("y", y);

                    var parent = target_kobold.parentNode;
                    parent.appendChild(yip_text);
                    window.setTimeout(this.destroy.bind(this), duration * 1000);
                    this.yip_text = yip_text;
                    this.parent = parent;
                    YipText.count += 1;
                }

                destroy()
                {
                    if ( this.yip_text )
                    {
                        this.parent.removeChild(this.yip_text);
                        this.yip_text = null;
                        YipText.count -= 1;
                    }
                }
            }

            YipText.count = 0;

            function create_yip_element(cls)
            {
                var yip_text = document.createElementNS("http://www.w3.org/2000/svg", "text");
                yip_text.appendChild(document.createTextNode("Yip"));
                yip_text.classList.add("yip", cls);
                return yip_text;
            }

            function update_share_url()
            {
                var url = new URL(window.location);
                url.search = "";
                var input_check = document.getElementById("share-check-song");
                var input_time = document.getElementById("share-start");
                var output = document.getElementById("share-url");
                var current = controller.example;
                var no_current = !current || !current.url;
                input_check.disabled = input_time.disabled = input_check.parentNode.ariaDisabled = input_time.parentNode.ariaDisabled = no_current;

                if ( no_current )
                {
                    input_check.checked = false;
                    input_time.value = 0;
                }
                else if ( input_check.checked )
                {
                    url.searchParams.set("url", current.url);
                    if ( input_time.value != 0 )
                        url.searchParams.set("t", input_time.value);
                }

                output.value = url.toString();
            }

            function copy_clipboard()
            {
                var elem = document.getElementById("share-url");
                elem.select();
                if ( window.navigator.clipboard )
                    window.navigator.clipboard.writeText(elem.value);
            }

            function update_share_url_time(time=null)
            {
                var input_time = document.getElementById("share-start");
                if ( time === null )
                    input_time.value = Math.floor(controller.transport.seconds);
                else
                    input_time.value = time;
                update_share_url();
            }

            function toggle_share()
            {
                var share = document.getElementById("share");
                share.classList.toggle("hidden");
                if ( !share.classList.contains("hidden") )
                {
                    document.getElementById("share-check-song").checked = true;
                    update_share_url();
                    copy_clipboard();
                }
            }

            function playhead_click(target, ev)
            {
                var rect = target.getBoundingClientRect();
                var t = (ev.clientX - rect.left) / rect.width;
                controller.progress = t;
            }

            class TimeRange
            {
                constructor()
                {
                    this.start = Infinity;
                    this.end = -Infinity;
                }

                add(start, end)
                {
                    if ( start === undefined )
                        return;

                    if ( start instanceof TimeRange )
                    {
                        if ( !start.valid )
                            return;

                        end = start.end;
                        start = start.start;
                    }

                    if ( start < this.start )
                        this.start = start;

                    if ( end > this.end )
                        this.end = end;
                }

                get valid()
                {
                    return this.start != Infinity && this.end != -Infinity;
                }

                ensure_valid()
                {
                    if ( !this.valid )
                        this.start = this.end = 0;
                }

                get duration()
                {
                    return this.end - this.start;
                }
            }

            class Multipart
            {
                constructor(parts, name)
                {
                    this.name = name;
                    this.parts = parts;
                    this.kobold_time = new TimeRange();
                    parts.map(p => p.kobold_time).forEach(t => this.kobold_time.add(t));
                    this.kobold_time.ensure_valid();
                }

                start(time)
                {
                    for ( var p of this.parts )
                        p.start(time);
                }

                stop(time)
                {
                    for ( var p of this.parts )
                        p.stop(time);
                }
            }

            class PlaybackController
            {
                constructor()
                {
                    this.part = null;
                    this.example = null;
                    this.start_time = 0;
                    this._scheduled_progress = null;
                    this.transport = Tone.getTransport();
                    this.playhead = document.getElementById("playhead");
                    this.play_icon = document.querySelector("#play-button > i");
                }

                _set_play_icon(icon)
                {
                    this.play_icon.classList.remove("fa-play", "fa-pause");
                    this.play_icon.classList.add("fa-" + icon);
                }

                play(part)
                {
                    this.stop();
                    this._set_playhead(0);
                    this._set_play_icon("pause");
                    this.part = part;
                    part.start(0);
                    this.transport.start();
                    this.transport.seconds = this.start_time + part.kobold_time.start;
                    this.start_time = 0;
                    this.transport.scheduleOnce(this.stop.bind(this), part.kobold_time.end);
                    this.transport.scheduleRepeat(this.on_tick.bind(this), 0.1);
                    if ( this._scheduled_progress !== null )
                    {
                        this.progress = this._scheduled_progress;
                        this._scheduled_progress = null;
                        this.pause();
                    }
                }

                stop()
                {
                    this._set_play_icon("play");
                    this.transport.cancel();
                    this.transport.stop(0);
                    if ( this.part )
                    {
                        this.part.stop(0);
                        this.part = null;
                    }

                    this._set_playhead(0);
                    for ( let key of Object.keys(yipping) )
                    {
                        yipping[key] = 0;
                        decrease_yip(key);
                    }
                }

                pause()
                {
                        this._set_play_icon("play");
                        this.transport.pause();
                }

                play_pause()
                {
                    if ( !this.part )
                    {
                        this.play_current();
                    }
                    else if ( this.transport.state == "paused" )
                    {
                        this._set_play_icon("pause");
                        this.transport.start();
                    }
                    else
                    {
                        this.pause();
                    }
                }

                play_current()
                {
                    examples[select_song.value].play();
                }

                get progress()
                {
                    if ( !this.part )
                        return 0;

                    var t = (this.transport.seconds - this.part.kobold_time.start) / this.part.kobold_time.duration;
                    return Math.max(0, Math.min(1, t));
                }

                set progress(p)
                {
                    if ( !this.part )
                    {
                        this._scheduled_progress = p;
                        this.play_current();
                        return;
                    }

                    var t = p * this.part.kobold_time.duration + this.part.kobold_time.start;
                    this.transport.seconds = t;
                    this._set_playhead(p);
                }

                on_tick()
                {
                    this._set_playhead(this.progress);
                }

                _set_playhead(progress)
                {
                    this.playhead.style.left = (progress * 100) + "%";
                }
            }

            class Example
            {
                constructor(title, url, part=undefined)
                {
                    this.title = title;
                    this.url = url;
                    this.part = part;
                    this.stopped = false;
                }

                play()
                {
                    controller.example = this;
                    update_share_url_time(0);
                    this.stopped = false;
                    if ( this.part )
                        controller.play(this.part);
                    else
                        fetch(this.url).then(r => r.arrayBuffer()).then(ab => { this.part = array_buffer_to_midi(ab, this.title, null, true, false) });
                }
            }

            var key_notes = {};
            var note_elements = {};
            var pending_sounds = {};
            var octaves = {};
            var mouse_note = null;
            var yipping = {};
            var key_elements = {};
            var manual_yips = {};
            // increase the value whenever the sound files change to avoid browser caching
            var nocache = "?nocache=1";

            const sampler = new Tone.Sampler({
                urls: {
                    "E3": "longyip.mp3" + nocache,
                },
                release: 0.05,
                baseUrl: "/media/sounds/",
            }).toDestination();

            const sampler_p = new Tone.Sampler({
                urls: {
                    "E3": "p.mp3" + nocache,
                },
                release: 0,
                baseUrl: "/media/sounds/",
            }).toDestination();
            // sampler_p.volume = -30;
            var p_len = 0.2;


            document.querySelectorAll(".key").forEach(key => {
                var note = key.getAttribute("data-note");
                note_elements[note] = key;
                note_elements[note + "5"] = key;

                var kb = key.getAttribute("data-key");
                key_notes[kb] = [key, 4];
                var key5 = key.getAttribute("data-key5");
                key_notes[key5] = [key, 5];

                var e_map5 = key.appendChild(document.createElement("div"));
                e_map5.classList.add("key-map5");
                e_map5.innerText = key5[key5.length-1];

                var e_map = key.appendChild(document.createElement("div"));
                e_map.classList.add("key-map");
                e_map.innerText = kb[kb.length-1];

                var e_note = key.appendChild(document.createElement("div"));
                e_note.classList.add("key-note");
                e_note.innerText = note;

                yipping[note] = 0;

                var kobold = document.getElementById(
                    "kobold_" + note.toLowerCase().replace("#", "s")
                );
                key_elements[note] = {
                    key: key,
                    kobold: kobold,
                };

                key.addEventListener("touchstart", e => note_mouse_down(key, e));
                key.addEventListener("touchend", e => note_mouse_up());
                key.addEventListener("mousedown", e => note_mouse_down(key, e));
                key.addEventListener("mouseup", e => note_mouse_up());
                key.addEventListener("mouseleave", e => note_mouse_up());
                key.addEventListener("contextmenu", e => e.preventDefault());

                kobold.addEventListener("mousedown", e => note_mouse_down(key, e));
                kobold.addEventListener("mouseup", e => note_mouse_up());
                kobold.addEventListener("mouseleave", e => note_mouse_up());
                kobold.addEventListener("contextmenu", e => e.preventDefault());
                kobold.addEventListener("touchstart", e => note_mouse_down(key, e));
                kobold.addEventListener("touchend", e => note_mouse_up());
                kobold.setAttribute("data-note", note);
            });

            window.addEventListener("keydown", note_down);
            window.addEventListener("keyup", note_up);

            /*var well_notes = {
                6: "C4",
                3: "D4",
                2: "E4",
                1: "F4",
                4: "G4",
                7: "A4",
                8: "B4",
                9: "C5",
            }
            var egg_notes = Array.from("7262726271317131942494248434843472627262713171317971984287148747").map(n => well_notes[n]);*/
            var base_url = "<?php echo href("/media/sounds"); ?>";
            var examples = {
                bells: new Example("Carol of the Yips", base_url + "/carol-of-the-bells.mid"),
                dunk: new Example("Drunken Kobold", base_url + "/drunken-sailor.mid"),
                beeth5: new Example("Kobold's 5th Yip", base_url + "/Beethoven-Symphony5-1.mid"),
                egg: new Example("Egg song", base_url + "/egg.mid"),
                moonlight: new Example("Moonlight Yipata", base_url + "/moonlight-sonata-3.mid"),
                rickroll: new Example("Yiproll", "https://bitmidi.com/uploads/79827.mid"),
                jurassic: new Example("Jurassic Yip", "https://bitmidi.com/uploads/63980.mid"),
                bohemian: new Example("Bohemian Yip", "https://bitmidi.com/uploads/87216.mid"),
                rushe: new Example("Rush Yip", base_url + "/rush-e.mid"),
            };
            var controller = new PlaybackController();
            controller.example = Object.values(examples)[0];

            var select_song = document.getElementById("song-select");
            for ( var [id, ex] of Object.entries(examples) )
            {
                var opt = select_song.appendChild(document.createElement("option"));
                opt.setAttribute("value", id);
                opt.appendChild(document.createTextNode(ex.title));
            }

            document
                .querySelector("#midi-in")
                .addEventListener("change", (e) => {
                    //get the files
                    const files = e.target.files;
                    if (files.length > 0)
                    {
                        const file = files[0];
                        parse_midi_file(file);
                    }
                });

            var window_url = new URL(window.location);
            var param_url = window_url.searchParams.get("url");
            controller.start_time = Number(window_url.searchParams.get("t"));
            if ( param_url )
            {
                load_url(param_url, false);
                var controls = document.getElementById("controls");
                controls.classList.add("hidden");
                var bpb = document.getElementById("big-play-button");
                bpb.classList.remove("hidden");
                bpb.addEventListener("click", e => {
                    bpb.classList.add("hidden");
                    controls.classList.remove("hidden");
                    controller.play_current();
                });
            }

            if ( window_url.searchParams.get("embed") == "1" )
                document.getElementById("svg-container").classList.add("embed");

            // Force the external font to load
            document.getElementById("cave-bg").appendChild(create_yip_element("dummy"));
        </script>

        <?php

    }
}

$page = new ChoirPage();
