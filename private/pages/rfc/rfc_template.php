<?php

$renderer = new RfcHtmlRenderer(__dir__ . "/draft-glax-poescont-01.xml");

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="robots" content="index,follow" />
    <meta name="citation_author" content="<?php $renderer->xp("string(./front/author[1]/@fullname)"); ?>"/>
    <meta name="citation_publication_date" content="<?php $renderer->xp("concat(string(./front/date/@month), ' ', string(./front/date/@year))"); ?>"/>
    <meta name="citation_title" content="<?php echo escape($renderer->title); ?>"/>
    <meta name="citation_pdf_url" content="<?php echo $renderer->docname; ?>.pdf"/>
    <?php include($this->template_path."/page_meta.php"); ?>
    <title><?php echo escape($renderer->title); ?></title>
    <style type="text/css">
        @media only screen and (min-width: 992px) and (max-width: 1199px) {
            body { font-size: 14pt; }
        }
        @media only screen
        and (min-width: 768px)
        and (max-width: 991px) {
            body { font-size: 14pt; }
        }
        @media only screen
        and (min-width: 480px)
        and (max-width: 767px) {
            body { font-size: 11pt; }
        }
        @media only screen
        and (max-width: 479px) {
            body { font-size: 8pt; }
        }
        @media only screen
        and (min-device-width : 375px)
        and (max-device-width : 667px) {
            body { font-size: 9.5pt; }
        }
        @media only screen
        and (min-device-width: 1200px) {
            body { font-size: 10pt; margin: 0 4em; }
        }
        h1, h2, h3, h4, h5, h6, .h1, .h2, .h3, .h4, .h5, .h6 {
            font-weight: bold;
            line-height: 0pt;
            display: inline;
            white-space: pre;
            font-family: monospace;
            font-size: 1em;
            font-weight: bold;
        }
        pre {
            font-size: 1em;
            margin-top: 0px;
            margin-bottom: 0px;
        }
        .pre {
            white-space: pre;
            font-family: monospace;
        }
        .header{
            font-weight: bold;
        }
        .newpage {
            page-break-before: always;
        }
        .invisible {
            text-decoration: none;
            color: white;
        }
        a.selflink {
            color: black;
            text-decoration: none;
        }
        @media print {
            body {
                font-family: monospace;
                font-size: 10.5pt;
            }
            h1, h2, h3, h4, h5, h6 {
                font-size: 1em;
            }

            a:link, a:visited {
                color: inherit;
                text-decoration: none;
            }
            .noprint {
                display: none;
            }
        }
        @media screen {
            body {
                display: flex;
                justify-content: center;
            }
            .grey, .grey a:link, .grey a:visited {
                color: #777;
            }
            tt {
                background-color: #EEE;
                padding: 2px 2px 0;
            }
            .docinfo {
                background-color: #EEE;
            }
            .top {
                border-top: 7px solid #EEE;
            }
            .bgwhite  { background-color: white; }
            .bgred    { background-color: #F44; }
            .bggrey   { background-color: #666; }
            .bgbrown  { background-color: #840; }
            .bgorange { background-color: #FA0; }
            .bgyellow { background-color: #EE0; }
            .bgmagenta{ background-color: #F4F; }
            .bgblue   { background-color: #66F; }
            .bgcyan   { background-color: #4DD; }
            .bggreen  { background-color: #4F4; }

            .legend   { font-size: 90%; }
            .cplate   { font-size: 70%; border: solid grey 1px; }
        }
        ul {
            margin: 0;
        }
        li {
            text-indent: 3ch;
        }
    </style>
    </head>
<body>
<article>
<?php
    $renderer->render_page();
?>
</article>
</body>
</html>
