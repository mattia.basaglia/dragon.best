#!/usr/bin/env python3
import sys
import json
import html
import pathlib
import argparse
import requests
from xml.etree import ElementTree


def render(file_name, file_data, api_key, format):
    print("Generating %s..." % format)

    with open(ns.file, "r") as file:
        file_data = file.read()

    response = requests.post(
        "https://author-tools.ietf.org/api/render/%s" % format,
        files={"file": (file_name.name, file_data)},
        data={"apikey": api_key}
    )

    if response.status_code != 200:
        sys.stderr.write(json.loads(response.content)["error"])
        sys.stderr.write("\n")
    else:
        suf = format if format != "text" else "txt"
        resp2 = requests.get(json.loads(response.content)["url"])
        with open(file_name.with_suffix("." + suf), "wb") as file:
            file.write(resp2.content)


parser = argparse.ArgumentParser()
parser.add_argument("--api-key", "-k", help="API key to generate XML from Markdown - https://datatracker.ietf.org/accounts/apikey")
parser.add_argument("file", type=pathlib.Path)
ns = parser.parse_args()

filename = ns.file

with open(ns.file, "r") as file:
    file_data = file.read()


print("Validating ...")
validation = json.loads(requests.post(
    "https://author-tools.ietf.org/api/validate",
    files={"file": (ns.file.name, file_data)},
    data={"apikey": ns.api_key}
).content)
for k, v in validation.items():
    print(k)
    print(v)

render(ns.file, file_data, ns.api_key, "xml")
render(ns.file, file_data, ns.api_key, "pdf")
#render(ns.file, file_data, ns.api_key, "text")
