---
coding: utf-8

title: Concat notation
abbrev: concat
docname: draft-glax-concat-01
submissiontype: independent
category: info
area: General

ipr: trust200902
workgroup: Meticulous Engineering Overseeing Workgroup
lang: en
kw:
  - cat
  - container

stand_alone: yes
pi: [toc, sortrefs, symrefs, comments]

author:
  -
    ins: M. Basaglia
    name: Mattia Basaglia
    email: glax@dragon.best
    uri: https://dragon.best/
  -
    ins: J. Bernards
    name: Joep Bernards
  -
    ins: J. Maas
    Name: Joost Maas

normative:
  RFC20:
  RFC2119:
  RFC8174:
  RFC5234:


# https://authors.ietf.org/
# https://github.com/cabo/kramdown-rfc
# https://author-tools.ietf.org/ -- RFC validator
# https://tools.ietf.org/tools/bap/abnf.cgi -- ABNF validator

--- abstract

This document defines the "concat" notation: a text-based language
used to describe pictures and videos whose subject includes cats,
containers and their interactions.


--- middle

Introduction
============

Cat pictures and videos are often shared across the internet,
many of such files display feline subjects interacting
with boxes and other containers.


Since currently there is no compact notation for describing such media,
this document describes a standard notation to describe the position
and interaction of cats, containers and related subjects pictured
in these images.


The notation language described in this document is text-based and
limits itself to the US-ASCII {{RFC20}} character encoding, allowing
the transfer of cat-related materials in environments with restricted
capabilities.


Conventions Used in This Document
---------------------------------

{::boilerplate bcp14-tagged}


Definition
==========

Terminology
-----------

This document uses some specific terms to refer to items being
described by the notation described herein.

To avoid ambiguity some of such terms are defined as follows.

Subject

: The term "subject" is used in this document to refer to the object
that is the focus in the media to be annotated.
This usually is an animate object, specifically a cat.
An annotation can have multiple subjects interacting in various ways.

Cat

: A cat is a special kind of subject of feline origin.  While usually
this document will assume a house cat is present in the source media,
other felines are also acceptable.

Container

: The term "container" is used to refer to inanimate objects inside of
which one or more subjects can be located.
Most commonly this will be a cardboard box but a variety of containers
can be used.


Grammar
-------

The grammar is defined following the ABFN notation {{RFC5234}}.

~~~ abnf
SEQUENCE     =  POSITION / POSITION "=>" SEQUENCE
POSITION     =  ADJACENT
ADJACENT     =  OVER / ADJACENT "+" OVER
OVER         =  MULTIPLE / MULTIPLE "/" POSITION
MULTIPLE     =  CONCAT / NUMBER [ "*" ] MULTIPLE / NUMBER "/" MULTIPLE
CONCAT       =  SUBJECT [ NUMBER ] / [ PARTIAL ] CONTAINER [ PARTIAL ]
CONTAINER    =  "[" OPT-POSITION "]" / "(" OPT-POSITION ")"
CONTAINER    =/ "{" OPT-POSITION "}" / "<" POSITION ">"
OPT-POSITION =  [ POSITION ]
SUBJECT      =  CAT / 1*ALPHA / "@"
CAT          =  "cat" / PARTIAL
PARTIAL      =  "c" / "a" / "t" / "ca" / "at"
ALPHA        =   %x41-5A / %x61-7A
NUMBER       =  1*DIGIT
DIGIT        = "0" / "1" / "2" / "3" / "4" / "5" / "6" / "7" / "8" / "9"
~~~
{: artwork-align="center" artwork-name="syntax"}

Elements
========

## Subjects

### Cats

The standard notation for a cat is the word `cat`.

### Partial Cats

When referencing cats partly inside a container, the annotation MUST
contain the full cat mark, adequately split inside and outside the
container.

If a cat is only partly visible in the frame of the picture of video,
the annotation MAY only reference the visible portion of the cat.

The partial cat notations is as follows:

* `c` Marks the head of the cat
* `a` Marks the body of the cat
* `t` Marks the tail of the cat
* `ca` Marks the head and body of the cat
* `at` Marks the body and tail of the cat

The annotation for a partial cat SHOULD use the terms mentioned above
that best describe the portion of the cat that is being referenced.

### Other Animals

Other animals or animate objects SHOULD be represented with a
suitable word describing the species of such animal.
The cat-specific words described in this document MUST NOT be used for
non-feline subjects.

### Balls of Yarn

Balls of yarn SHOULD be represented with `@`.

## Containers

When a cat or other subject is inside a container, the container
notation MUST be used.  Such notation is denoted by its subject being
between brackets.  The type of bracket depends on the shape of the
container as follows:

* Square brackets represent boxes or other containers with a
  rectangular opening.
* parentheses represent containers with a round opening or shape.
* curly braces SHALL be used to represent soft containers without a
  fixed shape

Additionally angle brackets MAY be used to group subjects outside a
container, such annotations MUST NOT contain partial cats.

## Positioning

The Concat notation only gives information about the general layout of
subjects and containers, but it does make a distinction between
horizontal and vertical positions.

The order of positional operands SHOULD follow the order of how they
appear from left to right in the source media.

### Horizontal Position

The `+` operator is used to represent subjects (or containers)
next to each other.

### Vertical Position

When a subject is above or on top another, the operator `/` MUST be
used.

### Multiple Repeated Objects

When multiple objects or configurations are repeated, the shorthand
notation MAY be used.

For horizontal positioning such notation is denoted by a number,
followed by an optional `*` and then the annotation to be repeated.

Similarly, for vertical position, repeated objects are denoted by
a number followed by `/` and the annotation to be repeated.

When using such a shorthand, the number of repetitions MUST be a
positive integer.


## Changes Over Time

In the case of videos or other animations, a proper Concat annotation
SHOULD make use of the state change operator (`=>`) to mark significant
changes in the cat position and major interactions.

### Disambiguation

Subject tokens MAY be followed by an integer identifier
to distinguish specific cats, balls of yarn, or other subjects.
An annotation containing such numeric disambiguations
MUST contain such disambiguations for all cats and balls of yarn.

Since in a static image a specific subject can only appear once,
disambiguation identifiers SHOULD be used only on annotations showing
state changes.


Internationalization Considerations
===================================

The word `cat` is in English and is provided to allow transfer of
Concat notations using only the US-ASCII {{RFC20}} character encoding.

Users of other languages MAY extend the alphabet and use their localized
words for cat and other animals.

Using non-standard words for cats SHOULD NOT be used unless all parties
involved in the production and consumption of the Concat annotation
have agreed upon a character encoding and a language prior to the
transmission of the annotation.


Security Considerations
=======================

A cat might find themselves in a container smaller than the perceived
volume of the cat.  While this might seem as a dangerous situation,
it's actually a natural occurrence when the cat is in its liquid form.

Cats might chew on the cardboard of the box containing them, to
mitigate this attack we recommend having multiple boxes to put the
cats into.


IANA Considerations
===================

This document has no IANA actions.

--- back


Examples
========

This appendix provides some examples of the Concat notation.

~~~~~~~~~~
[cat]
~~~~~~~~~~
{: title="Cat in a box"}

~~~~~~~~~~
[cat] + cat
~~~~~~~~~~
{: title="Cat in a box next to a cat not in a box"}

~~~~~~~~~~
cat / [cat]
~~~~~~~~~~
{: title="A cat over a box containing another cat"}

~~~~~~~~~~
[c]at
~~~~~~~~~~
{: title="A cat with their head inside a box"}

~~~~~~~~~~
3 * cat
~~~~~~~~~~
{: title="3 cats side by side"}

~~~~~~~~~~
3 / cat
~~~~~~~~~~
{: title="3 cats on top of each other"}

~~~~~~~~~~
cat + cat / [cat]
~~~~~~~~~~
{: title="A cat standing next to a box which has a cat on top and inside of it"}

~~~~~~~~~~
<cat + cat> / [cat]
~~~~~~~~~~
{: title="Two cats standing on a box with another cat inside of it"}

~~~~~~~~~~
cat1 + [cat2] => cat2 + [cat1]
~~~~~~~~~~
{: title="A cat inside a box and a cat outside swap places"}
