<?php

require_once(__dir__."/../../dragon.php");


interface RfcTocItem
{
    function next();
    function child();
    function depth();
}

class RfcTocAppendix implements RfcTocItem
{
    function __construct($letter)
    {
        $this->letter = $letter;
    }

    function next()
    {
        $this->active_chunk += 1;
    }

    function child()
    {
        return new RfcTocSection([$this->letter]);
    }

    function __toString()
    {
        return "Appendix {$this->letter}";
    }

    function depth()
    {
        return 1;
    }
}

class RfcTocSection implements RfcTocItem
{
    function __construct($chunks=[])
    {
        $this->chunks = $chunks;
        $this->active_chunk = 1;
    }

    function next()
    {
        $this->active_chunk += 1;
    }

    function child()
    {
        return new RfcTocSection($this->all_chunks());
    }

    function all_chunks()
    {
        $chunks = $this->chunks;
        $chunks[] = (string)$this->active_chunk;
        return $chunks;
    }

    function __toString()
    {
        return implode(".", $this->all_chunks());
    }

    function depth()
    {
        return sizeof($this->chunks) + 1;
    }
}

class RfcTocNone implements RfcTocItem
{

    function next()
    {
    }

    function child()
    {
        return null;
    }

    function __toString()
    {
        return "";
    }

    function depth()
    {
        return 1;
    }
}

class RfcTextRenderer
{
    function __construct($xml_file)
    {
        $this->document = new DOMDocument();
        $this->document->load($xml_file);

        $this->xpath = new Domxpath($this->document);
        $this->docname = $this->document->documentElement->getAttribute("docName");
        $this->title = $this->query("string(./front/title/text())");
        // should be based on ./@category
        $this->category = "Informational";
        $this->indent = "   ";
        $this->figure = 1;
    }

    function escape($text)
    {
        return $text;
    }

    function query($query, $node=null)
    {
        $result = $this->xpath->evaluate($query, $node);
        if ( !is_string($result) )
            $result = $result[0]->nodeValue;
        return $result;
    }

    function pre_item($item)
    {
        if ( is_array($item) )
        {
            if ( is_array($item[0]) )
            {
                $length = 1;
                $value = "[";
                foreach ( $item as $it )
                {
                    list($str, $len) = $this->pre_item($it);
                    $length += $len + 1;
                    $value .= $str . "|";
                }
                $value[strlen($value)-1] = "]";
                return [$value, $length];
            }

            return [$this->local_link($item[0], $item[1]), strlen($item[1])];
        }

        return [escape($item), strlen($item)];
    }

    function pre_line($items)
    {
        $length = 0;
        $formatted = [[], []];
        $i = 0;
        foreach ( $items as $item )
        {
            if ( $item == "--" )
            {
                $i = 1;
            }
            else
            {
                list($str, $len) = $this->pre_item($item);
                $length += $len;
                $formatted[$i][] = $str;
            }
        }

        $length += max(0, sizeof($formatted[0]) - 1);
        $length += max(0, sizeof($formatted[1]) - 1);
        $line = implode(" ", $formatted[0]);
        if ( $length < 72 )
            $line .= str_repeat(" ", 72 - $length);
        $line .= implode(" ", $formatted[1]);

        return $line;
    }

    function line($items)
    {
        echo $this->pre_line($items);
        echo "\n";
    }

    function docinfo($items)
    {
    }

    function heading($level, $item)
    {
        list($str, $length) = $this->pre_item($item);
        echo "\n";
        echo str_repeat(" ", (72 - $length) / 2);
        echo "$str\n\n";
    }

    function paragraph_text($contents, $indent, $extra_indent="")
    {
        if ( $indent )
            echo $this->indent . $extra_indent;
        echo str_replace("\n", "\n" . $this->indent . $extra_indent, $this->escape($contents));
    }

    function ref_link($node)
    {
        $target = $node->getAttribute("target");

        $elements = $this->xpath->query("//*[@anchor='$target']");
        if ( sizeof($elements) != 1 )
            return "[???]";

        if ( $elements[0]->tagName == "reference" )
        {
            $href = $elements[0]->getAttribute("target");
            return "[" . $this->local_link($href, $target) . "]";
        }

        if ( $elements[0]->tagName == "section" )
        {
            $name = $this->query("./name/text()", $elements[0]);
            return "[" . $this->local_link("#" . $target, $name) . "]";
        }

        return "[???]";
    }

    function render_section_title($section, $anchor, $title)
    {
        $depth = $section->depth() + 1;
        $title = $this->escape($title);
        echo "$section.  $title\n\n";
        $child = $section->child();
        $section->next();
        return $child;
    }

    function html_element_to_text($node, $section)
    {
        if ( in_array($node->tagName, ["ul", "li", "dl", "dd", "dt"]) )
        {
            $extra_indent = "";

            if ( $node->tagName == "li" )
            {
                echo $this->indent . "o  ";
                $extra_indent = "   ";
            }
            else if ( $node->tagName == "dd" )
            {
                $extra_indent = $this->indent;
                echo $this->indent . $extra_indent;
            }
            else if ( $node->tagName == "dt" )
            {
                echo $this->indent;
            }

            foreach ( $node->childNodes as $child )
            {

                if ( $child instanceof DOMText && strlen(trim($child->nodeValue)) == 0 )
                    continue;
                $this->xml_to_text($child, $section, false, $extra_indent);
            }
            if ( in_array($node->tagName, ["li", "dt"]) )
                echo "\n\n";
            return;
        }

        foreach ( $node->childNodes as $child )
        {
            $this->xml_to_text($child, $section, false);
        }
    }

    function xml_to_text($node, $section = null, $paragraph=true, $extra_indent = "")
    {
        if ( $node instanceof DOMText || $node instanceof DomCharacterData )
        {
            $this->paragraph_text($node->nodeValue, $paragraph, $extra_indent);
        }
        else if ( $node instanceof DOMNodeList )
        {
            foreach ( $node as $child)
            {
                $this->xml_to_text($child, $section, true, $extra_indent);
            }
        }
        else if ( $node instanceof DOMNode )
        {
            if ( $node->tagName == "xref" )
            {
                echo $this->ref_link($node);
            }
            else if ( $node->tagName == "name" )
            {
            }
            else if ( in_array($node->tagName, ["tt", "ul", "li", "dl", "dd", "dt"]) )
            {
                $this->html_element_to_text($node, $section);
            }
            else if ( $node->tagName == "bcp14" )
            {
                foreach ( $node->childNodes as $child )
                    $this->xml_to_text($child, $section, false, $extra_indent);
            }
            else if ( $node->tagName == "t" || $node->tagName == "artwork" )
            {
                foreach ( $node->childNodes as $child )
                {
                    $this->xml_to_text($child, $section, $paragraph, $extra_indent);
                    $paragraph = false;
                }
                echo "\n\n";
            }
            else
            {
                if ( $node->tagName == "section" )
                    $section = $this->render_section_title($section, $node->getAttribute("anchor"), $this->query("./name/text()", $node));

                foreach ( $node->childNodes as $child )
                {
                    if ( $child instanceof DomElement )
                        $this->xml_to_text($child, $section, true, $extra_indent);
                }

                if ( $node->tagName == "figure" )
                {
                    echo $this->indent . "Figure {$this->figure}: ";
                    echo $this->query("./name/text()", $node);
                    echo "\n\n";
                    $this->figure += 1;
                }
            }
        }
    }

    function render_preamble()
    {
        $lines_left = [
            $this->query("./front/workgroup/text()"),
            $this->query("string(./front/seriesInfo/@name)"),
            "Intended status: {$this->category}",
            "Expires: Never",
        ];
        $authors = $this->xpath->query("./front/author/@fullname");

        for ( $i = 0; $i < max(sizeof($lines_left), sizeof($authors)); $i++ )
        {
            $line = [];

            if ( $i < sizeof($lines_left) )
                $line[] = $lines_left[$i];

            $line[] = "--";

            if ( $i < sizeof($authors) )
                $line[] = $authors[$i]->value;

            $this->line($line);
        }
        $this->line(["--",
            $this->query("concat(string(./front/date/@month), ' ', string(./front/date/@year))")
        ]);

        $this->heading(1, $this->title);

        echo "Abstract\n\n";
        $this->xml_to_text($this->xpath->query("./front/abstract"));

        $copy_year = date("Y");
        $link = $this->external_url(href("."));
        echo <<<HERE
Status of This Memo

   This document is not an Internet Standards Track specification; it is
   published for informational purposes.

   This document is a product of the Meticulous Engineering Overseeing
   Workgroup (MEOW) and represents information that the MEOW has deemed
   valuable to provide for permanent record.  It represents the
   consensus of the Meticulous Engineering Overseeing Workgroup (MEOW).

   Information about the current status of this document, any errata,
   and how to provide feedback on it may be obtained at
   $link.

Copyright Notice

   Copyright (c) {$copy_year} Meticulous Engineering Overseeing
   Workgroup and the persons identified as the document authors.
   All rights reserved.


HERE;
    }

    function local_link($href, $text)
    {
        return $text;
    }

    function render_toc_line(RfcTocItem $section, $anchor, $title, $page)
    {
        $indent = str_repeat($this->indent, $section->depth());
        echo $indent;
        $sec_str = (string)$section;
        if ( $sec_str )
        {
            echo $this->local_link("#$anchor", $sec_str);
            $title = " " . $title;
        }
        $title .= " ";
        echo $this->escape($title);
        $ps = (string)$page;
        $ndots = 72 - strlen($indent) - strlen($sec_str) - strlen($title) - strlen($ps);
        echo str_repeat(".", $ndots);
        echo $this->local_link("#$anchor", $ps);
        echo "\n";
    }

    function render_toc_item(RfcTocItem $section, DomElement $node, $page)
    {
        $this->render_toc_line($section, $node->getAttribute("anchor"), $this->query("./name/text()", $node), $page);
        $page += 1;

        $child = $section->child();
        foreach ( $this->xpath->query("./section", $node) as $sub )
        {
            $page = $this->render_toc_item($child, $sub, $page);
            $child->next();
        }
        return $page;
    }

    function render_toc()
    {
        echo "Table of Contents\n\n";
        $section = new RfcTocSection();
        $page = 1;
        foreach ( $this->xpath->query("./middle/section") as $sub )
        {
            $page = $this->render_toc_item($section, $sub, $page);
            $section->next();
        }
        $this->render_toc_line($section, "references", "References", $page);
        $page += 1;

        $letter = ord("A");
        foreach ( $this->xpath->query("./back/section") as $sub )
        {
            $page = $this->render_toc_item(new RfcTocAppendix(chr($letter)), $sub, $page);
            $letter += 1;
        }

//         $this->render_toc_line(new RfcTocNone(), "addresses", "Authors' Addresses", $page);

        echo "\n";
    }

    function external_url($href)
    {
        return "<$href>";
    }

    function render_references($section)
    {
        $large_indent = str_repeat(" ", 12) . $this->indent;
        $this->render_section_title($section, "references", "References");
        foreach ( $this->xpath->query("./back/references/reference") as $ref )
        {
            $authors = [];
            foreach ( $this->xpath->query("./front/author/@fullname", $ref) as $author )
            {
                $authors[] = $author->nodeValue;
            }
            $text = implode(" and ", $authors) . ", ";
            $text = '"' . $this->query("./front/title/text()", $ref) . '", ';
            foreach ( $this->xpath->query("./seriesInfo", $ref) as $info )
                $text .= $info->getAttribute("name") . " " . $info->getAttribute("value") . ", ";
            $text .= $this->query("concat(string(./front/date/@month), ' ', string(./front/date/@year))", $ref) . ',';
            $lines = explode("\n", wordwrap($text, 72 - strlen($large_indent)));
            $target = $ref->getAttribute("target");
            $lines[] = $this->external_url($target) . ".";

            $anchor = $ref->getAttribute("anchor");
            $pre_line = $this->indent . "[$anchor]";
            echo $pre_line;
            echo str_repeat(" ", strlen($large_indent) - strlen($pre_line));
            echo implode("\n" . $large_indent, $lines);
            echo "\n\n";
        }
    }

    function render_middle()
    {
        $section = new RfcTocSection();
        foreach ( $this->xpath->query("./middle/section") as $sub )
        {
            $this->xml_to_text($sub, $section);
            $section->next();
        }
        $this->render_references($section);
    }

    function render_back()
    {
        $letter = ord("A");
        foreach ( $this->xpath->query("./back/section") as $sub )
        {
            $page = $this->xml_to_text($sub, new RfcTocAppendix(chr($letter)));
            $letter += 1;
        }
    }

    function render_doc_contents()
    {
        $this->render_preamble();
        $this->render_toc();
        $this->render_middle();
        $this->render_back();
    }

    function render_page()
    {
        $this->render_doc_contents();
    }
}

class RfcHtmlRenderer extends RfcTextRenderer
{
    function escape($text)
    {
        return escape($text);
    }

    function xp($query)
    {
        echo escape($this->query($query));
    }

    function docinfo($items)
    {
        echo '<span class="pre noprint docinfo">';
        echo $this->pre_line($items);
        echo "</span><br />\n";
    }

    function heading($level, $item)
    {
        list($str, $length) = $this->pre_item($item);
        echo "\n";
        echo str_repeat(" ", (72 - $length) / 2);
        echo "<span class='h$level'>$str</span>\n\n";
    }

    function render_section_title($section, $anchor, $title)
    {
        $depth = $section->depth() + 1;
        $title = $this->escape($title);
        echo "<span class='h$depth'><a class='selflink' id='$anchor' href='#$anchor'>$section</a>. $title</span>\n\n";
        $child = $section->child();
        $section->next();
        return $child;
    }

    function html_element_to_text($node, $section)
    {/*
        echo "<{$node->tagName}>";
        foreach ( $node->childNodes as $child )
        {
            $this->xml_to_text($child, $section, false);
        }
        echo "</{$node->tagName}>";

        if ( $node->tagName == "ul" || $node->tagName == "dl" )
            echo "\n";*/

        if ( in_array($node->tagName, ["ul", "dl"]) )
        {

            echo "<{$node->tagName}>";
            foreach ( $node->childNodes as $child )
            {

                if ( $child instanceof DomElement )
                    $this->xml_to_text($child, $section, false, "");
            }
            echo "</{$node->tagName}>";
            if ( $node->tagName == "ul" )
                echo "\n";
        }
        else if ( in_array($node->tagName, ["li", "dd", "dt"]) )
        {
            echo "<{$node->tagName}>";
            if ( $node->tagName == "dd" || $node->tagName == "dt" )
                echo $this->indent;

            foreach ( $node->childNodes as $child )
            {

                if ( $child instanceof DOMText && strlen(trim($child->nodeValue)) == 0 )
                    continue;
                $this->xml_to_text($child, $section, false);
            }

            echo "</{$node->tagName}>";
            if ( $node->tagName == "dt")
                echo "\n";
        }
        else
        {
            echo "<{$node->tagName}>";
            foreach ( $node->childNodes as $child )
            {
                $this->xml_to_text($child, $section, false);
            }
            echo "</{$node->tagName}>";
        }
    }


    function local_link($href, $text)
    {
        return (string)mkelement(["a", ["href" => "$href"], $text]);
    }

    function external_url($href)
    {
        return $this->local_link($href, $href);
    }

    function render_docinfo()
    {
        $this->docinfo([
            [["/", "RFC Home"]],
            [
                [$this->docname . ".txt", "TEXT"],
                [$this->docname . ".pdf", "PDF"],
                [$this->docname . ".html", "HTML"],
                [$this->docname . ".xml", "XML"],
            ]
        ]);
        $this->docinfo([]);
        $this->docinfo(["--", strtoupper($this->category)]);
    }

    function render_page()
    {
        $this->render_docinfo();
        echo "<pre>";
        $this->render_doc_contents();
        echo "</pre>";
    }
}

class RfcPage extends DurgPage
{
    protected function init_patterns()
    {
        $this->patterns["^/(?P<docname>[-a-z0-1]+)\.(?P<format>[a-z]+)$"] = "document";
    }

    function render($render_args=array())
    {
        throw new HttpStatus(404);
    }

    protected function document($match)
    {
        $xml = __dir__ . "/" . $match["docname"] . ".xml";
        if ( !file_exists($xml) )
            throw new HttpStatus(404);

        switch ( $match["format"] )
        {
            case "html":
                $renderer = new RfcHtmlRenderer($xml);
                $render_args = [
                    "renderer" => $renderer
                ];
                include("rfc_template.php");
                break;
            case "txt":
                header("Content-Type: text/plain");
                $renderer = new RfcTextRenderer($xml);
                $renderer->render_page();
                break;
            case "pdf":
                header("Content-Type: application/pdf");
                echo file_get_contents(__dir__ . "/" . $match["docname"] . ".pdf");
                break;
            case "xml":
                header("Content-Type: text/xml");
                echo file_get_contents(__dir__ . "/" . $match["docname"] . ".xml");
                break;
            default:
                throw new HttpStatus(404);
        }
    }

    protected function dispatch_noslash($path)
    {
        return false;
    }

    function get_meta_description($render_args=array())
    {
        if ( !isset($render_args["renderer"] ) )
            return "";
        return $render_args["renderer"]->query("./front/abstract/t/text()");
    }

    function title($render_args=array())
    {
        if ( !isset($render_args["renderer"] ) )
            return "";
        return $render_args["renderer"]->title;
    }
}

$page = new RfcPage();


