<?php

require_once(__dir__."/../dragon.php");
require_once(__dir__."/../lib/telegram.php");
require_once(__dir__."/../lib/number_writer.php");



class TelegramPage extends DurgPage
{
    public $title = "Telegram Chats and Bots";
    private $groups = ["ukdragons", "CrazyCatChat"];
    private $bots = [
        # @username => [description, api]
//         "DurgTimeBot" => ["Generates a dragon clock.", "clock"],
        "GlaxTimeBot" => ["Inline bot that generates stickers showing a dragon clock.", "clock"],
        "GlaxSaysBot" => ["Inline bot generating memes at the expense of Glax.", "glax_says"],
        "GlaxScremBot" => ["Inline bot generating animated stickers with Glax screaming or inhaling text and emoji.", "glax_screm"],
        "GlaxSealBot" => ["Similar to @GlaxSaysBot but shows the text around Glax.", "glax_seal"],
        "BestDragonBot" => ["Lists links to the best dragons pages", "dragons"],
        "GlaxWeatherBot" => ["Shows Glax in the current weather.", "glax_weather"],
        "GlaxFlagBot" => ["Generates Glax holding one or two flags.", "flag"],
        "IsThisAGlaxBot" => ["Generates an \"is this a pigeon?\" meme with fursuits", "is_this_an_api"],
        "YouWouldntBot" => ["Pokes fun to the old anti-piracy ads.", "you_wouldnt"],
        "SomeDragonsBot" => ["Generates a \"Some X. Get over it\" image.", "some_dragons"],
        "BrexitBusBot" => ["Generates the brexit bus with custom text.", "brexit_bus"],
        "ADRagonBot" => ["Generates an ADR sign with dragons.", "adragon"],
        "GooglaxBot" => ["Inline bot that generates an animated sticker making a search on the \"Googlax\" search engine.", ""],
        "GlaxMatrixBot" => ["Inline bot that generates an animated sticker showing a matrix-like text effect.", ""],
        "GlaxThinkBot" => ["Inline bot that generates an animated sticker showing Glax thinking about something.", ""],
        "GlaxGearBot" => ["Inline bot that generates text in the style of Baba is You.", ""],
        "GlaxMakeRobot" => ["Inline bot that generates text in the style of Baba is You.", ""],
        "GlaxGearBot" => ["Inline bot that generates animated gears.", ""],
        "PoesContBot" => ["Inline bot that generates stickers based on the PoesCont language.", ""],
        "GlaxMathBot" => ["Inline bot that generates stickers of Glax writing stuff on a blackboard.", ""],
        "GlaxCertBot" => ["Inline bot that generates certificate stickers.", "certificate"],
    ];

    function __construct()
    {
        parent::__construct();
        $this->number_writer = rawr_number_writer();
    }

    function extra_head($render_args)
    {
        ?>
        <style>
            .group {
                display: flex;
                align-items: flex-start;
            }
            .group h2 {
                margin-top: 0;
            }
            .group > img {
                margin-right: 1em;
                width: 160px;
                height: 160px;
            }
            .teledesc {
                white-space: pre-wrap;
            }
        </style>
        <?php
    }

    function main($render_args)
    {
        $use_tg_api = 0;
        if ( $use_tg_api )
        {
            echo mkelement(["h1", [], "Group Chats"]);
            $telegram = Telegram::instance();

            foreach ( $this->groups as $group_name )
            {
                $group = $telegram->group($group_name);
                $photo = $group->photo(true);
                $desc = $group->description;
                if ( !ctype_punct(substr($desc, -1)) )
                    $desc .= ".";

                echo mkelement(["section", ["class" => "group"], [
                    ["img", ["src" => href($photo->get_url())]],
                    ["div", [], [
                        ["h2", [], [new Link($group->link, $group->title)]],
                        ["p", ["class"=>"teledesc"], $desc],
                        ["p", [], [
                            ucfirst($this->number_writer->format_number($group->member_count)),
                            " of the best dragons hang around in here."
                        ]],
                    ]],
                ]]);
            }

            echo new HeadingAnchor("h1", "Bots");
            global $site;
            foreach ( $site->settings->telegram_bots as $token )
            {
                try {
                    $bot = $telegram->bot($token);
                    list($desc, $api) = $this->bots[$bot->username];
                    echo mkelement(["section", ["class" => "group"], [
                        ["img", ["src" => $bot->photo !== null ? href($bot->photo->file()->get_url()) : ""]],
                        ["div", [], [
                            ["h2", [], new Link($bot->link, "@{$bot->username}")],
                            ["p", [], $desc],
                            $api != "" ? ["p", [], ["See also the ", new Link("/api/$api/", "$api"), " API"]] : ""
                        ]],
                    ]]);
                } catch ( Throwable $err ) {}
            }
        }
        else
        {
            echo new HeadingAnchor("h1", "Bots");
            foreach ( $this->bots as $name => $info )
            {
                list($desc, $api) = $info;
                echo mkelement(["section", ["class" => "group"], [
                    ["div", [], [
                        ["h2", [], new Link("https://t.me/$name", "@$name")],
                        ["p", [], $desc],
                        $api != "" ? ["p", [], ["See also the ", new Link("/api/$api/", "$api"), " API"]] : ""
                    ]],
                ]]);
            }
        }
    }
}

$page = new TelegramPage();
