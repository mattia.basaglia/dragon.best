<?php

require_once(__dir__."/../dragon.php");

class YipYapPage extends DurgPage
{
    public $title = "COBOLD";
    public $description = "Programming language for kobolds";
    public $scripts = [
        "https://cdn.jsdelivr.net/pyodide/v0.25.1/full/pyodide.js",
        "/media/scripts/python_glue.js",
    ];

    public $styles = [
        "/media/styles/durg.css",
        "/media/styles/vm.css"
    ];

    function extra_head($render_args)
    {
        ?><style>
        #inspect-memory {
            display: inline-flex;
            flex-flow: row wrap;
        }
        #inspect-memory td, #inspect-memory tr {
            height: 33%;
            text-align: center;
        }
        dt {
            font-weight: bold;
            margin-inline-start: 20px;
        }
        dd {
            margin-inline-start: 60px;
        }
        #last-instruction-pseudocode {
            white-space: pre;
        }
        </style><?php
    }

    function main($render_args)
    {
        $this->body_title(null, $render_args);
        ?>
            <h2>Specs</h2>

            <p><em>COBOLD</em> (also known as <em>YipYap</em>) is an programming language for Kobolds.</p>
            <p><em>COBOLD</em> is primarily used in yipping and yapping.</p>

            <h3>Syntax</h3>
            <section>
                <p><em>COBOLD</em> programs MUST start with <code>yip yap</code>.
                Followed by a list of instructions separated by spaces.</p>
                <p>Comments start with <code>owo</code> and continue until
                the end of the line.</p>
            </section>

            <h3>Execution model</h3>
            <section>
                <p>The <em>COBOLD</em> execution model consists of the following items:</p>
                <dl>
                    <dt>Hold Register</dt>
                    <dt><code>hold</code></dt>
                    <dd>An unsigned 8-bit register used to perform most operations</dd>

                    <dt>Stack Memory<dt>
                    <dt><code>memory</code></dt>
                    <dd>An array of unsigned 8-bit integers that grows as needed, initialized with a single 0 value</dd>

                    <dt>Stack Pointer</dt>
                    <dt><code>stack</code></dt>
                    <dd>A register representing an index within <em>Stack Memory</em>. If this register grows past the
                        end of the <em>Stack Memory</em>, new elements are allocated.</dd>

                    <dt>Stack Value</dt>
                    <dt><code>*stack</code></dt>
                    <dt><code>memory[stack]</code></dt>
                    <dd>The value in memory pointed by the <em>Stack Pointer</em>.</dd>
                </dl>
            </section>
        <?php
            global $site;
            echo file_get_contents($site->settings->root_dir . "/media/scripts/yipyap/specs.htm")
        ?>

        <h2>Playground</h2>
        <p>Here you can execute and debug <em>COBOLD</em> scripts.
        Note that since this is a debug view loading a program takes some time and execution is rather slow.
        For running the code faster, you should use the <a href="/media/scripts/yipyap/yipyap.py">python script</a>.
        </p>

        <div id="main-loading">
            Initializing virtual machine, please wait...
        </div>
        <div id="main-loaded" style="display: none">
            <table class="table" id="playground-explain" style="display: none"></table>

            <div id="playground-controls">
                <p><label for="examples">Example</label> <select id="examples"></select></p>
                <ul class="buttons">
                    <li><button id="btn-load" title="Load Code"><i class="fa-solid fa-download"></i></li>
                    <li><button id="btn-play" title="Play"><i class="fa-solid fa-play"></i></button></li>
                    <li><button id="btn-pause" title="Pause"><i class="fa-solid fa-pause"></i></button></li>
                    <li><button id="btn-step" title="Run Step"><i class="fa-solid fa-forward-step"></i></button></li>
                </ul>
                <p class="grid-form">
                    <label for="run-speed">Instruction delay</label>
                    <input type="number" min="0" step="100" max="1000" id="run-speed" value="0"/>
                    <span>ms</span>
                </p>
            </div>
            <div id="playground-area">
                <textarea id="playground-editor"></textarea>
                <div id="playground-output-p" class="mono border"><div id="playground-output"></div></div>
            </div>

            <section id="inspect">
                <div>
                    <table class="mono table">
                        <caption>Registers</caption>
                        <thead>
                            <tr>
                                <th>Register</th>
                                <th>Hex</th>
                                <th>Dec</th>
                                <th>Char</th>
                            </tr>
                        </thead>
                        <tbody id="inspect-registers"></tbody>
                    </table>
                    <table class="table" style="table-layout:fixed; width: 280px;">
                        <caption>Last instruction</caption>
                        <tr>
                            <th style="width: 7ex">Name</th>
                            <th>Pseudocode</th>
                        </tr>
                        <tr style="height: 1.2em">
                            <td id="last-instruction-name"></td>
                            <td><code id="last-instruction-pseudocode"></code></td>
                        </tr>
                        <tr style="height: 5em"><td id="last-instruction-desc" colspan="2"></td></tr>
                    </table>
                </div>
                <div class="mono table" id="inspect-memory" style="max-width: 800px"></div>
            </section>

        </div>
        <script type="text/javascript">
            load_python("yipyap", async () => {
                await py_import("yipyap");
                await py_import("xml_helper");
                let web = await py_import("yipyap_web");
                web.init();
            });
        </script>
        <?php
    }
};

$page = new YipYapPage();

