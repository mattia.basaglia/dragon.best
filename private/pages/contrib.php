<?php

require_once(__dir__."/../dragon.php");
require_once(__dir__."/../lib/pages/gallery.php");
require_once(__dir__."/../contrib-list.php");


class ContribPage extends DurgPage
{
    use ExplicitGalleryTrait;

    public $title = "Contrib Art";
    public $description = "Art of Glax not made by Glax";

    public $styles = [
        "/media/styles/durg.css",
        "/media/styles/gallery.css",
        "https://cdnjs.cloudflare.com/ajax/libs/vis/4.21.0/vis.min.css",
    ];

    public $scripts = [
        "https://cdnjs.cloudflare.com/ajax/libs/vis/4.21.0/vis.min.js"
    ];

    function base_uri()
    {
        return "/contrib/";
    }

    function media_path()
    {
        return "/media/img/contrib/";
    }

    function server_image_path()
    {
        return dirname(dirname($this->self_dirname())) . $this->media_path();
    }

    protected function get_css_class($selected)
    {
        if ( $selected == 1 ) return "durgpic selected";
        if ( $selected == 2 ) return "bigdurg";
        return "durgpic";
    }

    function extra_head($render_args)
    {
        ?><style>
        .bigdurg.type-gif {
            image-rendering: pixelated;
            image-rendering: crisp-edges;
            image-rendering: -moz-crisp-edges;
        }
        audio {
            width: 512px;
            max-width: 100vw;
        }
        .vis-item a {
            color: inherit;
            text-decoration: none;
        }
        .vis-item img {
            max-width: 48px;
            max-height: 48px;
        }
        .vis-item:hover {
            z-index: 2;
        }
        .vis-tooltip .copyright {
            font-size: x-small;
        }
        .vis-tooltip .title {
            font-weight: bold;
        }
        </style><?php
    }

    function humanmeta_template($image)
    {
        if ( isset($image->meta["extra"]) )
            $extra = "<br/>%(extra.type:nometa) by %(extra.author) &copy; %(extra.copyrightYear)";
        else
            $extra = "";
        return "%(type:nometa) by %(author) &copy; %(copyrightYear) %(license:html)$extra<br/>%(source:html)";
    }

    function get_meta_description($render_args=array())
    {
        $focus = $render_args["image"] ?? null;
        if ( !$focus )
            return $this->description;
        $meta = $focus->meta;
        return "{$meta['type']} by {$meta['author']} (C) {$meta['copyrightYear']}";
    }

    function render_thumbnail(MediaFileInfo $image, $render_args)
    {
        if ( !$this->should_show_thumbnail($image) )
            return;

        $url = $this->base_uri() . rawurlencode($image->slug) . "/";
        $image->render_thumbnail(
            $this->get_css_class(0),
            $url,
            ["rel" => "image"],
            $this->thumbnail_meta($image)
        );
    }

    function main($render_args)
    {
        $focus = $render_args["image"] ?? null;

        if ( !$focus )
        {
            $this->body_title(null, $render_args);
            echo mkelement(["p", [], $this->description]);
            $this->timeline();
        }

        $this->render_gallery($focus, $render_args);
    }

    private function timeline()
    {
        global $auth;
        ?><div id="timeline"></div>
        <script>
            var items = [<?php
                $aggr = 0;
                foreach ( $this->image_groups as $g )
                {
                    foreach ( $g->images as $i )
                    {
                        if ( !$this->should_show_thumbnail($i) )
                            continue;

                        if ( $auth->is_admin() )
                            $aggr += $i->meta["price"];
                        echo json_encode([
                            "content" => (string)mkelement([
                                "a", ["href" => href("./{$i->slug}")], [
                                $i->is_visual() ? ["img", ["src"=>$i->thumbnail_url()]] : $i->title,
                            ]]),
                            "start" => $i->meta["dateCreated"],
                            "title" => "<div class='title'>{$i->title}</div><div class='copyright'>by {$i->meta['author']}</div>"
                                . (
                                    $auth->is_admin() ?
                                    "<div class='copyright'>".
                                    sprintf("%.2f", $i->meta['price']).
                                    " (".sprintf("%.2f", $aggr).")</div>"
                                    : ""
                                ),
                            # For the cost chart
                            "x" => $i->meta["dateCreated"],
                            "y" => $aggr,
                        ]).",";
                    }
                }
            ?>];
            var last_year = new Date();
            last_year.setFullYear(last_year.getFullYear() - 1);
            var options = {
                clickToUse: true,
                height: 256,
                start: last_year,
            };
            var container = document.getElementById('timeline');
            var timeline = new vis.Timeline(container, items, options);
        </script>
        <?php
        global $auth;
        if ( $auth->is_admin() )
        { ?>
            <div id="cost"></div>
            <script>
                var cost_container = document.getElementById('cost');
                var cost_options = {
                    clickToUse: true,
                    height: 256,
                    start: last_year,
                };
                var cost_chart = new vis.Graph2d(cost_container, items, cost_options);

                function onMouseover (properties) {
                    console.log('mouseover properties:', properties.value);
                }
                cost_chart.on('click', onMouseover)
            </script>
        <?php }
    }

    function render_focused(MediaFileInfo $image, $render_args)
    {
        $image->render(
            $this->get_css_class(2)." type-".$image->extension,
            ["property"=>"image"],
            ["dateCreated", "character", "uploadDate", "description", "name", "thumbnailUrl"]
        );
        $this->print_humanmeta($image);
    }

    function should_show_thumbnail(MediaFileInfo $image)
    {
        global $auth;
        $sfw = true;
        $nsfw = false;

        if ( isset($_GET["nsfw"]) )
        {
            $sfw = $_GET["nsfw"] != "only";
            $nsfw = true;
        }

        if ( $image->meta["sfw"] )
            return $sfw;
        return $nsfw;
    }

    function load_images()
    {
        global $auth;
        $items = contrib_art();

        $cg = new ImageGroup(2017);
        $groups = [$cg];

        foreach ( $items as $item )
        {
            if ( $item->attrs[0]["copyrightYear"] > $cg->name )
            {
                $cg = new ImageGroup($item->attrs[0]["copyrightYear"]);
                $groups[] = $cg;
            }
            $media_info = $item->to_media_info($this);
            if ( $auth->is_admin() )
                $media_info->meta["price"] = $item->norm_price();
            $cg->add($media_info);
        }

        return $groups;
    }
}

$page = new ContribPage();
