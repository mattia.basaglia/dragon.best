<?php

require_once(__dir__."/../dragon.php");
require_once(__dir__."/../lib/number_writer.php");


class LanguagePage extends DurgPage
{
    public $title = "Dragon Language";
    public $description = "";
//     public $scripts = [];
//     public $styles = [
//         "/media/styles/durg.css",
//     ];

    function base_glyph($label, $element, &$glyphs)
    {
        $d = "";

        foreach ( $element->childNodes as $node )
        {
            if ( $node->nodeType != XML_ELEMENT_NODE || $node->tagName != "path" )
                continue;

            $d .= $node->getAttribute("d") . " ";
        }


        $glyphs[$label] = $d;
    }

    function load()
    {
        global $site;
        $glyph_svg = $site->settings->root_dir . "/media/img/pages/lang/glyphs.svg";

        $dom = new DOMDocument();
        $dom->loadXML(file_get_contents($glyph_svg));
        foreach ( $dom->documentElement->childNodes as $node )
        {
            if ( $node->nodeType != XML_ELEMENT_NODE || $node->tagName != "g" )
                continue;

            $label = $node->getAttributeNs("http://www.inkscape.org/namespaces/inkscape", "label");
            $this->base_glyph($label, $node, $this->glyphs);
        }
    }

    function extra_head($render_args)
    {
        $this->load();

        ?>
        <style>
        textarea {
            width: 100%;
            height: 256px;
        }
        .table {
            border-collapse: collapse;
        }
        .table th, .table td {
            text-align: center;
            border: 1px solid var(--shade-3);
        }
        .table th {
            background: var(--button-background);
            padding: 0.5ex;
        }
        .uncommon {
            opacity: 20%;
        }

        #trans_output, #num_output {
            margin-top: 3px;
        }
        </style>
        <script>

        class DragonScriptDocument
        {
            constructor(script, size=128)
            {
                this.script = script;
                this.letter_spacing = this.script.char_width * 0.2;
                this.line_height = this.script.char_height * 1.2;
                this.x = 0;
                this.y = 0;
                this.width = 0;
                this.height = this.script.char_height;
                this.svg = document.createElementNS("http://www.w3.org/2000/svg", "svg");
                this.svg.style.fill = "none";
                this.svg.style.stroke = "#000000";
                this.svg.style.strokeLinecap = "round";
                this.svg.style.strokeLinejoin = "round";
                this.scale = size / 128;
                this.svg.style.strokeWidth = 6;
                this.style = this.svg.style;
                this.wrap_at = -1;
            }

            clear()
            {
                this.x = 0;
                this.y = 0;
                this.width = 0;
                this.height = this.script.char_height;
                this.svg.innerHTML = "";
            }

            combine_glyph(left, right)
            {
                let d = this.script.data.staff;

                if ( left == "g" && right == "d" )
                    right = "t";

                if ( left )
                {
                    if ( left == "s" && right && this.script.alt_s.has(right) )
                        left = "s alt";

                    d += this.script.data.consonants[left].left;
                }

                if ( right )
                {
                    if ( right == "s" && left && this.script.alt_s.has(left) )
                        right = "s alt";

                    d += this.script.data.consonants[right].right;
                }

                this.make_glyph(d);
            }

            combine_vowel(top, bottom)
            {
                let tatt = "top";
                let batt = "bottom";
                if ( top != bottom )
                {
                    if ( top == "e" )
                        batt = "mid";

                    if ( bottom == "e" )
                        tatt = "mid";
                }

                let d = this.script.data.vowels[top][tatt] + this.script.data.vowels[bottom][batt];
                this.make_glyph(d, -32);
            }

            new_line()
            {
                this.y += this.line_height;
                this.height += this.line_height;

                if ( this.x > this.width )
                    this.width = this.x;

                this.x = 0;
            }

            make_glyph(d, margin=0, margin_after=undefined)
            {
                if ( this.x != 0 )
                {
                    this.x += this.letter_spacing;
                    if ( this.wrap_at > 0 && this.x >= this.wrap_at )
                        this.new_line();
                }


                if ( margin_after === undefined )
                    margin_after = margin;
                this.x += margin;
                let element = document.createElementNS("http://www.w3.org/2000/svg", "path");
                element.setAttribute("d", d);
                element.setAttribute("transform", `translate(${this.x}, ${this.y})`);
                this.svg.appendChild(element);
                this.x += this.script.char_width + margin_after;
            }

            flush_last(last, last_cons, left)
            {
                if ( last )
                {
                    if ( last_cons )
                    {
                        if ( left )
                            this.combine_glyph(last, "");
                        else
                            this.combine_glyph("", last);
                    }
                    else
                    {
                        this.make_glyph(this.script.data.vowels[last].mid, -32);
                    }
                }

                return null;
            }

            process(text)
            {
                let last = null;
                let last_cons = false;

                for ( let i = 0; i < text.length; i++ )
                {
                    let ch = text[i];
                    let id = ch.toLowerCase();

                    if ( id in this.script.data.consonants )
                    {
                        if ( last && !last_cons )
                        {
                            this.make_glyph(this.script.data.vowels[last].mid, -32);
                            last = null;
                        }

                        if ( id == "s" && i < text.length - 1 && text[i+1] == "*" )
                        {
                            id += " alt";
                            i++;
                        }

                        if ( ch == ch.toUpperCase() )
                        {
                            if ( last )
                            {
                                this.combine_glyph("", last);
                                last = null;
                            }
                            this.combine_glyph(id, id);
                        }
                        else if ( last != null )
                        {
                            this.combine_glyph(last, id);
                            last = null;
                        }
                        else
                        {
                            last = id;
                            last_cons = true;
                        }
                    }
                    else if ( id == " " )
                    {
                        if ( last && last_cons )
                        {
                            this.combine_glyph(last, "");
                            this.x += this.letter_spacing;
                        }
                        else
                        {
                            if ( last )
                                this.make_glyph(this.script.data.vowels[last].mid, -32);
                            this.x += 2*this.letter_spacing;
                        }
                        last = null;
                    }
                    else if ( id == "\n" )
                    {
                        last = this.flush_last(last, last_cons, true);
                        this.new_line();
                    }
                    else if ( id == "'" )
                    {
                        if ( !last )
                        {
                            last = "";
                        }
                        else if ( last_cons )
                        {
                            this.combine_glyph(last, "")
                            last = null;
                            this.x -= 2 * this.letter_spacing;
                        }
                        else
                        {
                            this.make_glyph(this.script.data.vowels[last].mid, -32);
                            last = null;
                        }
                    }
                    else if ( id in this.script.data.vowels )
                    {
                        if ( last )
                        {
                            if ( last_cons )
                            {
                                let prev = i > 1 ? text[i - 2].toLowerCase() : "";
                                if ( prev in this.script.data.vowels || prev in this.script.data.consonants )
                                {
                                    // "oko" same as "ok'o"
                                    this.combine_glyph(last, "");
                                    this.x -= 2 * this.letter_spacing;
                                }
                                else
                                {
                                    this.combine_glyph("", last);
                                }
                                last = id;
                                last_cons = false;
                            }
                            else
                            {
                                this.combine_vowel(last, id);
                                last = null;
                            }
                        }
                        else
                        {
                            last = id;
                            last_cons = false;
                        }
                    }
                    else if ( id in this.script.data.digits )
                    {
                        last = this.flush_last(last, last_cons, false);
                        this.make_digit(Number(id));
                    }
                    else if ( "6789".indexOf(id) != -1 )
                    {
                        last = this.flush_last(last, last_cons, false);
                        let v = Number(id);
                        this.make_digit(1);
                        this.make_digit(v-6);
                    }
                    else if ( id in this.script.data.punct )
                    {
                        last = this.flush_last(last, last_cons, false);
                        this.make_glyph(this.script.data.punct[id], -this.letter_spacing);
                    }
                    else if ( id == "x" )
                    {
                        last = this.flush_last(last, last_cons, false);
                        this.combine_glyph("k", "s");
                    }
                }

                this.flush_last(last, last_cons, true);
            }

            make_digit(digit)
            {
                let margin_l = 0;
                let margin_r = undefined;

                if ( digit == 1 )
                    margin_l = 2;
                else if ( digit == 2 )
                    margin_r = 2;
                else if ( digit != 3 )
                    margin_l = 1;

                if ( margin_r === undefined )
                    margin_r = margin_l;

                margin_r *= this.letter_spacing;
                margin_l *= this.letter_spacing;


                this.make_glyph(this.script.data.digits[digit], -margin_l, -margin_r);
            }

            finalize()
            {
                if ( this.x > this.width )
                    this.width = this.x;

                let margin = (this.svg.style.strokeWidth - 6);
                this.svg.setAttribute("width", this.scale * (this.width + margin*2) );
                this.svg.setAttribute("height", this.scale * (this.height + margin*2) );
                this.svg.setAttribute("viewBox", `${-margin} ${-margin} ${this.width + margin*2} ${this.height + margin*2}`);
                return this.svg;
            }
        }

        class DragonScript
        {
            constructor(raw_data)
            {
                this.char_width = 128;
                this.char_height = 128;

                const vowels = "aeiou";
                const consonants = ["k", "g", "c", "j", "t", "d", "h", "y", "l", "n", "r", "s", "s alt", "z", "š", "ž", "þ", "ð"];
                this.norm_consonants = "kgcjtdhylnrszšžþðwm";
                this.alt_s = new Set(["k", "g", "t", "d", "h", "n", "ž", "l", "r"]);
                this.consh_map = {
                    "š": "sh",
                    "ž": "zh",
                    "þ": "th",
                    "ð": "dh",
                };
                this.consh_rmap = Object.fromEntries(Object.entries(this.consh_map).map(it => [it[1], it[0]]));
                this.consh_map["ŧ"] = "th";
                this.consh_map["đ"] = "dh";

                const digits = "012345";

                this.data = {
                    consonants: {},
                    vowels: {},
                    digits: {},
                    punct: {},
                };
                for ( let [name, path] of Object.entries(raw_data) )
                {
                    if ( vowels.indexOf(name) != -1 )
                    {
                        this.data.vowels[name] = {
                            top: path.replaceAll(/([0-9.]+),([0-9.]+)/g, (n, x, y) => `${x}, ${Number(y)-30.5}`),
                            bottom: path.replaceAll(/([0-9.]+),([0-9.]+)/g, (n, x, y) => `${x}, ${Number(y)+30.5}`),
                            mid: path,
                        };

                    }
                    else if ( digits.indexOf(name) != -1 )
                    {
                        this.data.digits[name] = path;
                    }
                    else if ( name == "staff" )
                    {
                        this.data.staff = path;
                    }
                    else
                    {
                        if ( name in this.consh_rmap )
                            name = this.consh_rmap[name];

                        this.data.consonants[name] = {
                            "left": path,
                            "right": path.replaceAll(/([0-9.]+),([0-9.]+)/g, (n, x, y) => `${128 - Number(x)},${y}`),
                        }
                    }
                }

                this.data.consonants["q"] = this.data.consonants["k"];
                this.data.consonants["w"] = this.data.consonants["b"];
                this.data.consonants["p"] = this.data.consonants["b"];
                this.data.consonants["f"] = this.data.consonants["b"];
                this.data.consonants["v"] = this.data.consonants["b"];
                this.data.punct["."] = this.data.punct[","] = this.data.vowels["o"].mid;
                this.data.punct["|"] = this.data.staff
            }

            doc(size=128)
            {
                return new DragonScriptDocument(this, size);
            }
        }

        class DragonScriptElement extends HTMLElement
        {
            connectedCallback()
            {
                if ( this.innerText )
                {
                    this.parse_derg();
                }
                else
                {
                    this.observer = new MutationObserver(this.parse_derg.bind(this));
                    this.observer.observe(this, {childList: true});
                }
            }

            parse_derg(arg)
            {
                let data = this.innerText;
                this.setAttribute("title", data);
                let height = this.getAttribute("font-size") ?? 32;
                let stroke_width = this.getAttribute("stroke-width") ?? 12;
                let color = this.getAttribute("color") ?? "var(--text)";
                let doc = derg_script.doc(height);
                doc.svg.style.strokeWidth = stroke_width;
                doc.svg.style.stroke = color;
                doc.process(data);
                doc.finalize();
                this.attachShadow({ mode: "open" });
                this.shadowRoot.appendChild(doc.svg);
            }
        }

        function make_table(element, labels, callback)
        {
            let top_row1 = element.appendChild(document.createElement("tr"));
            top_row1.appendChild(document.createElement("th"));
            top_row1.appendChild(document.createElement("th"));
            let top_row2 = element.appendChild(document.createElement("tr"));
            top_row2.appendChild(document.createElement("th"));
            top_row2.appendChild(document.createElement("th"));

            for ( let c2 of labels )
            {
                top_row1.appendChild(document.createElement("th"))
                    .appendChild(document.createTextNode(c2.toUpperCase()))
                ;
                top_row2.appendChild(document.createElement("th"))
                    .appendChild(document.createElement("derg-rawr"))
                    .appendChild(document.createTextNode(c2.toUpperCase()))
                ;
            }

            for ( let c1 of labels )
            {
                let row = element.appendChild(document.createElement("tr"));

                row.appendChild(document.createElement("th"))
                    .appendChild(document.createTextNode(c1.toUpperCase()))
                ;

                row.appendChild(document.createElement("th"))
                    .appendChild(document.createElement("derg-rawr"))
                    .appendChild(document.createTextNode(c1.toUpperCase()))
                ;

                for ( let c2 of labels )
                {
                    let td = row.appendChild(document.createElement("td"));
                    callback(c1, c2, td);
                }
            }
        }

        const derg_script = new DragonScript(<?php echo json_encode($this->glyphs); ?>);
        customElements.define("derg-rawr", DragonScriptElement);
        </script><?php
    }

    function main($render_args)
    {
        $this->body_title(null, $render_args);
        echo mkelement(["p", [], $this->description]);
        ?>

        <h2>Transliterate</h2>
        <textarea oninput="transliterate(this.value)"></textarea>
        <div id="trans_output"></div>

        <h2>Convert Numbers</h2>
        <input type="number" oninput="convert_number(Number(this.value))"></input>
        <div id="num_output"></div>

        <script>
            let trans_doc = derg_script.doc(32);
            trans_doc.svg.setAttribute("height", 32);
            document.getElementById("trans_output").appendChild(trans_doc.svg);

            let num_doc = derg_script.doc(32);
            num_doc.svg.setAttribute("height", 32);
            document.getElementById("num_output").appendChild(num_doc.svg);

            function transliterate(text)
            {
                trans_doc.wrap_at = (document.getElementById("content").clientWidth - 20) / trans_doc.scale;
                trans_doc.clear();
                trans_doc.process(text);
                trans_doc.finalize();
            }

            function convert_number(number)
            {
                num_doc.wrap_at = (document.getElementById("content").clientWidth - 20) / trans_doc.scale;
                num_doc.clear();
                num_doc.process(number.toString(6));
                num_doc.finalize();
            }
        </script>

        <h2>Consonants</h2>

        <table class="table">
            <tr>
                <th>Glyph</th>
                <th>Romanization</th>
                <th>IPA</th>
            </tr>
            <tr>
                <td><derg-rawr>K</derg-rawr></td><td>k</td><td>k</td>
            </tr>
            <tr>
                <td><derg-rawr>G</derg-rawr></td>
                <td>g</td>
                <td>g</td>
            </tr>
            <tr>
                <td><derg-rawr>C</derg-rawr></td>
                <td>c</td>
                <td>tʃ</td>
            </tr>
            <tr>
                <td><derg-rawr>J</derg-rawr></td>
                <td>j</td>
                <td>dʒ</td>
            </tr>
            <tr>
                <td><derg-rawr>T</derg-rawr></td>
                <td>t</td>
                <td>t</td>
            </tr>
            <tr>
                <td><derg-rawr>D</derg-rawr></td>
                <td>d</td>
                <td>d</td>
            </tr>
            <tr>
                <td><derg-rawr>H</derg-rawr></td>
                <td>h</td>
                <td>x</td>
            </tr>
            <tr>
                <td><derg-rawr>Y</derg-rawr></td>
                <td>y</td>
                <td>j</td>
            </tr>
            <tr>
                <td><derg-rawr>L</derg-rawr></td>
                <td>l</td>
                <td>l</td>
            </tr>
            <tr>
                <td><derg-rawr>N</derg-rawr></td>
                <td>n</td>
                <td>n</td>
            </tr>
            <tr>
                <td><derg-rawr>R</derg-rawr></td>
                <td>r</td>
                <td>r</td>
            </tr>
            <tr>
                <td><derg-rawr>S</derg-rawr></td>
                <td>s</td>
                <td>s</td>
            </tr>
            <tr>
                <td><derg-rawr>S*</derg-rawr></td>
                <td>s</td>
                <td>s</td>
            </tr>
            <tr>
                <td><derg-rawr>Z</derg-rawr></td>
                <td>z</td>
                <td>z</td>
            </tr>
            <tr>
                <td><derg-rawr>Š</derg-rawr></td>
                <td>sh, š</td>
                <td>ʃ</td>
            </tr>
            <tr>
                <td><derg-rawr>Ž</derg-rawr></td>
                <td>zh, ž</td>
                <td>ʒ</td>
            </tr>
            <tr>
                <td><derg-rawr>Þ</derg-rawr></td>
                <td>th, þ, ŧ</td>
                <td>θ</td>
            </tr>
            <tr>
                <td><derg-rawr>Ð</derg-rawr></td>
                <td>dh, ð, đ</td>
                <td>ð</td>
            </tr>
        </table>

        <h3>Labial Consonants</h3>

        <p>
        Dragons lips are different from mammalian lips, as such they might
        struggle with labial sounds. To transcribe foreign words that include
        said sounds, the following glyphs might be used:
        </p>

        <table class="table">
            <tr>
                <th>Letter</th>
                <th>Glyph</th>
            </tr>
            <tr>
                <td>b, p, f, v, w</td>
                <td><derg-rawr>W</derg-rawr></td>
            </tr>
            <tr>
                <th>Letter</th>
                <th>Glyph</th>
            </tr>
            <tr>
                <td>m</td>
                <td><derg-rawr>M</derg-rawr></td>
            </tr>
        </table>

        <h3>Consonant Pairs</h3>

        <p>
        The symmetrical glyphs above are used to represent the letters by themselves.
        In normal text, pairs of consonants are merged together and the left half is taken
        from the first consonant of the pair and the right half is taken from the second.
        </p>

        <p>
        when transcribing words with an odd number of consonants in a row, the symbol
        <em>'</em> can be used to represent a consonant pair in which one of the
        two sides is empty.
        </p>

        <p>
        When combining a consonant with <derg-rawr font-size="16">S</derg-rawr> the variant
        <derg-rawr font-size="16">S*</derg-rawr> is often used when the other consonant has
        lines passing throgh the middle of the glyph.
        <p>

        <p>Some pairings are considered <em>strong<em>, which take precedence over other pairs of consonants</p>

        <p>
        Follows a table of standard consonant pairs. Pairs that are not used in practice are grayed out,
        strong pairs are in blue.
        </p>


        <table class="table" id="consonant-pairs"></table>

        <script>
            let strong_before = new Set("kgpwtd");
            let strong_after = new Set("lrszšž");
            let no_pairs = new Set([
                "kg",
                "kj",
                "kd",
                "ky",
                "kz",
                "kž",
                "kð",
                "km",

                "gk",
                "gj",
                "gt",
                "gš",
                "gm",

                "cg",
                "cj",
                "cd",
                "cl",
                "cz",
                "cž",
                "cþ",
                "cð",
                "cw",
                "cm",

                "jk",
                "jg",
                "jc",
                "jt",
                "jd",
                "jr",
                "jð",
                "jm",

                "tc",
                "tj",
                "td",
                "tz",
                "tþ",
                "tð",
                "tw",
                "tm",

                "dg",
                "dc",
                "dj",
                "dt",
                "dh",
                "dn",
                "dþ",
                "dð",
                "ds",
                "dw",
                "dm",

                "hg",
                "hj",
                "hz",
                "hw",
                "hm",

                "yt",
                "yd",
                "yr",
                "yz",
                "yð",
                "yw",
                "ym",

                "lc",
                "lj",
                "lz",
                "lð",
                "lw",
                "lm",

                "nz",
                "nð",
                "nž",
                "nm",

                "rc",
                "rj",
                "rð",
                "rm",

                "sg",
                "sj",
                "sy",
                "sz",
                "sž",
                "sþ",
                "sð",

                "zk",
                "zc",
                "zj",
                "zy",
                "zš",
                "zž",
                "zð",
                "zw",
                "zm",

                "šg",
                "šj",
                "šz",
                "šž",
                "šð",

                "žk",
                "žc",
                "žt",
                "žd",
                "žs",
                "žš",
                "žþ",
                "žw",

                "þj",
                "þs",
                "þz",
                "þž",
                "þð",
                "þw",
                "þm",

                "ðj",
                "ðy",
                "ðš",
                "ðþ",
                "ðw",
                "ðm",

                "wk",
                "wg",
                "wc",
                "wj",
                "wt",
                "wd",
                "wh",
                "wy",
                "wm",

                "mk",
                "mg",
                "mc",
                "mj",
                "mt",
                "md",
                "mh",
                "my",
                "mz",
                "mš",
                "mž",
                "mþ",
                "mð",
                "mw",

            ]);
            make_table(
                document.getElementById("consonant-pairs"),
                derg_script.norm_consonants,
                (c1, c2, td) => {
                    let what = c1 + c2;
                    if ( what == "tš" || what == "dš" )
                        what = "c";
                    else if ( what == "tž" || what == "dž" )
                        what = "j";

                    if ( c1 != c2 )
                    {
                        let rawr = document.createElement("derg-rawr");

                        if ( no_pairs.has(what) )
                            rawr.setAttribute("class", "uncommon");
                        else if ( strong_before.has(c1) && strong_after.has(c2) )
                            rawr.setAttribute("color", "var(--glax-body-main)");

                        rawr.setAttribute("stroke-width", "6");
                        rawr.appendChild(document.createTextNode(what));
                        td.appendChild(rawr);
                    }
                });
        </script>

        <h2>Vowels</h2>

        <p>Usually vowels are unrounded.</p>

        <table class="table">
            <tr>
                <th>Glyph</th>
                <th>Romanization</th>
                <th>IPA</th>
            </tr>
            <tr>
                <td><derg-rawr>a</derg-rawr></td>
                <td>a</td>
                <td>a</td>
            </tr>
            <tr>
                <td><derg-rawr>i</derg-rawr></td>
                <td>i</td>
                <td>i</td>
            </tr>
            <tr>
                <td><derg-rawr>e</derg-rawr></td>
                <td>e</td>
                <td>ə</td>
            </tr>
            <tr>
                <td><derg-rawr>o</derg-rawr></td>
                <td>o</td>
                <td>ɑ</td>
            </tr>
            <tr>
                <td><derg-rawr>u</derg-rawr></td>
                <td>u</td>
                <td>ɯ</td>
            </tr>
        </table>


        <h3>Vowel Pairs</h3>

        <p>Pairs of vowels are stacked on top of each other,
        on transliteration they can be unpaired with <em>'</em>,
        just like consonants
        </p>

        <table class="table" id="vowel-pairs"></table>

        <script>
            make_table(
                document.getElementById("vowel-pairs"),
                "aieou",
                (c1, c2, td) => {
                    let rawr = document.createElement("derg-rawr");
                    rawr.setAttribute("stroke-width", "6");
                    rawr.appendChild(document.createTextNode(c1 + c2));
                    td.appendChild(rawr);
                }
            );
        </script>


        <h2>Numbers</h2>

        <p>Numbers are expressed in a big-endian positional notation in base 6.</p>

        <table class="table">
            <tr>
                <th>Glyph</th>
                <th>Value</th>
            </tr>
            <tr>
                <td><derg-rawr>0</derg-rawr></td>
                <td>0</td>
            </tr>
            <tr>
                <td><derg-rawr>1</derg-rawr></td>
                <td>1</td>
            </tr>
            <tr>
                <td><derg-rawr>2</derg-rawr></td>
                <td>2</td>
            </tr>
            <tr>
                <td><derg-rawr>3</derg-rawr></td>
                <td>3</td>
            </tr>
            <tr>
                <td><derg-rawr>4</derg-rawr></td>
                <td>4</td>
            </tr>
            <tr>
                <td><derg-rawr>5</derg-rawr></td>
                <td>5</td>
            </tr>
        </table>
        <?php

    }
}

$page = new LanguagePage();
