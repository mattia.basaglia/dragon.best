<?php
require_once(__dir__."/lib/site.php");
require_once(__dir__."/lib/pages/redirect.php");
require_once(__dir__."/lib/color.php");
require_once(__dir__."/lib/auth.php");
require_once(__dir__."/lib/db/connection.php");

define("root_dir", dirname(__dir__));

class DragonSite extends Site
{
    protected $default_settings = array(
        "debug" => false,
        "track" => true,
        "redirect_message" => "Redurgecting you to the right page...\n",
        "metadata" => [
            "site_name" => "Best Dragon",
            "type" => "website",
        ],
        "root_dir" => root_dir,
        "telegram_file_path" => "/media/img/telegram/",
        "admin_user_ids" => [],
        "alt_uris" => [],
        "api_font_path" => "private/api/assets/fonts/",
    );

    public $code_license = "AGPLv3+";
    public $content_license = "CC BY-SA";
    public $source_code_link = "https://gitlab.com/mattia.basaglia/dragon.best";
    public $source_code_name = "GitLab";

    function get_error_page(&$code)
    {
        if ( $code == 403 )
            $code = 404;

        if ( $code == 404 )
            return __dir__."/404.php";

        if ( $code >= 300 && $code < 400 )
        {
            return new RedirectPage(null, $code);
        }

        return __dir__."/error.php";
    }

    function connect_db()
    {
        if ( Connection::is_connected() )
            return Connection::instance();
        return Connection::connect(
            $this->settings->database,
            $this->settings->database_username ?? null,
            $this->settings->database_password ?? null,
            $this->settings->database_options ?? []
        );
    }
}

$palette = Palette::from_file(__dir__."/../media/styles/Palette.gpl");
global $site;
$site = new DragonSite(__dir__."/pages", __dir__."/settings.php");
$auth = new TelegramAuthSource($site);
$site->palette = $palette;
