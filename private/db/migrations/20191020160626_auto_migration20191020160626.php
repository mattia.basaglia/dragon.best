<?php


use Phinx\Migration\AbstractMigration;
require_once(__dir__."/../../lib/db/migration.php");

class AutoMigration20191020160626 extends BaseMigration
{
    /**
     * Change Models Method.
     *
     * Write your reversible migrations using this method.
     *
     * The following commands can be used in this method and the migration will
     * automatically reverse them when rolling back:
     *
     *    create_model
     *    add_field
     *    rename_field
     *    alter_field
     *    remove_field
     *    remove_model
     *
     */
    public function change_models()
    {
        
        $this->alter_field('DragonLocation', 'icon_url', ['string','column_name'=>'icon_url','default'=>'','has_column'=>true,'limit'=>128,'null'=>false,]);
        $this->create_model('DragonSize',
            [
                'table_name' => 'dragon_size',
            ],
            [
                'telegram_id' => ['string','column_name'=>'telegram_id','column_type'=>'string','default'=>'','has_column'=>true,'limit'=>16,'null'=>false,],
                'type' => ['string','column_name'=>'type','column_type'=>'string','default'=>'','has_column'=>true,'limit'=>8,'null'=>false,],
                'icon_url' => ['string','column_name'=>'icon_url','column_type'=>'string','default'=>'','has_column'=>true,'limit'=>128,'null'=>false,],
                'name' => ['string','column_name'=>'name','column_type'=>'string','default'=>'','has_column'=>true,'limit'=>128,'null'=>false,],
                'height' => ['decimal','column_name'=>'height','column_type'=>'decimal','default'=>0,'has_column'=>true,'null'=>false,],
                'length' => ['decimal','column_name'=>'length','column_type'=>'decimal','default'=>0,'has_column'=>true,'null'=>false,],
                'id' => ['integer','column_name'=>'id','column_type'=>'integer','default'=>0,'has_column'=>true,'identity'=>true,'null'=>false,],
            ]
        );
    }
}
