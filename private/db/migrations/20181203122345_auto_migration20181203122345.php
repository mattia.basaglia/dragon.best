<?php


use Phinx\Migration\AbstractMigration;
require_once(__dir__."/../../lib/db/migration.php");

class AutoMigration20181203122345 extends BaseMigration
{
    /**
     * Change Models Method.
     *
     * Write your reversible migrations using this method.
     *
     * The following commands can be used in this method and the migration will
     * automatically reverse them when rolling back:
     *
     *    create_model
     *    add_field
     *    rename_field
     *    alter_field
     *    remove_field
     *    remove_model
     *
     */
    public function change_models()
    {
        
        $this->create_model('CurrencyConversion',
            [
                'table_name' => 'currency_conversion',
            ],
            [
                'date' => ['date','column_name'=>'date','column_type'=>'date','default'=>null,'has_column'=>true,'null'=>false,],
                'from' => ['string','column_name'=>'from','column_type'=>'string','default'=>null,'has_column'=>true,'limit'=>3,'null'=>false,],
                'to' => ['string','column_name'=>'to','column_type'=>'string','default'=>'EUR','has_column'=>true,'limit'=>3,'null'=>false,],
                'rate' => ['decimal','column_name'=>'rate','column_type'=>'decimal','default'=>null,'has_column'=>true,'null'=>false,],
                'id' => ['integer','column_name'=>'id','column_type'=>'integer','default'=>0,'has_column'=>true,'identity'=>true,'null'=>false,],
            ]
        );
    }
}