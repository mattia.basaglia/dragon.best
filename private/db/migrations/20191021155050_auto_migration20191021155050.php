<?php


use Phinx\Migration\AbstractMigration;
require_once(__dir__."/../../lib/db/migration.php");

class AutoMigration20191021155050 extends BaseMigration
{
    /**
     * Change Models Method.
     *
     * Write your reversible migrations using this method.
     *
     * The following commands can be used in this method and the migration will
     * automatically reverse them when rolling back:
     *
     *    create_model
     *    add_field
     *    rename_field
     *    alter_field
     *    remove_field
     *    remove_model
     *
     */
    public function change_models()
    {
        
        $this->alter_field('DragonLocation', 'icon_url', ['string','column_name'=>'icon_url','default'=>'','has_column'=>true,'limit'=>128,'null'=>false,]);
        $this->add_field('DragonSize', 'color', ['string','column_name'=>'color','default'=>'#cccccc','has_column'=>true,'limit'=>7,'null'=>false,]);
    }
}