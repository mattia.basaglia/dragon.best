<?php

require_once(__DIR__.'/lib/page.php');

class DurgPage extends Page
{
    public $scripts = array();
    public $styles = array(
        "/media/styles/durg.css"
    );
    public $title = "Best dragon";

    public $copy_year = "2017";
    public $copy_author = "Mattia Basaglia";
    public $license = "CC BY-SA";
    public $default_image = "/media/img/rasterized/icon/scalable.png";

    public function get_meta_image($render_args)
    {
        return $this->default_image;
    }

    public function show_social_media_card($render_args, $ogmeta)
    {
        return $ogmeta["type"] == "photo";
    }
}
