#!/usr/bin/env python3

import re
import os
import sys
import pathlib
import argparse
import subprocess

template = pathlib.Path(__file__).parent / "_page_template.php"
out_path = pathlib.Path(__file__).parent.parent / "pages"

parser = argparse.ArgumentParser()
parser.add_argument("slug")
parser.add_argument("--title", "-t")
parser.add_argument("--description", "-d", default="")

args = parser.parse_args()


file_name = out_path / (args.slug + ".php")
replacements = {
    "PageClass": re.sub("(?:^|[-_])([a-z])", lambda m: m.group(1).upper(), args.slug) + "Page",
    "page title": re.sub("(^|[-_])([a-z])", lambda m: " " * len(m.group(1)) + m.group(2).upper(), args.slug) if args.title is None else args.title,
    "page description": args.description,
}

if file_name.exists():
    sys.stderr.write("File %s already exists\n" % file_name)
    sys.exit(1)

with open(template, "r") as inf:
    data = inf.read()

for needle, replace in replacements.items():
    data = data.replace(needle, replace)

with open(file_name, "w") as outf:
    outf.write(data)

print(file_name)

dbus_id = os.environ.get("KDEV_DBUS_ID")
if dbus_id:
    subprocess.call(["qdbus", dbus_id, "/org/kdevelop/DocumentController", "org.kdevelop.DocumentController.openDocumentsSimple", "(", str(file_name), ")"])
