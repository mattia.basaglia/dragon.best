#!/usr/bin/env php
<?php

require_once(__dir__ . "/../lib/color.php");

$palette = Palette::from_file(__dir__."/../../media/styles/Palette.gpl");
$prefix = "glax-";

print("\n:root {\n");

foreach ( $palette->colors as $name => $value )
{
    $name = str_replace("_", "-", $name);
    if ( substr($name, 0, 4) != "ych-" )
        $name = $prefix . $name;
    print("    --$name: $value;\n");
}

print("}\n\n");
