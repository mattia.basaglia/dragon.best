#!/usr/bin/php
<?php

function get_file_cached($uri)
{
    $temp = "/tmp/" . basename($uri);
    if ( !file_exists($temp) )
        file_put_contents($temp, file_get_contents($uri));
    return fopen($temp, "r");
}

$data = [];
if ( $data_file = get_file_cached("http://www.unicode.org/Public/emoji/latest/emoji-test.txt") )
{
    $data_regex = "/^([0-9A-F ]+);[^#]+# [^ ]+ (.*)/";
    $data_group = "/^# (sub)?group: (.*)/";
    $curr_group = "";
    $curr_subgroup = "";
    while ( !feof($data_file) )
    {
        $line = fgets($data_file);
        if ( preg_match($data_group, $line, $match) )
        {
            if ( $match[1] )
            {
                $curr_subgroup = $match[2];
                $data[$curr_group][$curr_subgroup] = [];
            }
            else
            {
                $curr_group = $match[2];
                $curr_subgroup = "";
                $data[$curr_group] = [];
            }
        }
        else if ( preg_match($data_regex, $line, $match) )
        {
            $data[$curr_group][$curr_subgroup] []= [
                explode(" ", strtolower(trim($match[1]))),
                $match[2]
            ];
        }
    }

    fclose($data_file);
}


$format = $argv[1] ?? "json";

if ( $format == "html" )
{
    $img_uri = "https://twitter.github.io/twemoji/2/72x72/";
    $img_size = 72;

    print("<!DOCTYPE html><html><head><title>Emoji</title>");
    print("<style>td{border: 1px solid black;}table{border-collapse: collapse;}.emoji{font-size:{$img_size}px}</style>");
    print("</head><body>");

    foreach ( $data as $group => $sub )
    {
        print("<h1>$group</h1>");
        print("<table><thead><tr><th>Name</th><th>Point</th><th>Html</th><th>Char</th><th>Image</th></tr></thead><tbody>");
        foreach ( $sub as $subgroup => $sequences )
        {
            print("<tr><th colspan='5'>$subgroup</th></tr>");
            foreach ( $sequences as $seq )
            {
                list($codes, $name) = $seq;
                $ents = "";
                foreach ( $codes as $code )
                    $ents .= "&#x$code;";
                print("<tr><td>$name</td><td>".implode("<br/>", $codes)."</td><td>".htmlentities($ents)."</td><td class='emoji'>$ents</td>");
                $img_code = implode("-", $codes);
                print("<td><img width='$img_size' height='$img_size' src='$img_uri$img_code.png' alt=''/></td></tr>");
            }

        }
        print("</tbody></table>");
    }

    print("</body></html>");
}
else if ( $format == "stats" )
{
    $total = 0;
    foreach ( $data as $group => $sub )
    {
        print("$group\n");
        $gt = 0;
        foreach ( $sub as $subgroup => $emoji )
        {
            $gt += sizeof($emoji);
            print("  $subgroup ".sizeof($emoji)."\n");
        }
        $total += $gt;
        print("$group total $gt\n\n");
    }
    print("Total $total\n");
}
else if ( $format == "search" )
{
    $what = $argv[2]??"";
    if ( !$what )
        exit(1);

    $matches = [];
    $cwidths = [0, 0, 0, 0, 0];
    foreach ( $data as $group => $sub )
        foreach ( $sub as $subgroup => $sequences )
            foreach ( $sequences as $seq )
                if ( stripos($seq[1], $what) !== false )
                {
                    $ents = "";
                    foreach ( $seq[0] as $code )
                        $ents .= "&#x$code;";
                    $match = [
                        $seq[1],
                        implode(" ", $seq[0]),
                        $ents,
                        "$group, $subgroup",
                        html_entity_decode($ents),
                    ];
                    $matches []= $match;
                    foreach ( $match as $i => $v )
                    {
                        $len = mb_strlen($v);
                        if ( $cwidths[$i] < $len )
                            $cwidths[$i] = $len;
                    }
                }

    if ( sizeof($matches) == 0 )
    {
        print("No results\n");
        exit(0);
    }

    function fmt($match, $width)
    {
        foreach ( $match as $i => $v )
            print(str_pad($v, $width[$i], " ")." ");
        print("\n");
    }

    fmt(["name", "point", "html", "group", "char"], $cwidths);
    foreach ( $matches as $match )
        fmt($match, $cwidths);

}
else
{
    $json_options = JSON_PARTIAL_OUTPUT_ON_ERROR;
    // $json_options = JSON_PARTIAL_OUTPUT_ON_ERROR | JSON_PRETTY_PRINT;
    print json_encode($data, $json_options);
}
