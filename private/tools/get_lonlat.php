#!/usr/bin/php
<?php
// Alternative:
// curl "https://nominatim.openstreetmap.org/search?format=json&limit=1&city=London&country=GB" | python -c "import json, sys; data = json.load(sys.stdin)[0]; print('[%s, %s]' % (data['lon'], data['lat']))"

$_SERVER = ["HTTP_HOST" => ""];
require(__dir__ . "/../settings.php");
require(__dir__ . "/../lib/http.php");
require(__dir__ . "/../convention-list.php");


global $api_key;
$api_key = $open_weather_map_api_key;

global $api_url;
$api_url = "https://api.openweathermap.org/data/2.5/weather/";

function get_lon_lat($city, $country)
{
    global $api_key, $api_url;


    $url = $api_url."?".http_build_query([
        "q" => "$city,$country",
        "appid" => $api_key,
    ]);
    $result = curl_get($url)[0];

    $data = json_decode($result);
    print("\"$city\" => [{$data->coord->lon}, {$data->coord->lat}],\n");
}

if ( isset($argv[1]) )
{
    if ( $argv[1] == "-h" || $argv[1] == "--help" || !isset($argv[2]) )
    {
        print("{$argv[0]} city country\n");
        print("\tShows the coordinates for the given city\n");
        print("\n");
        print("{$argv[0]}\n");
        print("\tShows the coordinates for all cons in the list without one\n");
    }
    else
    {
        get_lon_lat($argv[1], $argv[2]);
    }
}
else
{
    foreach ( $convention_list as $con )
    {
        if ( !isset(FurryEvent::$lon_lat[$con->city]) )
        {
            get_lon_lat($con->city, $con->country);
            sleep(0.5);
        }
    }
}
