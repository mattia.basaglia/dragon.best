<?php

require_once(__dir__."/../dragon.php");


class PageClass extends DurgPage
{
    public $title = "page title";
    public $description = "page description";
//     public $scripts = [];
//     public $styles = [
//         "/media/styles/durg.css",
//     ];

    function extra_head($render_args)
    {
        ?><style>
        </style><?php
    }

    function main($render_args)
    {
        $this->body_title(null, $render_args);
        echo mkelement(["p", [], $this->description]);
    }
}

$page = new PageClass();
