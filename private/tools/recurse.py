#!/usr/bin/env python3
import urllib.request
import urllib.parse
import argparse
import re
import sys
import http
import time
import html

parser = argparse.ArgumentParser()
parser.add_argument("action", default="check", nargs="?", choices=["check", "archive"])
parser.add_argument("page", default="", nargs="?")
parser.add_argument("root", default="https://dragon.best/", nargs="?")
parser.add_argument("--single", "-s", action="store_true", default=False)
parser.add_argument("--fail-fast", "-f", action="store_true", default=False)
ns = parser.parse_args()

website = ns.root
login = website + "auth/login/"
media = website + "media/"
api = website + "api/"
stickers = website + "stickers/"
archive = ns.action == "archive"

not_loaded = set()
loaded = set()
errored = set()
retry = set()
reverse = {}
expr = re.compile(r"""=['"](%s[^"']+)['"]""" % re.escape(website))


def register(child_url, parent):
    if child_url.startswith(login) or not child_url.startswith(website) or (child_url.startswith(api) and child_url[-1] != "/"):
        return

    if child_url not in loaded:
        not_loaded.add(child_url)
        reverse[child_url] = set()
    reverse[child_url].add(parent)


def load(url):
    print(url)
    not_loaded.remove(url)
    loaded.add(url)
    if url.startswith(stickers):
        return

    if archive and not url.startswith(media):
        archive_url = "https://web.archive.org/save/" + url
        print("    archive %s" % archive_url)
        try:
            resp = urllib.request.urlopen(archive_url)
        except urllib.error.HTTPError as e:
            time.sleep(30)
            urllib.request.urlopen(archive_url)

    if not url.startswith(media):
        try:
            response = urllib.request.urlopen(url)
            print("    read")
            if response.info().get_content_type() == "text/html":
                for child_url in expr.findall(response.read().decode("utf-8")):
                    register(html.unescape(child_url), url)
        except KeyboardInterrupt:
            print("    exiting")
            not_loaded.clear()
            return
        except http.client.RemoteDisconnected:
            retry.add(url)
            if url not in retry:
                load(url)
        except Exception as e:
            errored.add(url)
            print("    error: %s %s" % (type(e), e))
            if ns.fail_fast:
                not_loaded.clear()
                return
            #if graph:
                #dot.node(url_id, url[len(website)-1:], color="red")


if ns.single:
    load(ns.page)
else:
    register(ns.page or website, "")
    while not_loaded:
        load(next(iter(not_loaded)))
    sys.stdout.flush()

if errored:
    sys.stderr.write("Following errors occurred:\n")
    for error in errored:
        sys.stderr.write("* %s\n" % error)
        for r in reverse[error]:
            sys.stderr.write("  %s\n" % r)

    sys.stderr.flush()
    sys.exit(1)
